# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.30)
# Database: heartland
# Generation Time: 2015-08-19 09:31:53 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table modx_access_actiondom
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_actiondom`;

CREATE TABLE `modx_access_actiondom` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_access_actions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_actions`;

CREATE TABLE `modx_access_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_access_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_category`;

CREATE TABLE `modx_access_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_access_context
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_context`;

CREATE TABLE `modx_access_context` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_access_context` WRITE;
/*!40000 ALTER TABLE `modx_access_context` DISABLE KEYS */;

INSERT INTO `modx_access_context` (`id`, `target`, `principal_class`, `principal`, `authority`, `policy`)
VALUES
	(1,'web','modUserGroup',0,9999,3),
	(2,'mgr','modUserGroup',1,0,2),
	(3,'web','modUserGroup',1,0,2),
	(4,'web','modUserGroup',2,9999,12),
	(5,'mgr','modUserGroup',2,9999,12);

/*!40000 ALTER TABLE `modx_access_context` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_access_elements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_elements`;

CREATE TABLE `modx_access_elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_access_media_source
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_media_source`;

CREATE TABLE `modx_access_media_source` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_access_media_source` WRITE;
/*!40000 ALTER TABLE `modx_access_media_source` DISABLE KEYS */;

INSERT INTO `modx_access_media_source` (`id`, `target`, `principal_class`, `principal`, `authority`, `policy`, `context_key`)
VALUES
	(3,'2','modUserGroup',2,0,8,'mgr'),
	(2,'1','modUserGroup',1,0,8,'');

/*!40000 ALTER TABLE `modx_access_media_source` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_access_menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_menus`;

CREATE TABLE `modx_access_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_access_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_permissions`;

CREATE TABLE `modx_access_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `value` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `template` (`template`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_access_permissions` WRITE;
/*!40000 ALTER TABLE `modx_access_permissions` DISABLE KEYS */;

INSERT INTO `modx_access_permissions` (`id`, `template`, `name`, `description`, `value`)
VALUES
	(1,1,'about','perm.about_desc',1),
	(2,1,'access_permissions','perm.access_permissions_desc',1),
	(3,1,'actions','perm.actions_desc',1),
	(4,1,'change_password','perm.change_password_desc',1),
	(5,1,'change_profile','perm.change_profile_desc',1),
	(6,1,'charsets','perm.charsets_desc',1),
	(7,1,'class_map','perm.class_map_desc',1),
	(8,1,'components','perm.components_desc',1),
	(9,1,'content_types','perm.content_types_desc',1),
	(10,1,'countries','perm.countries_desc',1),
	(11,1,'create','perm.create_desc',1),
	(12,1,'credits','perm.credits_desc',1),
	(13,1,'customize_forms','perm.customize_forms_desc',1),
	(14,1,'dashboards','perm.dashboards_desc',1),
	(15,1,'database','perm.database_desc',1),
	(16,1,'database_truncate','perm.database_truncate_desc',1),
	(17,1,'delete_category','perm.delete_category_desc',1),
	(18,1,'delete_chunk','perm.delete_chunk_desc',1),
	(19,1,'delete_context','perm.delete_context_desc',1),
	(20,1,'delete_document','perm.delete_document_desc',1),
	(21,1,'delete_eventlog','perm.delete_eventlog_desc',1),
	(22,1,'delete_plugin','perm.delete_plugin_desc',1),
	(23,1,'delete_propertyset','perm.delete_propertyset_desc',1),
	(24,1,'delete_snippet','perm.delete_snippet_desc',1),
	(25,1,'delete_template','perm.delete_template_desc',1),
	(26,1,'delete_tv','perm.delete_tv_desc',1),
	(27,1,'delete_role','perm.delete_role_desc',1),
	(28,1,'delete_user','perm.delete_user_desc',1),
	(29,1,'directory_chmod','perm.directory_chmod_desc',1),
	(30,1,'directory_create','perm.directory_create_desc',1),
	(31,1,'directory_list','perm.directory_list_desc',1),
	(32,1,'directory_remove','perm.directory_remove_desc',1),
	(33,1,'directory_update','perm.directory_update_desc',1),
	(34,1,'edit_category','perm.edit_category_desc',1),
	(35,1,'edit_chunk','perm.edit_chunk_desc',1),
	(36,1,'edit_context','perm.edit_context_desc',1),
	(37,1,'edit_document','perm.edit_document_desc',1),
	(38,1,'edit_locked','perm.edit_locked_desc',1),
	(39,1,'edit_plugin','perm.edit_plugin_desc',1),
	(40,1,'edit_propertyset','perm.edit_propertyset_desc',1),
	(41,1,'edit_role','perm.edit_role_desc',1),
	(42,1,'edit_snippet','perm.edit_snippet_desc',1),
	(43,1,'edit_template','perm.edit_template_desc',1),
	(44,1,'edit_tv','perm.edit_tv_desc',1),
	(45,1,'edit_user','perm.edit_user_desc',1),
	(46,1,'element_tree','perm.element_tree_desc',1),
	(47,1,'empty_cache','perm.empty_cache_desc',1),
	(48,1,'error_log_erase','perm.error_log_erase_desc',1),
	(49,1,'error_log_view','perm.error_log_view_desc',1),
	(50,1,'export_static','perm.export_static_desc',1),
	(51,1,'file_create','perm.file_create_desc',1),
	(52,1,'file_list','perm.file_list_desc',1),
	(53,1,'file_manager','perm.file_manager_desc',1),
	(54,1,'file_remove','perm.file_remove_desc',1),
	(55,1,'file_tree','perm.file_tree_desc',1),
	(56,1,'file_update','perm.file_update_desc',1),
	(57,1,'file_upload','perm.file_upload_desc',1),
	(58,1,'file_view','perm.file_view_desc',1),
	(59,1,'flush_sessions','perm.flush_sessions_desc',1),
	(60,1,'frames','perm.frames_desc',1),
	(61,1,'help','perm.help_desc',1),
	(62,1,'home','perm.home_desc',1),
	(63,1,'import_static','perm.import_static_desc',1),
	(64,1,'languages','perm.languages_desc',1),
	(65,1,'lexicons','perm.lexicons_desc',1),
	(66,1,'list','perm.list_desc',1),
	(67,1,'load','perm.load_desc',1),
	(68,1,'logout','perm.logout_desc',1),
	(69,1,'logs','perm.logs_desc',1),
	(70,1,'menu_reports','perm.menu_reports_desc',1),
	(71,1,'menu_security','perm.menu_security_desc',1),
	(72,1,'menu_site','perm.menu_site_desc',1),
	(73,1,'menu_support','perm.menu_support_desc',1),
	(74,1,'menu_system','perm.menu_system_desc',1),
	(75,1,'menu_tools','perm.menu_tools_desc',1),
	(76,1,'menu_user','perm.menu_user_desc',1),
	(77,1,'menus','perm.menus_desc',1),
	(78,1,'messages','perm.messages_desc',1),
	(79,1,'namespaces','perm.namespaces_desc',1),
	(80,1,'new_category','perm.new_category_desc',1),
	(81,1,'new_chunk','perm.new_chunk_desc',1),
	(82,1,'new_context','perm.new_context_desc',1),
	(83,1,'new_document','perm.new_document_desc',1),
	(84,1,'new_static_resource','perm.new_static_resource_desc',1),
	(85,1,'new_symlink','perm.new_symlink_desc',1),
	(86,1,'new_weblink','perm.new_weblink_desc',1),
	(87,1,'new_document_in_root','perm.new_document_in_root_desc',1),
	(88,1,'new_plugin','perm.new_plugin_desc',1),
	(89,1,'new_propertyset','perm.new_propertyset_desc',1),
	(90,1,'new_role','perm.new_role_desc',1),
	(91,1,'new_snippet','perm.new_snippet_desc',1),
	(92,1,'new_template','perm.new_template_desc',1),
	(93,1,'new_tv','perm.new_tv_desc',1),
	(94,1,'new_user','perm.new_user_desc',1),
	(95,1,'packages','perm.packages_desc',1),
	(96,1,'policy_delete','perm.policy_delete_desc',1),
	(97,1,'policy_edit','perm.policy_edit_desc',1),
	(98,1,'policy_new','perm.policy_new_desc',1),
	(99,1,'policy_save','perm.policy_save_desc',1),
	(100,1,'policy_view','perm.policy_view_desc',1),
	(101,1,'policy_template_delete','perm.policy_template_delete_desc',1),
	(102,1,'policy_template_edit','perm.policy_template_edit_desc',1),
	(103,1,'policy_template_new','perm.policy_template_new_desc',1),
	(104,1,'policy_template_save','perm.policy_template_save_desc',1),
	(105,1,'policy_template_view','perm.policy_template_view_desc',1),
	(106,1,'property_sets','perm.property_sets_desc',1),
	(107,1,'providers','perm.providers_desc',1),
	(108,1,'publish_document','perm.publish_document_desc',1),
	(109,1,'purge_deleted','perm.purge_deleted_desc',1),
	(110,1,'remove','perm.remove_desc',1),
	(111,1,'remove_locks','perm.remove_locks_desc',1),
	(112,1,'resource_duplicate','perm.resource_duplicate_desc',1),
	(113,1,'resourcegroup_delete','perm.resourcegroup_delete_desc',1),
	(114,1,'resourcegroup_edit','perm.resourcegroup_edit_desc',1),
	(115,1,'resourcegroup_new','perm.resourcegroup_new_desc',1),
	(116,1,'resourcegroup_resource_edit','perm.resourcegroup_resource_edit_desc',1),
	(117,1,'resourcegroup_resource_list','perm.resourcegroup_resource_list_desc',1),
	(118,1,'resourcegroup_save','perm.resourcegroup_save_desc',1),
	(119,1,'resourcegroup_view','perm.resourcegroup_view_desc',1),
	(120,1,'resource_quick_create','perm.resource_quick_create_desc',1),
	(121,1,'resource_quick_update','perm.resource_quick_update_desc',1),
	(122,1,'resource_tree','perm.resource_tree_desc',1),
	(123,1,'save','perm.save_desc',1),
	(124,1,'save_category','perm.save_category_desc',1),
	(125,1,'save_chunk','perm.save_chunk_desc',1),
	(126,1,'save_context','perm.save_context_desc',1),
	(127,1,'save_document','perm.save_document_desc',1),
	(128,1,'save_plugin','perm.save_plugin_desc',1),
	(129,1,'save_propertyset','perm.save_propertyset_desc',1),
	(130,1,'save_role','perm.save_role_desc',1),
	(131,1,'save_snippet','perm.save_snippet_desc',1),
	(132,1,'save_template','perm.save_template_desc',1),
	(133,1,'save_tv','perm.save_tv_desc',1),
	(134,1,'save_user','perm.save_user_desc',1),
	(135,1,'search','perm.search_desc',1),
	(136,1,'settings','perm.settings_desc',1),
	(137,1,'source_save','perm.source_save_desc',1),
	(138,1,'source_delete','perm.source_delete_desc',1),
	(139,1,'source_edit','perm.source_edit_desc',1),
	(140,1,'source_view','perm.source_view_desc',1),
	(141,1,'sources','perm.sources_desc',1),
	(142,1,'steal_locks','perm.steal_locks_desc',1),
	(143,1,'tree_show_element_ids','perm.tree_show_element_ids_desc',1),
	(144,1,'tree_show_resource_ids','perm.tree_show_resource_ids_desc',1),
	(145,1,'undelete_document','perm.undelete_document_desc',1),
	(146,1,'unpublish_document','perm.unpublish_document_desc',1),
	(147,1,'unlock_element_properties','perm.unlock_element_properties_desc',1),
	(148,1,'usergroup_delete','perm.usergroup_delete_desc',1),
	(149,1,'usergroup_edit','perm.usergroup_edit_desc',1),
	(150,1,'usergroup_new','perm.usergroup_new_desc',1),
	(151,1,'usergroup_save','perm.usergroup_save_desc',1),
	(152,1,'usergroup_user_edit','perm.usergroup_user_edit_desc',1),
	(153,1,'usergroup_user_list','perm.usergroup_user_list_desc',1),
	(154,1,'usergroup_view','perm.usergroup_view_desc',1),
	(155,1,'view','perm.view_desc',1),
	(156,1,'view_category','perm.view_category_desc',1),
	(157,1,'view_chunk','perm.view_chunk_desc',1),
	(158,1,'view_context','perm.view_context_desc',1),
	(159,1,'view_document','perm.view_document_desc',1),
	(160,1,'view_element','perm.view_element_desc',1),
	(161,1,'view_eventlog','perm.view_eventlog_desc',1),
	(162,1,'view_offline','perm.view_offline_desc',1),
	(163,1,'view_plugin','perm.view_plugin_desc',1),
	(164,1,'view_propertyset','perm.view_propertyset_desc',1),
	(165,1,'view_role','perm.view_role_desc',1),
	(166,1,'view_snippet','perm.view_snippet_desc',1),
	(167,1,'view_sysinfo','perm.view_sysinfo_desc',1),
	(168,1,'view_template','perm.view_template_desc',1),
	(169,1,'view_tv','perm.view_tv_desc',1),
	(170,1,'view_user','perm.view_user_desc',1),
	(171,1,'view_unpublished','perm.view_unpublished_desc',1),
	(172,1,'workspaces','perm.workspaces_desc',1),
	(173,2,'add_children','perm.add_children_desc',1),
	(174,2,'copy','perm.copy_desc',1),
	(175,2,'create','perm.create_desc',1),
	(176,2,'delete','perm.delete_desc',1),
	(177,2,'list','perm.list_desc',1),
	(178,2,'load','perm.load_desc',1),
	(179,2,'move','perm.move_desc',1),
	(180,2,'publish','perm.publish_desc',1),
	(181,2,'remove','perm.remove_desc',1),
	(182,2,'save','perm.save_desc',1),
	(183,2,'steal_lock','perm.steal_lock_desc',1),
	(184,2,'undelete','perm.undelete_desc',1),
	(185,2,'unpublish','perm.unpublish_desc',1),
	(186,2,'view','perm.view_desc',1),
	(187,3,'load','perm.load_desc',1),
	(188,3,'list','perm.list_desc',1),
	(189,3,'view','perm.view_desc',1),
	(190,3,'save','perm.save_desc',1),
	(191,3,'remove','perm.remove_desc',1),
	(192,4,'add_children','perm.add_children_desc',1),
	(193,4,'create','perm.create_desc',1),
	(194,4,'copy','perm.copy_desc',1),
	(195,4,'delete','perm.delete_desc',1),
	(196,4,'list','perm.list_desc',1),
	(197,4,'load','perm.load_desc',1),
	(198,4,'remove','perm.remove_desc',1),
	(199,4,'save','perm.save_desc',1),
	(200,4,'view','perm.view_desc',1),
	(201,5,'create','perm.create_desc',1),
	(202,5,'copy','perm.copy_desc',1),
	(203,5,'list','perm.list_desc',1),
	(204,5,'load','perm.load_desc',1),
	(205,5,'remove','perm.remove_desc',1),
	(206,5,'save','perm.save_desc',1),
	(207,5,'view','perm.view_desc',1),
	(208,6,'load','perm.load_desc',1),
	(209,6,'list','perm.list_desc',1),
	(210,6,'view','perm.view_desc',1),
	(211,6,'save','perm.save_desc',1),
	(212,6,'remove','perm.remove_desc',1),
	(213,6,'view_unpublished','perm.view_unpublished_desc',1),
	(214,6,'copy','perm.copy_desc',1),
	(215,7,'resource_tree','perm.resource_tree_desc',1),
	(216,7,'resource_quick_update','perm.resource_quick_update_desc',1),
	(217,7,'resource_quick_create','perm.resource_quick_create_desc',1),
	(218,7,'resource_duplicate','perm.resource_duplicate_desc',1),
	(219,7,'resourcegroup_view','perm.resourcegroup_view_desc',1),
	(220,7,'resourcegroup_save','perm.resourcegroup_save_desc',1),
	(221,7,'resourcegroup_resource_list','perm.resourcegroup_resource_list_desc',1),
	(222,7,'resourcegroup_resource_edit','perm.resourcegroup_resource_edit_desc',1),
	(223,7,'resourcegroup_new','perm.resourcegroup_new_desc',1),
	(224,7,'resourcegroup_edit','perm.resourcegroup_edit_desc',1),
	(225,7,'resourcegroup_delete','perm.resourcegroup_delete_desc',1),
	(226,7,'remove_locks','perm.remove_locks_desc',1),
	(227,7,'remove','perm.remove_desc',1),
	(228,7,'purge_deleted','perm.purge_deleted_desc',1),
	(229,7,'publish_document','perm.publish_document_desc',1),
	(230,7,'providers','perm.providers_desc',1),
	(231,7,'property_sets','perm.property_sets_desc',1),
	(232,7,'policy_view','perm.policy_view_desc',1),
	(233,7,'policy_template_view','perm.policy_template_view_desc',1),
	(234,7,'policy_template_save','perm.policy_template_save_desc',1),
	(235,7,'policy_template_new','perm.policy_template_new_desc',1),
	(236,7,'policy_template_edit','perm.policy_template_edit_desc',1),
	(237,7,'policy_template_delete','perm.policy_template_delete_desc',1),
	(238,7,'policy_save','perm.policy_save_desc',1),
	(239,7,'policy_new','perm.policy_new_desc',1),
	(240,7,'policy_edit','perm.policy_edit_desc',1),
	(241,7,'policy_delete','perm.policy_delete_desc',1),
	(242,7,'packages','perm.packages_desc',1),
	(243,7,'new_weblink','perm.new_weblink_desc',1),
	(244,7,'new_user','perm.new_user_desc',1),
	(245,7,'new_tv','perm.new_tv_desc',1),
	(246,7,'new_template','perm.new_template_desc',1),
	(247,7,'new_symlink','perm.new_symlink_desc',1),
	(248,7,'new_static_resource','perm.new_static_resource_desc',1),
	(249,7,'new_snippet','perm.new_snippet_desc',1),
	(250,7,'new_role','perm.new_role_desc',1),
	(251,7,'new_propertyset','perm.new_propertyset_desc',1),
	(252,7,'new_plugin','perm.new_plugin_desc',1),
	(253,7,'new_document_in_root','perm.new_document_in_root_desc',1),
	(254,7,'new_document','perm.new_document_desc',1),
	(255,7,'new_context','perm.new_context_desc',1),
	(256,7,'new_chunk','perm.new_chunk_desc',1),
	(257,7,'new_category','perm.new_category_desc',1),
	(258,7,'namespaces','perm.namespaces_desc',1),
	(259,7,'messages','perm.messages_desc',1),
	(260,7,'menu_user','perm.menu_user_desc',1),
	(261,7,'menu_tools','perm.menu_tools_desc',1),
	(262,7,'menu_system','perm.menu_system_desc',1),
	(263,7,'menu_support','perm.menu_support_desc',1),
	(264,7,'menu_site','perm.menu_site_desc',1),
	(265,7,'menu_security','perm.menu_security_desc',1),
	(266,7,'menu_reports','perm.menu_reports_desc',1),
	(267,7,'menus','perm.menus_desc',1),
	(268,7,'logs','perm.logs_desc',1),
	(269,7,'logout','perm.logout_desc',1),
	(270,7,'load','perm.load_desc',1),
	(271,7,'list','perm.list_desc',1),
	(272,7,'lexicons','perm.lexicons_desc',1),
	(273,7,'languages','perm.languages_desc',1),
	(274,7,'import_static','perm.import_static_desc',1),
	(275,7,'home','perm.home_desc',1),
	(276,7,'help','perm.help_desc',1),
	(277,7,'frames','perm.frames_desc',1),
	(278,7,'flush_sessions','perm.flush_sessions_desc',1),
	(279,7,'file_view','perm.file_view_desc',1),
	(280,7,'file_upload','perm.file_upload_desc',1),
	(281,7,'file_update','perm.file_update_desc',1),
	(282,7,'file_tree','perm.file_tree_desc',1),
	(283,7,'file_remove','perm.file_remove_desc',1),
	(284,7,'file_manager','perm.file_manager_desc',1),
	(285,7,'file_list','perm.file_list_desc',1),
	(286,7,'file_create','perm.file_create_desc',1),
	(287,7,'export_static','perm.export_static_desc',1),
	(288,7,'error_log_view','perm.error_log_view_desc',1),
	(289,7,'error_log_erase','perm.error_log_erase_desc',1),
	(290,7,'empty_cache','perm.empty_cache_desc',1),
	(291,7,'element_tree','perm.element_tree_desc',1),
	(292,7,'edit_user','perm.edit_user_desc',1),
	(293,7,'edit_tv','perm.edit_tv_desc',1),
	(294,7,'edit_template','perm.edit_template_desc',1),
	(295,7,'edit_snippet','perm.edit_snippet_desc',1),
	(296,7,'edit_role','perm.edit_role_desc',1),
	(297,7,'edit_propertyset','perm.edit_propertyset_desc',1),
	(298,7,'edit_plugin','perm.edit_plugin_desc',1),
	(299,7,'edit_locked','perm.edit_locked_desc',1),
	(300,7,'edit_document','perm.edit_document_desc',1),
	(301,7,'edit_context','perm.edit_context_desc',1),
	(302,7,'edit_chunk','perm.edit_chunk_desc',1),
	(303,7,'edit_category','perm.edit_category_desc',1),
	(304,7,'directory_update','perm.directory_update_desc',1),
	(305,7,'directory_remove','perm.directory_remove_desc',1),
	(306,7,'directory_list','perm.directory_list_desc',1),
	(307,7,'directory_create','perm.directory_create_desc',1),
	(308,7,'directory_chmod','perm.directory_chmod_desc',1),
	(309,7,'delete_user','perm.delete_user_desc',1),
	(310,7,'delete_tv','perm.delete_tv_desc',1),
	(311,7,'delete_template','perm.delete_template_desc',1),
	(312,7,'delete_snippet','perm.delete_snippet_desc',1),
	(313,7,'delete_role','perm.delete_role_desc',1),
	(314,7,'delete_propertyset','perm.delete_propertyset_desc',1),
	(315,7,'delete_plugin','perm.delete_plugin_desc',1),
	(316,7,'delete_eventlog','perm.delete_eventlog_desc',1),
	(317,7,'delete_document','perm.delete_document_desc',1),
	(318,7,'delete_context','perm.delete_context_desc',1),
	(319,7,'delete_chunk','perm.delete_chunk_desc',1),
	(320,7,'delete_category','perm.delete_category_desc',1),
	(321,7,'database_truncate','perm.database_truncate_desc',1),
	(322,7,'database','perm.database_desc',1),
	(323,7,'dashboards','perm.dashboards_desc',1),
	(324,7,'customize_forms','perm.customize_forms_desc',1),
	(325,7,'credits','perm.credits_desc',1),
	(326,7,'create','perm.create_desc',1),
	(327,7,'countries','perm.countries_desc',1),
	(328,7,'content_types','perm.content_types_desc',1),
	(329,7,'components','perm.components_desc',1),
	(330,7,'class_map','perm.class_map_desc',1),
	(331,7,'charsets','perm.charsets_desc',1),
	(332,7,'change_profile','perm.change_profile_desc',1),
	(333,7,'change_password','perm.change_password_desc',1),
	(334,7,'actions','perm.actions_desc',1),
	(335,7,'access_permissions','perm.access_permissions_desc',1),
	(336,7,'about','perm.about_desc',1),
	(337,7,'save','perm.save_desc',1),
	(338,7,'save_category','perm.save_category_desc',1),
	(339,7,'save_chunk','perm.save_chunk_desc',1),
	(340,7,'save_context','perm.save_context_desc',1),
	(341,7,'save_document','perm.save_document_desc',1),
	(342,7,'save_plugin','perm.save_plugin_desc',1),
	(343,7,'save_propertyset','perm.save_propertyset_desc',1),
	(344,7,'save_role','perm.save_role_desc',1),
	(345,7,'save_snippet','perm.save_snippet_desc',1),
	(346,7,'save_template','perm.save_template_desc',1),
	(347,7,'save_tv','perm.save_tv_desc',1),
	(348,7,'save_user','perm.save_user_desc',1),
	(349,7,'search','perm.search_desc',1),
	(350,7,'settings','perm.settings_desc',1),
	(351,7,'sources','perm.sources_desc',1),
	(352,7,'source_delete','perm.source_delete_desc',1),
	(353,7,'source_edit','perm.source_edit_desc',1),
	(354,7,'source_save','perm.source_save_desc',1),
	(355,7,'source_view','perm.source_view_desc',1),
	(356,7,'steal_locks','perm.steal_locks_desc',1),
	(357,7,'tree_show_element_ids','perm.tree_show_element_ids_desc',1),
	(358,7,'tree_show_resource_ids','perm.tree_show_resource_ids_desc',1),
	(359,7,'undelete_document','perm.undelete_document_desc',1),
	(360,7,'unlock_element_properties','perm.unlock_element_properties_desc',1),
	(361,7,'unpublish_document','perm.unpublish_document_desc',1),
	(362,7,'usergroup_delete','perm.usergroup_delete_desc',1),
	(363,7,'usergroup_edit','perm.usergroup_edit_desc',1),
	(364,7,'usergroup_new','perm.usergroup_new_desc',1),
	(365,7,'usergroup_save','perm.usergroup_save_desc',1),
	(366,7,'usergroup_user_edit','perm.usergroup_user_edit_desc',1),
	(367,7,'usergroup_user_list','perm.usergroup_user_list_desc',1),
	(368,7,'usergroup_view','perm.usergroup_view_desc',1),
	(369,7,'view','perm.view_desc',1),
	(370,7,'view_category','perm.view_category_desc',1),
	(371,7,'view_chunk','perm.view_chunk_desc',1),
	(372,7,'view_context','perm.view_context_desc',1),
	(373,7,'view_document','perm.view_document_desc',1),
	(374,7,'view_element','perm.view_element_desc',1),
	(375,7,'view_eventlog','perm.view_eventlog_desc',1),
	(376,7,'view_offline','perm.view_offline_desc',1),
	(377,7,'view_plugin','perm.view_plugin_desc',1),
	(378,7,'view_propertyset','perm.view_propertyset_desc',1),
	(379,7,'view_role','perm.view_role_desc',1),
	(380,7,'view_snippet','perm.view_snippet_desc',1),
	(381,7,'view_sysinfo','perm.view_sysinfo_desc',1),
	(382,7,'view_template','perm.view_template_desc',1),
	(383,7,'view_tv','perm.view_tv_desc',1),
	(384,7,'view_unpublished','perm.view_unpublished_desc',1),
	(385,7,'view_user','perm.view_user_desc',1),
	(386,7,'workspaces','perm.workspaces_desc',1),
	(387,8,'resource_tree','perm.resource_tree_desc',1),
	(388,8,'resource_quick_update','perm.resource_quick_update_desc',1),
	(389,8,'resource_quick_create','perm.resource_quick_create_desc',1),
	(390,8,'resource_duplicate','perm.resource_duplicate_desc',1),
	(391,8,'resourcegroup_view','perm.resourcegroup_view_desc',1),
	(392,8,'resourcegroup_save','perm.resourcegroup_save_desc',1),
	(393,8,'resourcegroup_resource_list','perm.resourcegroup_resource_list_desc',1),
	(394,8,'resourcegroup_resource_edit','perm.resourcegroup_resource_edit_desc',1),
	(395,8,'resourcegroup_new','perm.resourcegroup_new_desc',1),
	(396,8,'resourcegroup_edit','perm.resourcegroup_edit_desc',1),
	(397,8,'resourcegroup_delete','perm.resourcegroup_delete_desc',1),
	(398,8,'remove_locks','perm.remove_locks_desc',1),
	(399,8,'remove','perm.remove_desc',1),
	(400,8,'purge_deleted','perm.purge_deleted_desc',1),
	(401,8,'publish_document','perm.publish_document_desc',1),
	(402,8,'providers','perm.providers_desc',1),
	(403,8,'property_sets','perm.property_sets_desc',1),
	(404,8,'policy_view','perm.policy_view_desc',1),
	(405,8,'policy_template_view','perm.policy_template_view_desc',1),
	(406,8,'policy_template_save','perm.policy_template_save_desc',1),
	(407,8,'policy_template_new','perm.policy_template_new_desc',1),
	(408,8,'policy_template_edit','perm.policy_template_edit_desc',1),
	(409,8,'policy_template_delete','perm.policy_template_delete_desc',1),
	(410,8,'policy_save','perm.policy_save_desc',1),
	(411,8,'policy_new','perm.policy_new_desc',1),
	(412,8,'policy_edit','perm.policy_edit_desc',1),
	(413,8,'policy_delete','perm.policy_delete_desc',1),
	(414,8,'packages','perm.packages_desc',1),
	(415,8,'new_weblink','perm.new_weblink_desc',1),
	(416,8,'new_user','perm.new_user_desc',1),
	(417,8,'new_tv','perm.new_tv_desc',1),
	(418,8,'new_template','perm.new_template_desc',1),
	(419,8,'new_symlink','perm.new_symlink_desc',1),
	(420,8,'new_static_resource','perm.new_static_resource_desc',1),
	(421,8,'new_snippet','perm.new_snippet_desc',1),
	(422,8,'new_role','perm.new_role_desc',1),
	(423,8,'new_propertyset','perm.new_propertyset_desc',1),
	(424,8,'new_plugin','perm.new_plugin_desc',1),
	(425,8,'new_document_in_root','perm.new_document_in_root_desc',1),
	(426,8,'new_document','perm.new_document_desc',1),
	(427,8,'new_context','perm.new_context_desc',1),
	(428,8,'new_chunk','perm.new_chunk_desc',1),
	(429,8,'new_category','perm.new_category_desc',1),
	(430,8,'namespaces','perm.namespaces_desc',1),
	(431,8,'messages','perm.messages_desc',1),
	(432,8,'menu_user','perm.menu_user_desc',1),
	(433,8,'menu_tools','perm.menu_tools_desc',1),
	(434,8,'menu_system','perm.menu_system_desc',1),
	(435,8,'menu_support','perm.menu_support_desc',1),
	(436,8,'menu_site','perm.menu_site_desc',1),
	(437,8,'menu_security','perm.menu_security_desc',1),
	(438,8,'menu_reports','perm.menu_reports_desc',1),
	(439,8,'menus','perm.menus_desc',1),
	(440,8,'logs','perm.logs_desc',1),
	(441,8,'logout','perm.logout_desc',1),
	(442,8,'load','perm.load_desc',1),
	(443,8,'list','perm.list_desc',1),
	(444,8,'lexicons','perm.lexicons_desc',1),
	(445,8,'languages','perm.languages_desc',1),
	(446,8,'import_static','perm.import_static_desc',1),
	(447,8,'home','perm.home_desc',1),
	(448,8,'help','perm.help_desc',1),
	(449,8,'frames','perm.frames_desc',1),
	(450,8,'flush_sessions','perm.flush_sessions_desc',1),
	(451,8,'file_view','perm.file_view_desc',1),
	(452,8,'file_upload','perm.file_upload_desc',1),
	(453,8,'file_update','perm.file_update_desc',1),
	(454,8,'file_tree','perm.file_tree_desc',1),
	(455,8,'file_remove','perm.file_remove_desc',1),
	(456,8,'file_manager','perm.file_manager_desc',1),
	(457,8,'file_list','perm.file_list_desc',1),
	(458,8,'file_create','perm.file_create_desc',1),
	(459,8,'export_static','perm.export_static_desc',1),
	(460,8,'error_log_view','perm.error_log_view_desc',1),
	(461,8,'error_log_erase','perm.error_log_erase_desc',1),
	(462,8,'empty_cache','perm.empty_cache_desc',1),
	(463,8,'element_tree','perm.element_tree_desc',1),
	(464,8,'edit_user','perm.edit_user_desc',1),
	(465,8,'edit_tv','perm.edit_tv_desc',1),
	(466,8,'edit_template','perm.edit_template_desc',1),
	(467,8,'edit_snippet','perm.edit_snippet_desc',1),
	(468,8,'edit_role','perm.edit_role_desc',1),
	(469,8,'edit_propertyset','perm.edit_propertyset_desc',1),
	(470,8,'edit_plugin','perm.edit_plugin_desc',1),
	(471,8,'edit_locked','perm.edit_locked_desc',1),
	(472,8,'edit_document','perm.edit_document_desc',1),
	(473,8,'edit_context','perm.edit_context_desc',1),
	(474,8,'edit_chunk','perm.edit_chunk_desc',1),
	(475,8,'edit_category','perm.edit_category_desc',1),
	(476,8,'directory_update','perm.directory_update_desc',1),
	(477,8,'directory_remove','perm.directory_remove_desc',1),
	(478,8,'directory_list','perm.directory_list_desc',1),
	(479,8,'directory_create','perm.directory_create_desc',1),
	(480,8,'directory_chmod','perm.directory_chmod_desc',1),
	(481,8,'delete_user','perm.delete_user_desc',1),
	(482,8,'delete_tv','perm.delete_tv_desc',1),
	(483,8,'delete_template','perm.delete_template_desc',1),
	(484,8,'delete_snippet','perm.delete_snippet_desc',1),
	(485,8,'delete_role','perm.delete_role_desc',1),
	(486,8,'delete_propertyset','perm.delete_propertyset_desc',1),
	(487,8,'delete_plugin','perm.delete_plugin_desc',1),
	(488,8,'delete_eventlog','perm.delete_eventlog_desc',1),
	(489,8,'delete_document','perm.delete_document_desc',1),
	(490,8,'delete_context','perm.delete_context_desc',1),
	(491,8,'delete_chunk','perm.delete_chunk_desc',1),
	(492,8,'delete_category','perm.delete_category_desc',1),
	(493,8,'database_truncate','perm.database_truncate_desc',1),
	(494,8,'database','perm.database_desc',1),
	(495,8,'dashboards','perm.dashboards_desc',1),
	(496,8,'customize_forms','perm.customize_forms_desc',1),
	(497,8,'credits','perm.credits_desc',1),
	(498,8,'create','perm.create_desc',1),
	(499,8,'countries','perm.countries_desc',1),
	(500,8,'content_types','perm.content_types_desc',1),
	(501,8,'components','perm.components_desc',1),
	(502,8,'class_map','perm.class_map_desc',1),
	(503,8,'charsets','perm.charsets_desc',1),
	(504,8,'change_profile','perm.change_profile_desc',1),
	(505,8,'change_password','perm.change_password_desc',1),
	(506,8,'actions','perm.actions_desc',1),
	(507,8,'access_permissions','perm.access_permissions_desc',1),
	(508,8,'about','perm.about_desc',1),
	(509,8,'save','perm.save_desc',1),
	(510,8,'save_category','perm.save_category_desc',1),
	(511,8,'save_chunk','perm.save_chunk_desc',1),
	(512,8,'save_context','perm.save_context_desc',1),
	(513,8,'save_document','perm.save_document_desc',1),
	(514,8,'save_plugin','perm.save_plugin_desc',1),
	(515,8,'save_propertyset','perm.save_propertyset_desc',1),
	(516,8,'save_role','perm.save_role_desc',1),
	(517,8,'save_snippet','perm.save_snippet_desc',1),
	(518,8,'save_template','perm.save_template_desc',1),
	(519,8,'save_tv','perm.save_tv_desc',1),
	(520,8,'save_user','perm.save_user_desc',1),
	(521,8,'search','perm.search_desc',1),
	(522,8,'settings','perm.settings_desc',1),
	(523,8,'sources','perm.sources_desc',1),
	(524,8,'source_delete','perm.source_delete_desc',1),
	(525,8,'source_edit','perm.source_edit_desc',1),
	(526,8,'source_save','perm.source_save_desc',1),
	(527,8,'source_view','perm.source_view_desc',1),
	(528,8,'steal_locks','perm.steal_locks_desc',1),
	(529,8,'tree_show_element_ids','perm.tree_show_element_ids_desc',1),
	(530,8,'tree_show_resource_ids','perm.tree_show_resource_ids_desc',1),
	(531,8,'undelete_document','perm.undelete_document_desc',1),
	(532,8,'unlock_element_properties','perm.unlock_element_properties_desc',1),
	(533,8,'unpublish_document','perm.unpublish_document_desc',1),
	(534,8,'usergroup_delete','perm.usergroup_delete_desc',1),
	(535,8,'usergroup_edit','perm.usergroup_edit_desc',1),
	(536,8,'usergroup_new','perm.usergroup_new_desc',1),
	(537,8,'usergroup_save','perm.usergroup_save_desc',1),
	(538,8,'usergroup_user_edit','perm.usergroup_user_edit_desc',1),
	(539,8,'usergroup_user_list','perm.usergroup_user_list_desc',1),
	(540,8,'usergroup_view','perm.usergroup_view_desc',1),
	(541,8,'view','perm.view_desc',1),
	(542,8,'view_category','perm.view_category_desc',1),
	(543,8,'view_chunk','perm.view_chunk_desc',1),
	(544,8,'view_context','perm.view_context_desc',1),
	(545,8,'view_document','perm.view_document_desc',1),
	(546,8,'view_element','perm.view_element_desc',1),
	(547,8,'view_eventlog','perm.view_eventlog_desc',1),
	(548,8,'view_offline','perm.view_offline_desc',1),
	(549,8,'view_plugin','perm.view_plugin_desc',1),
	(550,8,'view_propertyset','perm.view_propertyset_desc',1),
	(551,8,'view_role','perm.view_role_desc',1),
	(552,8,'view_snippet','perm.view_snippet_desc',1),
	(553,8,'view_sysinfo','perm.view_sysinfo_desc',1),
	(554,8,'view_template','perm.view_template_desc',1),
	(555,8,'view_tv','perm.view_tv_desc',1),
	(556,8,'view_unpublished','perm.view_unpublished_desc',1),
	(557,8,'view_user','perm.view_user_desc',1),
	(558,8,'workspaces','perm.workspaces_desc',1);

/*!40000 ALTER TABLE `modx_access_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_access_policies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_policies`;

CREATE TABLE `modx_access_policies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` mediumtext,
  `parent` int(10) unsigned NOT NULL DEFAULT '0',
  `template` int(10) unsigned NOT NULL DEFAULT '0',
  `class` varchar(255) NOT NULL DEFAULT '',
  `data` text,
  `lexicon` varchar(255) NOT NULL DEFAULT 'permissions',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `parent` (`parent`),
  KEY `class` (`class`),
  KEY `template` (`template`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_access_policies` WRITE;
/*!40000 ALTER TABLE `modx_access_policies` DISABLE KEYS */;

INSERT INTO `modx_access_policies` (`id`, `name`, `description`, `parent`, `template`, `class`, `data`, `lexicon`)
VALUES
	(1,'Resource','MODX Resource Policy with all attributes.',0,2,'','{\"add_children\":true,\"create\":true,\"copy\":true,\"delete\":true,\"list\":true,\"load\":true,\"move\":true,\"publish\":true,\"remove\":true,\"save\":true,\"steal_lock\":true,\"undelete\":true,\"unpublish\":true,\"view\":true}','permissions'),
	(2,'Administrator','Context administration policy with all permissions.',0,1,'','{\"about\":true,\"access_permissions\":true,\"actions\":true,\"change_password\":true,\"change_profile\":true,\"charsets\":true,\"class_map\":true,\"components\":true,\"content_types\":true,\"countries\":true,\"create\":true,\"credits\":true,\"customize_forms\":true,\"dashboards\":true,\"database\":true,\"database_truncate\":true,\"delete_category\":true,\"delete_chunk\":true,\"delete_context\":true,\"delete_document\":true,\"delete_eventlog\":true,\"delete_plugin\":true,\"delete_propertyset\":true,\"delete_role\":true,\"delete_snippet\":true,\"delete_template\":true,\"delete_tv\":true,\"delete_user\":true,\"directory_chmod\":true,\"directory_create\":true,\"directory_list\":true,\"directory_remove\":true,\"directory_update\":true,\"edit_category\":true,\"edit_chunk\":true,\"edit_context\":true,\"edit_document\":true,\"edit_locked\":true,\"edit_plugin\":true,\"edit_propertyset\":true,\"edit_role\":true,\"edit_snippet\":true,\"edit_template\":true,\"edit_tv\":true,\"edit_user\":true,\"element_tree\":true,\"empty_cache\":true,\"error_log_erase\":true,\"error_log_view\":true,\"export_static\":true,\"file_create\":true,\"file_list\":true,\"file_manager\":true,\"file_remove\":true,\"file_tree\":true,\"file_update\":true,\"file_upload\":true,\"file_view\":true,\"flush_sessions\":true,\"frames\":true,\"help\":true,\"home\":true,\"import_static\":true,\"languages\":true,\"lexicons\":true,\"list\":true,\"load\":true,\"logout\":true,\"logs\":true,\"menus\":true,\"menu_reports\":true,\"menu_security\":true,\"menu_site\":true,\"menu_support\":true,\"menu_system\":true,\"menu_tools\":true,\"menu_user\":true,\"messages\":true,\"namespaces\":true,\"new_category\":true,\"new_chunk\":true,\"new_context\":true,\"new_document\":true,\"new_document_in_root\":true,\"new_plugin\":true,\"new_propertyset\":true,\"new_role\":true,\"new_snippet\":true,\"new_static_resource\":true,\"new_symlink\":true,\"new_template\":true,\"new_tv\":true,\"new_user\":true,\"new_weblink\":true,\"packages\":true,\"policy_delete\":true,\"policy_edit\":true,\"policy_new\":true,\"policy_save\":true,\"policy_template_delete\":true,\"policy_template_edit\":true,\"policy_template_new\":true,\"policy_template_save\":true,\"policy_template_view\":true,\"policy_view\":true,\"property_sets\":true,\"providers\":true,\"publish_document\":true,\"purge_deleted\":true,\"remove\":true,\"remove_locks\":true,\"resource_duplicate\":true,\"resourcegroup_delete\":true,\"resourcegroup_edit\":true,\"resourcegroup_new\":true,\"resourcegroup_resource_edit\":true,\"resourcegroup_resource_list\":true,\"resourcegroup_save\":true,\"resourcegroup_view\":true,\"resource_quick_create\":true,\"resource_quick_update\":true,\"resource_tree\":true,\"save\":true,\"save_category\":true,\"save_chunk\":true,\"save_context\":true,\"save_document\":true,\"save_plugin\":true,\"save_propertyset\":true,\"save_role\":true,\"save_snippet\":true,\"save_template\":true,\"save_tv\":true,\"save_user\":true,\"search\":true,\"settings\":true,\"sources\":true,\"source_delete\":true,\"source_edit\":true,\"source_save\":true,\"source_view\":true,\"steal_locks\":true,\"tree_show_element_ids\":true,\"tree_show_resource_ids\":true,\"undelete_document\":true,\"unlock_element_properties\":true,\"unpublish_document\":true,\"usergroup_delete\":true,\"usergroup_edit\":true,\"usergroup_new\":true,\"usergroup_save\":true,\"usergroup_user_edit\":true,\"usergroup_user_list\":true,\"usergroup_view\":true,\"view\":true,\"view_category\":true,\"view_chunk\":true,\"view_context\":true,\"view_document\":true,\"view_element\":true,\"view_eventlog\":true,\"view_offline\":true,\"view_plugin\":true,\"view_propertyset\":true,\"view_role\":true,\"view_snippet\":true,\"view_sysinfo\":true,\"view_template\":true,\"view_tv\":true,\"view_unpublished\":true,\"view_user\":true,\"workspaces\":true}','permissions'),
	(3,'Load Only','A minimal policy with permission to load an object.',0,3,'','{\"load\":true}','permissions'),
	(4,'Load, List and View','Provides load, list and view permissions only.',0,3,'','{\"load\":true,\"list\":true,\"view\":true}','permissions'),
	(5,'Object','An Object policy with all permissions.',0,3,'','{\"load\":true,\"list\":true,\"view\":true,\"save\":true,\"remove\":true}','permissions'),
	(6,'Element','MODX Element policy with all attributes.',0,4,'','{\"add_children\":true,\"create\":true,\"delete\":true,\"list\":true,\"load\":true,\"remove\":true,\"save\":true,\"view\":true,\"copy\":true}','permissions'),
	(7,'Content Editor','Context administration policy with limited, content-editing related Permissions, but no publishing.',0,1,'','{\"change_profile\":true,\"class_map\":true,\"countries\":true,\"edit_document\":true,\"frames\":true,\"help\":true,\"home\":true,\"load\":true,\"list\":true,\"logout\":true,\"menu_reports\":true,\"menu_site\":true,\"menu_support\":true,\"menu_tools\":true,\"menu_user\":true,\"resource_duplicate\":true,\"resource_tree\":true,\"save_document\":true,\"source_view\":true,\"tree_show_resource_ids\":true,\"view\":true,\"view_document\":true,\"new_document\":true,\"delete_document\":true}','permissions'),
	(8,'Media Source Admin','Media Source administration policy.',0,5,'','{\"create\":true,\"copy\":true,\"load\":true,\"list\":true,\"save\":true,\"remove\":true,\"view\":true}','permissions'),
	(9,'Media Source User','Media Source user policy, with basic viewing and using - but no editing - of Media Sources.',0,5,'','{\"load\":true,\"list\":true,\"view\":true}','permissions'),
	(10,'Developer','Context administration policy with most Permissions except Administrator and Security functions.',0,0,'','{\"about\":true,\"change_password\":true,\"change_profile\":true,\"charsets\":true,\"class_map\":true,\"components\":true,\"content_types\":true,\"countries\":true,\"create\":true,\"credits\":true,\"customize_forms\":true,\"dashboards\":true,\"database\":true,\"delete_category\":true,\"delete_chunk\":true,\"delete_context\":true,\"delete_document\":true,\"delete_eventlog\":true,\"delete_plugin\":true,\"delete_propertyset\":true,\"delete_snippet\":true,\"delete_template\":true,\"delete_tv\":true,\"delete_role\":true,\"delete_user\":true,\"directory_chmod\":true,\"directory_create\":true,\"directory_list\":true,\"directory_remove\":true,\"directory_update\":true,\"edit_category\":true,\"edit_chunk\":true,\"edit_context\":true,\"edit_document\":true,\"edit_locked\":true,\"edit_plugin\":true,\"edit_propertyset\":true,\"edit_role\":true,\"edit_snippet\":true,\"edit_template\":true,\"edit_tv\":true,\"edit_user\":true,\"element_tree\":true,\"empty_cache\":true,\"error_log_erase\":true,\"error_log_view\":true,\"export_static\":true,\"file_create\":true,\"file_list\":true,\"file_manager\":true,\"file_remove\":true,\"file_tree\":true,\"file_update\":true,\"file_upload\":true,\"file_view\":true,\"frames\":true,\"help\":true,\"home\":true,\"import_static\":true,\"languages\":true,\"lexicons\":true,\"list\":true,\"load\":true,\"logout\":true,\"logs\":true,\"menu_reports\":true,\"menu_site\":true,\"menu_support\":true,\"menu_system\":true,\"menu_tools\":true,\"menu_user\":true,\"menus\":true,\"messages\":true,\"namespaces\":true,\"new_category\":true,\"new_chunk\":true,\"new_context\":true,\"new_document\":true,\"new_static_resource\":true,\"new_symlink\":true,\"new_weblink\":true,\"new_document_in_root\":true,\"new_plugin\":true,\"new_propertyset\":true,\"new_role\":true,\"new_snippet\":true,\"new_template\":true,\"new_tv\":true,\"new_user\":true,\"packages\":true,\"property_sets\":true,\"providers\":true,\"publish_document\":true,\"purge_deleted\":true,\"remove\":true,\"resource_duplicate\":true,\"resource_quick_create\":true,\"resource_quick_update\":true,\"resource_tree\":true,\"save\":true,\"save_category\":true,\"save_chunk\":true,\"save_context\":true,\"save_document\":true,\"save_plugin\":true,\"save_propertyset\":true,\"save_snippet\":true,\"save_template\":true,\"save_tv\":true,\"save_user\":true,\"search\":true,\"settings\":true,\"source_delete\":true,\"source_edit\":true,\"source_save\":true,\"source_view\":true,\"sources\":true,\"tree_show_element_ids\":true,\"tree_show_resource_ids\":true,\"undelete_document\":true,\"unpublish_document\":true,\"unlock_element_properties\":true,\"view\":true,\"view_category\":true,\"view_chunk\":true,\"view_context\":true,\"view_document\":true,\"view_element\":true,\"view_eventlog\":true,\"view_offline\":true,\"view_plugin\":true,\"view_propertyset\":true,\"view_role\":true,\"view_snippet\":true,\"view_sysinfo\":true,\"view_template\":true,\"view_tv\":true,\"view_user\":true,\"view_unpublished\":true,\"workspaces\":true}','permissions'),
	(11,'Context','A standard Context policy that you can apply when creating Context ACLs for basic read/write and view_unpublished access within a Context.',0,6,'','{\"load\":true,\"list\":true,\"view\":true,\"save\":true,\"remove\":true,\"copy\":true,\"view_unpublished\":true}','permissions'),
	(12,'- Client Content Editor','Context administration policy with reduced permissions.',0,7,'','{\"about\":false,\"access_permissions\":true,\"actions\":false,\"change_password\":true,\"change_profile\":true,\"charsets\":false,\"class_map\":false,\"components\":true,\"content_types\":false,\"countries\":false,\"create\":true,\"credits\":false,\"customize_forms\":false,\"dashboards\":false,\"database\":false,\"database_truncate\":false,\"delete_category\":false,\"delete_chunk\":false,\"delete_context\":false,\"delete_document\":true,\"delete_eventlog\":false,\"delete_plugin\":false,\"delete_propertyset\":false,\"delete_role\":false,\"delete_snippet\":false,\"delete_template\":false,\"delete_tv\":false,\"delete_user\":true,\"directory_chmod\":true,\"directory_create\":true,\"directory_list\":true,\"directory_remove\":true,\"directory_update\":true,\"edit_category\":false,\"edit_chunk\":false,\"edit_context\":false,\"edit_document\":true,\"edit_locked\":true,\"edit_plugin\":false,\"edit_propertyset\":false,\"edit_role\":false,\"edit_snippet\":false,\"edit_template\":false,\"edit_tv\":false,\"edit_user\":true,\"element_tree\":false,\"empty_cache\":true,\"error_log_erase\":false,\"error_log_view\":false,\"export_static\":false,\"file_create\":true,\"file_list\":true,\"file_manager\":true,\"file_remove\":true,\"file_tree\":true,\"file_update\":true,\"file_upload\":true,\"file_view\":true,\"flush_sessions\":false,\"frames\":true,\"help\":false,\"home\":true,\"import_static\":false,\"languages\":false,\"lexicons\":false,\"list\":true,\"load\":true,\"logout\":true,\"logs\":false,\"menus\":false,\"menu_reports\":false,\"menu_security\":false,\"menu_site\":true,\"menu_support\":false,\"menu_system\":false,\"menu_tools\":false,\"menu_user\":true,\"messages\":true,\"namespaces\":false,\"new_category\":false,\"new_chunk\":false,\"new_context\":false,\"new_document\":true,\"new_document_in_root\":true,\"new_plugin\":false,\"new_propertyset\":false,\"new_role\":false,\"new_snippet\":false,\"new_static_resource\":true,\"new_symlink\":true,\"new_template\":false,\"new_tv\":false,\"new_user\":true,\"new_weblink\":true,\"packages\":false,\"policy_delete\":false,\"policy_edit\":false,\"policy_new\":false,\"policy_save\":false,\"policy_template_delete\":false,\"policy_template_edit\":false,\"policy_template_new\":false,\"policy_template_save\":false,\"policy_template_view\":false,\"policy_view\":false,\"property_sets\":false,\"providers\":false,\"publish_document\":true,\"purge_deleted\":true,\"remove\":true,\"remove_locks\":true,\"resourcegroup_delete\":false,\"resourcegroup_edit\":false,\"resourcegroup_new\":false,\"resourcegroup_resource_edit\":false,\"resourcegroup_resource_list\":false,\"resourcegroup_save\":false,\"resourcegroup_view\":false,\"resource_duplicate\":true,\"resource_quick_create\":true,\"resource_quick_update\":true,\"resource_tree\":true,\"save\":true,\"save_category\":false,\"save_chunk\":false,\"save_context\":false,\"save_document\":true,\"save_plugin\":false,\"save_propertyset\":false,\"save_role\":false,\"save_snippet\":false,\"save_template\":false,\"save_tv\":false,\"save_user\":true,\"search\":true,\"settings\":false,\"sources\":false,\"source_delete\":false,\"source_edit\":false,\"source_save\":false,\"source_view\":true,\"steal_locks\":true,\"tree_show_element_ids\":false,\"tree_show_resource_ids\":true,\"undelete_document\":true,\"unlock_element_properties\":false,\"unpublish_document\":true,\"usergroup_delete\":false,\"usergroup_edit\":false,\"usergroup_new\":false,\"usergroup_save\":false,\"usergroup_user_edit\":false,\"usergroup_user_list\":false,\"usergroup_view\":false,\"view\":true,\"view_category\":false,\"view_chunk\":false,\"view_context\":false,\"view_document\":false,\"view_element\":false,\"view_eventlog\":false,\"view_offline\":true,\"view_plugin\":false,\"view_propertyset\":false,\"view_role\":false,\"view_snippet\":false,\"view_sysinfo\":false,\"view_template\":false,\"view_tv\":false,\"view_unpublished\":true,\"view_user\":false,\"workspaces\":false}','');

/*!40000 ALTER TABLE `modx_access_policies` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_access_policy_template_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_policy_template_groups`;

CREATE TABLE `modx_access_policy_template_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_access_policy_template_groups` WRITE;
/*!40000 ALTER TABLE `modx_access_policy_template_groups` DISABLE KEYS */;

INSERT INTO `modx_access_policy_template_groups` (`id`, `name`, `description`)
VALUES
	(1,'Admin','All admin policy templates.'),
	(2,'Object','All Object-based policy templates.'),
	(3,'Resource','All Resource-based policy templates.'),
	(4,'Element','All Element-based policy templates.'),
	(5,'MediaSource','All Media Source-based policy templates.');

/*!40000 ALTER TABLE `modx_access_policy_template_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_access_policy_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_policy_templates`;

CREATE TABLE `modx_access_policy_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template_group` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext,
  `lexicon` varchar(255) NOT NULL DEFAULT 'permissions',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_access_policy_templates` WRITE;
/*!40000 ALTER TABLE `modx_access_policy_templates` DISABLE KEYS */;

INSERT INTO `modx_access_policy_templates` (`id`, `template_group`, `name`, `description`, `lexicon`)
VALUES
	(1,1,'AdministratorTemplate','Context administration policy template with all permissions.','permissions'),
	(2,3,'ResourceTemplate','Resource Policy Template with all attributes.','permissions'),
	(3,2,'ObjectTemplate','Object Policy Template with all attributes.','permissions'),
	(4,4,'ElementTemplate','Element Policy Template with all attributes.','permissions'),
	(5,5,'MediaSourceTemplate','Media Source Policy Template with all attributes.','permissions'),
	(6,2,'ContextTemplate','Context Policy Template with all attributes.','permissions'),
	(7,1,'- Client Content Editor','Context administration policy template with all permissions.','permissions'),
	(8,1,'Duplicate of - Client Content Editor','Context administration policy template with all permissions.','permissions');

/*!40000 ALTER TABLE `modx_access_policy_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_access_resource_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_resource_groups`;

CREATE TABLE `modx_access_resource_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_access_resources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_resources`;

CREATE TABLE `modx_access_resources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_access_templatevars
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_access_templatevars`;

CREATE TABLE `modx_access_templatevars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target` varchar(100) NOT NULL DEFAULT '',
  `principal_class` varchar(100) NOT NULL DEFAULT 'modPrincipal',
  `principal` int(10) unsigned NOT NULL DEFAULT '0',
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  `policy` int(10) unsigned NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `target` (`target`),
  KEY `principal_class` (`principal_class`),
  KEY `principal` (`principal`),
  KEY `authority` (`authority`),
  KEY `policy` (`policy`),
  KEY `context_key` (`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_actiondom
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_actiondom`;

CREATE TABLE `modx_actiondom` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `set` int(11) NOT NULL DEFAULT '0',
  `action` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `xtype` varchar(100) NOT NULL DEFAULT '',
  `container` varchar(255) NOT NULL DEFAULT '',
  `rule` varchar(100) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `constraint` varchar(255) NOT NULL DEFAULT '',
  `constraint_field` varchar(100) NOT NULL DEFAULT '',
  `constraint_class` varchar(100) NOT NULL DEFAULT '',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `for_parent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `set` (`set`),
  KEY `action` (`action`),
  KEY `name` (`name`),
  KEY `active` (`active`),
  KEY `for_parent` (`for_parent`),
  KEY `rank` (`rank`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_actions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_actions`;

CREATE TABLE `modx_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namespace` varchar(100) NOT NULL DEFAULT 'core',
  `controller` varchar(255) NOT NULL,
  `haslayout` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `lang_topics` text NOT NULL,
  `assets` text NOT NULL,
  `help_url` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `namespace` (`namespace`),
  KEY `controller` (`controller`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_actions` WRITE;
/*!40000 ALTER TABLE `modx_actions` DISABLE KEYS */;

INSERT INTO `modx_actions` (`id`, `namespace`, `controller`, `haslayout`, `lang_topics`, `assets`, `help_url`)
VALUES
	(1,'formit','index',1,'formit:mgr','','');

/*!40000 ALTER TABLE `modx_actions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_actions_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_actions_fields`;

CREATE TABLE `modx_actions_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `action` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(100) NOT NULL DEFAULT 'field',
  `tab` varchar(255) NOT NULL DEFAULT '',
  `form` varchar(255) NOT NULL DEFAULT '',
  `other` varchar(255) NOT NULL DEFAULT '',
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `action` (`action`),
  KEY `type` (`type`),
  KEY `tab` (`tab`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_actions_fields` WRITE;
/*!40000 ALTER TABLE `modx_actions_fields` DISABLE KEYS */;

INSERT INTO `modx_actions_fields` (`id`, `action`, `name`, `type`, `tab`, `form`, `other`, `rank`)
VALUES
	(1,'resource/update','modx-resource-settings','tab','','modx-panel-resource','',0),
	(2,'resource/update','modx-resource-main-left','tab','','modx-panel-resource','',1),
	(3,'resource/update','id','field','modx-resource-main-left','modx-panel-resource','',0),
	(4,'resource/update','pagetitle','field','modx-resource-main-left','modx-panel-resource','',1),
	(5,'resource/update','longtitle','field','modx-resource-main-left','modx-panel-resource','',2),
	(6,'resource/update','description','field','modx-resource-main-left','modx-panel-resource','',3),
	(7,'resource/update','introtext','field','modx-resource-main-left','modx-panel-resource','',4),
	(8,'resource/update','modx-resource-main-right','tab','','modx-panel-resource','',2),
	(9,'resource/update','template','field','modx-resource-main-right','modx-panel-resource','',0),
	(10,'resource/update','alias','field','modx-resource-main-right','modx-panel-resource','',1),
	(11,'resource/update','menutitle','field','modx-resource-main-right','modx-panel-resource','',2),
	(12,'resource/update','link_attributes','field','modx-resource-main-right','modx-panel-resource','',3),
	(13,'resource/update','hidemenu','field','modx-resource-main-right','modx-panel-resource','',4),
	(14,'resource/update','published','field','modx-resource-main-right','modx-panel-resource','',5),
	(15,'resource/update','modx-page-settings','tab','','modx-panel-resource','',3),
	(16,'resource/update','modx-page-settings-left','tab','','modx-panel-resource','',4),
	(17,'resource/update','parent-cmb','field','modx-page-settings-left','modx-panel-resource','',0),
	(18,'resource/update','class_key','field','modx-page-settings-left','modx-panel-resource','',1),
	(19,'resource/update','content_type','field','modx-page-settings-left','modx-panel-resource','',2),
	(20,'resource/update','content_dispo','field','modx-page-settings-left','modx-panel-resource','',3),
	(21,'resource/update','menuindex','field','modx-page-settings-left','modx-panel-resource','',4),
	(22,'resource/update','modx-page-settings-right','tab','','modx-panel-resource','',5),
	(23,'resource/update','publishedon','field','modx-page-settings-right','modx-panel-resource','',0),
	(24,'resource/update','pub_date','field','modx-page-settings-right','modx-panel-resource','',1),
	(25,'resource/update','unpub_date','field','modx-page-settings-right','modx-panel-resource','',2),
	(26,'resource/update','modx-page-settings-right-box-left','tab','','modx-panel-resource','',6),
	(27,'resource/update','isfolder','field','modx-page-settings-right-box-left','modx-panel-resource','',0),
	(28,'resource/update','searchable','field','modx-page-settings-right-box-left','modx-panel-resource','',1),
	(29,'resource/update','richtext','field','modx-page-settings-right-box-left','modx-panel-resource','',2),
	(30,'resource/update','uri_override','field','modx-page-settings-right-box-left','modx-panel-resource','',3),
	(31,'resource/update','uri','field','modx-page-settings-right-box-left','modx-panel-resource','',4),
	(32,'resource/update','modx-page-settings-right-box-right','tab','','modx-panel-resource','',7),
	(33,'resource/update','cacheable','field','modx-page-settings-right-box-right','modx-panel-resource','',0),
	(34,'resource/update','syncsite','field','modx-page-settings-right-box-right','modx-panel-resource','',1),
	(35,'resource/update','deleted','field','modx-page-settings-right-box-right','modx-panel-resource','',2),
	(36,'resource/update','modx-panel-resource-tv','tab','','modx-panel-resource','tv',8),
	(37,'resource/update','modx-resource-access-permissions','tab','','modx-panel-resource','',9),
	(38,'resource/update','modx-resource-content','field','modx-resource-content','modx-panel-resource','',0),
	(39,'resource/create','modx-resource-settings','tab','','modx-panel-resource','',0),
	(40,'resource/create','modx-resource-main-left','tab','','modx-panel-resource','',1),
	(41,'resource/create','id','field','modx-resource-main-left','modx-panel-resource','',0),
	(42,'resource/create','pagetitle','field','modx-resource-main-left','modx-panel-resource','',1),
	(43,'resource/create','longtitle','field','modx-resource-main-left','modx-panel-resource','',2),
	(44,'resource/create','description','field','modx-resource-main-left','modx-panel-resource','',3),
	(45,'resource/create','introtext','field','modx-resource-main-left','modx-panel-resource','',4),
	(46,'resource/create','modx-resource-main-right','tab','','modx-panel-resource','',2),
	(47,'resource/create','template','field','modx-resource-main-right','modx-panel-resource','',0),
	(48,'resource/create','alias','field','modx-resource-main-right','modx-panel-resource','',1),
	(49,'resource/create','menutitle','field','modx-resource-main-right','modx-panel-resource','',2),
	(50,'resource/create','link_attributes','field','modx-resource-main-right','modx-panel-resource','',3),
	(51,'resource/create','hidemenu','field','modx-resource-main-right','modx-panel-resource','',4),
	(52,'resource/create','published','field','modx-resource-main-right','modx-panel-resource','',5),
	(53,'resource/create','modx-page-settings','tab','','modx-panel-resource','',3),
	(54,'resource/create','modx-page-settings-left','tab','','modx-panel-resource','',4),
	(55,'resource/create','parent-cmb','field','modx-page-settings-left','modx-panel-resource','',0),
	(56,'resource/create','class_key','field','modx-page-settings-left','modx-panel-resource','',1),
	(57,'resource/create','content_type','field','modx-page-settings-left','modx-panel-resource','',2),
	(58,'resource/create','content_dispo','field','modx-page-settings-left','modx-panel-resource','',3),
	(59,'resource/create','menuindex','field','modx-page-settings-left','modx-panel-resource','',4),
	(60,'resource/create','modx-page-settings-right','tab','','modx-panel-resource','',5),
	(61,'resource/create','publishedon','field','modx-page-settings-right','modx-panel-resource','',0),
	(62,'resource/create','pub_date','field','modx-page-settings-right','modx-panel-resource','',1),
	(63,'resource/create','unpub_date','field','modx-page-settings-right','modx-panel-resource','',2),
	(64,'resource/create','modx-page-settings-right-box-left','tab','','modx-panel-resource','',6),
	(65,'resource/create','isfolder','field','modx-page-settings-right-box-left','modx-panel-resource','',0),
	(66,'resource/create','searchable','field','modx-page-settings-right-box-left','modx-panel-resource','',1),
	(67,'resource/create','richtext','field','modx-page-settings-right-box-left','modx-panel-resource','',2),
	(68,'resource/create','uri_override','field','modx-page-settings-right-box-left','modx-panel-resource','',3),
	(69,'resource/create','uri','field','modx-page-settings-right-box-left','modx-panel-resource','',4),
	(70,'resource/create','modx-page-settings-right-box-right','tab','','modx-panel-resource','',7),
	(71,'resource/create','cacheable','field','modx-page-settings-right-box-right','modx-panel-resource','',0),
	(72,'resource/create','syncsite','field','modx-page-settings-right-box-right','modx-panel-resource','',1),
	(73,'resource/create','deleted','field','modx-page-settings-right-box-right','modx-panel-resource','',2),
	(74,'resource/create','modx-panel-resource-tv','tab','','modx-panel-resource','tv',8),
	(75,'resource/create','modx-resource-access-permissions','tab','','modx-panel-resource','',9),
	(76,'resource/create','modx-resource-content','field','modx-resource-content','modx-panel-resource','',0);

/*!40000 ALTER TABLE `modx_actions_fields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_active_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_active_users`;

CREATE TABLE `modx_active_users` (
  `internalKey` int(9) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '',
  `lasthit` int(20) NOT NULL DEFAULT '0',
  `id` int(10) DEFAULT NULL,
  `action` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`internalKey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_categories`;

CREATE TABLE `modx_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent` int(10) unsigned DEFAULT '0',
  `category` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `category` (`parent`,`category`),
  KEY `parent` (`parent`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_categories` WRITE;
/*!40000 ALTER TABLE `modx_categories` DISABLE KEYS */;

INSERT INTO `modx_categories` (`id`, `parent`, `category`)
VALUES
	(1,0,'Site Controls'),
	(2,0,'Navigation'),
	(3,0,'Page Controls'),
	(4,0,'Contact Form'),
	(5,0,'Site Structure'),
	(6,0,'SEO'),
	(7,0,'FormIt'),
	(8,0,'Header'),
	(9,0,'Homepage Columns'),
	(10,0,'sendGrid'),
	(11,0,'quickemail'),
	(12,0,'Analyticsdashboard');

/*!40000 ALTER TABLE `modx_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_categories_closure
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_categories_closure`;

CREATE TABLE `modx_categories_closure` (
  `ancestor` int(10) unsigned NOT NULL DEFAULT '0',
  `descendant` int(10) unsigned NOT NULL DEFAULT '0',
  `depth` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ancestor`,`descendant`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_categories_closure` WRITE;
/*!40000 ALTER TABLE `modx_categories_closure` DISABLE KEYS */;

INSERT INTO `modx_categories_closure` (`ancestor`, `descendant`, `depth`)
VALUES
	(1,1,0),
	(0,1,0),
	(2,2,0),
	(0,2,0),
	(3,3,0),
	(0,3,0),
	(4,4,0),
	(0,4,0),
	(5,5,0),
	(0,5,0),
	(6,6,0),
	(0,6,0),
	(7,7,0),
	(0,7,0),
	(8,8,0),
	(0,8,0),
	(9,9,0),
	(0,9,0),
	(10,10,0),
	(0,10,0),
	(11,11,0),
	(0,11,0),
	(12,12,0),
	(0,12,0);

/*!40000 ALTER TABLE `modx_categories_closure` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_class_map
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_class_map`;

CREATE TABLE `modx_class_map` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(120) NOT NULL DEFAULT '',
  `parent_class` varchar(120) NOT NULL DEFAULT '',
  `name_field` varchar(255) NOT NULL DEFAULT 'name',
  `path` tinytext,
  `lexicon` varchar(255) NOT NULL DEFAULT 'core:resource',
  PRIMARY KEY (`id`),
  UNIQUE KEY `class` (`class`),
  KEY `parent_class` (`parent_class`),
  KEY `name_field` (`name_field`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_class_map` WRITE;
/*!40000 ALTER TABLE `modx_class_map` DISABLE KEYS */;

INSERT INTO `modx_class_map` (`id`, `class`, `parent_class`, `name_field`, `path`, `lexicon`)
VALUES
	(1,'modDocument','modResource','pagetitle','','core:resource'),
	(2,'modWebLink','modResource','pagetitle','','core:resource'),
	(3,'modSymLink','modResource','pagetitle','','core:resource'),
	(4,'modStaticResource','modResource','pagetitle','','core:resource'),
	(5,'modTemplate','modElement','templatename','','core:resource'),
	(6,'modTemplateVar','modElement','name','','core:resource'),
	(7,'modChunk','modElement','name','','core:resource'),
	(8,'modSnippet','modElement','name','','core:resource'),
	(9,'modPlugin','modElement','name','','core:resource');

/*!40000 ALTER TABLE `modx_class_map` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_content_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_content_type`;

CREATE TABLE `modx_content_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` tinytext,
  `mime_type` tinytext,
  `file_extensions` tinytext,
  `headers` mediumtext,
  `binary` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_content_type` WRITE;
/*!40000 ALTER TABLE `modx_content_type` DISABLE KEYS */;

INSERT INTO `modx_content_type` (`id`, `name`, `description`, `mime_type`, `file_extensions`, `headers`, `binary`)
VALUES
	(1,'HTML','HTML content','text/html','',NULL,0),
	(2,'XML','XML content','text/xml','.xml',NULL,0),
	(3,'text','plain text content','text/plain','.txt',NULL,0),
	(4,'CSS','CSS content','text/css','.css',NULL,0),
	(5,'javascript','javascript content','text/javascript','.js',NULL,0),
	(6,'RSS','For RSS feeds','application/rss+xml','.rss',NULL,0),
	(7,'JSON','JSON','application/json','.json',NULL,0),
	(8,'PDF','PDF Files','application/pdf','.pdf',NULL,1);

/*!40000 ALTER TABLE `modx_content_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_context
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_context`;

CREATE TABLE `modx_context` (
  `key` varchar(100) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` tinytext,
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`key`),
  KEY `name` (`name`),
  KEY `rank` (`rank`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_context` WRITE;
/*!40000 ALTER TABLE `modx_context` DISABLE KEYS */;

INSERT INTO `modx_context` (`key`, `name`, `description`, `rank`)
VALUES
	('web','Website','The default front-end context for your web site.',0),
	('mgr','Manager','The default manager or administration context for content management activity.',0);

/*!40000 ALTER TABLE `modx_context` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_context_resource
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_context_resource`;

CREATE TABLE `modx_context_resource` (
  `context_key` varchar(255) NOT NULL,
  `resource` int(11) unsigned NOT NULL,
  PRIMARY KEY (`context_key`,`resource`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_context_setting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_context_setting`;

CREATE TABLE `modx_context_setting` (
  `context_key` varchar(255) NOT NULL,
  `key` varchar(50) NOT NULL,
  `value` mediumtext,
  `xtype` varchar(75) NOT NULL DEFAULT 'textfield',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `area` varchar(255) NOT NULL DEFAULT '',
  `editedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`context_key`,`key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_context_setting` WRITE;
/*!40000 ALTER TABLE `modx_context_setting` DISABLE KEYS */;

INSERT INTO `modx_context_setting` (`context_key`, `key`, `value`, `xtype`, `namespace`, `area`, `editedon`)
VALUES
	('mgr','allow_tags_in_post','1','combo-boolean','core','system','0000-00-00 00:00:00'),
	('mgr','modRequest.class','modManagerRequest','textfield','core','system','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `modx_context_setting` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_dashboard
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_dashboard`;

CREATE TABLE `modx_dashboard` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `hide_trees` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `hide_trees` (`hide_trees`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_dashboard` WRITE;
/*!40000 ALTER TABLE `modx_dashboard` DISABLE KEYS */;

INSERT INTO `modx_dashboard` (`id`, `name`, `description`, `hide_trees`)
VALUES
	(1,'Default','',0);

/*!40000 ALTER TABLE `modx_dashboard` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_dashboard_widget
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_dashboard_widget`;

CREATE TABLE `modx_dashboard_widget` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `type` varchar(100) NOT NULL,
  `content` mediumtext,
  `namespace` varchar(255) NOT NULL DEFAULT '',
  `lexicon` varchar(255) NOT NULL DEFAULT 'core:dashboards',
  `size` varchar(255) NOT NULL DEFAULT 'half',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `type` (`type`),
  KEY `namespace` (`namespace`),
  KEY `lexicon` (`lexicon`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_dashboard_widget` WRITE;
/*!40000 ALTER TABLE `modx_dashboard_widget` DISABLE KEYS */;

INSERT INTO `modx_dashboard_widget` (`id`, `name`, `description`, `type`, `content`, `namespace`, `lexicon`, `size`)
VALUES
	(1,'w_newsfeed','w_newsfeed_desc','file','[[++manager_path]]controllers/default/dashboard/widget.modx-news.php','core','core:dashboards','half'),
	(2,'w_securityfeed','w_securityfeed_desc','file','[[++manager_path]]controllers/default/dashboard/widget.modx-security.php','core','core:dashboards','half'),
	(3,'w_whosonline','w_whosonline_desc','file','[[++manager_path]]controllers/default/dashboard/widget.grid-online.php','core','core:dashboards','half'),
	(4,'w_recentlyeditedresources','w_recentlyeditedresources_desc','file','[[++manager_path]]controllers/default/dashboard/widget.grid-rer.php','core','core:dashboards','half'),
	(5,'w_configcheck','w_configcheck_desc','file','[[++manager_path]]controllers/default/dashboard/widget.configcheck.php','core','core:dashboards','full'),
	(6,'analytics','Google Analytics dashboard widget','file','[[++core_path]]components/analytics/elements/widget/widget.analytics.php','analytics','analytics:default','double');

/*!40000 ALTER TABLE `modx_dashboard_widget` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_dashboard_widget_placement
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_dashboard_widget_placement`;

CREATE TABLE `modx_dashboard_widget_placement` (
  `dashboard` int(10) unsigned NOT NULL DEFAULT '0',
  `widget` int(10) unsigned NOT NULL DEFAULT '0',
  `rank` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`dashboard`,`widget`),
  KEY `rank` (`rank`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_dashboard_widget_placement` WRITE;
/*!40000 ALTER TABLE `modx_dashboard_widget_placement` DISABLE KEYS */;

INSERT INTO `modx_dashboard_widget_placement` (`dashboard`, `widget`, `rank`)
VALUES
	(1,3,1),
	(1,5,0),
	(1,4,2);

/*!40000 ALTER TABLE `modx_dashboard_widget_placement` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_document_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_document_groups`;

CREATE TABLE `modx_document_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `document_group` int(10) NOT NULL DEFAULT '0',
  `document` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `document_group` (`document_group`),
  KEY `document` (`document`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_documentgroup_names
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_documentgroup_names`;

CREATE TABLE `modx_documentgroup_names` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `private_memgroup` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `private_webgroup` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_element_property_sets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_element_property_sets`;

CREATE TABLE `modx_element_property_sets` (
  `element` int(10) unsigned NOT NULL DEFAULT '0',
  `element_class` varchar(100) NOT NULL DEFAULT '',
  `property_set` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`element`,`element_class`,`property_set`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_extension_packages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_extension_packages`;

CREATE TABLE `modx_extension_packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `name` varchar(100) NOT NULL DEFAULT 'core',
  `path` text,
  `table_prefix` varchar(255) NOT NULL DEFAULT '',
  `service_class` varchar(255) NOT NULL DEFAULT '',
  `service_name` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `namespace` (`namespace`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_fc_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_fc_profiles`;

CREATE TABLE `modx_fc_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `rank` (`rank`),
  KEY `active` (`active`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_fc_profiles_usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_fc_profiles_usergroups`;

CREATE TABLE `modx_fc_profiles_usergroups` (
  `usergroup` int(11) NOT NULL DEFAULT '0',
  `profile` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`usergroup`,`profile`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_fc_sets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_fc_sets`;

CREATE TABLE `modx_fc_sets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `profile` int(11) NOT NULL DEFAULT '0',
  `action` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `template` int(11) NOT NULL DEFAULT '0',
  `constraint` varchar(255) NOT NULL DEFAULT '',
  `constraint_field` varchar(100) NOT NULL DEFAULT '',
  `constraint_class` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `profile` (`profile`),
  KEY `action` (`action`),
  KEY `active` (`active`),
  KEY `template` (`template`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_formit_forms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_formit_forms`;

CREATE TABLE `modx_formit_forms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `form` varchar(255) NOT NULL DEFAULT '',
  `context_key` varchar(100) NOT NULL DEFAULT '',
  `values` text NOT NULL,
  `ip` varchar(15) NOT NULL DEFAULT '',
  `date` int(11) NOT NULL DEFAULT '0',
  `encrypted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table modx_lexicon_entries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_lexicon_entries`;

CREATE TABLE `modx_lexicon_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `topic` varchar(255) NOT NULL DEFAULT 'default',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `language` varchar(20) NOT NULL DEFAULT 'en',
  `createdon` datetime DEFAULT NULL,
  `editedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `topic` (`topic`),
  KEY `namespace` (`namespace`),
  KEY `language` (`language`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_lexicon_entries` WRITE;
/*!40000 ALTER TABLE `modx_lexicon_entries` DISABLE KEYS */;

INSERT INTO `modx_lexicon_entries` (`id`, `name`, `value`, `topic`, `namespace`, `language`, `createdon`, `editedon`)
VALUES
	(1,'setting_primary_email','Primary Email','default','core','en','2014-10-06 00:34:29','0000-00-00 00:00:00'),
	(2,'setting_facebook','Facebook Link','default','core','en','2014-10-06 00:34:36','0000-00-00 00:00:00'),
	(3,'setting_linkedin','Linkedin URL','default','core','en','2014-10-06 00:35:00','0000-00-00 00:00:00'),
	(4,'setting_twitter','Twitter Link','default','core','en','2014-10-06 00:35:06','0000-00-00 00:00:00'),
	(5,'setting_telephone','Primary Telephone','default','core','en','2014-10-06 00:35:53','0000-00-00 00:00:00'),
	(6,'setting_primary_email','Primary Email','setting','core','en','2015-03-22 15:49:44','0000-00-00 00:00:00'),
	(7,'setting_telephone','Primary Telephone','setting','core','en','2015-03-22 15:53:45','0000-00-00 00:00:00'),
	(8,'setting_google_analytics','Google Analytics','default','core','en','2015-04-15 07:01:27','0000-00-00 00:00:00'),
	(9,'setting_google_analytics','Google Analytics','setting','core','en','2015-04-15 07:02:46','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `modx_lexicon_entries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_manager_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_manager_log`;

CREATE TABLE `modx_manager_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(10) unsigned NOT NULL DEFAULT '0',
  `occurred` datetime DEFAULT '0000-00-00 00:00:00',
  `action` varchar(100) NOT NULL DEFAULT '',
  `classKey` varchar(100) NOT NULL DEFAULT '',
  `item` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_manager_log` WRITE;
/*!40000 ALTER TABLE `modx_manager_log` DISABLE KEYS */;

INSERT INTO `modx_manager_log` (`id`, `user`, `occurred`, `action`, `classKey`, `item`)
VALUES
	(1,1,'2014-10-06 00:34:29','setting_create','modSystemSetting','primary_email'),
	(2,1,'2014-10-06 00:34:36','setting_create','modSystemSetting','facebook'),
	(3,1,'2014-10-06 00:35:00','setting_create','modSystemSetting','linkedin'),
	(4,1,'2014-10-06 00:35:06','setting_create','modSystemSetting','twitter'),
	(5,1,'2014-10-06 00:35:53','setting_create','modSystemSetting','telephone'),
	(6,1,'2014-10-06 00:42:33','setting_update','modSystemSetting','friendly_urls'),
	(7,1,'2014-10-06 00:42:45','setting_update','modSystemSetting','use_alias_path'),
	(8,1,'2014-10-06 00:42:50','setting_update','modSystemSetting','global_duplicate_uri_check'),
	(9,1,'2014-10-06 00:42:53','setting_update','modSystemSetting','use_alias_path'),
	(10,1,'2014-10-06 00:43:06','setting_update','modSystemSetting','use_weblink_target'),
	(11,1,'2014-10-06 00:44:04','policy_import','modAccessPolicy','12'),
	(12,1,'2014-10-06 00:44:17','policy_template_import','modAccessPolicyTemplate','8'),
	(13,1,'2014-10-06 00:44:46','category_create','modCategory','1'),
	(14,1,'2014-10-06 00:44:51','category_create','modCategory','2'),
	(15,1,'2014-10-06 00:45:09','category_create','modCategory','3'),
	(16,1,'2014-10-06 00:52:57','category_create','modCategory','4'),
	(17,1,'2014-10-06 00:54:29','category_create','modCategory','5'),
	(18,1,'2014-10-06 00:56:10','chunk_create','modChunk','1'),
	(19,1,'2014-10-06 00:58:53','chunk_create','modChunk','2'),
	(20,1,'2014-10-06 01:00:03','chunk_update','modChunk','1'),
	(21,1,'2014-10-06 01:00:24','chunk_create','modChunk','3'),
	(22,1,'2014-10-06 01:02:07','chunk_create','modChunk','4'),
	(23,1,'2014-10-06 01:02:15','chunk_create','modChunk','5'),
	(24,1,'2014-10-06 01:02:24','chunk_create','modChunk','6'),
	(25,1,'2014-10-06 01:02:30','chunk_update','modChunk','6'),
	(26,1,'2014-10-06 01:06:46','chunk_update','modChunk','6'),
	(27,1,'2014-10-06 01:07:21','chunk_update','modChunk','6'),
	(28,1,'2014-10-06 01:10:07','content_type_save','modContentType','1'),
	(29,1,'2014-10-06 01:16:40','user_group_create','modUserGroup','2'),
	(30,1,'2014-10-06 01:18:25','user_group_update','modUserGroup','2'),
	(31,1,'2014-10-06 01:18:41','source_create','sources.modMediaSource','2'),
	(32,1,'2014-10-06 01:18:56','source_update','sources.modMediaSource','1'),
	(33,1,'2014-10-06 01:19:42','source_create','sources.modAccessMediaSource','1'),
	(34,1,'2014-10-06 01:19:52','user_group_update','modUserGroup','2'),
	(35,1,'2014-10-06 01:29:07','user_group_update','modUserGroup','2'),
	(36,1,'2014-10-06 01:40:23','resource_create','modDocument','2'),
	(37,1,'2014-10-06 01:42:29','category_create','modCategory','6'),
	(38,1,'2014-10-06 01:42:33','tv_create','modTemplateVar','1'),
	(39,1,'2014-10-06 01:43:15','tv_update','modTemplateVar','1'),
	(40,1,'2014-10-06 01:44:06','tv_create','modTemplateVar','2'),
	(41,1,'2014-12-05 23:40:18','source_update','sources.modMediaSource','1'),
	(42,1,'2014-12-05 23:40:37','source_update','sources.modMediaSource','1'),
	(43,1,'2014-12-05 23:40:51','source_update','sources.modMediaSource','2'),
	(44,1,'2014-12-05 23:41:07','template_create','modTemplate','2'),
	(45,1,'2014-12-05 23:44:48','template_update','modTemplate','2'),
	(46,1,'2014-12-08 03:58:56','resource_update','modResource','1'),
	(47,1,'2014-12-08 03:58:57','resource_update','modResource','1'),
	(48,1,'2014-12-08 03:59:28','chunk_update','modChunk','4'),
	(49,1,'2014-12-08 03:59:39','chunk_update','modChunk','5'),
	(50,1,'2014-12-08 03:59:57','chunk_create','modChunk','7'),
	(51,1,'2014-12-08 04:00:12','chunk_update','modChunk','6'),
	(52,1,'2014-12-08 04:00:35','chunk_create','modChunk','8'),
	(53,1,'2014-12-08 04:17:31','template_create','modTemplate','3'),
	(54,1,'2014-12-08 04:18:04','resource_update','modResource','2'),
	(55,1,'2014-12-08 04:18:19','resource_update','modResource','2'),
	(56,1,'2014-12-08 04:23:15','template_create','modTemplate','4'),
	(57,1,'2014-12-08 04:35:22','resource_update','modResource','2'),
	(58,1,'2014-12-08 04:35:43','resource_create','modDocument','3'),
	(59,1,'2014-12-08 04:36:07','resource_create','modDocument','4'),
	(60,1,'2014-12-08 04:38:12','resource_create','modDocument','5'),
	(61,1,'2014-12-08 04:38:19','resource_update','modResource','5'),
	(62,1,'2014-12-08 04:38:37','resource_create','modDocument','6'),
	(63,1,'2014-12-08 04:38:57','resource_update','modResource','5'),
	(64,1,'2014-12-08 04:39:16','resource_update','modResource','4'),
	(65,1,'2014-12-08 04:39:22','resource_update','modResource','6'),
	(66,1,'2014-12-08 04:39:35','resource_create','modDocument','7'),
	(67,1,'2014-12-08 04:40:06','resource_update','modResource','7'),
	(68,1,'2014-12-08 04:40:44','resource_create','modDocument','8'),
	(69,1,'2014-12-08 04:40:54','resource_update','modResource','7'),
	(70,1,'2014-12-08 04:41:18','resource_create','modDocument','9'),
	(71,1,'2014-12-08 04:42:41','resource_create','modDocument','10'),
	(72,1,'2014-12-08 11:20:29','tv_create','modTemplateVar','3'),
	(73,1,'2014-12-08 11:20:50','category_create','modCategory','8'),
	(74,1,'2014-12-08 11:21:29','tv_update','modTemplateVar','3'),
	(75,1,'2014-12-08 11:22:25','tv_create','modTemplateVar','4'),
	(76,1,'2014-12-08 11:23:17','tv_create','modTemplateVar','5'),
	(77,1,'2014-12-08 11:23:38','tv_update','modTemplateVar','4'),
	(78,1,'2014-12-08 11:24:54','chunk_create','modChunk','9'),
	(79,1,'2014-12-08 11:28:17','tv_create','modTemplateVar','6'),
	(80,1,'2014-12-08 11:28:46','tv_update','modTemplateVar','6'),
	(81,1,'2014-12-08 11:29:02','tv_update','modTemplateVar','1'),
	(82,1,'2014-12-08 11:29:11','tv_update','modTemplateVar','2'),
	(83,1,'2014-12-08 11:32:18','resource_update','modResource','1'),
	(84,1,'2014-12-08 11:32:51','resource_update','modResource','1'),
	(85,1,'2014-12-08 11:33:55','resource_create','modDocument','11'),
	(86,1,'2014-12-08 11:35:29','resource_update','modResource','11'),
	(87,1,'2014-12-08 11:37:10','resource_create','modDocument','12'),
	(88,1,'2014-12-08 11:37:21','resource_update','modResource','12'),
	(89,1,'2014-12-08 11:37:53','tv_create','modTemplateVar','7'),
	(90,1,'2014-12-08 11:38:08','tv_update','modTemplateVar','3'),
	(91,1,'2014-12-08 11:38:25','resource_update','modResource','1'),
	(92,1,'2014-12-08 11:38:42','template_update','modTemplate','2'),
	(93,1,'2014-12-08 11:38:58','resource_update','modResource','1'),
	(94,1,'2014-12-08 11:40:02','resource_update','modResource','1'),
	(95,1,'2014-12-08 11:41:29','tv_create','modTemplateVar','8'),
	(96,1,'2014-12-08 11:42:28','tv_update','modTemplateVar','8'),
	(97,1,'2014-12-08 11:42:43','resource_update','modResource','2'),
	(98,1,'2014-12-08 11:51:09','resource_update','modResource','8'),
	(99,1,'2014-12-08 11:51:12','resource_update','modResource','2'),
	(100,1,'2014-12-08 11:51:18','resource_update','modResource','2'),
	(101,1,'2014-12-08 12:01:48','setting_update','modSystemSetting','site_name'),
	(102,1,'2014-12-08 12:30:38','resource_update','modResource','3'),
	(103,1,'2014-12-08 12:30:58','resource_update','modResource','3'),
	(104,1,'2014-12-08 12:48:39','chunk_create','modChunk','10'),
	(105,1,'2014-12-08 12:48:59','template_create','modTemplate','5'),
	(106,1,'2014-12-08 12:49:10','resource_update','modResource','7'),
	(107,1,'2014-12-08 12:49:11','template_update','modTemplate','5'),
	(108,1,'2014-12-08 12:50:30','resource_update','modResource','7'),
	(109,1,'2014-12-08 12:51:21','template_update','modTemplate','5'),
	(110,1,'2014-12-08 12:51:41','resource_update','modResource','7'),
	(111,1,'2014-12-08 12:55:55','resource_update','modResource','3'),
	(112,1,'2014-12-08 12:56:01','resource_update','modResource','2'),
	(113,1,'2014-12-08 12:56:07','resource_update','modResource','6'),
	(114,1,'2014-12-08 12:56:47','resource_update','modResource','8'),
	(115,1,'2014-12-08 12:56:51','resource_update','modResource','9'),
	(116,1,'2014-12-08 12:56:56','resource_update','modResource','10'),
	(117,1,'2014-12-08 15:50:47','chunk_create','modChunk','11'),
	(118,1,'2014-12-08 15:53:31','chunk_create','modChunk','12'),
	(119,1,'2014-12-10 20:08:23','user_create','modUser','2'),
	(120,1,'2014-12-10 20:09:03','resource_update','modResource','2'),
	(121,1,'2014-12-10 20:09:23','resource_update','modResource','8'),
	(122,1,'2014-12-10 20:09:32','resource_update','modResource','9'),
	(123,1,'2014-12-10 20:09:41','resource_update','modResource','10'),
	(124,1,'2014-12-10 20:10:02','resource_update','modResource','7'),
	(125,2,'2015-01-25 10:27:21','change_profile_password','modUser','2'),
	(126,2,'2015-01-25 10:46:36','resource_update','modResource','1'),
	(127,2,'2015-01-25 10:47:38','resource_update','modResource','1'),
	(128,2,'2015-01-25 10:50:40','resource_update','modResource','1'),
	(129,2,'2015-01-25 11:03:55','resource_update','modResource','2'),
	(130,2,'2015-01-25 11:09:32','resource_update','modResource','2'),
	(131,1,'2015-01-25 11:19:45','category_create','modCategory','9'),
	(132,1,'2015-01-25 11:20:14','tv_create','modTemplateVar','9'),
	(133,1,'2015-01-25 11:20:32','tv_duplicate','modTemplateVar','10'),
	(134,1,'2015-01-25 11:20:39','tv_update','modTemplateVar','9'),
	(135,1,'2015-01-25 11:20:49','tv_update','modTemplateVar','10'),
	(136,1,'2015-01-25 11:21:00','tv_update','modTemplateVar','9'),
	(137,2,'2015-01-25 11:21:05','resource_create','modDocument','13'),
	(138,1,'2015-01-25 11:21:14','tv_duplicate','modTemplateVar','11'),
	(139,1,'2015-01-25 11:21:21','tv_update','modTemplateVar','11'),
	(140,1,'2015-01-25 11:21:45','tv_duplicate','modTemplateVar','12'),
	(141,1,'2015-01-25 11:21:55','tv_update','modTemplateVar','11'),
	(142,1,'2015-01-25 11:22:12','tv_duplicate','modTemplateVar','13'),
	(143,1,'2015-01-25 11:22:36','tv_update','modTemplateVar','13'),
	(144,2,'2015-01-25 11:22:39','resource_update','modResource','13'),
	(145,1,'2015-01-25 11:22:59','tv_duplicate','modTemplateVar','14'),
	(146,1,'2015-01-25 11:23:13','tv_duplicate','modTemplateVar','15'),
	(147,1,'2015-01-25 11:23:44','tv_duplicate','modTemplateVar','16'),
	(148,1,'2015-01-25 11:23:52','tv_duplicate','modTemplateVar','17'),
	(149,1,'2015-01-25 11:24:03','tv_duplicate','modTemplateVar','18'),
	(150,1,'2015-01-25 11:25:03','template_update','modTemplate','2'),
	(151,1,'2015-01-25 11:26:01','resource_update','modResource','1'),
	(152,1,'2015-01-25 11:26:26','tv_update','modTemplateVar','11'),
	(153,1,'2015-01-25 11:26:36','tv_update','modTemplateVar','14'),
	(154,1,'2015-01-25 11:26:43','tv_update','modTemplateVar','16'),
	(155,1,'2015-01-25 11:26:58','tv_update','modTemplateVar','12'),
	(156,1,'2015-01-25 11:28:00','resource_update','modResource','1'),
	(157,1,'2015-01-25 11:28:25','template_update','modTemplate','2'),
	(158,2,'2015-01-25 11:29:01','resource_update','modResource','3'),
	(159,2,'2015-01-25 11:29:32','resource_update','modResource','3'),
	(160,2,'2015-01-25 11:30:16','resource_update','modResource','3'),
	(161,2,'2015-01-25 11:32:36','resource_update','modResource','8'),
	(162,2,'2015-01-25 11:33:27','resource_update','modResource','9'),
	(163,2,'2015-01-25 11:34:47','resource_update','modResource','10'),
	(164,2,'2015-01-25 11:35:05','resource_update','modResource','10'),
	(165,2,'2015-01-25 11:40:28','resource_update','modResource','10'),
	(166,2,'2015-01-25 11:40:41','resource_update','modResource','9'),
	(167,2,'2015-01-25 11:40:52','resource_update','modResource','8'),
	(168,2,'2015-01-25 11:58:27','resource_create','modDocument','14'),
	(169,2,'2015-01-25 11:58:37','resource_sort','modResource','14'),
	(170,2,'2015-01-25 11:58:41','resource_update','modResource','14'),
	(171,2,'2015-01-25 12:23:08','resource_update','modResource','7'),
	(172,2,'2015-01-25 12:32:47','resource_update','modResource','6'),
	(173,2,'2015-01-25 12:32:56','resource_update','modResource','6'),
	(174,2,'2015-01-25 12:33:47','resource_update','modResource','6'),
	(175,1,'2015-01-26 15:13:18','resource_update','modResource','14'),
	(176,2,'2015-01-26 21:25:00','resource_sort','modResource','13'),
	(177,2,'2015-01-26 21:27:28','resource_create','modDocument','15'),
	(178,2,'2015-01-26 21:27:36','resource_sort','modResource','15'),
	(179,2,'2015-01-26 21:30:35','resource_create','modDocument','16'),
	(180,2,'2015-01-26 21:30:43','resource_sort','modResource','16'),
	(181,2,'2015-01-26 21:31:09','resource_create','modDocument','17'),
	(182,2,'2015-01-26 21:31:17','resource_sort','modResource','17'),
	(183,2,'2015-01-26 21:31:55','resource_create','modDocument','18'),
	(184,2,'2015-01-26 21:32:03','resource_update','modResource','18'),
	(185,2,'2015-01-26 21:32:09','resource_sort','modResource','18'),
	(186,2,'2015-01-26 21:32:54','resource_create','modDocument','19'),
	(187,2,'2015-01-26 21:33:09','resource_sort','modResource','19'),
	(188,2,'2015-01-26 21:33:42','resource_create','modDocument','20'),
	(189,2,'2015-01-26 21:33:57','resource_sort','modResource','20'),
	(190,2,'2015-01-26 21:34:03','resource_sort','modResource','20'),
	(191,2,'2015-01-26 21:34:10','resource_update','modResource','20'),
	(192,2,'2015-01-26 21:35:08','resource_update','modResource','14'),
	(193,1,'2015-01-27 04:15:53','resource_update','modResource','2'),
	(194,1,'2015-01-27 04:19:23','template_update','modTemplate','4'),
	(195,1,'2015-01-27 04:19:29','template_update','modTemplate','4'),
	(196,1,'2015-01-27 04:20:00','template_update','modTemplate','4'),
	(197,1,'2015-01-27 04:20:33','template_update','modTemplate','4'),
	(198,1,'2015-01-27 04:20:41','template_update','modTemplate','4'),
	(199,1,'2015-01-27 04:22:13','template_update','modTemplate','4'),
	(200,1,'2015-01-27 04:23:06','template_update','modTemplate','4'),
	(201,1,'2015-01-27 04:23:21','template_update','modTemplate','4'),
	(202,1,'2015-01-27 04:23:49','template_update','modTemplate','4'),
	(203,1,'2015-01-27 04:32:10','template_update','modTemplate','4'),
	(204,1,'2015-01-27 04:32:56','template_duplicate','modTemplate','6'),
	(205,1,'2015-01-27 04:33:06','template_update','modTemplate','4'),
	(206,1,'2015-01-27 04:33:25','template_update','modTemplate','6'),
	(207,1,'2015-01-27 04:33:32','template_update','modTemplate','6'),
	(208,1,'2015-01-27 04:33:43','template_update','modTemplate','6'),
	(209,1,'2015-01-27 04:34:10','resource_update','modResource','9'),
	(210,1,'2015-01-27 04:34:20','resource_update','modResource','14'),
	(211,1,'2015-01-27 04:34:34','resource_update','modResource','10'),
	(212,1,'2015-01-27 04:34:40','resource_update','modResource','14'),
	(213,1,'2015-01-27 04:34:47','resource_update','modResource','8'),
	(214,1,'2015-01-27 04:35:04','template_update','modTemplate','6'),
	(215,1,'2015-01-27 04:36:24','snippet_create','modSnippet','10'),
	(216,1,'2015-01-27 04:36:47','template_update','modTemplate','6'),
	(217,1,'2015-01-27 04:37:07','template_update','modTemplate','6'),
	(218,1,'2015-01-27 04:37:25','template_update','modTemplate','6'),
	(219,1,'2015-01-27 04:37:41','snippet_delete','modSnippet','10'),
	(220,1,'2015-01-27 04:39:19','snippet_create','modSnippet','11'),
	(221,1,'2015-01-27 04:39:38','template_update','modTemplate','6'),
	(222,1,'2015-01-27 04:39:50','template_update','modTemplate','6'),
	(223,1,'2015-01-27 04:40:14','resource_update','modResource','16'),
	(224,1,'2015-01-27 04:40:23','resource_update','modResource','17'),
	(225,1,'2015-01-27 04:40:31','resource_update','modResource','18'),
	(226,1,'2015-01-27 04:40:40','resource_update','modResource','19'),
	(227,1,'2015-01-27 04:40:46','resource_update','modResource','20'),
	(228,1,'2015-01-27 04:44:33','snippet_update','modSnippet','11'),
	(229,1,'2015-01-27 04:44:46','template_update','modTemplate','6'),
	(230,2,'2015-01-31 07:46:40','resource_update','modResource','1'),
	(231,2,'2015-01-31 07:49:24','resource_update','modResource','1'),
	(232,2,'2015-01-31 08:05:51','resource_update','modResource','1'),
	(233,2,'2015-01-31 08:07:07','resource_update','modResource','1'),
	(234,2,'2015-01-31 08:26:29','resource_update','modResource','13'),
	(235,2,'2015-01-31 08:27:41','resource_update','modResource','13'),
	(236,2,'2015-01-31 08:28:30','resource_update','modResource','13'),
	(237,2,'2015-01-31 08:29:02','resource_update','modResource','13'),
	(238,2,'2015-01-31 08:30:00','resource_update','modResource','13'),
	(239,2,'2015-01-31 08:51:59','resource_update','modResource','20'),
	(240,2,'2015-01-31 08:53:45','resource_update','modResource','20'),
	(241,2,'2015-01-31 08:53:51','resource_sort','modResource','20'),
	(242,2,'2015-01-31 08:54:15','resource_update','modResource','19'),
	(243,2,'2015-01-31 08:55:22','resource_create','modDocument','21'),
	(244,2,'2015-01-31 08:57:10','resource_update','modResource','21'),
	(245,2,'2015-01-31 09:01:51','resource_create','modDocument','22'),
	(246,2,'2015-01-31 09:02:01','resource_sort','modResource','21'),
	(247,2,'2015-01-31 09:02:05','resource_update','modResource','22'),
	(248,2,'2015-01-31 09:08:06','resource_create','modDocument','23'),
	(249,2,'2015-01-31 09:16:24','resource_update','modResource','23'),
	(250,1,'2015-03-12 19:49:13','user_update','modUser','2'),
	(251,2,'2015-03-22 15:27:54','resource_update','modResource','1'),
	(252,2,'2015-03-22 15:28:59','resource_sort','modResource','21'),
	(253,2,'2015-03-22 15:29:49','delete_resource','modDocument','18'),
	(254,2,'2015-03-22 15:35:00','delete_resource','modDocument','20'),
	(255,2,'2015-03-22 15:35:21','delete_resource','modDocument','19'),
	(256,2,'2015-03-22 15:41:37','resource_sort','modResource','23'),
	(257,2,'2015-03-22 15:43:06','delete_resource','modDocument','16'),
	(258,2,'2015-03-22 15:43:30','delete_resource','modDocument','17'),
	(259,2,'2015-03-22 15:43:46','resource_update','modResource','5'),
	(260,2,'2015-03-22 15:43:56','resource_update','modResource','5'),
	(261,2,'2015-03-22 15:44:37','resource_update','modResource','15'),
	(262,2,'2015-03-22 15:45:02','resource_update','modResource','2'),
	(263,2,'2015-03-22 15:45:17','resource_update','modResource','2'),
	(264,2,'2015-03-22 15:49:44','setting_update','modSystemSetting','primary_email'),
	(265,2,'2015-03-22 15:53:45','setting_update','modSystemSetting','telephone'),
	(266,2,'2015-03-23 18:15:46','resource_update','modResource','3'),
	(267,1,'2015-03-25 12:39:34','chunk_create','modChunk','13'),
	(268,1,'2015-03-25 12:40:31','template_create','modTemplate','7'),
	(269,1,'2015-03-25 12:40:49','template_update','modTemplate','2'),
	(270,1,'2015-03-25 12:41:34','resource_create','modDocument','24'),
	(271,1,'2015-03-25 12:41:45','resource_update','modResource','24'),
	(272,1,'2015-03-25 12:41:57','duplicate_resource','modDocument','25'),
	(273,1,'2015-03-25 12:41:58','resource_update','modResource','24'),
	(274,1,'2015-03-27 05:50:07','tv_duplicate','modTemplateVar','19'),
	(275,1,'2015-03-27 05:50:26','tv_duplicate','modTemplateVar','20'),
	(276,1,'2015-03-27 05:50:37','tv_duplicate','modTemplateVar','21'),
	(277,1,'2015-03-27 05:50:48','tv_duplicate','modTemplateVar','22'),
	(278,1,'2015-03-27 05:51:02','tv_duplicate','modTemplateVar','23'),
	(279,1,'2015-03-27 05:53:06','template_update','modTemplate','2'),
	(280,1,'2015-03-27 05:54:01','resource_update','modResource','1'),
	(281,2,'2015-03-28 12:40:11','resource_update','modResource','1'),
	(282,2,'2015-03-28 12:41:46','resource_update','modResource','1'),
	(283,2,'2015-03-28 14:08:31','resource_update','modResource','8'),
	(284,2,'2015-03-28 16:14:10','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(285,2,'2015-03-28 16:14:15','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(286,2,'2015-03-28 16:16:02','resource_update','modResource','8'),
	(287,2,'2015-03-28 16:25:23','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(288,2,'2015-03-28 16:26:56','resource_update','modResource','8'),
	(289,2,'2015-03-28 16:37:01','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(290,2,'2015-03-28 16:39:08','resource_update','modResource','9'),
	(291,2,'2015-03-28 16:40:24','resource_update','modResource','9'),
	(292,2,'2015-03-28 16:40:45','resource_update','modResource','9'),
	(293,2,'2015-03-28 16:44:50','resource_update','modResource','9'),
	(294,2,'2015-03-28 17:03:36','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(295,2,'2015-03-28 17:04:24','resource_update','modResource','14'),
	(296,2,'2015-03-28 18:55:50','resource_create','modDocument','26'),
	(297,2,'2015-03-28 18:57:52','resource_sort','modResource','26'),
	(298,2,'2015-03-28 19:03:17','resource_update','modResource','26'),
	(299,2,'2015-03-28 19:03:36','resource_sort','modResource','26'),
	(300,2,'2015-03-28 19:03:39','resource_update','modResource','26'),
	(301,2,'2015-03-28 19:17:45','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(302,2,'2015-03-28 19:20:49','resource_update','modResource','26'),
	(303,2,'2015-03-28 19:35:52','resource_update','modResource','26'),
	(304,2,'2015-03-28 20:06:15','resource_update','modResource','26'),
	(305,2,'2015-03-28 21:11:17','resource_update','modResource','24'),
	(306,2,'2015-03-28 21:18:24','resource_update','modResource','24'),
	(307,2,'2015-03-28 21:18:52','resource_update','modResource','24'),
	(308,2,'2015-03-28 21:25:15','resource_update','modResource','24'),
	(309,2,'2015-03-28 21:55:06','resource_update','modResource','12'),
	(310,2,'2015-03-28 22:09:25','resource_update','modResource','25'),
	(311,2,'2015-03-28 22:09:50','resource_update','modResource','25'),
	(312,2,'2015-03-28 22:12:48','resource_update','modResource','21'),
	(313,2,'2015-03-28 22:18:46','resource_update','modResource','25'),
	(314,2,'2015-03-28 22:25:09','resource_update','modResource','21'),
	(315,2,'2015-03-28 22:36:59','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(316,2,'2015-03-28 22:40:39','resource_update','modResource','8'),
	(317,2,'2015-03-28 22:41:14','resource_update','modResource','9'),
	(318,2,'2015-03-28 22:41:37','resource_update','modResource','9'),
	(319,2,'2015-03-28 22:42:20','resource_update','modResource','14'),
	(320,2,'2015-03-28 22:44:26','resource_update','modResource','26'),
	(321,2,'2015-03-28 22:54:33','resource_update','modResource','22'),
	(322,2,'2015-03-28 22:56:52','resource_update','modResource','22'),
	(323,2,'2015-03-28 22:57:08','resource_update','modResource','21'),
	(324,2,'2015-03-28 23:02:33','resource_sort','modResource','5'),
	(325,2,'2015-03-28 23:02:41','resource_update','modResource','21'),
	(326,2,'2015-03-29 19:49:16','resource_update','modResource','1'),
	(327,2,'2015-03-29 19:53:05','resource_update','modResource','2'),
	(328,2,'2015-03-29 20:04:31','resource_update','modResource','13'),
	(329,2,'2015-03-29 20:06:20','resource_update','modResource','1'),
	(330,2,'2015-03-29 20:06:35','resource_update','modResource','3'),
	(331,2,'2015-03-29 20:06:56','resource_update','modResource','8'),
	(332,2,'2015-03-29 20:07:44','resource_update','modResource','9'),
	(333,2,'2015-03-29 20:08:13','resource_update','modResource','10'),
	(334,2,'2015-03-29 20:08:48','resource_update','modResource','14'),
	(335,2,'2015-03-29 20:09:49','resource_update','modResource','22'),
	(336,2,'2015-03-29 20:10:46','resource_update','modResource','23'),
	(337,2,'2015-03-29 20:23:45','resource_update','modResource','23'),
	(338,2,'2015-03-29 20:23:58','resource_update','modResource','21'),
	(339,2,'2015-03-29 20:24:14','resource_update','modResource','26'),
	(340,2,'2015-03-29 20:26:16','resource_update','modResource','6'),
	(341,2,'2015-03-29 20:28:24','resource_update','modResource','5'),
	(342,2,'2015-03-29 20:31:38','resource_update','modResource','5'),
	(343,2,'2015-03-29 20:31:50','resource_update','modResource','7'),
	(344,2,'2015-03-29 20:37:48','resource_update','modResource','1'),
	(345,2,'2015-03-29 20:38:08','resource_update','modResource','2'),
	(346,2,'2015-03-29 20:38:24','resource_update','modResource','13'),
	(347,2,'2015-03-29 20:40:18','resource_update','modResource','3'),
	(348,2,'2015-03-29 20:40:36','resource_update','modResource','8'),
	(349,2,'2015-03-29 20:49:05','resource_update','modResource','8'),
	(350,2,'2015-03-29 20:49:19','resource_update','modResource','9'),
	(351,2,'2015-03-29 20:49:58','resource_update','modResource','10'),
	(352,2,'2015-03-29 20:50:26','resource_update','modResource','14'),
	(353,2,'2015-03-29 20:50:44','resource_update','modResource','22'),
	(354,2,'2015-03-29 20:50:55','resource_update','modResource','23'),
	(355,2,'2015-03-29 20:51:43','resource_update','modResource','21'),
	(356,2,'2015-03-29 20:51:58','resource_update','modResource','26'),
	(357,2,'2015-03-29 20:53:57','resource_update','modResource','7'),
	(358,2,'2015-03-30 17:35:10','resource_update','modResource','3'),
	(359,2,'2015-03-30 17:37:09','resource_update','modResource','3'),
	(360,2,'2015-03-30 18:00:28','resource_update','modResource','14'),
	(361,2,'2015-03-30 18:06:06','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(362,2,'2015-03-30 18:09:24','resource_update','modResource','10'),
	(363,2,'2015-03-30 19:26:47','resource_update','modResource','10'),
	(364,2,'2015-03-30 19:27:17','file_upload','','/home2/avidity1/public_html/heartland/'),
	(365,2,'2015-03-30 19:28:01','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(366,2,'2015-03-30 19:28:37','file_remove','','/home2/avidity1/public_html/heartland/rebates_promo_hero.jpg'),
	(367,2,'2015-03-30 19:28:51','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(368,2,'2015-03-30 19:29:26','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(369,2,'2015-03-30 19:29:42','resource_update','modResource','24'),
	(370,2,'2015-03-30 19:34:59','resource_update','modResource','12'),
	(371,2,'2015-03-30 19:35:18','resource_update','modResource','12'),
	(372,2,'2015-03-30 19:42:05','resource_update','modResource','12'),
	(373,2,'2015-03-30 19:42:26','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(374,2,'2015-03-30 19:42:36','resource_update','modResource','22'),
	(375,2,'2015-03-30 19:54:23','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(376,2,'2015-03-30 19:55:21','resource_update','modResource','22'),
	(377,2,'2015-03-30 19:57:09','resource_update','modResource','8'),
	(378,2,'2015-03-30 20:17:10','resource_update','modResource','22'),
	(379,2,'2015-03-30 20:19:02','resource_update','modResource','9'),
	(380,2,'2015-03-30 20:19:22','resource_update','modResource','14'),
	(381,2,'2015-03-30 20:19:42','resource_update','modResource','26'),
	(382,2,'2015-03-30 20:31:48','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(383,2,'2015-03-30 20:31:55','resource_update','modResource','23'),
	(384,2,'2015-03-30 20:34:03','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(385,2,'2015-03-30 20:36:08','resource_update','modResource','23'),
	(386,2,'2015-03-30 20:58:15','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(387,2,'2015-03-30 20:58:24','resource_update','modResource','21'),
	(388,2,'2015-03-30 21:01:57','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(389,2,'2015-03-30 21:02:04','resource_update','modResource','13'),
	(390,2,'2015-03-30 21:02:27','resource_update','modResource','13'),
	(391,2,'2015-03-30 21:03:36','resource_update','modResource','25'),
	(392,2,'2015-03-31 16:56:23','file_remove','','/home2/avidity1/public_html/heartland/resources/img/comfort_team_hero.jpg'),
	(393,2,'2015-03-31 16:56:37','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(394,2,'2015-03-31 16:56:49','resource_update','modResource','25'),
	(395,2,'2015-03-31 16:58:31','resource_sort','modResource','22'),
	(396,2,'2015-03-31 16:58:35','resource_sort','modResource','22'),
	(397,2,'2015-03-31 16:58:38','resource_sort','modResource','21'),
	(398,2,'2015-03-31 16:59:19','resource_update','modResource','22'),
	(399,2,'2015-03-31 17:00:30','resource_update','modResource','21'),
	(400,2,'2015-03-31 17:00:54','resource_sort','modResource','26'),
	(401,2,'2015-03-31 17:01:06','duplicate_resource','modDocument','27'),
	(402,2,'2015-03-31 17:01:15','resource_sort','modResource','27'),
	(403,2,'2015-03-31 17:01:29','duplicate_resource','modDocument','28'),
	(404,2,'2015-03-31 17:01:41','resource_sort','modResource','28'),
	(405,2,'2015-03-31 17:01:49','duplicate_resource','modDocument','29'),
	(406,2,'2015-03-31 17:01:57','resource_sort','modResource','29'),
	(407,2,'2015-03-31 17:02:13','duplicate_resource','modDocument','30'),
	(408,2,'2015-03-31 17:02:23','resource_sort','modResource','30'),
	(409,2,'2015-03-31 17:03:07','resource_update','modResource','22'),
	(410,2,'2015-03-31 17:03:18','resource_update','modResource','21'),
	(411,2,'2015-03-31 17:06:12','resource_update','modResource','23'),
	(412,2,'2015-03-31 17:07:55','resource_update','modResource','23'),
	(413,2,'2015-03-31 17:08:24','resource_update','modResource','23'),
	(414,2,'2015-03-31 17:08:49','resource_update','modResource','6'),
	(415,2,'2015-03-31 17:16:14','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(416,2,'2015-03-31 17:16:52','resource_update','modResource','6'),
	(417,2,'2015-03-31 17:24:01','file_upload','','/home2/avidity1/public_html/heartland/'),
	(418,2,'2015-03-31 17:24:16','file_remove','','/home2/avidity1/public_html/heartland/commercial_hero.jpg'),
	(419,2,'2015-03-31 17:24:30','file_remove','','/home2/avidity1/public_html/heartland/resources/img/commercial_hero.jpg'),
	(420,2,'2015-03-31 17:25:36','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(421,2,'2015-03-31 17:25:48','resource_update','modResource','23'),
	(422,2,'2015-03-31 17:31:12','file_remove','','/home2/avidity1/public_html/heartland/resources/img/commercial_hero.jpg'),
	(423,2,'2015-03-31 17:31:25','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(424,2,'2015-03-31 17:31:33','resource_update','modResource','23'),
	(425,2,'2015-03-31 17:35:34','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(426,2,'2015-03-31 17:35:43','resource_update','modResource','25'),
	(427,2,'2015-03-31 17:42:36','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(428,2,'2015-03-31 17:42:47','resource_update','modResource','2'),
	(429,2,'2015-03-31 18:28:48','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(430,2,'2015-03-31 18:28:48','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(431,2,'2015-03-31 18:28:49','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(432,2,'2015-03-31 18:29:34','resource_update','modResource','1'),
	(433,2,'2015-03-31 18:45:02','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(434,2,'2015-03-31 18:45:17','resource_update','modResource','3'),
	(435,2,'2015-03-31 18:51:44','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(436,2,'2015-03-31 18:51:52','resource_update','modResource','8'),
	(437,2,'2015-03-31 18:52:06','resource_update','modResource','27'),
	(438,2,'2015-03-31 18:53:11','file_remove','','/home2/avidity1/public_html/heartland/resources/img/heating_hero.jpg'),
	(439,2,'2015-03-31 18:53:21','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(440,2,'2015-03-31 18:53:28','resource_update','modResource','27'),
	(441,2,'2015-03-31 18:53:50','resource_update','modResource','8'),
	(442,2,'2015-03-31 18:56:10','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(443,2,'2015-03-31 18:58:02','resource_update','modResource','8'),
	(444,2,'2015-03-31 18:59:02','resource_update','modResource','27'),
	(445,2,'2015-03-31 19:04:37','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(446,2,'2015-03-31 19:05:23','resource_update','modResource','9'),
	(447,2,'2015-03-31 19:08:53','resource_update','modResource','28'),
	(448,2,'2015-03-31 19:09:07','resource_update','modResource','28'),
	(449,2,'2015-03-31 19:18:12','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(450,2,'2015-03-31 19:18:28','resource_update','modResource','10'),
	(451,2,'2015-03-31 19:18:48','resource_update','modResource','29'),
	(452,2,'2015-03-31 19:26:34','resource_update','modResource','29'),
	(453,2,'2015-03-31 19:32:17','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(454,2,'2015-03-31 19:32:28','resource_update','modResource','26'),
	(455,2,'2015-03-31 19:33:49','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(456,2,'2015-03-31 19:35:21','resource_update','modResource','26'),
	(457,2,'2015-03-31 19:40:43','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(458,2,'2015-03-31 19:41:34','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(459,2,'2015-03-31 19:41:45','resource_update','modResource','6'),
	(460,2,'2015-03-31 19:48:32','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(461,2,'2015-03-31 19:48:41','resource_update','modResource','5'),
	(462,2,'2015-03-31 19:54:23','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(463,2,'2015-03-31 19:54:29','resource_update','modResource','7'),
	(464,2,'2015-03-31 20:13:22','duplicate_resource','modDocument','31'),
	(465,2,'2015-03-31 20:13:32','resource_sort','modResource','31'),
	(466,2,'2015-03-31 20:13:42','duplicate_resource','modDocument','32'),
	(467,2,'2015-03-31 20:13:50','resource_sort','modResource','32'),
	(468,2,'2015-03-31 20:14:11','duplicate_resource','modDocument','33'),
	(469,2,'2015-03-31 20:14:17','resource_sort','modResource','33'),
	(470,2,'2015-03-31 20:14:24','duplicate_resource','modDocument','34'),
	(471,2,'2015-03-31 20:14:28','resource_sort','modResource','34'),
	(472,2,'2015-03-31 20:14:38','duplicate_resource','modDocument','35'),
	(473,2,'2015-03-31 20:14:44','resource_sort','modResource','35'),
	(474,2,'2015-03-31 20:14:56','duplicate_resource','modDocument','36'),
	(475,2,'2015-03-31 20:15:00','resource_sort','modResource','36'),
	(476,2,'2015-03-31 20:15:10','duplicate_resource','modDocument','37'),
	(477,2,'2015-03-31 20:15:15','resource_sort','modResource','37'),
	(478,2,'2015-03-31 20:15:30','duplicate_resource','modDocument','38'),
	(479,2,'2015-03-31 20:15:34','resource_sort','modResource','38'),
	(480,2,'2015-03-31 20:15:41','resource_update','modResource','27'),
	(481,1,'2015-04-01 04:11:45','template_update','modTemplate','4'),
	(482,1,'2015-04-01 04:12:35','template_update','modTemplate','6'),
	(483,1,'2015-04-01 04:13:11','template_create','modTemplate','8'),
	(484,1,'2015-04-01 04:14:03','tv_create','modTemplateVar','24'),
	(485,1,'2015-04-01 04:14:23','resource_update','modResource','22'),
	(486,1,'2015-04-01 04:14:43','tv_update','modTemplateVar','8'),
	(487,1,'2015-04-01 04:15:06','template_update','modTemplate','8'),
	(488,1,'2015-04-01 04:15:32','resource_update','modResource','22'),
	(489,1,'2015-04-01 04:15:51','resource_update','modResource','21'),
	(490,1,'2015-04-01 04:16:09','template_update','modTemplate','8'),
	(491,2,'2015-04-01 17:37:49','resource_update','modResource','5'),
	(492,2,'2015-04-01 17:51:50','resource_update','modResource','5'),
	(493,2,'2015-04-01 17:52:27','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(494,2,'2015-04-01 17:52:45','resource_update','modResource','9'),
	(495,2,'2015-04-01 17:53:04','resource_update','modResource','28'),
	(496,2,'2015-04-01 17:53:20','resource_update','modResource','34'),
	(497,2,'2015-04-01 18:00:58','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(498,2,'2015-04-01 18:01:08','resource_update','modResource','14'),
	(499,2,'2015-04-01 18:01:22','resource_update','modResource','30'),
	(500,2,'2015-04-01 18:01:37','resource_update','modResource','36'),
	(501,2,'2015-04-01 18:09:09','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(502,2,'2015-04-01 18:09:32','resource_update','modResource','1'),
	(503,2,'2015-04-01 21:15:03','resource_update','modResource','22'),
	(504,2,'2015-04-01 21:15:47','resource_update','modResource','21'),
	(505,2,'2015-04-02 21:40:58','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(506,2,'2015-04-02 21:41:10','resource_update','modResource','1'),
	(507,2,'2015-04-02 21:44:41','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(508,2,'2015-04-02 21:44:42','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(509,2,'2015-04-02 21:44:42','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(510,2,'2015-04-02 21:49:37','resource_update','modResource','2'),
	(511,2,'2015-04-02 21:51:51','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(512,2,'2015-04-02 21:51:52','file_upload','','/home2/avidity1/public_html/heartland/resources/img/'),
	(513,2,'2015-04-02 21:53:01','resource_update','modResource','21'),
	(514,2,'2015-04-02 21:53:51','resource_update','modResource','13'),
	(515,2,'2015-04-02 21:55:22','resource_update','modResource','21'),
	(516,2,'2015-04-02 21:56:11','resource_update','modResource','21'),
	(517,2,'2015-04-02 21:57:38','resource_update','modResource','13'),
	(518,1,'2015-04-08 07:10:28','resource_update','modResource','22'),
	(519,1,'2015-04-08 07:10:39','resource_update','modResource','21'),
	(520,1,'2015-04-14 10:00:09','setting_update','modSystemSetting','mail_smtp_hosts'),
	(521,1,'2015-04-14 10:00:13','setting_update','modSystemSetting','mail_smtp_auth'),
	(522,1,'2015-04-14 10:00:18','setting_update','modSystemSetting','mail_smtp_port'),
	(523,1,'2015-04-14 10:01:21','setting_update','modSystemSetting','mail_smtp_pass'),
	(524,1,'2015-04-14 10:01:30','setting_update','modSystemSetting','mail_smtp_prefix'),
	(525,1,'2015-04-14 10:01:30','setting_update','modSystemSetting','mail_smtp_single_to'),
	(526,1,'2015-04-14 10:01:33','setting_update','modSystemSetting','mail_smtp_user'),
	(527,1,'2015-04-14 10:01:38','setting_update','modSystemSetting','mail_use_smtp'),
	(528,1,'2015-04-14 10:03:44','setting_update','modSystemSetting','primary_email'),
	(529,1,'2015-04-14 10:04:07','chunk_update','modChunk','2'),
	(530,1,'2015-04-14 10:05:03','setting_update','modSystemSetting','primary_email'),
	(531,1,'2015-04-14 10:18:23','chunk_duplicate','modChunk','14'),
	(532,1,'2015-04-14 12:37:03','setting_update','modSystemSetting','mail_smtp_hosts'),
	(533,1,'2015-04-14 12:37:04','setting_update','modSystemSetting','mail_smtp_keepalive'),
	(534,1,'2015-04-14 12:37:30','setting_update','modSystemSetting','mail_smtp_user'),
	(535,1,'2015-04-14 12:38:58','chunk_update','modChunk','10'),
	(536,1,'2015-04-14 12:40:32','chunk_update','modChunk','10'),
	(537,1,'2015-04-14 12:41:33','chunk_update','modChunk','10'),
	(538,1,'2015-04-14 12:42:42','template_update','modTemplate','5'),
	(539,1,'2015-04-14 12:43:13','template_update','modTemplate','5'),
	(540,1,'2015-04-14 12:43:23','chunk_update','modChunk','10'),
	(541,1,'2015-04-14 12:47:59','setting_update','modSystemSetting','mail_smtp_hosts'),
	(542,1,'2015-04-14 12:49:27','setting_update','modSystemSetting','mail_smtp_user'),
	(543,1,'2015-04-14 12:49:27','setting_update','modSystemSetting','mail_use_smtp'),
	(544,1,'2015-04-14 12:52:59','setting_update','modSystemSetting','mail_smtp_prefix'),
	(545,1,'2015-04-14 12:54:44','setting_update','modSystemSetting','mail_smtp_hosts'),
	(546,1,'2015-04-14 12:54:45','setting_update','modSystemSetting','mail_smtp_keepalive'),
	(547,1,'2015-04-14 12:54:54','setting_update','modSystemSetting','mail_smtp_single_to'),
	(548,1,'2015-04-14 12:55:01','setting_update','modSystemSetting','mail_smtp_user'),
	(549,1,'2015-04-14 12:56:02','template_update','modTemplate','5'),
	(550,1,'2015-04-14 12:59:23','setting_update','modSystemSetting','mail_smtp_auth'),
	(551,1,'2015-04-14 15:24:57','setting_update','modSystemSetting','mail_smtp_hosts'),
	(552,1,'2015-04-14 15:24:57','setting_update','modSystemSetting','mail_smtp_keepalive'),
	(553,1,'2015-04-14 15:25:00','setting_update','modSystemSetting','mail_smtp_single_to'),
	(554,1,'2015-04-14 15:25:03','setting_update','modSystemSetting','mail_smtp_prefix'),
	(555,1,'2015-04-14 15:25:12','setting_update','modSystemSetting','mail_smtp_user'),
	(556,1,'2015-04-14 15:25:13','setting_update','modSystemSetting','mail_use_smtp'),
	(557,1,'2015-04-14 17:02:49','template_update','modTemplate','5'),
	(558,1,'2015-04-14 17:05:24','chunk_update','modChunk','2'),
	(559,1,'2015-04-14 17:06:12','chunk_update','modChunk','14'),
	(560,1,'2015-04-14 17:06:33','chunk_update','modChunk','10'),
	(561,1,'2015-04-15 06:13:27','chunk_update','modChunk','14'),
	(562,1,'2015-04-15 06:36:39','resource_create','modDocument','39'),
	(563,1,'2015-04-15 06:37:08','resource_update','modResource','39'),
	(564,1,'2015-04-15 06:37:23','resource_update','modResource','39'),
	(565,1,'2015-04-15 06:37:37','resource_update','modResource','39'),
	(566,1,'2015-04-15 06:40:23','resource_update','modResource','39'),
	(567,1,'2015-04-15 07:00:40','setting_update','modSystemSetting','primary_email'),
	(568,1,'2015-04-15 07:01:27','setting_create','modSystemSetting','google_analytics'),
	(569,1,'2015-04-15 07:02:47','setting_update','modSystemSetting','google_analytics'),
	(570,1,'2015-04-15 07:09:20','dashboard_update','modDashboard','1'),
	(571,1,'2015-04-15 07:11:06','dashboard_update','modDashboard','1'),
	(572,1,'2015-04-15 15:31:39','setting_update','modSystemSetting','primary_email'),
	(573,1,'2015-04-23 17:03:22','user_create','modUser','3'),
	(574,1,'2015-04-23 17:03:45','user_update','modUser','3'),
	(575,3,'2015-04-24 11:14:14','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/heartlandheating.com/resources/img/'),
	(576,3,'2015-04-24 11:14:15','file_upload','','/var/www/vhosts/beardedgingerdesigns.com/heartlandheating.com/resources/img/'),
	(577,3,'2015-04-24 11:17:05','resource_update','modResource','2'),
	(578,3,'2015-04-24 11:23:22','resource_update','modResource','2'),
	(579,3,'2015-04-24 11:23:37','resource_update','modResource','2'),
	(580,3,'2015-04-24 11:24:11','resource_update','modResource','2'),
	(581,3,'2015-04-24 11:24:34','resource_update','modResource','2'),
	(582,3,'2015-04-24 11:35:36','resource_create','modDocument','40'),
	(583,3,'2015-04-24 11:35:59','resource_sort','modResource','40'),
	(584,3,'2015-04-24 11:36:11','resource_sort','modResource','2'),
	(585,3,'2015-04-24 11:36:17','resource_sort','modResource','2'),
	(586,3,'2015-04-24 11:36:32','resource_sort','modResource','2'),
	(587,3,'2015-04-24 11:36:38','resource_update','modResource','40'),
	(588,3,'2015-04-24 11:37:27','resource_update','modResource','40'),
	(589,3,'2015-04-24 11:38:42','delete_resource','modDocument','40'),
	(590,3,'2015-04-24 11:38:47','resource_update','modResource','40'),
	(591,3,'2015-04-24 11:39:04','delete_resource','modDocument','40'),
	(592,1,'2015-08-13 11:10:11','chunk_update','modChunk','10'),
	(593,1,'2015-08-13 11:10:19','chunk_update','modChunk','10'),
	(594,1,'2015-08-13 11:11:26','chunk_update','modChunk','12');

/*!40000 ALTER TABLE `modx_manager_log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_media_sources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_media_sources`;

CREATE TABLE `modx_media_sources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `class_key` varchar(100) NOT NULL DEFAULT 'sources.modFileMediaSource',
  `properties` mediumtext,
  `is_stream` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `class_key` (`class_key`),
  KEY `is_stream` (`is_stream`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_media_sources` WRITE;
/*!40000 ALTER TABLE `modx_media_sources` DISABLE KEYS */;

INSERT INTO `modx_media_sources` (`id`, `name`, `description`, `class_key`, `properties`, `is_stream`)
VALUES
	(1,'Filesystem','','sources.modFileMediaSource','a:0:{}',1),
	(2,'Client Files','','sources.modFileMediaSource','a:1:{s:8:\"basePath\";a:6:{s:4:\"name\";s:8:\"basePath\";s:4:\"desc\";s:23:\"prop_file.basePath_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";a:0:{}s:5:\"value\";s:7:\"Images/\";s:7:\"lexicon\";s:11:\"core:source\";}}',1);

/*!40000 ALTER TABLE `modx_media_sources` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_media_sources_contexts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_media_sources_contexts`;

CREATE TABLE `modx_media_sources_contexts` (
  `source` int(11) NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT 'web',
  PRIMARY KEY (`source`,`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_media_sources_elements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_media_sources_elements`;

CREATE TABLE `modx_media_sources_elements` (
  `source` int(11) unsigned NOT NULL DEFAULT '0',
  `object_class` varchar(100) NOT NULL DEFAULT 'modTemplateVar',
  `object` int(11) unsigned NOT NULL DEFAULT '0',
  `context_key` varchar(100) NOT NULL DEFAULT 'web',
  PRIMARY KEY (`source`,`object`,`object_class`,`context_key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_media_sources_elements` WRITE;
/*!40000 ALTER TABLE `modx_media_sources_elements` DISABLE KEYS */;

INSERT INTO `modx_media_sources_elements` (`source`, `object_class`, `object`, `context_key`)
VALUES
	(1,'modTemplateVar',1,'web'),
	(1,'modTemplateVar',2,'web'),
	(1,'modTemplateVar',3,'web'),
	(1,'modTemplateVar',4,'web'),
	(1,'modTemplateVar',5,'web'),
	(1,'modTemplateVar',6,'web'),
	(1,'modTemplateVar',7,'web'),
	(1,'modTemplateVar',8,'web'),
	(1,'modTemplateVar',9,'web'),
	(1,'modTemplateVar',10,'web'),
	(1,'modTemplateVar',11,'web'),
	(1,'modTemplateVar',12,'web'),
	(1,'modTemplateVar',13,'web'),
	(1,'modTemplateVar',14,'web'),
	(1,'modTemplateVar',15,'web'),
	(1,'modTemplateVar',16,'web'),
	(1,'modTemplateVar',17,'web'),
	(1,'modTemplateVar',18,'web'),
	(1,'modTemplateVar',19,'web'),
	(1,'modTemplateVar',20,'web'),
	(1,'modTemplateVar',21,'web'),
	(1,'modTemplateVar',22,'web'),
	(1,'modTemplateVar',23,'web'),
	(1,'modTemplateVar',24,'web');

/*!40000 ALTER TABLE `modx_media_sources_elements` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_member_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_member_groups`;

CREATE TABLE `modx_member_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_group` int(10) unsigned NOT NULL DEFAULT '0',
  `member` int(10) unsigned NOT NULL DEFAULT '0',
  `role` int(10) unsigned NOT NULL DEFAULT '1',
  `rank` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `role` (`role`),
  KEY `rank` (`rank`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_member_groups` WRITE;
/*!40000 ALTER TABLE `modx_member_groups` DISABLE KEYS */;

INSERT INTO `modx_member_groups` (`id`, `user_group`, `member`, `role`, `rank`)
VALUES
	(1,1,1,2,0),
	(3,1,2,2,0),
	(5,1,3,2,0);

/*!40000 ALTER TABLE `modx_member_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_membergroup_names
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_membergroup_names`;

CREATE TABLE `modx_membergroup_names` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `parent` int(10) unsigned NOT NULL DEFAULT '0',
  `rank` int(10) unsigned NOT NULL DEFAULT '0',
  `dashboard` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `parent` (`parent`),
  KEY `rank` (`rank`),
  KEY `dashboard` (`dashboard`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_membergroup_names` WRITE;
/*!40000 ALTER TABLE `modx_membergroup_names` DISABLE KEYS */;

INSERT INTO `modx_membergroup_names` (`id`, `name`, `description`, `parent`, `rank`, `dashboard`)
VALUES
	(1,'Administrator',NULL,0,0,1),
	(2,'Client User','',0,0,1);

/*!40000 ALTER TABLE `modx_membergroup_names` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_menus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_menus`;

CREATE TABLE `modx_menus` (
  `text` varchar(255) NOT NULL DEFAULT '',
  `parent` varchar(255) NOT NULL DEFAULT '',
  `action` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `icon` varchar(255) NOT NULL DEFAULT '',
  `menuindex` int(11) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `handler` text NOT NULL,
  `permissions` text NOT NULL,
  `namespace` varchar(100) NOT NULL DEFAULT 'core',
  PRIMARY KEY (`text`),
  KEY `parent` (`parent`),
  KEY `action` (`action`),
  KEY `namespace` (`namespace`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_menus` WRITE;
/*!40000 ALTER TABLE `modx_menus` DISABLE KEYS */;

INSERT INTO `modx_menus` (`text`, `parent`, `action`, `description`, `icon`, `menuindex`, `params`, `handler`, `permissions`, `namespace`)
VALUES
	('topnav','','','topnav_desc','',0,'','','','core'),
	('site','topnav','','','',0,'','','menu_site','core'),
	('new_resource','site','resource/create','new_resource_desc','',0,'','','new_document','core'),
	('preview','site','','preview_desc','',4,'','MODx.preview(); return false;','','core'),
	('import_site','site','system/import/html','import_site_desc','',5,'','','import_static','core'),
	('import_resources','site','system/import','import_resources_desc','',6,'','','import_static','core'),
	('resource_groups','site','security/resourcegroup','resource_groups_desc','',7,'','','access_permissions','core'),
	('content_types','site','system/contenttype','content_types_desc','',8,'','','content_types','core'),
	('media','topnav','','media_desc','',1,'','','file_manager','core'),
	('file_browser','media','media/browser','file_browser_desc','',0,'','','file_manager','core'),
	('sources','media','source','sources_desc','',1,'','','sources','core'),
	('components','topnav','','','',2,'','','components','core'),
	('installer','components','workspaces','installer_desc','',0,'','','packages','core'),
	('manage','topnav','','','',3,'','','menu_tools','core'),
	('users','manage','security/user','user_management_desc','',0,'','','view_user','core'),
	('refresh_site','manage','','refresh_site_desc','',1,'','MODx.clearCache(); return false;','empty_cache','core'),
	('remove_locks','manage','','remove_locks_desc','',2,'','\nMODx.msg.confirm({\n    title: _(\'remove_locks\')\n    ,text: _(\'confirm_remove_locks\')\n    ,url: MODx.config.connectors_url\n    ,params: {\n        action: \'system/remove_locks\'\n    }\n    ,listeners: {\n        \'success\': {fn:function() {\n            var tree = Ext.getCmp(\"modx-resource-tree\");\n            if (tree && tree.rendered) {\n                tree.refresh();\n            }\n         },scope:this}\n    }\n});','remove_locks','core'),
	('flush_access','manage','','flush_access_desc','',3,'','MODx.msg.confirm({\n    title: _(\'flush_access\')\n    ,text: _(\'flush_access_confirm\')\n    ,url: MODx.config.connector_url\n    ,params: {\n        action: \'security/access/flush\'\n    }\n    ,listeners: {\n        \'success\': {fn:function() { location.href = \'./\'; },scope:this}\n    }\n});','access_permissions','core'),
	('flush_sessions','manage','','flush_sessions_desc','',4,'','MODx.msg.confirm({\n    title: _(\'flush_sessions\')\n    ,text: _(\'flush_sessions_confirm\')\n    ,url: MODx.config.connector_url\n    ,params: {\n        action: \'security/flush\'\n    }\n    ,listeners: {\n        \'success\': {fn:function() { location.href = \'./\'; },scope:this}\n    }\n});','flush_sessions','core'),
	('reports','manage','','reports_desc','',5,'','','menu_reports','core'),
	('site_schedule','reports','resource/site_schedule','site_schedule_desc','',0,'','','view_document','core'),
	('view_logging','reports','system/logs','view_logging_desc','',1,'','','logs','core'),
	('eventlog_viewer','reports','system/event','eventlog_viewer_desc','',2,'','','view_eventlog','core'),
	('view_sysinfo','reports','system/info','view_sysinfo_desc','',3,'','','view_sysinfo','core'),
	('usernav','','','usernav_desc','',0,'','','','core'),
	('user','usernav','','','<span id=\"user-avatar\">{$userImage}</span> <span id=\"user-username\">{$username}</span>',5,'','','menu_user','core'),
	('profile','user','security/profile','profile_desc','',0,'','','change_profile','core'),
	('messages','user','security/message','messages_desc','',1,'','','messages','core'),
	('logout','user','','logout_desc','',2,'','MODx.logout(); return false;','logout','core'),
	('admin','usernav','','','<i class=\"icon-gear icon icon-large\"></i>',6,'','','settings','core'),
	('system_settings','admin','system/settings','system_settings_desc','',0,'','','settings','core'),
	('bespoke_manager','admin','security/forms','bespoke_manager_desc','',1,'','','customize_forms','core'),
	('dashboards','admin','system/dashboards','dashboards_desc','',2,'','','dashboards','core'),
	('contexts','admin','context','contexts_desc','',3,'','','view_contexts','core'),
	('edit_menu','admin','system/action','edit_menu_desc','',4,'','','actions','core'),
	('acls','admin','security/permission','acls_desc','',5,'','','access_permissions','core'),
	('propertysets','admin','element/propertyset','propertysets_desc','',6,'','','property_sets','core'),
	('lexicon_management','admin','workspaces/lexicon','lexicon_management_desc','',7,'','','lexicons','core'),
	('namespaces','admin','workspaces/namespace','namespaces_desc','',8,'','','namespaces','core'),
	('about','usernav','help','','<i class=\"icon-question-circle icon icon-large\"></i>',7,'','','','core'),
	('formit','components','1','formit.menu_desc','images/icons/plugin.gif',0,'','','','core');

/*!40000 ALTER TABLE `modx_menus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_namespaces
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_namespaces`;

CREATE TABLE `modx_namespaces` (
  `name` varchar(40) NOT NULL DEFAULT '',
  `path` text,
  `assets_path` text,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_namespaces` WRITE;
/*!40000 ALTER TABLE `modx_namespaces` DISABLE KEYS */;

INSERT INTO `modx_namespaces` (`name`, `path`, `assets_path`)
VALUES
	('core','{core_path}','{assets_path}'),
	('formit','{core_path}components/formit/',''),
	('tinymce','{core_path}components/tinymce/',NULL),
	('wayfinder','{core_path}components/wayfinder/',''),
	('sendgrid','{core_path}components/sendgrid/','{assets_path}components/grid/'),
	('quickemail','{core_path}components/quickemail/',''),
	('analytics','{core_path}components/analytics/','');

/*!40000 ALTER TABLE `modx_namespaces` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_property_set
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_property_set`;

CREATE TABLE `modx_property_set` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `category` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '',
  `properties` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category` (`category`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_register_messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_register_messages`;

CREATE TABLE `modx_register_messages` (
  `topic` int(10) unsigned NOT NULL,
  `id` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `valid` datetime NOT NULL,
  `accessed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `accesses` int(10) unsigned NOT NULL DEFAULT '0',
  `expires` int(20) NOT NULL DEFAULT '0',
  `payload` mediumtext NOT NULL,
  `kill` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`topic`,`id`),
  KEY `created` (`created`),
  KEY `valid` (`valid`),
  KEY `accessed` (`accessed`),
  KEY `accesses` (`accesses`),
  KEY `expires` (`expires`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_register_queues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_register_queues`;

CREATE TABLE `modx_register_queues` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `options` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_register_queues` WRITE;
/*!40000 ALTER TABLE `modx_register_queues` DISABLE KEYS */;

INSERT INTO `modx_register_queues` (`id`, `name`, `options`)
VALUES
	(1,'locks','a:1:{s:9:\"directory\";s:5:\"locks\";}'),
	(2,'resource_reload','a:1:{s:9:\"directory\";s:15:\"resource_reload\";}');

/*!40000 ALTER TABLE `modx_register_queues` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_register_topics
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_register_topics`;

CREATE TABLE `modx_register_topics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `queue` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `options` mediumtext,
  PRIMARY KEY (`id`),
  KEY `queue` (`queue`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_register_topics` WRITE;
/*!40000 ALTER TABLE `modx_register_topics` DISABLE KEYS */;

INSERT INTO `modx_register_topics` (`id`, `queue`, `name`, `created`, `updated`, `options`)
VALUES
	(1,1,'/resource/','2014-10-06 01:40:23',NULL,NULL),
	(2,2,'/resourcereload/','2014-12-08 03:58:54',NULL,NULL);

/*!40000 ALTER TABLE `modx_register_topics` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_session
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_session`;

CREATE TABLE `modx_session` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `access` int(20) unsigned NOT NULL,
  `data` mediumtext,
  PRIMARY KEY (`id`),
  KEY `access` (`access`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_session` WRITE;
/*!40000 ALTER TABLE `modx_session` DISABLE KEYS */;

INSERT INTO `modx_session` (`id`, `access`, `data`)
VALUES
	('hj3vi3pjfe8v18hhp5dceml055',1439974467,'modx.user.contextTokens|a:0:{}'),
	('qo1g1d60m8r5c9qg0n2gjrhkb7',1439971855,'modx.user.contextTokens|a:0:{}'),
	('b97le79pfk2iiam0ij2g9ln3f1',1439968592,'modx.user.contextTokens|a:0:{}'),
	('pardjvltg3fh483f1d93tp9lv1',1439968592,'modx.user.contextTokens|a:0:{}'),
	('loj4nslu0ud9u0p8sh9aiapll2',1439966581,'modx.user.contextTokens|a:0:{}'),
	('i32607ikr0kdo7fm663lb28cg5',1439965726,'modx.user.contextTokens|a:0:{}'),
	('uhe1mbk5l2bhss2btsfbg0ne66',1439965432,'modx.user.contextTokens|a:0:{}'),
	('rfenpm66b3m6j4lfsj622mbl24',1439964804,'modx.user.contextTokens|a:0:{}'),
	('5i0oll63go6fjgo6odhtffeig4',1439964804,'modx.user.contextTokens|a:0:{}'),
	('mq19ur6okk7h28aee25ctutpp5',1439961617,'modx.user.contextTokens|a:0:{}'),
	('nldghbks9l413e0dolhenbjhm4',1439958382,'modx.user.contextTokens|a:0:{}'),
	('moqia2untrav4a53j402li1qe3',1439958321,'modx.user.contextTokens|a:0:{}'),
	('e4696drt193sdcnpfd05vtvll7',1439956874,'modx.user.contextTokens|a:0:{}'),
	('iuele8v0mv5cfoq0usqelic741',1439956394,'modx.user.contextTokens|a:0:{}'),
	('559bil7nkbigrcsltcl5touvq0',1439955939,'modx.user.contextTokens|a:0:{}'),
	('kfq25oobsnt907b1hdtv2unsv0',1439955131,'modx.user.contextTokens|a:0:{}'),
	('opq9gk8eastnj4jajisa3dqne3',1439954627,'modx.user.contextTokens|a:0:{}'),
	('s4ldiukah210sks47atm6fa4c5',1439954475,'modx.user.contextTokens|a:0:{}'),
	('hjikpelu78r6kcn7grpmrl84h7',1439953590,'modx.user.contextTokens|a:0:{}'),
	('oqt22j7aj83a9lnphb8o5ghjn7',1439951634,'modx.user.contextTokens|a:0:{}'),
	('72u3cp330i8p4ef1honrsdhee7',1439950630,'modx.user.contextTokens|a:0:{}'),
	('m0h07n4u30c009iqjbkh0i6ok0',1439950024,'modx.user.contextTokens|a:0:{}'),
	('sjk9dipot0daof88oeg7krv4q2',1439949677,'modx.user.contextTokens|a:0:{}'),
	('ielcjq4ntdbmld5psjabe5vji1',1439948304,'modx.user.contextTokens|a:0:{}'),
	('i6uqj9acihke84dmg57uah1k94',1439947964,'modx.user.contextTokens|a:0:{}'),
	('phcba2lj1hjc0sttqj1rif7ld1',1439942475,'modx.user.contextTokens|a:0:{}'),
	('lqg58abahg97v5nn8215tgkjn0',1439942474,'modx.user.contextTokens|a:0:{}'),
	('qmbss9fkt27hfuo97eva1kr795',1439940036,'modx.user.contextTokens|a:0:{}'),
	('7m3j8lchg8509ke7ag37es9pp3',1439940036,'modx.user.contextTokens|a:0:{}'),
	('ao0gr0gbehnl1er9j65prv9uj4',1439940036,'modx.user.contextTokens|a:0:{}'),
	('27g3a1so9q740ccpb9i36i75t6',1439940035,'modx.user.contextTokens|a:0:{}'),
	('k6rqfbbsjuli54r2og6jir3c23',1439938641,'modx.user.contextTokens|a:0:{}'),
	('dpluc0ijcaenhicv4d5bq3r4j4',1439937384,'modx.user.contextTokens|a:0:{}'),
	('i3u4t4tcudh82a3b78aiel7m73',1439937378,'modx.user.contextTokens|a:0:{}'),
	('h4094qq1pqhd6eqe64gmapkpd2',1439937000,'modx.user.contextTokens|a:0:{}'),
	('usfedjgtg04a3dq84fm6fnr4h1',1439936931,'modx.user.contextTokens|a:0:{}'),
	('qdmhj2pmqriqp2g11vfb1eqso5',1439936588,'modx.user.contextTokens|a:0:{}'),
	('me1ecddlakm90eqeaejvkrfs14',1439936243,'modx.user.contextTokens|a:0:{}'),
	('u17upfb29u8j495rtjg054r364',1439932933,'modx.user.contextTokens|a:0:{}'),
	('hnis5r6flj7gutq6igh3hvjeb4',1439931801,'modx.user.contextTokens|a:0:{}'),
	('ad7ujrfiruo321c6he9iip63d2',1439929925,'modx.user.contextTokens|a:0:{}'),
	('jkl2om2n62070b6p92r7up8ul0',1439928817,'modx.user.contextTokens|a:0:{}'),
	('9e85nivtir1v93bga2mat31dc2',1439925942,'modx.user.contextTokens|a:0:{}'),
	('f1tauoul687tmvoq4hhptajva5',1439925477,'modx.user.contextTokens|a:0:{}'),
	('pboti43oubjsb620msq8i0m5i5',1439925408,'modx.user.contextTokens|a:0:{}'),
	('nkljatcfbk3i4dd21lnqof2rq6',1439924795,'modx.user.contextTokens|a:0:{}'),
	('kcnt9se41gre2pr5co3eov6gg2',1439923828,'modx.user.contextTokens|a:0:{}'),
	('ah6kh48r3ouq2uhko1m50t87l3',1439923827,'modx.user.contextTokens|a:0:{}'),
	('l6sae5b3tgjot0d78gko45d4q2',1439923682,'modx.user.contextTokens|a:0:{}'),
	('6tv1qt6baveicfss7v8conmg07',1439923678,'modx.user.contextTokens|a:0:{}'),
	('iho2lvn387lhlffo298nknfjd7',1439923672,'modx.user.contextTokens|a:0:{}'),
	('serlspadfingtuhrsgtnji4i70',1439923648,'modx.user.contextTokens|a:0:{}'),
	('mu86mknfcqfapm13gkdo27bec0',1439923629,'modx.user.contextTokens|a:0:{}'),
	('bb5s63afl1fbbjcr3a280b2ij5',1439923628,'modx.user.contextTokens|a:0:{}'),
	('a3ekc6hgcgkedv32uf1gd47521',1439922518,'modx.user.contextTokens|a:0:{}'),
	('ku8ntmmgp3q2dn5roggvi7p735',1439921675,'modx.user.contextTokens|a:0:{}'),
	('1vrg8df8k5kgbkc8e01c9sii06',1439919905,'modx.user.contextTokens|a:0:{}'),
	('lokdnrqs0bl32i11620ngb4lk6',1439919847,'modx.user.contextTokens|a:0:{}'),
	('bi5lkts97gqlg8qoi23m78orc5',1439919765,'modx.user.contextTokens|a:0:{}'),
	('h9649o852oogq9csksmo55rhl0',1439919765,'modx.user.contextTokens|a:0:{}'),
	('0asviplufrp6cah36gc1nibhk1',1439919706,'modx.user.contextTokens|a:0:{}'),
	('g6slvkflhrlssep6qk35rqh774',1439917892,'modx.user.contextTokens|a:0:{}'),
	('bck6c6hug2cu6anlkpso0960q5',1439916440,'modx.user.contextTokens|a:0:{}'),
	('rq63rjb0jahc788rtaepr8rit5',1439914972,'modx.user.contextTokens|a:0:{}'),
	('deel87gfnttmmdbbftp4lpfih2',1439914426,'modx.user.contextTokens|a:0:{}'),
	('ve1sp2fieasuh752858a3aabq4',1439914426,'modx.user.contextTokens|a:0:{}'),
	('qkrhh5cjolmati0ddtqd4b5e35',1439914425,'modx.user.contextTokens|a:0:{}'),
	('t28c2u12bgg865e1kffkd3cjn5',1439914131,'modx.user.contextTokens|a:0:{}'),
	('qahp0o0ncch8h7q6mpjl4f0697',1439913862,'modx.user.contextTokens|a:0:{}'),
	('vmt00hgvr7hjpcdrsqao5r4re2',1439913857,'modx.user.contextTokens|a:0:{}'),
	('lid42bgqhgo47qrdhaphbp0rm0',1439913857,'modx.user.contextTokens|a:0:{}'),
	('70961nchkg1jcrfdm08lkp4lg6',1439913856,'modx.user.contextTokens|a:0:{}'),
	('07tqt5cs1ejki5omd37o3a2pa6',1439913856,'modx.user.contextTokens|a:0:{}'),
	('jua4mmltrkpvq11u3ldpmeof83',1439913856,'modx.user.contextTokens|a:0:{}'),
	('6sa37jhnnss2une2gghl0fqvo4',1439913080,'modx.user.contextTokens|a:0:{}'),
	('2saefrgvl7nih91s05l43mlt17',1439913078,'modx.user.contextTokens|a:0:{}'),
	('j6qe933ah2pc0aei19pjlulvo1',1439912961,'modx.user.contextTokens|a:0:{}'),
	('cvlen9k6qk4jhkvvveqvejc9i4',1439912687,'modx.user.contextTokens|a:0:{}'),
	('93cu6gq1hj8gpo5humdu4q5957',1439911963,'modx.user.contextTokens|a:0:{}'),
	('tbggl3vjkiu04k7s1s409vqlq3',1439911801,'modx.user.contextTokens|a:0:{}'),
	('j0d10s803mnh973pchnpr9lb93',1439911800,'modx.user.contextTokens|a:0:{}'),
	('h4qv3q6u5m1v19k9hvb66mdoi0',1439911531,'modx.user.contextTokens|a:0:{}'),
	('fvqljutsi5v591vs3hij5htcf0',1439910799,'modx.user.contextTokens|a:0:{}'),
	('q13feph5r8uesindu2k3s1rsg7',1439910474,'modx.user.contextTokens|a:0:{}'),
	('44pke0ivd2u8lr3muae73vtjc1',1439910066,'modx.user.contextTokens|a:0:{}'),
	('mftto3ev94flociuntun7osrv4',1439909937,'modx.user.contextTokens|a:0:{}'),
	('26vr978c5m7od8no1gpipbbu35',1439909219,'modx.user.contextTokens|a:0:{}'),
	('rv7260l4i52ngqgpola5fg5bn4',1439909050,'modx.user.contextTokens|a:0:{}'),
	('siequepg3u8klhdakrgpkqcll3',1439908790,'modx.user.contextTokens|a:0:{}'),
	('lbppah6hnnvelpostn6j7gdqt0',1439908697,'modx.user.contextTokens|a:0:{}'),
	('tuohh7u7ffs5hu2gaotjl0q232',1439908507,'modx.user.contextTokens|a:0:{}'),
	('8lmqd9fccsmc5o87icc229onb3',1439908425,'modx.user.contextTokens|a:0:{}'),
	('2i7c0g63r85mh5lhsckgdmib97',1439908377,'modx.user.contextTokens|a:0:{}'),
	('pcetae3pcnof9h656a15tp3054',1439908376,'modx.user.contextTokens|a:0:{}'),
	('3q8sh9bjd7tm2hg4dlvrgdqqi4',1439908371,'modx.user.contextTokens|a:0:{}'),
	('4qcaimg6q5uvetgmsftrop74p4',1439908343,'modx.user.contextTokens|a:0:{}'),
	('igduci06pi2f9qkp0fu4j3nir6',1439908192,'modx.user.contextTokens|a:0:{}'),
	('bbtp5p0dm50235r7cb8i9vaba0',1439907812,'modx.user.contextTokens|a:0:{}'),
	('kl8cdufnpqpnui76shkfvodn82',1439907777,'modx.user.contextTokens|a:0:{}'),
	('glpso60jtsugft6170so71b313',1439907762,'modx.user.contextTokens|a:0:{}'),
	('tgagg323n8mdjtlv4hl6m28i92',1439907758,'modx.user.contextTokens|a:0:{}'),
	('a2v9d5lrlqgfhqd3nu63s9bes6',1439907741,'modx.user.contextTokens|a:0:{}'),
	('vgdt5anedkog5270c0sijbfqg6',1439907738,'modx.user.contextTokens|a:0:{}'),
	('ke5ks3f2sagjpg2brs4i7ssrq5',1439907719,'modx.user.contextTokens|a:0:{}'),
	('mhd8a8hjjefvr37cdo0e7lhcc3',1439907567,'modx.user.contextTokens|a:0:{}'),
	('foicvscrq2ssii55vbkmh3o121',1439907409,'modx.user.contextTokens|a:0:{}'),
	('rim3rpmpb1vocj9ks014liu5h0',1439904823,'modx.user.contextTokens|a:0:{}'),
	('5bel0h4fe6vvaas1mlm87ib033',1439904602,'modx.user.contextTokens|a:0:{}'),
	('4it6abk30pkc6d0chgn4bg1mq5',1439904149,'modx.user.contextTokens|a:0:{}'),
	('ehcsmom25j8e5bg86n8f8j5m41',1439901196,'modx.user.contextTokens|a:0:{}'),
	('4kd90osunsiqrk1ctvhlmumd66',1439901135,'modx.user.contextTokens|a:0:{}'),
	('tn39tgs678e1u40099gsac7ml1',1439899684,'modx.user.contextTokens|a:0:{}'),
	('mqv5mpfkq2certteig5tubpr10',1439898530,'modx.user.contextTokens|a:0:{}'),
	('3insbfq9spgcgd82gq59s5h3k4',1439898530,'modx.user.contextTokens|a:0:{}'),
	('fsto84ii2sl8bej4p7hin1u4t5',1439895288,'modx.user.contextTokens|a:0:{}'),
	('m7ci4e5ovhbnkpveifl58alvn6',1439895264,'modx.user.contextTokens|a:0:{}'),
	('595m5iuk2jr9mq9vpvh7807ul4',1439894998,'modx.user.contextTokens|a:0:{}'),
	('sheij340srg2abjr4pk943r4h3',1439893798,'modx.user.contextTokens|a:0:{}'),
	('57up5o9meq3t9b1bk98um13704',1439893260,'modx.user.contextTokens|a:0:{}'),
	('bfm6h8qrselt6ttqh34drr6o35',1439889892,'modx.user.contextTokens|a:0:{}'),
	('11u6387rha1is0lb7kv1qucfd3',1439888695,'modx.user.contextTokens|a:0:{}'),
	('i0kabog5c9lttml6dl4smtged7',1439885536,'modx.user.contextTokens|a:0:{}'),
	('dkeljd1lfi1sce3f5stmakr1u2',1439885515,'modx.user.contextTokens|a:0:{}'),
	('duq81mctvj5gjrluid2ha2dmr7',1439885514,'modx.user.contextTokens|a:0:{}'),
	('6jbnl5dn4j927j6uau54b0pb20',1439879932,'modx.user.contextTokens|a:0:{}'),
	('qdfo1rt6cfj3r23tk0v865s373',1439877871,'modx.user.contextTokens|a:0:{}'),
	('ap65i5v7a67jqqhhjg6l6lahi0',1439875656,'modx.user.contextTokens|a:0:{}'),
	('3cmsjausi7ilel3glbrmkct3q0',1439875652,'modx.user.contextTokens|a:0:{}'),
	('gndls6qa6r2lrf47colmshkfu7',1439875648,'modx.user.contextTokens|a:0:{}'),
	('debvkstum31agp0jkcl0jiniq0',1439874667,'modx.user.contextTokens|a:0:{}'),
	('oscoodm6a20vfhsadk7b5vql05',1439874627,'modx.user.contextTokens|a:0:{}'),
	('n99rbj0gh2hjn16s6dtsce3c10',1439874626,'modx.user.contextTokens|a:0:{}'),
	('g5dudn8r9ord39qne754gf61l1',1439869671,'modx.user.contextTokens|a:0:{}'),
	('fiphpckupdmn4gv25vgr6qosb1',1439869561,'modx.user.contextTokens|a:0:{}'),
	('fhaqci2cgpfbtg9365gj4d3iv5',1439869561,'modx.user.contextTokens|a:0:{}'),
	('jso0peetaie5ibbsr81rmptdn2',1439868928,'modx.user.contextTokens|a:0:{}'),
	('vac2f29100oop59ua0kosr4jm1',1439867563,'modx.user.contextTokens|a:0:{}'),
	('kp906747geqebo5mtjsrcc5756',1439867356,'modx.user.contextTokens|a:0:{}'),
	('dmejn6844eim3hic89bqgfojc1',1439865935,'modx.user.contextTokens|a:0:{}'),
	('hbpdhnvn6pq99q58pgjp8ssgc1',1439864334,'modx.user.contextTokens|a:0:{}'),
	('p3kk9vshlgsue7odfut9ge39v7',1439864137,'modx.user.contextTokens|a:0:{}'),
	('l6rs0rlitsjlb9o23i3b24g403',1439863708,'modx.user.contextTokens|a:0:{}'),
	('1iqafsa44v1hq0u6ftckc7i5v2',1439861146,'modx.user.contextTokens|a:0:{}'),
	('6m7v9f7v54er0kpdjucrievb53',1439860167,'modx.user.contextTokens|a:0:{}'),
	('jthmp9n6nt7i6lvcigst6ejin1',1439860166,'modx.user.contextTokens|a:0:{}'),
	('h60an8dt1u5t8open4407gpu47',1439859220,'modx.user.contextTokens|a:0:{}'),
	('ec397l6prvabvopqlilm59scb0',1439859145,'modx.user.contextTokens|a:0:{}'),
	('nsoifvj8f0basspqs4q7tvpk60',1439859145,'modx.user.contextTokens|a:0:{}'),
	('dnsdjhpa2flqvnshikpqhe18f3',1439859145,'modx.user.contextTokens|a:0:{}'),
	('pgakd75igs58naoatsvqohufb0',1439859145,'modx.user.contextTokens|a:0:{}'),
	('2jkuapj997lbkflvrvnnam4fr0',1439859145,'modx.user.contextTokens|a:0:{}'),
	('spsodno8itms653l9bcqmf1l23',1439859144,'modx.user.contextTokens|a:0:{}'),
	('qs84b4r839g68c5p7k0fanpuo6',1439859144,'modx.user.contextTokens|a:0:{}'),
	('9u6odfc3aqpo06s355cpcvrtq6',1439859144,'modx.user.contextTokens|a:0:{}'),
	('572q1atf3fff6s07qvf5b8mdb1',1439859144,'modx.user.contextTokens|a:0:{}'),
	('psvm60a633mp89v9vg78dugsd4',1439859144,'modx.user.contextTokens|a:0:{}'),
	('qeqh0b1q3pomuqr75cic9vmur5',1439859144,'modx.user.contextTokens|a:0:{}'),
	('dbccefnvv0vdvimoafkovdh6c3',1439856365,'modx.user.contextTokens|a:0:{}'),
	('virhiav6sedtc9v8v2h8029v81',1439852628,'modx.user.contextTokens|a:0:{}'),
	('4k1tk3iv5ln0ek54g9theb6vv3',1439851426,'modx.user.contextTokens|a:0:{}'),
	('ea5eqb2p6t34se6s8i8d1h9ve0',1439851308,'modx.user.contextTokens|a:0:{}'),
	('tli5blcqo56joqi8m5dcr0eu46',1439851159,'modx.user.contextTokens|a:0:{}'),
	('v6k33hilpnf5u6fue7u47ml070',1439850272,'modx.user.contextTokens|a:0:{}'),
	('aum92vas5oscr3o5hgncdqjur0',1439850272,'modx.user.contextTokens|a:0:{}'),
	('tbsc9uhdd7pevirlncirpnshs2',1439847968,'modx.user.contextTokens|a:0:{}'),
	('h8t55026b7lc6dvco1i3loqst6',1439844006,'modx.user.contextTokens|a:0:{}'),
	('tqdrv82r04rmub1gn912kotss5',1439844006,'modx.user.contextTokens|a:0:{}'),
	('ufqi0sbfp5097bijuh9ibj4244',1439843928,'modx.user.contextTokens|a:0:{}'),
	('loa3lk27ckgqs3l5akkvfk0fr7',1439843817,'modx.user.contextTokens|a:0:{}'),
	('8a15cdu5cjl5vejjcvooonk9a1',1439843074,'modx.user.contextTokens|a:0:{}'),
	('9r5hoc00retpo3tehtt4fh7941',1439841030,'modx.user.contextTokens|a:0:{}'),
	('etoh7pi58nboi0aquihbg2fq14',1439840169,'modx.user.contextTokens|a:0:{}'),
	('pjerf1djhegp65i30baoc1k737',1439839898,'modx.user.contextTokens|a:0:{}'),
	('f0v2tqpkampinpvr3ubiip80l6',1439839278,'modx.user.contextTokens|a:0:{}'),
	('j3bc39h88mtd4bl3e3pch6o150',1439837963,'modx.user.contextTokens|a:0:{}'),
	('lp29fetrpj2tlo3r063hp5ni67',1439835795,'modx.user.contextTokens|a:0:{}'),
	('09mt94mlk9os6ggptfa5lldbn3',1439835793,'modx.user.contextTokens|a:0:{}'),
	('d6i7ih6ifqbqha8mhejada5jg0',1439835792,'modx.user.contextTokens|a:0:{}'),
	('dmp2tvdfj6vqipbur4kkjhr7m4',1439835790,'modx.user.contextTokens|a:0:{}'),
	('5itlj80ulql06ml03sdb35f4t2',1439835787,'modx.user.contextTokens|a:0:{}'),
	('94bae6kbb8428udppio4rnpud0',1439835786,'modx.user.contextTokens|a:0:{}'),
	('evlu4gveb9439ftqivpv5b34f7',1439835784,'modx.user.contextTokens|a:0:{}'),
	('u5d15ed5clt2gkin37i9u7ksm6',1439832144,'modx.user.contextTokens|a:0:{}'),
	('am2ovo95hs1eq574q2bl8l5rv3',1439832069,'modx.user.contextTokens|a:0:{}'),
	('r0g16un4m303olre6k7ssi4kn0',1439829001,'modx.user.contextTokens|a:0:{}'),
	('clbjm68jgkk6pd69mbiq2bnn60',1439828521,'modx.user.contextTokens|a:0:{}'),
	('eghb9fls6cnumkfum9cbong1a3',1439828219,'modx.user.contextTokens|a:0:{}'),
	('ci0aqk90u46perm002063rq4o3',1439826973,'modx.user.contextTokens|a:0:{}'),
	('c4rugpuam0t16m9oui1mg3n0g6',1439825960,'modx.user.contextTokens|a:0:{}'),
	('l3imijs5910h2r4oeb7mro7oa0',1439824015,'modx.user.contextTokens|a:0:{}'),
	('ib3jot5brcs6c3ekfio9h0tqt5',1439822753,'modx.user.contextTokens|a:0:{}'),
	('3nca2ucit7oe886us458dmj6t2',1439822136,'modx.user.contextTokens|a:0:{}'),
	('s7e3un311tp1lt5u1pr8rhc2u1',1439820468,'modx.user.contextTokens|a:0:{}'),
	('6oiscg8t8fl29mm6q30op3aqk5',1439818995,'modx.user.contextTokens|a:0:{}'),
	('v18en0872eil0jkkk5urbf57k3',1439818838,'modx.user.contextTokens|a:0:{}'),
	('js3p65q950fnm3ejq1q849pcn6',1439817707,'modx.user.contextTokens|a:0:{}'),
	('ghngfif0kb0vedr36e4ob9etn0',1439816800,'modx.user.contextTokens|a:0:{}'),
	('pc9ccke7p2a8qv84t3i6sf3193',1439815172,'modx.user.contextTokens|a:0:{}'),
	('cioptb36290ovk3524ik3dpnq5',1439815172,'modx.user.contextTokens|a:0:{}'),
	('fr7dvrgoeh3hfd683fsmfd2r01',1439815171,'modx.user.contextTokens|a:0:{}'),
	('4bdb9h3904tqr3v8tbncac8uc3',1439813603,'modx.user.contextTokens|a:0:{}'),
	('dd6bl6qu6ha7r44g5qc6q4r5t2',1439813511,'modx.user.contextTokens|a:0:{}'),
	('c7s85ph36bopjf599o3rfrcs36',1439812526,'modx.user.contextTokens|a:0:{}'),
	('bf4d70gli7j49u2s3u31lh9ej6',1439811145,'modx.user.contextTokens|a:0:{}'),
	('rhrkkvkknjjenq1g74e275cd04',1439808350,'modx.user.contextTokens|a:0:{}'),
	('f30qb5g5ej3gsnt845n2ha80f5',1439807938,'modx.user.contextTokens|a:0:{}'),
	('1n7iq22m2kh57iie3s7gvub6b3',1439804256,'modx.user.contextTokens|a:0:{}'),
	('0qm5jv2i3e8flt1qui623sgae5',1439802374,'modx.user.contextTokens|a:0:{}'),
	('164tinaee8ssia814pbdkvbpf5',1439800962,'modx.user.contextTokens|a:0:{}'),
	('urtnoghdbsliqf7khrc4kkmdj6',1439798463,'modx.user.contextTokens|a:0:{}'),
	('6v8urvmbvgpmsr3i92mevvhaf3',1439798452,'modx.user.contextTokens|a:0:{}'),
	('s70il6kbfrvpp11dgg5phuuh85',1439798449,'modx.user.contextTokens|a:0:{}'),
	('73jsenbb463mbn0rhl8n5o02c6',1439797781,'modx.user.contextTokens|a:0:{}'),
	('up94svappdlsce9rhh5vg4k4c4',1439795304,'modx.user.contextTokens|a:0:{}'),
	('cd4j67bmkuvsfdq2tcg9vq2sb5',1439793395,'modx.user.contextTokens|a:0:{}'),
	('0kpfb7dom1062j9b87j4pm9e96',1439790134,'modx.user.contextTokens|a:0:{}'),
	('ctquk4k7stb1j7ceuep1tpldi0',1439789811,'modx.user.contextTokens|a:0:{}'),
	('61qqrsc9v9js3kherp3sbj8pc2',1439789203,'modx.user.contextTokens|a:0:{}'),
	('f9h9tr363vc9bai9s6836qfqe1',1439784268,'modx.user.contextTokens|a:0:{}'),
	('ka8t6ht7sdub7e15226s9946a1',1439784268,'modx.user.contextTokens|a:0:{}'),
	('j6eb1rsjj6hfe88d2inqd97ji7',1439783366,'modx.user.contextTokens|a:0:{}'),
	('c2t5cbo2v6276nm7cvsadc61n0',1439782903,'modx.user.contextTokens|a:0:{}'),
	('2euj519g3vfc12m56tfi5le1k4',1439782535,'modx.user.contextTokens|a:0:{}'),
	('aka5ttn7885uid2eups8k9pgu5',1439781641,'modx.user.contextTokens|a:0:{}'),
	('5ptsmjtqlt46pp1nvkbioo8r46',1439780190,'modx.user.contextTokens|a:0:{}'),
	('9m1nj145ovd182lmmp4b9qhd07',1439780148,'modx.user.contextTokens|a:0:{}'),
	('l15r3gr3s8fbmvdqc3tugn7po4',1439780067,'modx.user.contextTokens|a:0:{}'),
	('p7jqpfb4h2kkrk8eoagr2ttaa6',1439778748,'modx.user.contextTokens|a:0:{}'),
	('0tlrt07ju0134546m7ni7ukst7',1439778653,'modx.user.contextTokens|a:0:{}'),
	('gg7krga7u2pbvprtpafpo8pj93',1439778583,'modx.user.contextTokens|a:0:{}'),
	('b8lu8v7ssvfs51plduu5rh0f70',1439774833,'modx.user.contextTokens|a:0:{}'),
	('10t9jte7bmr0jee8c236k2p846',1439772859,'modx.user.contextTokens|a:0:{}'),
	('sjqpla3961jci3ue3misroti11',1439771938,'modx.user.contextTokens|a:0:{}'),
	('0du0pspuqgu3lpkcbfjurcti80',1439770796,'modx.user.contextTokens|a:0:{}'),
	('kbhda21oile20lai107njovin5',1439770795,'modx.user.contextTokens|a:0:{}'),
	('3lrn1uh43gh71pvctvg4j06q75',1439770794,'modx.user.contextTokens|a:0:{}'),
	('rrep7unb95du8106fej9q5rrn3',1439770228,'modx.user.contextTokens|a:0:{}'),
	('9fhpfkuu97t6hpaoue60a0qt12',1439770218,'modx.user.contextTokens|a:0:{}'),
	('vmq16qvbhdim4t2teg7s4mnt71',1439770172,'modx.user.contextTokens|a:0:{}'),
	('ge5tkhi6p2qrejmopk1v2k1oo6',1439770014,'modx.user.contextTokens|a:0:{}'),
	('k5j2hn874j1hbk8ohg00b9kc70',1439769579,'modx.user.contextTokens|a:0:{}'),
	('hnjr041q0hpoukln7sqbi855k6',1439767039,'modx.user.contextTokens|a:0:{}'),
	('lqq0po2rjqlr811oh8g6u1hkn0',1439767020,'modx.user.contextTokens|a:0:{}'),
	('215r812m2hvb89enjdkbe5lfo0',1439767019,'modx.user.contextTokens|a:0:{}'),
	('jrcn17ca6qfcuq0gdlkq5mejr6',1439766970,'modx.user.contextTokens|a:0:{}'),
	('v0aea3m17qmrph54stba46r9m4',1439766967,'modx.user.contextTokens|a:0:{}'),
	('8hsgb4vovkig52g2v9724ocbm2',1439765865,'modx.user.contextTokens|a:0:{}'),
	('sc9bslocltugdtpqrdo7nlbgr1',1439765585,'modx.user.contextTokens|a:0:{}'),
	('58v7mhdgs2kcdi78f7q8g4rni3',1439763122,'modx.user.contextTokens|a:0:{}'),
	('ia3k1c041ambr4gigl849n1lv1',1439762943,'modx.user.contextTokens|a:0:{}'),
	('ab67g5lqmrtkdu1csanntqmjt5',1439762722,'modx.user.contextTokens|a:0:{}'),
	('cp29uvmps60uh3ahubb3f6ivk0',1439762605,'modx.user.contextTokens|a:0:{}'),
	('pcn3h3hqlc7ie7knpol1tj4v30',1439762017,'modx.user.contextTokens|a:0:{}'),
	('mktjh1m50smfm1jmaq63bnuu17',1439761550,'modx.user.contextTokens|a:0:{}'),
	('k8gekhc1oms7319mlaa7jhm7b7',1439760918,'modx.user.contextTokens|a:0:{}'),
	('ut5hvs4a1gkdjd1djob7aa7j21',1439760852,'modx.user.contextTokens|a:0:{}'),
	('is2pukva2fo88c8qpp9v44rrr5',1439760852,'modx.user.contextTokens|a:0:{}'),
	('il979u9m5u0ll9sjd195srarq3',1439760479,'modx.user.contextTokens|a:0:{}'),
	('36g6b3tnloud5dcpo3qt07hod7',1439756898,'modx.user.contextTokens|a:0:{}'),
	('q3uqe6m76d6nq5ga8v1kgv3164',1439755951,'modx.user.contextTokens|a:0:{}'),
	('jrnegc8co1ao3qaenk13t7ll05',1439754739,'modx.user.contextTokens|a:0:{}'),
	('1b9vjg883u0c08i67m3m6o7gj5',1439753301,'modx.user.contextTokens|a:0:{}'),
	('iia3e733piijlc3o1q0nj6sop2',1439753299,'modx.user.contextTokens|a:0:{}'),
	('rfcilh78ltqh9folvfj6ivnkv6',1439753277,'modx.user.contextTokens|a:0:{}'),
	('jqvus27ua0j5fu0qc0i7s1rk15',1439752090,'modx.user.contextTokens|a:0:{}'),
	('id73hcjdl67gkgmsi4tofotgt0',1439751229,'modx.user.contextTokens|a:0:{}'),
	('t2n6trc2cdihi7a0cpf2m3hrn4',1439750635,'modx.user.contextTokens|a:0:{}'),
	('pbir4rebuobqfaa2t3ufg9j9d2',1439750169,'modx.user.contextTokens|a:0:{}'),
	('4unenfl7a1mft0c1lo4t6nd3a6',1439750168,'modx.user.contextTokens|a:0:{}'),
	('46lij2noagh29h20k2uk4pcg34',1439750168,'modx.user.contextTokens|a:0:{}'),
	('m8ma43rd5e4va9l5bcae0mquk3',1439319583,'modx.user.contextTokens|a:0:{}'),
	('5dj99t2sfi333rndhlmhb9bns3',1439318637,'modx.user.contextTokens|a:0:{}'),
	('kp4goc9ofksesr57gp5bdmj9m4',1439318636,'modx.user.contextTokens|a:0:{}'),
	('al4bhvgma3ut4jjk3o0vei17c5',1439316776,'modx.user.contextTokens|a:0:{}'),
	('ct8oe1ts58cpbe3sva7241ueu4',1439314333,'modx.user.contextTokens|a:0:{}'),
	('hhg1k7pstuaof1c1br73p5ql63',1439313708,'modx.user.contextTokens|a:0:{}'),
	('20pvc3r08rv1k7i0fviebs4251',1439313547,'modx.user.contextTokens|a:0:{}'),
	('il9i3v3mguvbi9pm0hvjgtm0s3',1439313211,'modx.user.contextTokens|a:0:{}'),
	('fhhqor1jjoi5hnrr64uhovdsk0',1439482181,'modx.user.contextTokens|a:1:{s:3:\"mgr\";i:1;}modx.mgr.user.token|s:52:\"modx552d8c97c05080.38362777_155ccc144de0489.55941262\";modx.mgr.session.cookie.lifetime|i:0;modx.mgr.user.config|a:0:{}'),
	('4iqq6p0ef16l82245ecdmi68s3',1439310329,'modx.user.contextTokens|a:0:{}'),
	('96n43c0du5eq0qo6mj97mcmq94',1439310327,'modx.user.contextTokens|a:0:{}'),
	('kaeg0p5bp03r3k36f94t617e45',1439310090,'modx.user.contextTokens|a:0:{}'),
	('v77vd6dblpfr8hrshsetbrvlf2',1439309670,'modx.user.contextTokens|a:0:{}'),
	('984ksf441ti1vs2fp9ai9gb335',1439309374,'modx.user.contextTokens|a:0:{}'),
	('5592irvcg0h5lo47prln0d0lf1',1439307538,'modx.user.contextTokens|a:0:{}'),
	('t2b3oq9kr8c0jiuj0kbi93b5s7',1439307537,'modx.user.contextTokens|a:0:{}'),
	('228j68a7mrm1t1g4q6aqe69fv7',1439307455,'modx.user.contextTokens|a:0:{}'),
	('04o4018ciscf20lnv1qmrdd6k2',1439306871,'modx.user.contextTokens|a:0:{}'),
	('86hblfpqk7fv3sof0ik6uui9h4',1439306798,'modx.user.contextTokens|a:0:{}'),
	('vtb7hjcqgav5u346981fkju372',1439306775,'modx.user.contextTokens|a:0:{}'),
	('rdn25li4goe7foo0168p51du05',1439306658,'modx.user.contextTokens|a:0:{}'),
	('ji4j76egn5guv69vaddhnm51r2',1439306657,'modx.user.contextTokens|a:0:{}'),
	('veqk2089ui1gk0bs6ph7n4mnd0',1439306653,'modx.user.contextTokens|a:0:{}'),
	('l2b1o680pjgc5575oma2sfpdi7',1439306365,'modx.user.contextTokens|a:0:{}'),
	('tl1te03hceqe2poo00lsuk3cv2',1439305998,'modx.user.contextTokens|a:0:{}'),
	('lom8blq48168ci0eejn2hnf4h7',1439305998,'modx.user.contextTokens|a:0:{}'),
	('lmbjkldongp2ahlm2kfniui4k3',1439305806,'modx.user.contextTokens|a:0:{}'),
	('iek838q5mjcep8fu2n80s87od2',1439305160,'modx.user.contextTokens|a:0:{}'),
	('8q89oc0bc21bb5ekren8o6jba2',1439304866,'modx.user.contextTokens|a:0:{}'),
	('90h48diqtlsm0g0dlgjlgv0974',1439303764,'modx.user.contextTokens|a:0:{}'),
	('tlnb6fcq7iiia4n965v8n0gv95',1439303187,'modx.user.contextTokens|a:0:{}'),
	('j312ranban411bn14fg29r5ip3',1439301493,'modx.user.contextTokens|a:0:{}'),
	('sjncvchuaafis4ll4dj4u4bpu3',1439301491,'modx.user.contextTokens|a:0:{}'),
	('ep9g2p1icre8615je0ekduncf7',1439301397,'modx.user.contextTokens|a:0:{}'),
	('trlijvpqvkpsg60r5qaf2uikq3',1439301396,'modx.user.contextTokens|a:0:{}'),
	('sarltk223e9t6j48u7dcu7pms0',1439301349,'modx.user.contextTokens|a:0:{}'),
	('vhood6mcgfcd2bst4nl9edpv82',1439301347,'modx.user.contextTokens|a:0:{}'),
	('co466hrbd8n68dbfidoojm3237',1439300952,'modx.user.contextTokens|a:0:{}'),
	('tvidstu2a8r34ekgur0c1rrt42',1439298783,'modx.user.contextTokens|a:0:{}'),
	('hlaqjr68cbknfh3ptaf60vu1i2',1439295378,'modx.user.contextTokens|a:0:{}'),
	('sv64s1uipgqj8545405k75hup4',1439295377,'modx.user.contextTokens|a:0:{}'),
	('0ljvrktik619f0tpsepq3s4272',1439295375,'modx.user.contextTokens|a:0:{}'),
	('b82qglia27luedp8fli9svp8m3',1439295373,'modx.user.contextTokens|a:0:{}'),
	('l05if2pft1qsn3ggpngm0hu5g1',1439295371,'modx.user.contextTokens|a:0:{}'),
	('4j86p9f5p2018nk8ehtaltpio0',1439295368,'modx.user.contextTokens|a:0:{}'),
	('n9ng1ttjm2p953vo5ao2fh6j93',1439295366,'modx.user.contextTokens|a:0:{}'),
	('7hshga1g0niu8njndmft23u4a2',1439295363,'modx.user.contextTokens|a:0:{}'),
	('vvasph9gqvbb1ir983htj9p8r3',1439295359,'modx.user.contextTokens|a:0:{}'),
	('j57robv0m3csloc26rnijcao94',1439295355,'modx.user.contextTokens|a:0:{}'),
	('nhktc6evovvu693b5qo8orm0r5',1439295341,'modx.user.contextTokens|a:0:{}'),
	('0topaa0iphpe51ka3pcdhm7at6',1439295339,'modx.user.contextTokens|a:0:{}'),
	('gkcplsn6ubd5nq4hudfeapgbj5',1439294424,'modx.user.contextTokens|a:0:{}'),
	('k1cmid7c56ju6acf921hgavkr0',1439294423,'modx.user.contextTokens|a:0:{}'),
	('n8f33a6sa7rihe4pi60n8la047',1439290079,'modx.user.contextTokens|a:0:{}'),
	('c48f072q1buqp77gc5phi162h5',1439288858,'modx.user.contextTokens|a:0:{}'),
	('l0o4af3lvmvhgtu53p9md4okb3',1439288748,'modx.user.contextTokens|a:0:{}'),
	('721o6irjm2ls3u4a2v5nsin0j0',1439287773,'modx.user.contextTokens|a:0:{}'),
	('i9g87dr330ipqr6nblu36d9n07',1439285847,'modx.user.contextTokens|a:0:{}'),
	('lpo2m4unv8lp5edf9ugnrpd041',1439281793,'modx.user.contextTokens|a:0:{}'),
	('ph6iplqras43iatj2shs5v7bf2',1439281387,'modx.user.contextTokens|a:0:{}'),
	('3inoq4oa36m72cuimh7pbl22u5',1439281204,'modx.user.contextTokens|a:0:{}'),
	('p58di4usufpg67p2ndl83mdoa0',1439281118,'modx.user.contextTokens|a:0:{}'),
	('0edbpsbs1b184bifl7ofqult91',1439277895,'modx.user.contextTokens|a:0:{}'),
	('nsidel8q7v3mp6e384bkjh24a4',1439271513,'modx.user.contextTokens|a:0:{}'),
	('b23qqevvjn63fpohlarqeu5tb1',1439271512,'modx.user.contextTokens|a:0:{}'),
	('che4un1rtb5943slmunonqgj74',1439270749,'modx.user.contextTokens|a:0:{}'),
	('nd97k6iftml1o3ccq5igia1cj4',1439270737,'modx.user.contextTokens|a:0:{}'),
	('50fr4jgh94r5ou61qlq404tpc3',1439267810,'modx.user.contextTokens|a:0:{}'),
	('3nicn0ifgljrge3ontbatgjr45',1439267060,'modx.user.contextTokens|a:0:{}'),
	('2uj91c80hntq2unudva2gjhgv5',1439263889,'modx.user.contextTokens|a:0:{}'),
	('27lbihjobvkivv15d5cnnpidq3',1439263190,'modx.user.contextTokens|a:0:{}'),
	('iqosuia2qr7i3796gepcbvmoc6',1439256966,'modx.user.contextTokens|a:0:{}'),
	('eq9stgt46mah4kb4fd4urs3ro4',1439258206,'modx.user.contextTokens|a:0:{}'),
	('sod9r2jg3d14r08d4el955t3v5',1439258641,'modx.user.contextTokens|a:0:{}'),
	('58aaihs0199iof1c6ektbtap76',1439258655,'modx.user.contextTokens|a:0:{}'),
	('jg85urlureu3qa1273u9eqbvl7',1439262259,'modx.user.contextTokens|a:0:{}'),
	('h9ejpu4a7gul985bok7u8mpvm7',1439262260,'modx.user.contextTokens|a:0:{}'),
	('482023aa4b69hqlgokuvgnoer2',1439263185,'modx.user.contextTokens|a:0:{}'),
	('a2ieiuslag49621jmailmflbq0',1439256102,'modx.user.contextTokens|a:0:{}'),
	('8d5gvrv8493l27nkobitpinbi6',1439255889,'modx.user.contextTokens|a:0:{}'),
	('aluedg98d2j13ec8js0in7mcq5',1439255882,'modx.user.contextTokens|a:0:{}'),
	('m1ap8i24ftt189nn1d4ja90hu6',1439255865,'modx.user.contextTokens|a:0:{}'),
	('hrkft3eokvg0q2qchs61hm7ft4',1439252069,'modx.user.contextTokens|a:0:{}'),
	('mkbedt55rajdhq1f6ul0m4j164',1439251521,'modx.user.contextTokens|a:0:{}'),
	('bbnsiqqagga41a6uth5cdu6jk5',1439250126,'modx.user.contextTokens|a:0:{}'),
	('ur29515283r3uj95nmigns5d66',1439249972,'modx.user.contextTokens|a:0:{}'),
	('tnkro11ihltk709j4id9an23t2',1439249971,'modx.user.contextTokens|a:0:{}'),
	('p4k8espfhljsbnv964dmn6vqh2',1439247002,'modx.user.contextTokens|a:0:{}'),
	('g0utqulisv8mb4isulmk1hqfe5',1439246039,'modx.user.contextTokens|a:0:{}'),
	('4jns9a2sa2lr1o3ie0la3jinu2',1439245617,'modx.user.contextTokens|a:0:{}'),
	('k4uglav98ak86ij24l7lei9k90',1439241163,'modx.user.contextTokens|a:0:{}'),
	('67gndqg9vbsb90opb2hva2j696',1439239984,'modx.user.contextTokens|a:0:{}'),
	('k3k3nt8ua5u1rds6q3a2jq6me4',1439239983,'modx.user.contextTokens|a:0:{}'),
	('d5mm5s7ieqb8hke84e5ep6vhk0',1439237966,'modx.user.contextTokens|a:0:{}'),
	('fs5srgjuoflvnn6u0ev0h0l695',1439237253,'modx.user.contextTokens|a:0:{}'),
	('7l9vd7fqdh44978ura2fb1p3d3',1439237249,'modx.user.contextTokens|a:0:{}'),
	('917aks3qebolrreajgn0om1kg3',1439235742,'modx.user.contextTokens|a:0:{}'),
	('ttq56bsl3m5vf4jd43vahj7cj6',1439235364,'modx.user.contextTokens|a:0:{}'),
	('fsrkf2ljaho2n7hevr37nc3067',1439234401,'modx.user.contextTokens|a:0:{}'),
	('t530m9i77f3unfn8akf2klij73',1439233096,'modx.user.contextTokens|a:0:{}'),
	('icssjmfmflfj3oalrjei1hll25',1439233021,'modx.user.contextTokens|a:0:{}'),
	('m89a9c9uslnmbn71md193eqfp5',1439232933,'modx.user.contextTokens|a:0:{}'),
	('i14npbv5b98g2h38kffrfgdfo7',1439231896,'modx.user.contextTokens|a:0:{}'),
	('8qelf8hifn7nq9aejdh2cpc6f5',1439231896,'modx.user.contextTokens|a:0:{}'),
	('7mmqsd9mvp1ttghv3erq2636k0',1439230906,'modx.user.contextTokens|a:0:{}'),
	('5ph43t59cu9j9s6lmbnvqmvjj2',1439230616,'modx.user.contextTokens|a:0:{}'),
	('2g4glkj2rip6akm15ibr103bq4',1439230593,'modx.user.contextTokens|a:0:{}'),
	('g4ciqfohfgp7akop623krpsur6',1439230592,'modx.user.contextTokens|a:0:{}'),
	('cd0b4qumktl8qhkau8kqcak3s5',1439230590,'modx.user.contextTokens|a:0:{}'),
	('25o37ngfshohlkck30cqoo2gg3',1439230589,'modx.user.contextTokens|a:0:{}'),
	('jsg41fd2av2e0rs8kfjosa3r21',1439230588,'modx.user.contextTokens|a:0:{}'),
	('827qamh34acbd160m5tjiip2g1',1439230319,'modx.user.contextTokens|a:0:{}'),
	('3rpkaqutqc7cdgp2j02201db22',1439226885,'modx.user.contextTokens|a:0:{}'),
	('slrmfmcgj7plnks5btibkbb153',1439226809,'modx.user.contextTokens|a:0:{}'),
	('ddm7k64352n8pn8nn8j0pbu0r3',1439224760,'modx.user.contextTokens|a:0:{}'),
	('rfpkdade4jr9bg9fjf5hplrkm3',1439224271,'modx.user.contextTokens|a:0:{}'),
	('8l937nevbpouc2p5l1uvo83jt2',1439223514,'modx.user.contextTokens|a:0:{}'),
	('pq9ldj4njd512bfjki66sai7b1',1439223238,'modx.user.contextTokens|a:0:{}'),
	('t0csvcp6v6pdanf9ek04ts7ks6',1439222741,'modx.user.contextTokens|a:0:{}'),
	('9cn13cs3mmjg38nltfl66ku6c7',1439222720,'modx.user.contextTokens|a:0:{}'),
	('7gr8ufg8favuit7j38ik659ov2',1439222690,'modx.user.contextTokens|a:0:{}'),
	('nv1o3ema5hkdkace79jpbda813',1439221323,'modx.user.contextTokens|a:0:{}'),
	('c84668g14gfgrv418l5egfhks4',1439221262,'modx.user.contextTokens|a:0:{}'),
	('h8qi279lfigpn7fse99p6695i5',1439221103,'modx.user.contextTokens|a:0:{}'),
	('74nngloltpgtl58i5dpql8o8q5',1439217340,'modx.user.contextTokens|a:0:{}'),
	('u30aemas4f6m4qv3ugg9mbq1g6',1439217219,'modx.user.contextTokens|a:0:{}'),
	('ond412viocuq5cgq1t697jp490',1439217217,'modx.user.contextTokens|a:0:{}'),
	('geikqc6r1nlgtttvfsusoc9vj5',1439217215,'modx.user.contextTokens|a:0:{}'),
	('3qltskmuu510a984iv3l8jo4j6',1439212783,'modx.user.contextTokens|a:0:{}'),
	('pdej5qp7qd6qqg3m8iotactpf3',1439211780,'modx.user.contextTokens|a:0:{}'),
	('es3euqpjvisg97lb8okpi6hdl6',1439211765,'modx.user.contextTokens|a:0:{}'),
	('ca271ihl1okvit5hq6m8rd9p50',1439206525,'modx.user.contextTokens|a:0:{}'),
	('16p84flrmh5l4vlckftvilr783',1439205311,'modx.user.contextTokens|a:0:{}'),
	('vbif60qd2ujtvujuodji1i3e71',1439205305,'modx.user.contextTokens|a:0:{}'),
	('5vm0lidu91u91frb752altk991',1439205072,'modx.user.contextTokens|a:0:{}'),
	('npvvrpnsfsdf635kfpuumamoe6',1439199428,'modx.user.contextTokens|a:0:{}'),
	('aa49o8fq21el73bt33h5i9kbj0',1439197854,'modx.user.contextTokens|a:0:{}'),
	('grjhd0o76nc355ud7rh4gtk1j3',1439197274,'modx.user.contextTokens|a:0:{}'),
	('tp53sieonj52cm8gf99vbjhui4',1439196473,'modx.user.contextTokens|a:0:{}'),
	('db76n3h8uc5s4kv7av81vkj0p5',1439196449,'modx.user.contextTokens|a:0:{}'),
	('sje35scuefvv33poj5g4v9gkb2',1439196404,'modx.user.contextTokens|a:0:{}'),
	('9e5s5v50mltnruk13bch78ukk7',1439196346,'modx.user.contextTokens|a:0:{}'),
	('72q57au4jaev9iokavmts7ogv4',1439196311,'modx.user.contextTokens|a:0:{}'),
	('c8i825ohaigpqip6ff69a27984',1439195474,'modx.user.contextTokens|a:0:{}'),
	('32lir00149ht8g5f43p0ijab52',1438753272,'modx.user.contextTokens|a:0:{}'),
	('bmn1jjkq94sfa3kgir4erggsr7',1438753273,'modx.user.contextTokens|a:0:{}'),
	('2o8s8g95e38rqeq5ppqcjn30n3',1438753452,'modx.user.contextTokens|a:0:{}'),
	('qnvi7k14rs889sq8b9ovhgl9j7',1438753497,'modx.user.contextTokens|a:0:{}'),
	('sk1f2m7ndr8te8kjd8ano31ta0',1438754257,'modx.user.contextTokens|a:0:{}'),
	('4bn4qm8jdhh1d25kfg4kq5h786',1438754271,'modx.user.contextTokens|a:0:{}'),
	('u9k3gteoqlafm4c59v6fl60g92',1438754352,'modx.user.contextTokens|a:0:{}'),
	('6c58i7t9hkiip3ku8lg8rhfir2',1438756708,'modx.user.contextTokens|a:0:{}'),
	('1m6vk82c1h7ctqm96ihm3pvjb2',1438759152,'modx.user.contextTokens|a:0:{}'),
	('94th2c4qb5st54ivkcfa2iiof0',1438759153,'modx.user.contextTokens|a:0:{}'),
	('fcmofkk652ptuikgd27ees9kj3',1438761526,'modx.user.contextTokens|a:0:{}'),
	('f4m0td5g89as0fsai5bpbvafj0',1438763005,'modx.user.contextTokens|a:0:{}'),
	('t6p3b7b0a57qt7v3jfes0ibjp0',1438765744,'modx.user.contextTokens|a:0:{}'),
	('gqgi09chsth23mcqeurrg58ag5',1438765849,'modx.user.contextTokens|a:0:{}'),
	('3on6qkcdjcqaq5444lt7s95a76',1438765849,'modx.user.contextTokens|a:0:{}'),
	('abfvmlq5rtlsvgd9isd0f63nj4',1438765850,'modx.user.contextTokens|a:0:{}'),
	('2uts2oc6abh8uclatttkr1c650',1438766149,'modx.user.contextTokens|a:0:{}'),
	('qr2pa2gtthl31l0ulohb1cvi35',1438766154,'modx.user.contextTokens|a:0:{}'),
	('bh9eg5qppstt5g0ckk9nh7vhj0',1438766508,'modx.user.contextTokens|a:0:{}'),
	('evv9f5mo43tnim740jim83dh87',1438766524,'modx.user.contextTokens|a:0:{}'),
	('sat7e5d7nogl86lqf1qc8903a7',1438766524,'modx.user.contextTokens|a:0:{}'),
	('9enjpguoqklq8t4gkluc4lp1n2',1438767622,'modx.user.contextTokens|a:0:{}'),
	('uekhnnvpu4fksucko3dh2kbv20',1438772518,'modx.user.contextTokens|a:0:{}'),
	('6f4nrkdfmmerhfnd4jj2unj746',1438772570,'modx.user.contextTokens|a:0:{}'),
	('avol8pg3t4an3lqeflkh4st386',1438772900,'modx.user.contextTokens|a:0:{}'),
	('9kcq3c31svl5de12tik30lg6q3',1438773623,'modx.user.contextTokens|a:0:{}'),
	('k281sln1fug1tsg555e7q931g3',1438774443,'modx.user.contextTokens|a:0:{}'),
	('d5ktovqlokt18al4acss3fhg46',1438774478,'modx.user.contextTokens|a:0:{}'),
	('bo5b01fr5ndubo8e7vq1dg0rg4',1438776359,'modx.user.contextTokens|a:0:{}'),
	('jn6n89e6opdat911brrr0338l2',1438776636,'modx.user.contextTokens|a:0:{}'),
	('s6u16e69fren0n38b1a8mu5st5',1438776643,'modx.user.contextTokens|a:0:{}'),
	('ur87k91akk1tpmjdhuv2kts6e0',1438776646,'modx.user.contextTokens|a:0:{}'),
	('fa73e7pl786iom14lck6vsvh10',1438776652,'modx.user.contextTokens|a:0:{}'),
	('kdgpjpprg8qgpg9e9kkk32l6i5',1438776656,'modx.user.contextTokens|a:0:{}'),
	('e2kbnbvjhpnerglom41kc9f4v2',1438776661,'modx.user.contextTokens|a:0:{}'),
	('dj2tspg1vsmgh14pahjvuhem26',1438776667,'modx.user.contextTokens|a:0:{}'),
	('87gg87o2dk6pshm01p96p3apj6',1438776671,'modx.user.contextTokens|a:0:{}'),
	('6ckro5er32sobmqqa8jun0aav3',1438776676,'modx.user.contextTokens|a:0:{}'),
	('m8rkq1nhpkrj8u2e1ohv1png70',1438776677,'modx.user.contextTokens|a:0:{}'),
	('8flfp8pmab2g76bkar146obme5',1438776681,'modx.user.contextTokens|a:0:{}'),
	('v8ucoc0v53af6pe8gojc0srk74',1438776686,'modx.user.contextTokens|a:0:{}'),
	('paao1tgvao1a37pdnkefk73up0',1438776692,'modx.user.contextTokens|a:0:{}'),
	('j0b929rbb7o0fh5b5ktge7kbk7',1438776696,'modx.user.contextTokens|a:0:{}'),
	('luoh5aftj1v9175lqv8u2sk095',1438776702,'modx.user.contextTokens|a:0:{}'),
	('kco314qoitfu0hehgm1o7bh313',1438776707,'modx.user.contextTokens|a:0:{}'),
	('uqglr5cb493ndsrqa1h5ggvs81',1438776711,'modx.user.contextTokens|a:0:{}'),
	('7b84908l9jo4vjics9d2tuir85',1438776716,'modx.user.contextTokens|a:0:{}'),
	('euqi3bbvklf3rgomm3hq46rou1',1438776721,'modx.user.contextTokens|a:0:{}'),
	('npri2j2lfqjfj4211c09rbt1p6',1438776727,'modx.user.contextTokens|a:0:{}'),
	('io7dtdt7bmqfard12o9d21jd27',1438776731,'modx.user.contextTokens|a:0:{}'),
	('gmv7pmg505aoafq82o4j86ald3',1438776735,'modx.user.contextTokens|a:0:{}'),
	('uu2le00sju5av7qsvnfbu27q95',1438776737,'modx.user.contextTokens|a:0:{}'),
	('uslke55ahpu5ar6fk349vvi2l6',1438776741,'modx.user.contextTokens|a:0:{}'),
	('q5hoansh30uq12er3ge53h32c2',1438776746,'modx.user.contextTokens|a:0:{}'),
	('7og3fl856b27glk705b5lg86n1',1438776749,'modx.user.contextTokens|a:0:{}'),
	('v5lh5qta4t7bduonl3c7ttqaq6',1438776751,'modx.user.contextTokens|a:0:{}'),
	('7ahe2jehsf3l9ld0gobj8skd43',1438776752,'modx.user.contextTokens|a:0:{}'),
	('kps9v1r9q5ci7c8ka4hbaaa2h2',1438776756,'modx.user.contextTokens|a:0:{}'),
	('21lq8refphe9a7todqgrc7bpk3',1438776761,'modx.user.contextTokens|a:0:{}'),
	('gpcukmmfob0apu2e1aatqslvv6',1438973428,'modx.user.contextTokens|a:0:{}'),
	('0u37olb51rcouaqunlgkbqopc5',1438973493,'modx.user.contextTokens|a:0:{}'),
	('ig3d6q0vibcqq530aqctokpeo1',1438973909,'modx.user.contextTokens|a:0:{}'),
	('iu3gov8t73jk9s57fpf8oi84a1',1438975476,'modx.user.contextTokens|a:0:{}'),
	('vhb9d87h3i2fuaat977trp7qi3',1438976254,'modx.user.contextTokens|a:0:{}'),
	('584aa030q4d597kapcbqrdj8f5',1438977169,'modx.user.contextTokens|a:0:{}'),
	('5t3ps6n42qhmp757435qcoqcl2',1438977882,'modx.user.contextTokens|a:0:{}'),
	('3krorrsucnuhuloni6j7k5hg36',1438979712,'modx.user.contextTokens|a:0:{}'),
	('eplp8nl9hq5oqu1kebt63eogn7',1438981394,'modx.user.contextTokens|a:0:{}'),
	('kr8s6qg2o8dk289fh7r7o4ted4',1438981716,'modx.user.contextTokens|a:0:{}'),
	('65659k97oiu5u1gh7jqhnbei32',1438982014,'modx.user.contextTokens|a:0:{}'),
	('h3euq6a75ssa1ui3btkr9uvsd2',1438982350,'modx.user.contextTokens|a:0:{}'),
	('is4fbd6rb2nab4m16acqm2gms4',1438982761,'modx.user.contextTokens|a:0:{}'),
	('qtgjbuvdiqb7bt098f04olp1r2',1438983141,'modx.user.contextTokens|a:0:{}'),
	('fibm6uh11gusa5fh41gd034b14',1438983144,'modx.user.contextTokens|a:0:{}'),
	('hgf645cl9hqr2md8kiguajfnj3',1438983147,'modx.user.contextTokens|a:0:{}'),
	('uacc5hdkh2h4hs0ot5psmsrps4',1438983149,'modx.user.contextTokens|a:0:{}'),
	('2pjn1o1lrd4m2bdccf0ibpure0',1438983152,'modx.user.contextTokens|a:0:{}'),
	('damm39rs61dj53ln2o4mt9ppb2',1438983155,'modx.user.contextTokens|a:0:{}'),
	('q3b7i7ifoc26dpucfavm1ue6m2',1438983158,'modx.user.contextTokens|a:0:{}'),
	('d2qk26arplc2o9bdc4415c65g4',1438983171,'modx.user.contextTokens|a:0:{}'),
	('teig1nle6seh7qgjsk5ji7qql0',1438983530,'modx.user.contextTokens|a:0:{}'),
	('tn5r4dneanqs4i8gumca6775v4',1438983532,'modx.user.contextTokens|a:0:{}'),
	('sbt95n68k3rle9r4ve7n267qh7',1438983536,'modx.user.contextTokens|a:0:{}'),
	('n4huu2u7b15b33c6g6rgt35gj0',1438983538,'modx.user.contextTokens|a:0:{}'),
	('q90u89bdo9kluvappmganna9q1',1438983542,'modx.user.contextTokens|a:0:{}'),
	('rf242tct36j44o03jih0rhr0m6',1438983548,'modx.user.contextTokens|a:0:{}'),
	('qbd4id06skamih7fevfg8ukgu7',1438983551,'modx.user.contextTokens|a:0:{}'),
	('bkoclvf4ugohrv8f6g0v2lnri6',1438983554,'modx.user.contextTokens|a:0:{}'),
	('632jkssi1gci6mbm3tqlh84090',1438983559,'modx.user.contextTokens|a:0:{}'),
	('tpf1du1bf685ljmthhq7lq5ea5',1438983565,'modx.user.contextTokens|a:0:{}'),
	('8hc1ss64mqa76eaaf5liusked1',1438983570,'modx.user.contextTokens|a:0:{}'),
	('ou8qrhjmeo8v5lobm1rlvdm792',1438983572,'modx.user.contextTokens|a:0:{}'),
	('ffnsdva7nk055ofpf0tikp2bh3',1438983579,'modx.user.contextTokens|a:0:{}'),
	('ht3nlirt2p61v54dtqup9ojbd0',1438983586,'modx.user.contextTokens|a:0:{}'),
	('bqvkpnd3svi64cltjdu2vaaan5',1438983588,'modx.user.contextTokens|a:0:{}'),
	('kgk0i0v4lh5a4uvujfesg7h1v2',1438984145,'modx.user.contextTokens|a:0:{}'),
	('2c850h0qliep1g3at0h7sp1eg6',1438984243,'modx.user.contextTokens|a:0:{}'),
	('rfjno3k2mmrecfdp0d5psgths1',1438984615,'modx.user.contextTokens|a:0:{}'),
	('0vhcshslnmms2170q5htm4ecd5',1438986711,'modx.user.contextTokens|a:0:{}'),
	('sq9l5gm6fiv3fj7mt4ajvht164',1438986895,'modx.user.contextTokens|a:0:{}'),
	('5vraprkuk1hg1onoarhqmrsng5',1438990272,'modx.user.contextTokens|a:0:{}'),
	('sqgkq1lnnljnr55au0a7fasdt7',1438990947,'modx.user.contextTokens|a:0:{}'),
	('d46rcsi84u5nqgmsmvshh849h0',1438993613,'modx.user.contextTokens|a:0:{}'),
	('qerfl1bs6edq57aqnlh2muqm94',1438993616,'modx.user.contextTokens|a:0:{}'),
	('nidfgpmp3qphqm8t32q6qi3001',1438993820,'modx.user.contextTokens|a:0:{}'),
	('624s2l2ojto3ni4llqr5b21kt6',1438994394,'modx.user.contextTokens|a:0:{}'),
	('8n7lnkvnvsukomockm3ocfqa53',1438994559,'modx.user.contextTokens|a:0:{}'),
	('8ojjin4c6jjn34fbpfamn162m4',1438995051,'modx.user.contextTokens|a:0:{}'),
	('1kbvhu9rd0fahctgpt6ts23eu1',1438995054,'modx.user.contextTokens|a:0:{}'),
	('k62j1bserav4s6s8ho9062fsf1',1438996466,'modx.user.contextTokens|a:0:{}'),
	('splg1oeumtj9tl1l4nhp5gkcj3',1438996470,'modx.user.contextTokens|a:0:{}'),
	('reliq9bo0jkvq9mm18nh003bh2',1438996923,'modx.user.contextTokens|a:0:{}'),
	('mo27ir9i2q0god1kp4giiu43r3',1438998288,'modx.user.contextTokens|a:0:{}'),
	('a48km5i662s3gicaffl2rt0687',1438999453,'modx.user.contextTokens|a:0:{}'),
	('np08ts74uadhe0uti32vkhj6q1',1438999455,'modx.user.contextTokens|a:0:{}'),
	('cmkvdn0a3fejrt5e1n1kiaa3u6',1438999868,'modx.user.contextTokens|a:0:{}'),
	('19jc3lbbdl2mslmf8c4s7dfuh5',1438999872,'modx.user.contextTokens|a:0:{}'),
	('e3ntvd6h0vfetpvecf0a6uo8s2',1439000621,'modx.user.contextTokens|a:0:{}'),
	('d1s2s80tf2sb71vr5q9kofq612',1439000641,'modx.user.contextTokens|a:0:{}'),
	('c7ckrgf5c61p30evae33jr9mt4',1439000646,'modx.user.contextTokens|a:0:{}'),
	('vs0n64up4qlk335bpbndcrm0m3',1439000648,'modx.user.contextTokens|a:0:{}'),
	('jtaq8foik0ju43du6lnpgqejt7',1439001231,'modx.user.contextTokens|a:0:{}'),
	('kp9oskq8cq93sf2hhenbs5ja44',1439001235,'modx.user.contextTokens|a:0:{}'),
	('h391j5baj4bnqm8j5f6r9r92j4',1439002071,'modx.user.contextTokens|a:0:{}'),
	('1hbc01eaqc4q57468h3muq2os0',1439002071,'modx.user.contextTokens|a:0:{}'),
	('ma76454nic7c3tfajej7unvmo0',1439002213,'modx.user.contextTokens|a:0:{}'),
	('5ruep6uv7m5dghh9cudhdn0ff6',1439002398,'modx.user.contextTokens|a:0:{}'),
	('1ltkvboevuucbfcud6jp6u1hj5',1439002398,'modx.user.contextTokens|a:0:{}'),
	('qaovdhrr43p3ondhc7o1h0tkb7',1439002398,'modx.user.contextTokens|a:0:{}'),
	('o1nariraj083e9bausks493273',1439003435,'modx.user.contextTokens|a:0:{}'),
	('bnu04ntb0nbcmisbbmojejhfr5',1439003438,'modx.user.contextTokens|a:0:{}'),
	('jprsm98o8gr2jlo9en7g2i59k0',1439006084,'modx.user.contextTokens|a:0:{}'),
	('n8a7hjb0tve0s68s7gqoe8a1v4',1439006084,'modx.user.contextTokens|a:0:{}'),
	('160vks7s1aii17ib7s2lpii2c0',1439006085,'modx.user.contextTokens|a:0:{}'),
	('lgv5dl8mjsln9eoclu0u0d5994',1439006085,'modx.user.contextTokens|a:0:{}'),
	('i26m4crj76hjhon15h5n951d25',1439006085,'modx.user.contextTokens|a:0:{}'),
	('6112aps3jffe3ej218gf7glp91',1439006250,'modx.user.contextTokens|a:0:{}'),
	('bce15r4qe8idkqqufplkpvepm7',1439006250,'modx.user.contextTokens|a:0:{}'),
	('jnes10csinlkt70a38tktk6vg5',1439006251,'modx.user.contextTokens|a:0:{}'),
	('k843f7g4tcc6s1cpjs0o2i6025',1439006251,'modx.user.contextTokens|a:0:{}'),
	('dlrio83c1cjcer2qskur46l6j4',1439006498,'modx.user.contextTokens|a:0:{}'),
	('b0899idalpfjn0gfg9720uu1h2',1439006846,'modx.user.contextTokens|a:0:{}'),
	('fdfprmhe5fnnomm3l28ms57un4',1439006888,'modx.user.contextTokens|a:0:{}'),
	('a19qhck77hhfqvpisdisbtsf55',1439012470,'modx.user.contextTokens|a:0:{}'),
	('cmqko30d13op0sgv0rmdhv9fl1',1439015570,'modx.user.contextTokens|a:0:{}'),
	('e5k7nrt05m16o60e85upbooab3',1439020487,'modx.user.contextTokens|a:0:{}'),
	('2tak59mm1dfjjic0jf9j2k2p37',1439021589,'modx.user.contextTokens|a:0:{}'),
	('d28omujfhpt5uq0u6ecbmvnt26',1439021589,'modx.user.contextTokens|a:0:{}'),
	('ba3vd16f2lp9rsb5lbl9irboc2',1439021608,'modx.user.contextTokens|a:0:{}'),
	('qmtnnie3htm6vpp2buk0no0ir4',1439021616,'modx.user.contextTokens|a:0:{}'),
	('47i1kt52okru5uhajvvsha6kd6',1439022476,'modx.user.contextTokens|a:0:{}'),
	('1r6tbrjkul9tsietjll24fn8f1',1439022750,'modx.user.contextTokens|a:0:{}'),
	('9sonae0haer597c7t7p5guqp33',1439022750,'modx.user.contextTokens|a:0:{}'),
	('i84dvoktldbuonv6oiivstqfg3',1439030810,'modx.user.contextTokens|a:0:{}'),
	('h1lvborklk6mu6u8ep29de5ue7',1439035445,'modx.user.contextTokens|a:0:{}'),
	('q33b2e35b8dmoigp8le0reljh7',1439035446,'modx.user.contextTokens|a:0:{}'),
	('634fhvsq971id9kmn711q1q9d7',1439035549,'modx.user.contextTokens|a:0:{}'),
	('cce9ri1jn3jioeb6tfvtp2b6h3',1439036949,'modx.user.contextTokens|a:0:{}'),
	('gc3h3irp0mi0lvodaqapn368h2',1439039012,'modx.user.contextTokens|a:0:{}'),
	('se8g46c84r6u2b6sv1354v4c35',1439039563,'modx.user.contextTokens|a:0:{}'),
	('p3qvfst7gm1mo06frud8b6k2f5',1439039563,'modx.user.contextTokens|a:0:{}'),
	('esuqjgtqpl0pp8vdresuu6ml92',1439039564,'modx.user.contextTokens|a:0:{}'),
	('okce108nimi6t6nrvf67q0hq14',1439041891,'modx.user.contextTokens|a:0:{}'),
	('jj40mib44i61dv1pcb2fjut3u0',1439042152,'modx.user.contextTokens|a:0:{}'),
	('i6633tsapoccqfe7lf46fa9c47',1439044129,'modx.user.contextTokens|a:0:{}'),
	('8qikfctelt8mhclllmqtiq0153',1439044132,'modx.user.contextTokens|a:0:{}'),
	('obdgs4mm85ssheppr34nsnm7k2',1439044134,'modx.user.contextTokens|a:0:{}'),
	('4o9cu64o9o0smtd0c1d6qud6k5',1439044140,'modx.user.contextTokens|a:0:{}'),
	('q2g09j7bvqjg11p9jf5dggt106',1439044146,'modx.user.contextTokens|a:0:{}'),
	('3hp2h9do4ii9ivfp4scg61pep4',1439044151,'modx.user.contextTokens|a:0:{}'),
	('b7fvchk0d0f3av0kh9dpf7hnk4',1439044236,'modx.user.contextTokens|a:0:{}'),
	('eo2ht1lgmr1so19ldgi7fo7pd5',1439044528,'modx.user.contextTokens|a:0:{}'),
	('qkh6mnf9tue4eg0fggo8a35042',1439045741,'modx.user.contextTokens|a:0:{}'),
	('99n13iuqrqg8mrbsitlffmete4',1439047042,'modx.user.contextTokens|a:0:{}'),
	('3f2srfchsvl3c18ef12126mlb7',1439047223,'modx.user.contextTokens|a:0:{}'),
	('rvb8l4v6s9d6gmekjuv3hjaio1',1439049349,'modx.user.contextTokens|a:0:{}'),
	('gpldpuc5qvlfu2fkcqd7pddap1',1439049426,'modx.user.contextTokens|a:0:{}'),
	('61m175s6a0g8k5f934apbdrat7',1439049671,'modx.user.contextTokens|a:0:{}'),
	('cgctedl2htnrajh7a8sh97sd34',1439049674,'modx.user.contextTokens|a:0:{}'),
	('p4458i6qtk8ursptdd4q65bbu2',1439049676,'modx.user.contextTokens|a:0:{}'),
	('pk8t44sv9p2f58u2mk0heu19o1',1439053701,'modx.user.contextTokens|a:0:{}'),
	('j7o2aub794if23f2haaaj62ja7',1439054325,'modx.user.contextTokens|a:0:{}'),
	('1v1aeq5dbngl8d1b5bl4re4m90',1439054504,'modx.user.contextTokens|a:0:{}'),
	('q789kqami4tv7ip7jmdgdingq2',1439055300,'modx.user.contextTokens|a:0:{}'),
	('klepsl970k63299920c3j993s1',1439055978,'modx.user.contextTokens|a:0:{}'),
	('75dbmb1lb8b6qs43b58ldi4170',1439056551,'modx.user.contextTokens|a:0:{}'),
	('5n324b1ibfk6jtvc55mhjen1j7',1439057575,'modx.user.contextTokens|a:0:{}'),
	('j170tslc22erl6k5a42p5safv7',1439060422,'modx.user.contextTokens|a:0:{}'),
	('ihoj8hfd6pqer736otno7ua9g1',1439060578,'modx.user.contextTokens|a:0:{}'),
	('13ljl4otikjaqe3e9jfahprd94',1439060890,'modx.user.contextTokens|a:0:{}'),
	('fiq66r22v0n7mae3evjgdq5jd4',1439060933,'modx.user.contextTokens|a:0:{}'),
	('13d3fkdnoguh3f9e15mpi3qca0',1439061817,'modx.user.contextTokens|a:0:{}'),
	('mkdc9hil8nlgr2ac55ga521j61',1439061969,'modx.user.contextTokens|a:0:{}'),
	('lgpnf5bos6rptto9f8fdltb851',1439062000,'modx.user.contextTokens|a:0:{}'),
	('rf31sq4ff2ck0lu0ugb6nhkc96',1439062851,'modx.user.contextTokens|a:0:{}'),
	('tqtktoivak06tg26jjbta08416',1439062996,'modx.user.contextTokens|a:0:{}'),
	('g33aantdkp7ouvvj1n6ooejl84',1439063317,'modx.user.contextTokens|a:0:{}'),
	('oq424gcsps1rnug9ko83rcpcv3',1439063444,'modx.user.contextTokens|a:0:{}'),
	('bmi2451sg6kjiiv7durdde5sd4',1439064504,'modx.user.contextTokens|a:0:{}'),
	('csh3fhsbj0q0qmponrg3tjsv94',1439064687,'modx.user.contextTokens|a:0:{}'),
	('ao019u8gfbr7vre5h8iqs3pse0',1439067254,'modx.user.contextTokens|a:0:{}'),
	('cf9prvjlfs83i93sddesd5gbs0',1439067880,'modx.user.contextTokens|a:0:{}'),
	('u5pr0cvbh9jls9ed6ee7h3me57',1439068299,'modx.user.contextTokens|a:0:{}'),
	('d8hpbadeog56n7oleni4nka235',1439068777,'modx.user.contextTokens|a:0:{}'),
	('b4kts97uut2vcojj8h8c3ehun5',1439069327,'modx.user.contextTokens|a:0:{}'),
	('ahhibmallhvlndjk8resp57nv0',1439071754,'modx.user.contextTokens|a:0:{}'),
	('e855lrpumg7mmagqrhhlc10ng6',1439073016,'modx.user.contextTokens|a:0:{}'),
	('ke78hmj3t8ojfv7a0frq4e0j82',1439073558,'modx.user.contextTokens|a:0:{}'),
	('nl15uonahg7njdtrtj772tspu3',1439076218,'modx.user.contextTokens|a:0:{}'),
	('q23qro5mgb1tbdn0oe9hhmpif5',1439078037,'modx.user.contextTokens|a:0:{}'),
	('9473pcj04f84dfhn6rqrp8brg7',1439080014,'modx.user.contextTokens|a:0:{}'),
	('01500gphnnc9tmdjricq886mf7',1439080014,'modx.user.contextTokens|a:0:{}'),
	('iqss4gtqtt5e33mtcjdalslhp0',1439080320,'modx.user.contextTokens|a:0:{}'),
	('ga4vepco7umuefu1ssrq7uhqu1',1439082133,'modx.user.contextTokens|a:0:{}'),
	('3sq9rctina5ajdm56n70mroqn6',1439083348,'modx.user.contextTokens|a:0:{}'),
	('ltjdetmpc91v65u4gqnq97s985',1439083578,'modx.user.contextTokens|a:0:{}'),
	('nnk2igklmorrapfkgcbi0l28o1',1439083579,'modx.user.contextTokens|a:0:{}'),
	('fpo4tjesjrbn26d4an086tuob6',1439083579,'modx.user.contextTokens|a:0:{}'),
	('j5dvvhp0ofgrucctr5eqr4gjl4',1439084707,'modx.user.contextTokens|a:0:{}'),
	('ggl3qvnqe4pgash68dv34q0ls3',1439085827,'modx.user.contextTokens|a:0:{}'),
	('9upm4ocarge1o6er36kirl5uf3',1439086948,'modx.user.contextTokens|a:0:{}'),
	('m6lerclmnquoh7qgehjp6hoh64',1439088665,'modx.user.contextTokens|a:0:{}'),
	('p3vm7s2667u8g6au15irqk46f3',1439088748,'modx.user.contextTokens|a:0:{}'),
	('ckqo95pr00g0ruehustigcnqi1',1439089003,'modx.user.contextTokens|a:0:{}'),
	('ajsplvk6k5vvgulqm0bk2gq2o4',1439089429,'modx.user.contextTokens|a:0:{}'),
	('a5raivo9taimvugi97te2vv653',1439089432,'modx.user.contextTokens|a:0:{}'),
	('senrsoac08t25k51ilce152al6',1439089434,'modx.user.contextTokens|a:0:{}'),
	('77u58va50vtb6kt85nun1bguv2',1439089436,'modx.user.contextTokens|a:0:{}'),
	('l5jojncb7o92mau7egt14c7lb5',1439089449,'modx.user.contextTokens|a:0:{}'),
	('ersj5eui34cdthoajn8pcvu5t4',1439089456,'modx.user.contextTokens|a:0:{}'),
	('je2mv6jb9fb89ik938p2te0605',1439089470,'modx.user.contextTokens|a:0:{}'),
	('r92v7r8p1dta8t5oa4gsdut794',1439090548,'modx.user.contextTokens|a:0:{}'),
	('ioavgtu73ussgp0i06meqd3gg5',1439091576,'modx.user.contextTokens|a:0:{}'),
	('a89k0ltaii7nirklus9afp8do4',1439091576,'modx.user.contextTokens|a:0:{}'),
	('e2pn5vjlj2vpaqb4875t2oc6m7',1439091576,'modx.user.contextTokens|a:0:{}'),
	('7ehgenndts1n0dttmp7da9llv1',1439091690,'modx.user.contextTokens|a:0:{}'),
	('8ta6t5h754mh6o3s2bcbhdpot4',1439092388,'modx.user.contextTokens|a:0:{}'),
	('ei6fqnkm7p4tjbt0trdcn394v4',1439093710,'modx.user.contextTokens|a:0:{}'),
	('el6p6280cjo4l9hc2ije25v9d3',1439093897,'modx.user.contextTokens|a:0:{}'),
	('h5f0hkv881k8jqpi6fieuu6sv0',1439095183,'modx.user.contextTokens|a:0:{}'),
	('johkbqaj1hfjmk4t02anc01j74',1439095183,'modx.user.contextTokens|a:0:{}'),
	('uafo55pv35dnur3hhqia4gkaf2',1439095733,'modx.user.contextTokens|a:0:{}'),
	('dpfljsufk6ijiqij2vkgdjh093',1439102269,'modx.user.contextTokens|a:0:{}'),
	('ueotos1epbi59rseajedetp507',1439103134,'modx.user.contextTokens|a:0:{}'),
	('92b3f67r4rl49m266d04divjq5',1439105168,'modx.user.contextTokens|a:0:{}'),
	('ngdk5lt2fppgq4jrilmhp5jgi5',1439106375,'modx.user.contextTokens|a:0:{}'),
	('r760s6epako9qr7g60klnmf454',1439110805,'modx.user.contextTokens|a:0:{}'),
	('upfsthfgtrnsrluk4m709qkds1',1439110858,'modx.user.contextTokens|a:0:{}'),
	('ud5qc5eh345ep9cpcd0atu9cf0',1439111545,'modx.user.contextTokens|a:0:{}'),
	('85sn66q39i8hvg8pinsio0uk70',1439111545,'modx.user.contextTokens|a:0:{}'),
	('4u5e1lq953cbm46nbs1p5gq193',1439111695,'modx.user.contextTokens|a:0:{}'),
	('pilt35gduha5g8716dkc12kc50',1439112303,'modx.user.contextTokens|a:0:{}'),
	('rr7vmst2n9aplha6ltgn5q6kl7',1439112331,'modx.user.contextTokens|a:0:{}'),
	('i6alh9tjdufjt0o4o1j5o6nmj5',1439112925,'modx.user.contextTokens|a:0:{}'),
	('7fl91h80itveqo46dc63lpdbj1',1439112926,'modx.user.contextTokens|a:0:{}'),
	('1qitbkjqok5pudt3ltp4pp90q0',1439114776,'modx.user.contextTokens|a:0:{}'),
	('g0qpqclqa8dl8tj8jp8holdag0',1439115816,'modx.user.contextTokens|a:0:{}'),
	('oaih149r5pr5r8lri0afd3nr52',1439116578,'modx.user.contextTokens|a:0:{}'),
	('1as5sas2tlg2ak6nhtbnq0d406',1439116747,'modx.user.contextTokens|a:0:{}'),
	('bp5nqgng9st67o87r5ho8rmk66',1439116789,'modx.user.contextTokens|a:0:{}'),
	('2pe3kgsh3lnf8kbeq83nsp5hf3',1439117112,'modx.user.contextTokens|a:0:{}'),
	('2020fkguqg11sphm0f062g4572',1439117117,'modx.user.contextTokens|a:0:{}'),
	('hfpk63ehq0j94vjsc58kn18l83',1439118998,'modx.user.contextTokens|a:0:{}'),
	('r715afiu8eaibgjd2q2l9shil3',1439121419,'modx.user.contextTokens|a:0:{}'),
	('25081q7pt82ijht65jo6m2b626',1439123426,'modx.user.contextTokens|a:0:{}'),
	('dqmdl4sr3amjnq8fng83rtu2a2',1439124388,'modx.user.contextTokens|a:0:{}'),
	('38tk7s0q05oqhu7d9eqkir19n6',1439124746,'modx.user.contextTokens|a:0:{}'),
	('ae69gm6tirb0c0ge8g3lg4jgk2',1439126860,'modx.user.contextTokens|a:0:{}'),
	('1kcmrif2holh0bbthjcoq7k2s3',1439128425,'modx.user.contextTokens|a:0:{}'),
	('6v1asba4j2a6v3ad1ae5hl5fd3',1439128425,'modx.user.contextTokens|a:0:{}'),
	('298ng8fru4ck59ft67ft8blqp3',1439128622,'modx.user.contextTokens|a:0:{}'),
	('h82dlatj1uf3p9eii1uvlbafj2',1439129610,'modx.user.contextTokens|a:0:{}'),
	('h551ubnedfdrs9k7ge9q7cbsb4',1439129613,'modx.user.contextTokens|a:0:{}'),
	('3q0s8sjjfksv3rq081ufbka9e6',1439130909,'modx.user.contextTokens|a:0:{}'),
	('1oiljm3vjtd9km7g9vdvoon9m3',1439132107,'modx.user.contextTokens|a:0:{}'),
	('gp3bh00tqs4c5ai82gn0ee8590',1439132247,'modx.user.contextTokens|a:0:{}'),
	('ifbs95emadhqpeia8c7ui0ic53',1439132265,'modx.user.contextTokens|a:0:{}'),
	('7auqp89gbloac6t4s48h2jkvq7',1439134720,'modx.user.contextTokens|a:0:{}'),
	('g3ndvu9tvkqg8vr0j4a2s9j1p7',1439135258,'modx.user.contextTokens|a:0:{}'),
	('dgu8gu06s5k1jr5r0n9ig5hm25',1439136121,'modx.user.contextTokens|a:0:{}'),
	('3rtr9ko6jopa2gj6svudql05m5',1439136122,'modx.user.contextTokens|a:0:{}'),
	('cp8sq1h0pivk442u0tfpq97bl1',1439136446,'modx.user.contextTokens|a:0:{}'),
	('36n4f0slrgd7hk688slh6sn781',1439140221,'modx.user.contextTokens|a:0:{}'),
	('5tlf1ok84h3i746qgl8viuni61',1439140303,'modx.user.contextTokens|a:0:{}'),
	('5frrpdqhgvr90itmgg5dnh8724',1439140304,'modx.user.contextTokens|a:0:{}'),
	('0pjmulj2airt3htkae8c2grtj6',1439140326,'modx.user.contextTokens|a:0:{}'),
	('8u64gnq7ovulgb6nihk3eu0ib6',1439140326,'modx.user.contextTokens|a:0:{}'),
	('40mr7ud8r2aog41573j5dbtma7',1439140375,'modx.user.contextTokens|a:0:{}'),
	('ekc2mmava7hs8ge5lt9m75pgu3',1439141349,'modx.user.contextTokens|a:0:{}'),
	('eevj4e1ntcq47hq8ehg3r9knc6',1439143185,'modx.user.contextTokens|a:0:{}'),
	('mqetdrumd6qrgf8ae3r2bpaui3',1439143327,'modx.user.contextTokens|a:0:{}'),
	('bki5gn8c3u298h1pispd0l2ol7',1439143328,'modx.user.contextTokens|a:0:{}'),
	('iqppus6tgmpmtij4abqqr2svj5',1439143861,'modx.user.contextTokens|a:0:{}'),
	('t1tjdh5qh105i837jmhfb6gsn7',1439144413,'modx.user.contextTokens|a:0:{}'),
	('etopt2o4bdq69607bkeo8i6em6',1439144722,'modx.user.contextTokens|a:0:{}'),
	('e9cfpclngbgs8nmeemd117ks36',1439146712,'modx.user.contextTokens|a:0:{}'),
	('s4fmni58013kqijkbmpsl9u0k4',1439146764,'modx.user.contextTokens|a:0:{}'),
	('te13rmtrud67d4pqbik8fbm866',1439147336,'modx.user.contextTokens|a:0:{}'),
	('lvomv05tj53jelcvv5vnofa1d1',1439147925,'modx.user.contextTokens|a:0:{}'),
	('n20t286t0eadvandmleu8tk5t1',1439151315,'modx.user.contextTokens|a:0:{}'),
	('j6vi5ieqkocva225i397i4qoq1',1439158670,'modx.user.contextTokens|a:0:{}'),
	('h3idtbe1nik2ah84somc0t4d35',1439159299,'modx.user.contextTokens|a:0:{}'),
	('990vtmk8io08pi1b4m3spmrd37',1439159320,'modx.user.contextTokens|a:0:{}'),
	('16va3ia4nu7k3t0khgr79jqun4',1439160449,'modx.user.contextTokens|a:0:{}'),
	('rkl3flm62ajolbi75v601ecm43',1439161092,'modx.user.contextTokens|a:0:{}'),
	('9hh8p9cqsds42nkro0d45sg044',1439161969,'modx.user.contextTokens|a:0:{}'),
	('b25s8353u1egbjeeonf7hub2g2',1439161979,'modx.user.contextTokens|a:0:{}'),
	('a9uhst99ktvemaq9q38og86co5',1439162099,'modx.user.contextTokens|a:0:{}'),
	('8v5mo95n7fgpp7e178m2u6v543',1439162845,'modx.user.contextTokens|a:0:{}'),
	('a4p9p235lv4r94cdj8m9oqmth1',1439163014,'modx.user.contextTokens|a:0:{}'),
	('v9o4prbh2ju52e309tn3gdf3l4',1439163039,'modx.user.contextTokens|a:0:{}'),
	('dlsf0j1s3nd3r3ms3ghugkacj7',1439163274,'modx.user.contextTokens|a:0:{}'),
	('c06pllj0vfem6mbb5e58run427',1439163374,'modx.user.contextTokens|a:0:{}'),
	('hekg5g55i78468vtave8uiv2e2',1439163840,'modx.user.contextTokens|a:0:{}'),
	('ib79idqmcq3qjq26bleghiqbp5',1439164036,'modx.user.contextTokens|a:0:{}'),
	('2017l3ui6pb57tpklhlvjcatd6',1439164122,'modx.user.contextTokens|a:0:{}'),
	('luaqcc9u5ovftpmmmn9kobo4b1',1439164123,'modx.user.contextTokens|a:0:{}'),
	('0n8e3rdj50e02d0o4pnte347o1',1439164372,'modx.user.contextTokens|a:0:{}'),
	('03r5bqgcbhu1qqalrmt7r0ptc2',1439164814,'modx.user.contextTokens|a:0:{}'),
	('eftgni4r2c1t06at3g8hbg6nf6',1439165238,'modx.user.contextTokens|a:0:{}'),
	('ffi5ci4tqnudh6hnta581oeto4',1439166001,'modx.user.contextTokens|a:0:{}'),
	('236bbu6ofllf29u7do6h93hcg6',1439166644,'modx.user.contextTokens|a:0:{}'),
	('1cjvkkn8j6k7o9ikl9a43fc4f2',1439166901,'modx.user.contextTokens|a:0:{}'),
	('mboksdoueevo8478t34nr7mkp7',1439168338,'modx.user.contextTokens|a:0:{}'),
	('t3p8sbkp9tgi4a4hthrkrpi722',1439169370,'modx.user.contextTokens|a:0:{}'),
	('321cr4n63nigg5nlq2m29hlcb4',1439169371,'modx.user.contextTokens|a:0:{}'),
	('3a0vgccksm6tdpblqarvv04ku2',1439169620,'modx.user.contextTokens|a:0:{}'),
	('ul0icfn1qm45q1c9rio49q11h7',1439170716,'modx.user.contextTokens|a:0:{}'),
	('a156hm3qjq0gftn2mho4c5r6t5',1439171225,'modx.user.contextTokens|a:0:{}'),
	('9v1koclt384o16bplb1o12ehn2',1439172882,'modx.user.contextTokens|a:0:{}'),
	('l30ojhuhb51s35n2hpj0g6s203',1439174440,'modx.user.contextTokens|a:0:{}'),
	('odddknostd4tocefpookg03en1',1439174984,'modx.user.contextTokens|a:0:{}'),
	('1d7ks066f6h69pkckjajhhdqb7',1439174986,'modx.user.contextTokens|a:0:{}'),
	('uaifvgbdoc5tiheer9qf13mik3',1439174988,'modx.user.contextTokens|a:0:{}'),
	('0bimn3kndvl9se56i0h0ph3ma7',1439176647,'modx.user.contextTokens|a:0:{}'),
	('ajapsd3anga2f4o7n99e6svtl7',1439176647,'modx.user.contextTokens|a:0:{}'),
	('pv2g3lo3i31v01u0u0p7kria92',1439178749,'modx.user.contextTokens|a:0:{}'),
	('j1bgfsevhb01spjhvba8ljv8i1',1439178922,'modx.user.contextTokens|a:0:{}'),
	('mikjutbgsbn62slnip5fi7ldm2',1439178937,'modx.user.contextTokens|a:0:{}'),
	('fl3v4ui4frdck8mtts8n9thr94',1439179427,'modx.user.contextTokens|a:0:{}'),
	('24mnec1iq67kgpoguskqkmq890',1439179649,'modx.user.contextTokens|a:0:{}'),
	('3tkhuuqn2953ks7jjbsdhqnkm7',1439180865,'modx.user.contextTokens|a:0:{}'),
	('k584bkerh2bhcu9ovth8rbnul3',1439183721,'modx.user.contextTokens|a:0:{}'),
	('vkplf4638q6883mf41ulteb144',1439183722,'modx.user.contextTokens|a:0:{}'),
	('4ro25gr71uf4aktk0vevu7f1r6',1439183722,'modx.user.contextTokens|a:0:{}'),
	('h796ihkacsv58j93q5t3a7t6c7',1439184728,'modx.user.contextTokens|a:0:{}'),
	('360seqtprgr1a48imfr3h60oi0',1439184729,'modx.user.contextTokens|a:0:{}'),
	('bmdre06hdvbd77o7hevcpegk00',1439184783,'modx.user.contextTokens|a:0:{}'),
	('fns3c4q7ev2gpktv39c530nll5',1439184861,'modx.user.contextTokens|a:0:{}'),
	('gfb02anuuqtctb8g2reiq56ot0',1439184898,'modx.user.contextTokens|a:0:{}'),
	('hf15gck3tl8uv8pr4hsh9h1g87',1439186879,'modx.user.contextTokens|a:0:{}'),
	('79ddstlo3pethru9t7n7oa2jc0',1439186933,'modx.user.contextTokens|a:0:{}'),
	('pdgd4i1cc7ia58omb957fj56j1',1439188422,'modx.user.contextTokens|a:0:{}'),
	('cvqit83rojhva23d441ihgc3m6',1439188644,'modx.user.contextTokens|a:0:{}'),
	('40tftpsufctsaet01b0s087i35',1439188731,'modx.user.contextTokens|a:0:{}'),
	('1jfjlnn3bitv6j2dqumvtv8e71',1439190281,'modx.user.contextTokens|a:0:{}'),
	('a190d9eukok73edrn7nk1i15b7',1439193247,'modx.user.contextTokens|a:0:{}'),
	('15sml4af32val5uii55vlq2t77',1439194917,'modx.user.contextTokens|a:0:{}'),
	('8iimasnj0he5ue82goh9l6d4a7',1438973426,'modx.user.contextTokens|a:0:{}'),
	('jeokmg69tpr8gogqf4v008kb12',1438973424,'modx.user.contextTokens|a:0:{}'),
	('e8u056uu2q6rr28lt47tqvlf45',1438973379,'modx.user.contextTokens|a:0:{}'),
	('6sgcbpup8i5l6r3apbfe5v6jh1',1438972622,'modx.user.contextTokens|a:0:{}'),
	('re622kfagtrrckevho0v6i2sj6',1438972610,'modx.user.contextTokens|a:0:{}'),
	('d25amadqvphainfnit9ocuaib6',1438972346,'modx.user.contextTokens|a:0:{}'),
	('kled1dojq347n64bueve1d24r4',1438971639,'modx.user.contextTokens|a:0:{}'),
	('a6nc2qhv4ichc4dc0b3enkqo14',1438970928,'modx.user.contextTokens|a:0:{}'),
	('f8idivkc3l8phr68kg3j39jk77',1438970904,'modx.user.contextTokens|a:0:{}'),
	('g4erqgt766879omt4optgp7l53',1438970428,'modx.user.contextTokens|a:0:{}'),
	('jle94t45ufccufoahiers6jm13',1438969360,'modx.user.contextTokens|a:0:{}'),
	('igfeqo6alqampm2f8sdo7npiq4',1438969182,'modx.user.contextTokens|a:0:{}'),
	('dum65h4p2qonomdaskctaqv9h0',1438968628,'modx.user.contextTokens|a:0:{}'),
	('vf48ggrmcf6gssbq32bbm90k55',1438968170,'modx.user.contextTokens|a:0:{}'),
	('2igo00usgarssm4frcmaf00hb4',1438968077,'modx.user.contextTokens|a:0:{}'),
	('3sdfehaj6vec6qior5sh02utb2',1438968075,'modx.user.contextTokens|a:0:{}'),
	('2rhg7ueqkv0ob6hh97em8u23r3',1438967162,'modx.user.contextTokens|a:0:{}'),
	('dedjmg8p0bht347tsseb7oep07',1438964204,'modx.user.contextTokens|a:0:{}'),
	('5uf45psb5c194e0plb6a16vfg7',1438963530,'modx.user.contextTokens|a:0:{}'),
	('rqa2hs0k3vk6ho6uf8n620sfm3',1438962426,'modx.user.contextTokens|a:0:{}'),
	('rf5ogeo6ohboqr3ecqrranbpu2',1438960500,'modx.user.contextTokens|a:0:{}'),
	('p909ghchchm0tfp9o4aq729sm3',1438959829,'modx.user.contextTokens|a:0:{}'),
	('1ike5tjdgm937qpg3vrnpgpdc6',1438958981,'modx.user.contextTokens|a:0:{}'),
	('0bqkis6qenmmmf182fpo0lf7t0',1438958832,'modx.user.contextTokens|a:0:{}'),
	('2h3k2rgtm8qjkkrn9ctltrfkt1',1438958148,'modx.user.contextTokens|a:0:{}'),
	('6g7rvs98acmr7351lphj470rv2',1438957851,'modx.user.contextTokens|a:0:{}'),
	('qj0anejatuluohgldr8uciing7',1438955926,'modx.user.contextTokens|a:0:{}'),
	('8ilg716vab5uqk5tv5s008e9i4',1438955653,'modx.user.contextTokens|a:0:{}'),
	('ogork46j25te8t8ppa3dan66d5',1438955099,'modx.user.contextTokens|a:0:{}'),
	('9m18ajaqrrlr8nvgvtdj54kbc7',1438954971,'modx.user.contextTokens|a:0:{}'),
	('jv2lhvekpot7rl9r6r26a5nj52',1438954436,'modx.user.contextTokens|a:0:{}'),
	('3t9sellip1etbcen4g8kb8r7l3',1438954435,'modx.user.contextTokens|a:0:{}'),
	('g5o4r1uga0os9enavc3na101h4',1438954097,'modx.user.contextTokens|a:0:{}'),
	('ohrd9t60d3non5m4uv2qkeqnp4',1438950002,'modx.user.contextTokens|a:0:{}'),
	('gg983taq1g3i7jqi9sb6h85sk1',1438948726,'modx.user.contextTokens|a:0:{}'),
	('0861aleve3m8uka6oirfp2fa21',1438946789,'modx.user.contextTokens|a:0:{}'),
	('jb9pnbme7flo45d0d2fpb0lpu6',1438943462,'modx.user.contextTokens|a:0:{}'),
	('raip7784i3pucdiqd58vngck01',1438943461,'modx.user.contextTokens|a:0:{}'),
	('enkm2i65unbhs2l6u1nvo1i8m4',1438942287,'modx.user.contextTokens|a:0:{}'),
	('kd3egp5a2vj502ogv1jqevfgv5',1438942287,'modx.user.contextTokens|a:0:{}'),
	('apfhj48ob9a5rfquh808et3u51',1438941666,'modx.user.contextTokens|a:0:{}'),
	('4alv50ro490n8rofh3ov4o4610',1438941663,'modx.user.contextTokens|a:0:{}'),
	('r1in3pf1sd41cqf7ob2l77e9u6',1438940793,'modx.user.contextTokens|a:0:{}'),
	('b3ucfkukb0gu7up6nti0i7usm0',1438940604,'modx.user.contextTokens|a:0:{}'),
	('teer8je5iao5t5cilq4al44a24',1438939238,'modx.user.contextTokens|a:0:{}'),
	('8s87qeqe49e5qgejdo71k8lbc4',1438936247,'modx.user.contextTokens|a:0:{}'),
	('mch5pqk5g3cckrbg9m4orjgf90',1438935126,'modx.user.contextTokens|a:0:{}'),
	('ipdgebks826n1t9u4rqpemtf26',1438934346,'modx.user.contextTokens|a:0:{}'),
	('vegbd35ctbmd6l8o8rqacbobj1',1438934286,'modx.user.contextTokens|a:0:{}'),
	('sgalmr71utk44hi70kb3j2f251',1438931723,'modx.user.contextTokens|a:0:{}'),
	('8gjsqj10mbpmgdgfhc41u2ig87',1438930707,'modx.user.contextTokens|a:0:{}'),
	('1dfkq89l8fcflkfm539gho1u27',1438930269,'modx.user.contextTokens|a:0:{}'),
	('poqdq6sva8vnvobp5hfub5qao3',1438929053,'modx.user.contextTokens|a:0:{}'),
	('t352jsfitutvvk6l18lgo6u5a4',1438928025,'modx.user.contextTokens|a:0:{}'),
	('o2ctjmk5kubdt2bqdf27m8q8p6',1438927152,'modx.user.contextTokens|a:0:{}'),
	('s8cl81lca6qajppftlambrksu5',1438926536,'modx.user.contextTokens|a:0:{}'),
	('dc34tlm8tgpvbr52tmj3b4ph53',1438926535,'modx.user.contextTokens|a:0:{}'),
	('dgjsjaqk0sc92nu19h0qf8bvn0',1438926499,'modx.user.contextTokens|a:0:{}'),
	('2kiniu6e61opar66gv1kebsfh2',1438926476,'modx.user.contextTokens|a:0:{}'),
	('slvvtmh1ukp1s71vlok51q59e0',1438926442,'modx.user.contextTokens|a:0:{}'),
	('1cpbnjko2e313igqdd0g3piv83',1438922605,'modx.user.contextTokens|a:0:{}'),
	('ar94njt59j2o8sck201as0sv82',1438922346,'modx.user.contextTokens|a:0:{}'),
	('oq2vlm6bhj0jcc3cg6g6f7km36',1438922343,'modx.user.contextTokens|a:0:{}'),
	('ljo9fo6f8bj599ponlgm8hj6t7',1438922340,'modx.user.contextTokens|a:0:{}'),
	('jrhvk95qq484oajsqh3qnas676',1438922338,'modx.user.contextTokens|a:0:{}'),
	('h2k9aa0c50oqhspck7k1smam30',1438922335,'modx.user.contextTokens|a:0:{}'),
	('pen5h358fc70ngceuc3647fro1',1438922332,'modx.user.contextTokens|a:0:{}'),
	('hpdmgbn8qkorihvck3o4mr31s4',1438922329,'modx.user.contextTokens|a:0:{}'),
	('o9ec2me64ro633mqb2gbgggd63',1438922327,'modx.user.contextTokens|a:0:{}'),
	('mu44lqel432n0kg6ovrvd9pin5',1438922324,'modx.user.contextTokens|a:0:{}'),
	('6lloeqqbflsqlked6fbksr8bp5',1438922321,'modx.user.contextTokens|a:0:{}'),
	('99ltk671qu8nvttchejksgrc33',1438922316,'modx.user.contextTokens|a:0:{}'),
	('5lqb08ekfd6haao7ko3uuvsqr1',1438922316,'modx.user.contextTokens|a:0:{}'),
	('k1h6i3gqmqeqfjubq4l3a31qm4',1438922241,'modx.user.contextTokens|a:0:{}'),
	('dte4dske68fkpctr8v6203ne92',1438921451,'modx.user.contextTokens|a:0:{}'),
	('7peafthvldaqecttotil0t46f3',1438921089,'modx.user.contextTokens|a:0:{}'),
	('aoj37ka09ebhcjd6spldhv5v20',1438920637,'modx.user.contextTokens|a:0:{}'),
	('54mt9alcejc6cq17ln1tg164u3',1438920548,'modx.user.contextTokens|a:0:{}'),
	('smsn92e0lrietiubseu1ck7ou6',1438920375,'modx.user.contextTokens|a:0:{}'),
	('u9gghg0ikqr86huvions7egl17',1438919905,'modx.user.contextTokens|a:0:{}'),
	('o505p1gg58llldgpio8eatl3f3',1438919230,'modx.user.contextTokens|a:0:{}'),
	('66cfe3doudfltbmnh680r83bp4',1438919097,'modx.user.contextTokens|a:0:{}'),
	('eb8qj73fqvjnc4b1gs8sp8lha1',1438918058,'modx.user.contextTokens|a:0:{}'),
	('8lsjrkj85d31jeqp6kicu44ni0',1438916787,'modx.user.contextTokens|a:0:{}'),
	('to104mrj6rlu2183sb0qdtq0k0',1438915528,'modx.user.contextTokens|a:0:{}'),
	('bebk29erl5ugka8h182cql7n36',1438914032,'modx.user.contextTokens|a:0:{}'),
	('rrsqqgi1h4nsot28m1ctma0am1',1438913511,'modx.user.contextTokens|a:0:{}'),
	('gvusgjpvpg4vl2dh5vate3qdt6',1438913472,'modx.user.contextTokens|a:0:{}'),
	('jehs41i8ki84k7e0kmg0dup557',1438913467,'modx.user.contextTokens|a:0:{}'),
	('k5kr1depimld91lpi5lc1egq84',1438913461,'modx.user.contextTokens|a:0:{}'),
	('ujg9ke6k0jj3lcqftlnqf0ko85',1438913286,'modx.user.contextTokens|a:0:{}'),
	('8pj1mm76bjgrpfpfgidstmms00',1438912521,'modx.user.contextTokens|a:0:{}'),
	('5vhjo9h0c388ectiae11clu2p7',1438911067,'modx.user.contextTokens|a:0:{}'),
	('kb204qiino2o3bovqq7m6b4u22',1438910647,'modx.user.contextTokens|a:0:{}'),
	('essdh47iid55gf3cf4uhvbjp52',1438909364,'modx.user.contextTokens|a:0:{}'),
	('h0pvsock298gqp5vk0v9ovap46',1438908964,'modx.user.contextTokens|a:0:{}'),
	('gf4289ut9hb4pttpj7vpenqf45',1438908485,'modx.user.contextTokens|a:0:{}'),
	('0ppuni48cbgv427pt2fqb1n0b3',1438907835,'modx.user.contextTokens|a:0:{}'),
	('oo549vcvc6kjrt1g6o2ifm6kr2',1438907766,'modx.user.contextTokens|a:0:{}'),
	('4oe2dpbcgnhkqlqm599l6j9dj5',1438907450,'modx.user.contextTokens|a:0:{}'),
	('d4k2grg3j0tc6b9voprmcun6f0',1438907385,'modx.user.contextTokens|a:0:{}'),
	('ko6i48fbe1e17brii6te803rl6',1438907218,'modx.user.contextTokens|a:0:{}'),
	('rodrn5k4eq518sba1snfmi9ck4',1438907159,'modx.user.contextTokens|a:0:{}'),
	('vgho37ua7t3l6rvv6vqmpa6au3',1438906713,'modx.user.contextTokens|a:0:{}'),
	('98rnefv6ek6s6948e9947c2gn2',1438906697,'modx.user.contextTokens|a:0:{}'),
	('uoune63546t105mq6ta4em3ka4',1438906696,'modx.user.contextTokens|a:0:{}'),
	('ifj8obvho7a9pcrk8u2mll0dh5',1438906689,'modx.user.contextTokens|a:0:{}'),
	('4m5cs67vfb51sht5slsccdf6n7',1438906688,'modx.user.contextTokens|a:0:{}'),
	('lpe0hokf7qqminiv5ls731gcg3',1438906664,'modx.user.contextTokens|a:0:{}'),
	('uvqjl9o6mdflo9f3p5csdtpg87',1438906661,'modx.user.contextTokens|a:0:{}'),
	('023vba1cr6epvbks8unn9dko94',1438904884,'modx.user.contextTokens|a:0:{}'),
	('54uh9eveemkl38o8d4nq44dvd1',1438904883,'modx.user.contextTokens|a:0:{}'),
	('i2l2hu45i2382i2k3ih70p55g6',1438902396,'modx.user.contextTokens|a:0:{}'),
	('6k1ndk13apj56km72g9pio0p63',1439749940,'modx.user.contextTokens|a:0:{}'),
	('s1bsvd4biqtlap8b7p4hleou70',1439749347,'modx.user.contextTokens|a:0:{}'),
	('4m46dqt0oa2j4cuqq79vd4fsd1',1439748146,'modx.user.contextTokens|a:0:{}'),
	('kflod4ka7cofqo232ttnhhc113',1439747720,'modx.user.contextTokens|a:0:{}'),
	('clqj8eubq2f7ugu14ihhuerbo5',1439747674,'modx.user.contextTokens|a:0:{}'),
	('1hjjv1gpmpr0bv39urmp2q2q13',1439747671,'modx.user.contextTokens|a:0:{}'),
	('5a23dsitqtcrllag4fepsf9cb5',1439747630,'modx.user.contextTokens|a:0:{}'),
	('uoprbsjg37a61f7ommr1tfqs52',1439747090,'modx.user.contextTokens|a:0:{}'),
	('joppk12bmjjlm2hhj6ra89gnh4',1439741835,'modx.user.contextTokens|a:0:{}'),
	('neahdp217tnkbm1rotuctn6s06',1439739810,'modx.user.contextTokens|a:0:{}'),
	('c3d1t4b5lqdr6sm9ban7ne2q24',1439739251,'modx.user.contextTokens|a:0:{}'),
	('i0fe9v3s4enqobmkcsde90vdo0',1439739182,'modx.user.contextTokens|a:0:{}'),
	('iubkfgsdbpce4bqs6rr9pnvi36',1439738334,'modx.user.contextTokens|a:0:{}'),
	('63jh7018kic8vpt84cbt090is3',1439737514,'modx.user.contextTokens|a:0:{}'),
	('hokv341nnb9sm5qd2in2g655g5',1439737270,'modx.user.contextTokens|a:0:{}'),
	('hql2pgqfe88sjr3nqs63b2e9f3',1439736539,'modx.user.contextTokens|a:0:{}'),
	('i0iohp8c76731rsilmi35179s6',1439736537,'modx.user.contextTokens|a:0:{}'),
	('rs8rujuqsiv1jc2ucmo2ss4i80',1439736154,'modx.user.contextTokens|a:0:{}'),
	('5fsm2emmovjkhbt2h5gth63mj4',1439732106,'modx.user.contextTokens|a:0:{}'),
	('o5qqmucqhr7e29hn1ld8h85su0',1439730736,'modx.user.contextTokens|a:0:{}'),
	('rnirg6afbqomsrr10tq5v47cf3',1439730735,'modx.user.contextTokens|a:0:{}'),
	('h0ubneks13ae46te1ktt8d71v3',1439729422,'modx.user.contextTokens|a:0:{}'),
	('qqj9p1chqgpn128h9eb3geern0',1439728619,'modx.user.contextTokens|a:0:{}'),
	('tk72a2979gse76v26ptgpof287',1439728344,'modx.user.contextTokens|a:0:{}'),
	('gn1e3qu5pids8csbqqf0cftor3',1439724870,'modx.user.contextTokens|a:0:{}'),
	('s5vncm8o794a1md6fi802uoei0',1439723321,'modx.user.contextTokens|a:0:{}'),
	('f7vksjahahpd0hohsn0076ba57',1439721823,'modx.user.contextTokens|a:0:{}'),
	('eb5n1lftgs47hgoh40p70m0d37',1439719482,'modx.user.contextTokens|a:0:{}'),
	('73picte3msp4ihsq850im4i936',1439719480,'modx.user.contextTokens|a:0:{}'),
	('9ttst7l2vrshds41kgcavf1qi4',1439712876,'modx.user.contextTokens|a:0:{}'),
	('fhth1h868g1rdn64o9smvvls43',1439709308,'modx.user.contextTokens|a:0:{}'),
	('1o8pheo8ri3t2q4931ln98ua22',1439704820,'modx.user.contextTokens|a:0:{}'),
	('ohmk4a7smrt0gpevbmmfg11ur5',1439704793,'modx.user.contextTokens|a:0:{}'),
	('n5vm2jvmjf3t2ha4mn091v2au2',1439700730,'modx.user.contextTokens|a:0:{}'),
	('lucvk931srldtlvndhvse76hr1',1439700173,'modx.user.contextTokens|a:0:{}'),
	('mrd6s9k3c9m217gao4cqaq4cm3',1439699525,'modx.user.contextTokens|a:0:{}'),
	('b6pt1rgetsdg5jnsul7auajsv5',1439699335,'modx.user.contextTokens|a:0:{}'),
	('q0bcj7aqd04qcp800aa1njf9a2',1439699215,'modx.user.contextTokens|a:0:{}'),
	('2c5lvc7e82q91m5jt3h6h09rc2',1439699215,'modx.user.contextTokens|a:0:{}'),
	('5a1d8ufg0346tp8s5akc8hj0n4',1439698909,'modx.user.contextTokens|a:0:{}'),
	('08l8jnj0n0vvoj0k31tdjdrme0',1439692445,'modx.user.contextTokens|a:0:{}'),
	('0gukiia4tei3k5uucgj2ht9jb0',1439692369,'modx.user.contextTokens|a:0:{}'),
	('jh88avqjnu0coerdfhckqm2ol3',1439692218,'modx.user.contextTokens|a:0:{}'),
	('0dr7afhrshi4ns4oiml0oj9o84',1439691137,'modx.user.contextTokens|a:0:{}'),
	('6opd81dfpfhsq30edg0j7aauv7',1439690914,'modx.user.contextTokens|a:0:{}'),
	('q81tfve2khoeli612nl744g6g2',1439687506,'modx.user.contextTokens|a:0:{}'),
	('ppbvio88ll3rgh9a5qkm1ugu64',1439687506,'modx.user.contextTokens|a:0:{}'),
	('91q2gk1s0dhlnd1bdocdehnhq5',1439685637,'modx.user.contextTokens|a:0:{}'),
	('2ihiigpkf91cknecgikjg0jge2',1439684190,'modx.user.contextTokens|a:0:{}'),
	('ashsofmdqfrcmu983b9a4r2ar3',1439684015,'modx.user.contextTokens|a:0:{}'),
	('0c4em2dfcngq98d0ve6ovok2k5',1439683812,'modx.user.contextTokens|a:0:{}'),
	('2i9c2jf4qhm4u7dt6a1jp00es6',1439681305,'modx.user.contextTokens|a:0:{}'),
	('7kn8sl7od57snm01ev6itpd5k4',1439680065,'modx.user.contextTokens|a:0:{}'),
	('c8p9r0ftvjcfncsasmgp1k50h7',1439676371,'modx.user.contextTokens|a:0:{}'),
	('ba3rh1jodf947bt3jtbm00scv0',1439675111,'modx.user.contextTokens|a:0:{}'),
	('kkvpb8u4qdlvre6j6e6enk4162',1439671353,'modx.user.contextTokens|a:0:{}'),
	('g91hurp5d0rmccgjep5ojmk3p7',1439671212,'modx.user.contextTokens|a:0:{}'),
	('fo9qcq8nu0m2ub9gnom3346oa2',1439670856,'modx.user.contextTokens|a:0:{}'),
	('89iodlfe6fqld5b4k62oiv42c7',1439670828,'modx.user.contextTokens|a:0:{}'),
	('r72a721sjajf34p0l93rdpsms2',1439670372,'modx.user.contextTokens|a:0:{}'),
	('o6c5alrd9ib09a2h1mts3t1vs4',1439670352,'modx.user.contextTokens|a:0:{}'),
	('jcjva00fm4gf4hj08i34rkv4d0',1439668354,'modx.user.contextTokens|a:0:{}'),
	('3vk26lbms5simnov9448iuq5o5',1439668353,'modx.user.contextTokens|a:0:{}'),
	('mq8k89ckrsb8ud3il2e6furg77',1439668196,'modx.user.contextTokens|a:0:{}'),
	('bpi5d2fg26n2pv60ubnjt7ehq1',1439668196,'modx.user.contextTokens|a:0:{}'),
	('1b6ecv83r296805i9c63j17787',1439667724,'modx.user.contextTokens|a:0:{}'),
	('a9ug10iusees9smi40nhkik1l3',1439666854,'modx.user.contextTokens|a:0:{}'),
	('85gpcb99bpous5oj3i3af54cc1',1439666313,'modx.user.contextTokens|a:0:{}'),
	('rs5rudocev94j48nti4vhsuqm2',1439666105,'modx.user.contextTokens|a:0:{}'),
	('22k538m1rlsk8iqhd64t4paio2',1439665872,'modx.user.contextTokens|a:0:{}'),
	('c7b08rpuetj55ium5j48gpnj25',1439665813,'modx.user.contextTokens|a:0:{}'),
	('5bsm9brk2lenr6m2mm3pcnm6h6',1439665588,'modx.user.contextTokens|a:0:{}'),
	('a40pnpfk2jl0ubnfbd21385sc7',1439662652,'modx.user.contextTokens|a:0:{}'),
	('gi8eo5pgqbkf86u5s1ja7u6ju0',1439662480,'modx.user.contextTokens|a:0:{}'),
	('3n9qmte8khah2apk8pegpc76d6',1439661943,'modx.user.contextTokens|a:0:{}'),
	('8k3rr0iq330ms61ren22b0c891',1439659841,'modx.user.contextTokens|a:0:{}'),
	('3kto8prcv5ie73joijl0cuk7r7',1439657601,'modx.user.contextTokens|a:0:{}'),
	('hecqvah85k282ro4qp1kidq433',1439657601,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('bpdf36p39n355ehphiffrdegg6',1439657601,'modx.user.contextTokens|a:0:{}'),
	('55lmnla7eupnvo4ir6c6la7k57',1439657600,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('82lhd1eshbema11bhslhhip9t2',1439657600,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('c5g5mkiq44o4tg43rctcrvler1',1439657599,'modx.user.contextTokens|a:0:{}'),
	('9jaimblipchmvfg7esfpjmtu11',1439657599,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('7f2fa3ckeq2e08ho0gjg5uahn6',1439657598,'modx.user.contextTokens|a:0:{}'),
	('im9fjvlcl3r9a66etkl1n4a1t4',1439657598,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('o8r0266pfss0jh2sbi405kfmu7',1439657598,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('3miio5cqd37dd4ia2cs7i3klt1',1439657597,'modx.user.contextTokens|a:0:{}'),
	('qfok1nlnrv57kgp3rb70uiugo3',1439657597,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('fi1m8chaqcdqi8ahvuti45fmi7',1439657596,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('nd16khi5pfhqkosvd3qdpn8466',1439657596,'modx.user.contextTokens|a:0:{}'),
	('9jj4gho79b1a9mcrm8ae7debl2',1439657596,'modx.user.contextTokens|a:0:{}'),
	('01iq29p90kijvata6s6k9qs101',1439657596,'modx.user.contextTokens|a:0:{}'),
	('jln0drot092kfm046gcvhl2du6',1439657595,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('77qsko21age3e43jqm5lpago56',1439657593,'modx.user.contextTokens|a:0:{}'),
	('ncpj8ke2auh7d281cod3tdu411',1439657593,'modx.user.contextTokens|a:0:{}'),
	('becqbfqoabvne3hjqekor5chu7',1439657593,'modx.user.contextTokens|a:0:{}'),
	('02phiu0qj30mdhl6pgomelgj41',1439657593,'modx.user.contextTokens|a:0:{}'),
	('c5fersg653j8q95mq71gpvss81',1439657592,'modx.user.contextTokens|a:0:{}'),
	('jifhpf5mftnnj5d7kln6jrhca6',1439657592,'modx.user.contextTokens|a:0:{}'),
	('00094o1ru6ec03eaddaff7s613',1439657592,'modx.user.contextTokens|a:0:{}'),
	('cb70feaj3f5p8o4pnkdgbu3eo6',1439657592,'modx.user.contextTokens|a:0:{}'),
	('lt2ibonq266akfbb996lbm6ej6',1439657591,'modx.user.contextTokens|a:0:{}'),
	('2uj5g2u9q2f4m7dp8kk3tpb5s1',1439657591,'modx.user.contextTokens|a:0:{}'),
	('msq4fpmnu0unqcbrua78jg6aa1',1439657591,'modx.user.contextTokens|a:0:{}'),
	('qp7d2t46tg5kg6nmv9e9i170c4',1439657591,'modx.user.contextTokens|a:0:{}'),
	('qm01113t09rfgrr721o2bkpds6',1439657591,'modx.user.contextTokens|a:0:{}'),
	('leosc58s866sa5csohle4db6v1',1439657590,'modx.user.contextTokens|a:0:{}'),
	('cd87hmv9084ahaogt3o3hupdi0',1439657590,'modx.user.contextTokens|a:0:{}'),
	('7sl2alf151slqtq3i70nhobq53',1439657589,'modx.user.contextTokens|a:0:{}'),
	('hilfkkbvtsetb5d73maon3vs94',1439657589,'modx.user.contextTokens|a:0:{}'),
	('mctba3ck5qh946kp9te1b5aam6',1439657589,'modx.user.contextTokens|a:0:{}'),
	('2t76gati6p7v509sipm89s6s96',1439657589,'modx.user.contextTokens|a:0:{}'),
	('8eang0c4bda0khsi4rn7vn8fp7',1439657589,'modx.user.contextTokens|a:0:{}'),
	('dug70hds78r5mi2u5rfr37s614',1439657588,'modx.user.contextTokens|a:0:{}'),
	('0a0e1dofb3ottc51gd6l4csjl6',1439656762,'modx.user.contextTokens|a:0:{}'),
	('vjtg3aejlatr61vblieeei5gr6',1439655525,'modx.user.contextTokens|a:0:{}'),
	('0ginprp2agqguob4st5tmj75o0',1439654675,'modx.user.contextTokens|a:0:{}'),
	('5c94fnni9kd8ofrkm92js1b672',1439651299,'modx.user.contextTokens|a:0:{}'),
	('tan7cbrc8skqsi2uft423054n5',1439651298,'modx.user.contextTokens|a:0:{}'),
	('s8ajv8cb2ecqh8voflp7clev34',1439650912,'modx.user.contextTokens|a:0:{}'),
	('8mipk8aco8lj8j7hv8v4b57ef3',1439650911,'modx.user.contextTokens|a:0:{}'),
	('fd1lmb9ro5k78lpfd82aklu247',1439649552,'modx.user.contextTokens|a:0:{}'),
	('v0og1c6c46g7df9j4erj9ctek7',1438715071,'modx.user.contextTokens|a:0:{}'),
	('v0j6e22o75org3s6kt95j4fam2',1438715798,'modx.user.contextTokens|a:0:{}'),
	('qorcrom9bqhp0hrai6egsu8tj6',1438715799,'modx.user.contextTokens|a:0:{}'),
	('ig3b8k84phgspfmhjpe14es4t6',1438715954,'modx.user.contextTokens|a:0:{}'),
	('ri9qgm4p95s854gflk8ai7ogs3',1438716013,'modx.user.contextTokens|a:0:{}'),
	('mafq55opmsl8f8iagvhpn1cr84',1438716543,'modx.user.contextTokens|a:0:{}'),
	('feevkp3kn5fn63fqe8fkg7cah0',1438716997,'modx.user.contextTokens|a:0:{}'),
	('i1apbg1sc010fq1c323odrkss7',1438717025,'modx.user.contextTokens|a:0:{}'),
	('nio4v2u0potkmrflamr0un16i4',1438718507,'modx.user.contextTokens|a:0:{}'),
	('q6vd97pu062q9edd00vkqc2ro4',1438718839,'modx.user.contextTokens|a:0:{}'),
	('1uau4ph8isqs17in67sg4l11b4',1438719252,'modx.user.contextTokens|a:0:{}'),
	('64ohaskkgak3oj1ni0413h75n1',1438719790,'modx.user.contextTokens|a:0:{}'),
	('ogqrj1v15nndmdsrfh4hiro6i0',1438721900,'modx.user.contextTokens|a:0:{}'),
	('179aveuuou4kt7olmfdgg198f1',1438722439,'modx.user.contextTokens|a:0:{}'),
	('6e7a7631e3g8kfa56gifl21ql0',1438722546,'modx.user.contextTokens|a:0:{}'),
	('d3jdkkrump9ja85tcja378lmi3',1438722661,'modx.user.contextTokens|a:0:{}'),
	('f6fi4hmna7or4ujt21efbbckn5',1438722848,'modx.user.contextTokens|a:0:{}'),
	('30377m3rec0tcrql56vr3vccl5',1438722855,'modx.user.contextTokens|a:0:{}'),
	('j4ssrp17tivcsuhvktd3rembn1',1438722972,'modx.user.contextTokens|a:0:{}'),
	('gnmaopro1sslrcg0hbjk57aql2',1438722990,'modx.user.contextTokens|a:0:{}'),
	('p795lla50tqo44esrk1sj5i1v0',1438723353,'modx.user.contextTokens|a:0:{}'),
	('99ngritcjvge5ago2v3fkmqut7',1438723591,'modx.user.contextTokens|a:0:{}'),
	('0q2cji2c6jnhc6mcnrqkg1blm5',1438726218,'modx.user.contextTokens|a:0:{}'),
	('nk29982t1e434c34jtfl6dmgb1',1438727833,'modx.user.contextTokens|a:0:{}'),
	('re7ba44sps0jg69hqo1dnp1a41',1438729054,'modx.user.contextTokens|a:0:{}'),
	('568jai9309gp8npl89bjpdacf0',1438731725,'modx.user.contextTokens|a:0:{}'),
	('end1m244uis6i09tdg11fr2770',1438732271,'modx.user.contextTokens|a:0:{}'),
	('huu3kfoctskgbc5vg5tgddug03',1438733546,'modx.user.contextTokens|a:0:{}'),
	('na2bcrmqau2rqej46gk6pgllt7',1438734123,'modx.user.contextTokens|a:0:{}'),
	('hf1fdbmih8aso2qudqljbmfgq7',1438734347,'modx.user.contextTokens|a:0:{}'),
	('5jn060fotrljqtqq6l6qdis7v0',1438734468,'modx.user.contextTokens|a:0:{}'),
	('sugbj8f52ejpq1h2qta63fj425',1438734566,'modx.user.contextTokens|a:0:{}'),
	('1kijjnm0klr36hsnk1171br962',1438738469,'modx.user.contextTokens|a:0:{}'),
	('6pr6772g04su4hcd9hdgesbik0',1438738478,'modx.user.contextTokens|a:0:{}'),
	('rhhl603184saaencgfne1bj6i0',1438739882,'modx.user.contextTokens|a:0:{}'),
	('lgntpa5au4e93aeh793jr2u7j4',1438739883,'modx.user.contextTokens|a:0:{}'),
	('td111alm10r6oe16iumlut63r7',1438741162,'modx.user.contextTokens|a:0:{}'),
	('qaggk7ltas74u8iphua0l2cdf0',1438745588,'modx.user.contextTokens|a:0:{}'),
	('38l2i8qkun0nnrvnot1n4d1vd3',1438748017,'modx.user.contextTokens|a:0:{}'),
	('ue1oujbknn2c84p0vl33vjcjr0',1438748020,'modx.user.contextTokens|a:0:{}'),
	('nujequ4fuq1hja20l74vrhj5c7',1438748021,'modx.user.contextTokens|a:0:{}'),
	('vknk39fah5djqoc1rtmem8c5i2',1438748959,'modx.user.contextTokens|a:0:{}'),
	('oq67h5uas22neo5d2m8afp0ec0',1438750030,'modx.user.contextTokens|a:0:{}'),
	('khmurfpbqknritur5u9v581d84',1438751729,'modx.user.contextTokens|a:0:{}'),
	('oi96ige9sjlqupa6aj9nh23vm4',1439649306,'modx.user.contextTokens|a:0:{}'),
	('1iae70378q9j7f5o1o51qeq925',1439649272,'modx.user.contextTokens|a:0:{}'),
	('flq8e0rk2gq8048gl8uh1rhn21',1439648667,'modx.user.contextTokens|a:0:{}'),
	('36johmrqah8v56068koivkhno7',1439648410,'modx.user.contextTokens|a:0:{}'),
	('vk00f9j1or3rfe1tajonup3sq2',1439648080,'modx.user.contextTokens|a:0:{}'),
	('jbc54v8ql6csqu60bcbtt95di2',1439646235,'modx.user.contextTokens|a:0:{}'),
	('gq09dqgmj6eo3887ia8v9d9642',1439645583,'modx.user.contextTokens|a:0:{}'),
	('r6ic8naihj2v1gkemm0ftgtko3',1439643316,'modx.user.contextTokens|a:0:{}'),
	('0t3hakp9p33e826lj3qb2dkrf6',1439642490,'modx.user.contextTokens|a:0:{}'),
	('p7pa0ag5shjiq5pv7t4smnake6',1439641136,'modx.user.contextTokens|a:0:{}'),
	('u562j6c70k1eip1ql901o1mcb5',1439639158,'modx.user.contextTokens|a:0:{}'),
	('lhth4cf987kaofuo07tceomhe5',1439632777,'modx.user.contextTokens|a:0:{}'),
	('bta0pllrp7phgk00gpdtm1il92',1439631828,'modx.user.contextTokens|a:0:{}'),
	('kmuc2mqqttq9tabh016rjggtk7',1439631714,'modx.user.contextTokens|a:0:{}'),
	('dr4jvcppp39ffljba4b6l8is42',1439630848,'modx.user.contextTokens|a:0:{}'),
	('monbiti2reehf3e04964m8raq2',1439629535,'modx.user.contextTokens|a:0:{}'),
	('7jjs39l52h23r3f9cpjrr2v8g5',1439629476,'modx.user.contextTokens|a:0:{}'),
	('hopb23n7q6t75ddf7ut2lkikg0',1439628550,'modx.user.contextTokens|a:0:{}'),
	('epj8d5gri49kis1653cn9adr92',1439628549,'modx.user.contextTokens|a:0:{}'),
	('nopkjehq46k0jltu89tt507ns3',1439627042,'modx.user.contextTokens|a:0:{}'),
	('gu4nchm35t9lt6vpulm58fb8h1',1439627004,'modx.user.contextTokens|a:0:{}'),
	('6bmtse3e1trtgep9l8op3dc124',1439621266,'modx.user.contextTokens|a:0:{}'),
	('31vg717nn8hb4fu536q22e1cu5',1439621261,'modx.user.contextTokens|a:0:{}'),
	('nju3ojd32i16tljej54hqh9kp2',1439618357,'modx.user.contextTokens|a:0:{}'),
	('jlfqd6ucqpgv4cm8kvqhaq6i51',1439614308,'modx.user.contextTokens|a:0:{}'),
	('ee3iad9k2qkg5t26boai2v0590',1439611324,'modx.user.contextTokens|a:0:{}'),
	('2b72qcmbqup4h99us31kfopj72',1439611322,'modx.user.contextTokens|a:0:{}'),
	('utqgsb8k53p1h0pdhsb2vq3ms5',1439609771,'modx.user.contextTokens|a:0:{}'),
	('52c5n8l7ge3rpcid1443ejfep7',1439609319,'modx.user.contextTokens|a:0:{}'),
	('r6143vlt9hodri06i1sejofj02',1439608607,'modx.user.contextTokens|a:0:{}'),
	('mqbgn2h3f2i3tc83j85eahd6o6',1439608334,'modx.user.contextTokens|a:0:{}'),
	('e7qe7ac54v99h8pdm0tit8aag1',1439605824,'modx.user.contextTokens|a:0:{}'),
	('pu22o11h9uahc2r8ku7bsa5232',1439605507,'modx.user.contextTokens|a:0:{}'),
	('fg2qkbvpdt9ojie74ubf1ho050',1439605506,'modx.user.contextTokens|a:0:{}'),
	('np0g7bihd7ofdqahhj1rf2man1',1439603955,'modx.user.contextTokens|a:0:{}'),
	('1vp2d718jivbt10anrd9c9lg47',1439600072,'modx.user.contextTokens|a:0:{}'),
	('2tocnct8nsalu5na8h21dr6e13',1439600072,'modx.user.contextTokens|a:0:{}'),
	('c0itk5io2dfvulifpq6c9ugh90',1439595430,'modx.user.contextTokens|a:0:{}'),
	('466gcsb7eauujf3bi57kfhdhs2',1439595315,'modx.user.contextTokens|a:0:{}'),
	('4i5hk2269dmuvv7cu555pon855',1439594820,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('e5ejgr6jpjag8at55qpu7r0a46',1439594473,'modx.user.contextTokens|a:0:{}'),
	('d3d8hb5ijbinff9s6ud7r2hqc7',1439594421,'modx.user.contextTokens|a:0:{}'),
	('ou8kivd5pkgnu0orfvguj2bo73',1439594417,'modx.user.contextTokens|a:0:{}'),
	('ta4erkhv3ucd233r57t7ekl4c7',1439594263,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('47evvelne0ksj0q4gag9garhb7',1439594084,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('jke92vcasmp0jp9nk24k81ni63',1439593306,'modx.user.contextTokens|a:0:{}'),
	('n49jahacfgidjeq5gnfdfjsr14',1439593245,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('5eq5bhrp6oe7vfhq7brqdm9kt1',1439593088,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('28dt7vvt3kga0p0da7cgnacoe2',1439593073,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('06p3hr0rpovsqcoof4ff73mog3',1439593169,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('ohi38mou6nh6erdiq8nhvbdfn0',1439592834,'modx.user.contextTokens|a:0:{}'),
	('2icogujcrl5f6qb83vp4in8tv0',1439588760,'modx.user.contextTokens|a:0:{}'),
	('jmpj9fgbrr5pcmd8in609g8oo5',1439588731,'modx.user.contextTokens|a:0:{}'),
	('jfhiu8ti79d9p4q1liji5vek82',1439588542,'modx.user.contextTokens|a:0:{}'),
	('j2vm15nj284njrv7qtqka799j1',1439585785,'modx.user.contextTokens|a:0:{}'),
	('8pridkbanoiqol2pbkbhj3n556',1439585659,'modx.user.contextTokens|a:0:{}'),
	('jftqgcpc79mlk71ri7mgdv3sr1',1439585528,'modx.user.contextTokens|a:0:{}'),
	('a15vdgt8msqdrea7chko3vqde4',1439585356,'modx.user.contextTokens|a:0:{}'),
	('5djb548u41mnc5tcgc0rei6ca5',1439584905,'modx.user.contextTokens|a:0:{}'),
	('878t5st6ik3n5jojt01fkrj8r0',1439584901,'modx.user.contextTokens|a:0:{}'),
	('i6qh23kihbmu8aiergfk6lukm3',1439582989,'modx.user.contextTokens|a:0:{}'),
	('h79d46j4hs5nc4q5in6fuanlg7',1439582988,'modx.user.contextTokens|a:0:{}'),
	('dh6jdtt9r3g7ctgs06611coam6',1439582038,'modx.user.contextTokens|a:0:{}'),
	('5ti1o6dlou2egkchbvqet2l261',1439581308,'modx.user.contextTokens|a:0:{}'),
	('m9r4tlvm71e9jbabc317fo9nj4',1439580462,'modx.user.contextTokens|a:0:{}'),
	('iilv9fsnl16r2ogo8fm9ghj0o3',1439579822,'modx.user.contextTokens|a:0:{}'),
	('1fthc19memu9f4137o6oh8m370',1439579615,'modx.user.contextTokens|a:0:{}'),
	('r4p4p1rvg03atn2jl2gcj97ir4',1439577720,'modx.user.contextTokens|a:0:{}'),
	('camcmkoji5iqe9p6ks4foshl24',1439577559,'modx.user.contextTokens|a:0:{}'),
	('9l015ef08a6f2rtffcvj9fiq95',1439574333,'modx.user.contextTokens|a:0:{}'),
	('5814r1paep5lkg0qkos1t8fct1',1439575002,'modx.user.contextTokens|a:0:{}'),
	('cnvh4o90buv5v12lpof919te83',1439574230,'modx.user.contextTokens|a:0:{}'),
	('2v28b2las3susc4mqadjakv2j1',1439573760,'modx.user.contextTokens|a:0:{}'),
	('8lff9nkj9dopudc1a7vjogaff0',1439573754,'modx.user.contextTokens|a:0:{}'),
	('j0htvm9mn36nrluue153qoj2u7',1439573746,'modx.user.contextTokens|a:0:{}'),
	('gef9cal7hjudogubp572o0km03',1439573703,'modx.user.contextTokens|a:0:{}'),
	('e5qt1hglhu5hq03ikm7toes7h3',1439573245,'modx.user.contextTokens|a:0:{}'),
	('ubatp7rs3mvscpsh87faar4d41',1439573219,'modx.user.contextTokens|a:0:{}'),
	('k43o2rg0g0fshidhdnqhvpp9q0',1439571632,'modx.user.contextTokens|a:0:{}'),
	('en46v2g3bnd9l9cscup7g542l6',1439569797,'modx.user.contextTokens|a:0:{}'),
	('jd6phlv6d9pe2v9kldjpoops67',1439569771,'modx.user.contextTokens|a:0:{}'),
	('8hhujaf0v53i8d61d68eq43op1',1439569768,'modx.user.contextTokens|a:0:{}'),
	('l4c786ritpni6o4afilcncsm44',1439569767,'modx.user.contextTokens|a:0:{}'),
	('lm9b6u0cpi7iitk9an77orfo50',1439569765,'modx.user.contextTokens|a:0:{}'),
	('vhcp0mem19pgptqhap545cr506',1439569440,'modx.user.contextTokens|a:0:{}'),
	('p7d7aabqi0akr2dp0eoe98utc3',1439567489,'modx.user.contextTokens|a:0:{}'),
	('1hd4el2us8scgeluackv203cu1',1439567445,'modx.user.contextTokens|a:0:{}'),
	('7h88u5n292qeiiv3e9lahaoc54',1439567436,'modx.user.contextTokens|a:0:{}'),
	('dgco13qsgnf0objnaa35v35gv6',1439567427,'modx.user.contextTokens|a:0:{}'),
	('0o1aenfr0polh4loqik02ia390',1439567422,'modx.user.contextTokens|a:0:{}'),
	('fp227543d8qsobkdoks3164k21',1439566984,'modx.user.contextTokens|a:0:{}'),
	('qth0nhejf1o8it2ocduhgtbs05',1439566824,'modx.user.contextTokens|a:0:{}'),
	('ao44mrdshf70o4p7001u2hihl5',1439565650,'modx.user.contextTokens|a:0:{}'),
	('s9pch0ja6pf9n2svpnl95t6nv4',1439565596,'modx.user.contextTokens|a:0:{}'),
	('kep29759h811aj0jbrpi3ivfm1',1439562905,'modx.user.contextTokens|a:0:{}'),
	('2r97jlojd8seesids6srj22vh6',1439562679,'modx.user.contextTokens|a:0:{}'),
	('r857de0s2hm40aguqgse56lkn7',1439562174,'modx.user.contextTokens|a:0:{}'),
	('j5loojfb2jjinqgn6bp1bpf845',1439562046,'modx.user.contextTokens|a:0:{}'),
	('7jr7ss0kqmoe6nvd4cladsthb7',1439561902,'modx.user.contextTokens|a:0:{}'),
	('7vrtsirs681ormr12uc1kgfsi6',1439560900,'modx.user.contextTokens|a:0:{}'),
	('66l1m2riev14v1gljo47ch5m64',1439558490,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('i34o4ebejitu4sk8cp6m9854q2',1439558283,'modx.user.contextTokens|a:0:{}'),
	('i6unj91ifc287t322q46na8446',1439557636,'modx.user.contextTokens|a:0:{}'),
	('5urq71l97640m5al95ou4rslr7',1439554162,'modx.user.contextTokens|a:0:{}'),
	('eblg3s81kavl2885kcoso497k6',1439551022,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('qpqkt0oai8lgut5cd95npeup96',1439551021,'modx.user.contextTokens|a:0:{}'),
	('qut5s2t4jpqppa9q890esunm85',1439550999,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('cm68d2bsl78eh73v9ah2g06oi1',1439550994,'modx.user.contextTokens|a:0:{}'),
	('1m9iksts35hntjjqi6afj7j2m0',1439550664,'modx.user.contextTokens|a:0:{}'),
	('6eiea4eprhfaqajn76946soos5',1439547844,'modx.user.contextTokens|a:0:{}'),
	('d5011bmqqind8bnsm0c7j45684',1439547794,'modx.user.contextTokens|a:0:{}'),
	('7t7lao7fnla62rgd3p90r0o190',1439547687,'modx.user.contextTokens|a:0:{}'),
	('e9rub7e8gh02ptn78p087q9e23',1439545557,'modx.user.contextTokens|a:0:{}'),
	('npooeat6vvgauo9i397d6vajq1',1439545450,'modx.user.contextTokens|a:0:{}'),
	('bt7um7tj0r4dc2j2hpcvpbv5u5',1439545217,'modx.user.contextTokens|a:0:{}'),
	('bstsanlisds38ckmij5j0vkqq2',1439543033,'modx.user.contextTokens|a:0:{}'),
	('2r71570f6qnv3a41qcfuvc4715',1439542546,'modx.user.contextTokens|a:0:{}'),
	('sdm4a6hk6qqi9eoue4eslpvf84',1439540664,'modx.user.contextTokens|a:0:{}'),
	('ptm0em4mi7fr4kfelvt16bi0m0',1439535707,'modx.user.contextTokens|a:0:{}'),
	('2l4ehr4mq2lf57i2vk09k864i2',1439534503,'modx.user.contextTokens|a:0:{}'),
	('b1chn4ejrd03afnc1fu831m890',1439530686,'modx.user.contextTokens|a:0:{}'),
	('n3uo393dm72pm1e87jb3jt69i7',1439530626,'modx.user.contextTokens|a:0:{}'),
	('1sbfc3ma1250uktc8cbra4n2e0',1439527669,'modx.user.contextTokens|a:0:{}'),
	('omru72qg1hojipnldht3noi484',1439527064,'modx.user.contextTokens|a:0:{}'),
	('mpjjnqoq5ur802fhov3gdktfm1',1439526643,'modx.user.contextTokens|a:0:{}'),
	('0pjkc86garreqr9k7mgchc7666',1439526483,'modx.user.contextTokens|a:0:{}'),
	('46qqhii9eoogklu5gvk4dd1ii1',1439524246,'modx.user.contextTokens|a:0:{}'),
	('61afg4dlj1057et1dsauh9f4m0',1439523132,'modx.user.contextTokens|a:0:{}'),
	('r8kgfg2e1e0fhfsf3m035stb76',1439522013,'modx.user.contextTokens|a:0:{}'),
	('33vv61qsslue4q8t4dbktl9al6',1439518783,'modx.user.contextTokens|a:0:{}'),
	('oshl1bjpi3q8221a0lg2nvlk31',1439518782,'modx.user.contextTokens|a:0:{}'),
	('tcbvffj4sal0evmbc0uvj1c3b3',1439511072,'modx.user.contextTokens|a:0:{}'),
	('sqddgmldph7igfam98gd4gdn21',1439512529,'modx.user.contextTokens|a:0:{}'),
	('qqh750apr7dscpgl6ldoo16uf1',1439512754,'modx.user.contextTokens|a:0:{}'),
	('ai4k9btk1r5l5povhvdf97sr74',1439514469,'modx.user.contextTokens|a:0:{}'),
	('3p6oi39gt51cifpejk4o0ifr27',1439515271,'modx.user.contextTokens|a:0:{}'),
	('geqggrn8u20vu1fhsoid71pfg0',1439516570,'modx.user.contextTokens|a:0:{}'),
	('8mou0ki8h0f18tdfvupfs61h32',1439518604,'modx.user.contextTokens|a:0:{}'),
	('9l8b2jlt8oclr6297ath28g166',1439509459,'modx.user.contextTokens|a:0:{}'),
	('ec7j50nvokr97mbu7mc3eu3gt1',1439509458,'modx.user.contextTokens|a:0:{}'),
	('vqoaaeci6nd00f2mue524suml2',1439509245,'modx.user.contextTokens|a:0:{}'),
	('pjga14t6au29n71rvali759o73',1439507365,'modx.user.contextTokens|a:0:{}'),
	('s96gr5olndp3d5ksqjegi8o5l3',1439506787,'modx.user.contextTokens|a:0:{}'),
	('8vev6nbu9l3cg4htrccgid0ol5',1439503893,'modx.user.contextTokens|a:0:{}'),
	('a0uktup5ts2eajv7etptvomf66',1439500698,'modx.user.contextTokens|a:0:{}'),
	('colievvhflci0b9dnbtefs46n0',1439499824,'modx.user.contextTokens|a:0:{}'),
	('vrqpqk13i9aq68iilit9p4e1r7',1439499119,'modx.user.contextTokens|a:0:{}'),
	('jvnf7rooh0em3uhrmuo8vsvmf6',1439499119,'modx.user.contextTokens|a:0:{}'),
	('cabsghcbsn0acishfcvp40cd56',1439497165,'modx.user.contextTokens|a:0:{}'),
	('ofgq6kk0ggib9ede77bdj15i37',1439496990,'modx.user.contextTokens|a:0:{}'),
	('p6rlh9m23e0ne0sl26ttlsv4k6',1439494863,'modx.user.contextTokens|a:0:{}'),
	('gapu93cn6ojhm685vkidkcaqg2',1439493928,'modx.user.contextTokens|a:0:{}'),
	('2rj05s65vfphif1lhgqismfb46',1439493880,'modx.user.contextTokens|a:0:{}'),
	('ra03gv2e516idkt3uu32dkat94',1439493422,'modx.user.contextTokens|a:0:{}'),
	('nt09jm8uurh19a9jmh5a1hh6n5',1439492885,'modx.user.contextTokens|a:0:{}'),
	('uonvueeakn7o02fgep0038gui6',1439492640,'modx.user.contextTokens|a:0:{}'),
	('kul9meshupt5gv6fl80317kcm4',1439492097,'modx.user.contextTokens|a:0:{}'),
	('n9gdu1pvrba21vrtq1uddkole7',1439491519,'modx.user.contextTokens|a:0:{}'),
	('s8gafggbko3ru6lub5ng09de24',1439490547,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('da0fgg59m132ntahmjcqip3j66',1439488277,'modx.user.contextTokens|a:0:{}'),
	('9ghed9jn4lalvjqkg559rk04i3',1439486327,'modx.user.contextTokens|a:0:{}'),
	('l412mg1461ks1co6jc1ffp8893',1439485090,'modx.user.contextTokens|a:0:{}'),
	('jm82tns24vgv0c0lf5uu5mdtl7',1439484626,'modx.user.contextTokens|a:0:{}'),
	('9rlqqguiuhifriafgf9uh3fkg6',1439484082,'modx.user.contextTokens|a:0:{}'),
	('mhuvunql5en43ibg37tfmp59d3',1439483768,'modx.user.contextTokens|a:0:{}'),
	('pjcbei7t7o7qbml2s8trsl4ft1',1439483425,'modx.user.contextTokens|a:0:{}'),
	('muj7mdjg2nu9vssi91ntr4ke15',1439482319,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('u02vslfkbf420nvrfeslgena26',1439482304,'modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}modx.user.contextTokens|a:0:{}'),
	('pdva4ke8ie9h2tm53mj833sua7',1439482234,'modx.user.contextTokens|a:0:{}modx.user.0.resourceGroups|a:1:{s:3:\"web\";a:0:{}}modx.user.0.attributes|a:1:{s:3:\"web\";a:4:{s:16:\"modAccessContext\";a:1:{s:3:\"web\";a:1:{i:0;a:3:{s:9:\"principal\";i:0;s:9:\"authority\";s:1:\"0\";s:6:\"policy\";a:1:{s:4:\"load\";b:1;}}}}s:22:\"modAccessResourceGroup\";a:0:{}s:17:\"modAccessCategory\";a:0:{}s:28:\"sources.modAccessMediaSource\";a:0:{}}}'),
	('6cejnq0akla3150t586i193j53',1439481199,'modx.user.contextTokens|a:0:{}'),
	('4tmrfun8mu18pkvt3i9hkjg2m5',1439480583,'modx.user.contextTokens|a:0:{}'),
	('atbf8srajegjfmunn0vjal6k86',1439479276,'modx.user.contextTokens|a:0:{}'),
	('fputjakukhplj8hrp0ai8fvl63',1439479102,'modx.user.contextTokens|a:0:{}'),
	('9gegst2d7fidkf7lnsteqlgdt1',1439478975,'modx.user.contextTokens|a:0:{}'),
	('cr940msqbq9lmg3rovgqj6nq83',1439478975,'modx.user.contextTokens|a:0:{}'),
	('hsjtvqibnraa4dpvbonnnt5422',1439478677,'modx.user.contextTokens|a:0:{}'),
	('ffu5rvf6j0gi8p4rsfq2vt3sj4',1439477022,'modx.user.contextTokens|a:0:{}'),
	('0fan7em14dnolf07ilp4vtnja4',1439476549,'modx.user.contextTokens|a:0:{}'),
	('gi9qa2ef2utrdatnlci0iqdg21',1439474990,'modx.user.contextTokens|a:0:{}'),
	('1he51etrbfbo2dgmh85dn8ms05',1439474541,'modx.user.contextTokens|a:0:{}'),
	('5vnmtn850hgba8c21q0quclb22',1439473106,'modx.user.contextTokens|a:0:{}'),
	('c2bllq884g47m76lo7438gcjq1',1439473093,'modx.user.contextTokens|a:0:{}'),
	('p849qelqli97hr6bn0g7b7jsq5',1439472745,'modx.user.contextTokens|a:0:{}'),
	('p91nrojdg8dlnhj8urh8dpcdi4',1439472488,'modx.user.contextTokens|a:0:{}'),
	('qk94msp7m77bpe7igp96k2g1i5',1439471805,'modx.user.contextTokens|a:0:{}'),
	('e9v55kq67padceia535bk9f3i2',1439471483,'modx.user.contextTokens|a:0:{}'),
	('f5ivt4o5f2dot57hl1630237e1',1439471482,'modx.user.contextTokens|a:0:{}'),
	('i2f06rgrrsmibu2bb79qvdoj16',1439471384,'modx.user.contextTokens|a:0:{}'),
	('u124r9hvmt1ctbidlpa3489rv2',1439469283,'modx.user.contextTokens|a:0:{}'),
	('1nqm4libn428l9q7umi868oaj5',1439468188,'modx.user.contextTokens|a:0:{}'),
	('23ld8ujpu89avh5ctq9hcmqfc2',1439468157,'modx.user.contextTokens|a:0:{}'),
	('45j7f9q1q0svfpiarnof7r01u2',1439466144,'modx.user.contextTokens|a:0:{}'),
	('8ncoi7p21mimh8s1m6q7j32p03',1439462228,'modx.user.contextTokens|a:0:{}'),
	('n8ujsrgqba3akthtmeibdea7v2',1439455966,'modx.user.contextTokens|a:0:{}'),
	('2g3ku0j2datjp8qikumeruo4j3',1439455858,'modx.user.contextTokens|a:0:{}'),
	('bc5psta6v0o3act24l12djk6g5',1439455818,'modx.user.contextTokens|a:0:{}'),
	('kko24v5uq39fcdg9igpm07ido0',1439453915,'modx.user.contextTokens|a:0:{}'),
	('8vv326o9us5bfrriso5st4sjb3',1439453910,'modx.user.contextTokens|a:0:{}'),
	('ajfk18eecu4rpuc4natjekrrk1',1439453006,'modx.user.contextTokens|a:0:{}'),
	('30j7nk7gp080odl41oqn31j315',1439452994,'modx.user.contextTokens|a:0:{}'),
	('of463jiu4mg44jg78mhs8tvs67',1439450696,'modx.user.contextTokens|a:0:{}'),
	('5k5pc4jq3ckr7qtn441q1b58c3',1439447647,'modx.user.contextTokens|a:0:{}'),
	('gg9ellgba1ulu31l9efaovcu10',1439444929,'modx.user.contextTokens|a:0:{}'),
	('0nevv5kukk08gh06jt3gf96eq4',1439444777,'modx.user.contextTokens|a:0:{}'),
	('92cjqi9edlmnqo50fnv3jbp3o5',1439444408,'modx.user.contextTokens|a:0:{}'),
	('kd8cp2oqjhc05jvrhjoebt52b1',1439443504,'modx.user.contextTokens|a:0:{}'),
	('7qghftidb7bjp6dgbs6l42nmf6',1439443502,'modx.user.contextTokens|a:0:{}'),
	('603j2pah3b9k8l05qr44j7s364',1439442289,'modx.user.contextTokens|a:0:{}'),
	('f66vo8v6tvhc3h8dl5dudng954',1439442288,'modx.user.contextTokens|a:0:{}'),
	('rcnjctf6gfl1v0g6jdkp1ihnh7',1439440573,'modx.user.contextTokens|a:0:{}'),
	('9qmbthqkqc5ejt2o3ap5sr54c4',1439440572,'modx.user.contextTokens|a:0:{}'),
	('gh9akguluefa41toq7rql0v173',1439440570,'modx.user.contextTokens|a:0:{}'),
	('es5au4demj2gh9d949q9sm4sg5',1439440568,'modx.user.contextTokens|a:0:{}'),
	('5it5nmob48b19krhh9dodgvom6',1439440566,'modx.user.contextTokens|a:0:{}'),
	('pc8mt4ge56fqc566bhgscs3su3',1439440563,'modx.user.contextTokens|a:0:{}'),
	('ckb4j6fkj0jgfi0ltodfvqeau0',1439440561,'modx.user.contextTokens|a:0:{}'),
	('7ua5sbrqqrctipajk67r98d9f2',1439440558,'modx.user.contextTokens|a:0:{}'),
	('poedfied27i7ocn68hjl2pc0q0',1439440557,'modx.user.contextTokens|a:0:{}'),
	('vsi7u08nhngv7sbe5eeou1kfu5',1439440555,'modx.user.contextTokens|a:0:{}'),
	('tglchtk38hfnq8gik0fenulbi3',1439440553,'modx.user.contextTokens|a:0:{}'),
	('idn2dtbn3glp7jd0lec0qvlks0',1439440504,'modx.user.contextTokens|a:0:{}'),
	('7kpn60n7upco0npplpjn75mv86',1439440068,'modx.user.contextTokens|a:0:{}'),
	('1ijmiuqculaj3fktuu8cctr390',1439438722,'modx.user.contextTokens|a:0:{}'),
	('62ilb13le2tvg1n7dkemft1d37',1439438719,'modx.user.contextTokens|a:0:{}'),
	('7f1mf0pg2s3he0b12u40htosj1',1439438716,'modx.user.contextTokens|a:0:{}'),
	('trmfdr3qq12573hcfr86d8bgt6',1439438714,'modx.user.contextTokens|a:0:{}'),
	('dounpvsqjk5ie9mshmtd4hlef0',1439438712,'modx.user.contextTokens|a:0:{}'),
	('taa7mevpn0avm7o9t812s9qeu7',1439436362,'modx.user.contextTokens|a:0:{}'),
	('i9heu5e642v63p43vm06stn5c6',1439436229,'modx.user.contextTokens|a:0:{}'),
	('d012ko27iuktb034eufeql1r74',1439435009,'modx.user.contextTokens|a:0:{}'),
	('5kutfmj5akp0nvushu0bd1cet3',1439434695,'modx.user.contextTokens|a:0:{}'),
	('odicecbeqcmjvrbg9ddtr4ovq6',1439432277,'modx.user.contextTokens|a:0:{}'),
	('c00nsp11p71n2ebjvhfaqg2ss2',1439430847,'modx.user.contextTokens|a:0:{}'),
	('3queue5c9anvijtneh0j1uuel1',1439430627,'modx.user.contextTokens|a:0:{}'),
	('c65mlratss1irpmh6rjb1pmm66',1439429249,'modx.user.contextTokens|a:0:{}'),
	('1bvhuc44pfjl0l1dbehk010s87',1439429248,'modx.user.contextTokens|a:0:{}'),
	('ckbth66qk3sfegopept4sq5e84',1439426468,'modx.user.contextTokens|a:0:{}'),
	('1p0ls7f4ojh07at1t07qif0r25',1439424701,'modx.user.contextTokens|a:0:{}'),
	('d8ripkev8ednujsc49vg5g2kq5',1439424562,'modx.user.contextTokens|a:0:{}'),
	('7gpnbsdrvkao2isjbeags2k0g2',1439423870,'modx.user.contextTokens|a:0:{}'),
	('tmln1de29o4stu5bfoi03tg6k6',1439423867,'modx.user.contextTokens|a:0:{}'),
	('o6gg27bp88glsjgmd0aicro666',1439422760,'modx.user.contextTokens|a:0:{}'),
	('7a3hfhtchs025e80ss8gtplj34',1439420416,'modx.user.contextTokens|a:0:{}'),
	('349omh01t87anfni4igivil047',1439419834,'modx.user.contextTokens|a:0:{}'),
	('8coi23qb09vgkfi6vmtjrhhnr2',1439419575,'modx.user.contextTokens|a:0:{}'),
	('shthodntu9f3cofvoq0156er93',1439419574,'modx.user.contextTokens|a:0:{}'),
	('frj82fjg01urriqrkuvk288qh1',1439419529,'modx.user.contextTokens|a:0:{}'),
	('0t24m88kg3fcmiggl9hqbgbmm7',1439419507,'modx.user.contextTokens|a:0:{}'),
	('khgta8oaoqtd362b5pn1s0pa02',1439419497,'modx.user.contextTokens|a:0:{}'),
	('afacuudgkfg00f5cbjb6duju66',1439419478,'modx.user.contextTokens|a:0:{}'),
	('g4bmq4nrnbi1bol1j0mgv5asp2',1439419450,'modx.user.contextTokens|a:0:{}'),
	('imtob7eajebhgcippeqdds0k82',1439419224,'modx.user.contextTokens|a:0:{}'),
	('f5l9v07nf7tp5petsno3ldequ3',1439419223,'modx.user.contextTokens|a:0:{}'),
	('b7q5pf9fufaait5ecn4lsfrl25',1439419215,'modx.user.contextTokens|a:0:{}'),
	('qq2sdc19krsjl3979b55q28eh5',1439419214,'modx.user.contextTokens|a:0:{}'),
	('nqq2buhvh3jidrp14p0fjib9f1',1439419205,'modx.user.contextTokens|a:0:{}'),
	('puut78f8e5v447mjltrmhkleq5',1439419108,'modx.user.contextTokens|a:0:{}'),
	('n00h2spht9l4u32qe0o1nulrb0',1439418930,'modx.user.contextTokens|a:0:{}'),
	('7j7bip5ppj4enjmggldn6484g0',1439418502,'modx.user.contextTokens|a:0:{}'),
	('c1u61ungjgtjff15seph4rq8m2',1439418298,'modx.user.contextTokens|a:0:{}'),
	('osck6rabs601j8lcef4qtr1mv3',1439417756,'modx.user.contextTokens|a:0:{}'),
	('525mced0ra76bagnto5ukrbbs0',1439412663,'modx.user.contextTokens|a:0:{}'),
	('9s3mjtvpudhcn3q4ghhp7ma0n1',1439412088,'modx.user.contextTokens|a:0:{}'),
	('vqr3d3id18vk756r71q6fpoaf4',1439409962,'modx.user.contextTokens|a:0:{}'),
	('6jgvm1n7skf7ct2u7t9ub9i2b1',1439409732,'modx.user.contextTokens|a:0:{}'),
	('80mn13frv4nmkjmpvm1n02ua63',1439408788,'modx.user.contextTokens|a:0:{}'),
	('d9647kt8njejtqossajm1v4d35',1439408787,'modx.user.contextTokens|a:0:{}'),
	('7b65tacr8dpbf6l44el6kheeq0',1439408725,'modx.user.contextTokens|a:0:{}'),
	('ee6b2ujbhi97d4unc27vesuf81',1439407708,'modx.user.contextTokens|a:0:{}'),
	('5k969r7o1tf19rpaboso3lpgu2',1439407669,'modx.user.contextTokens|a:0:{}'),
	('c1i9clhgaju9va6irujmd105o6',1439406947,'modx.user.contextTokens|a:0:{}'),
	('olvb955ei9e9jlas26f9kgudb6',1439404848,'modx.user.contextTokens|a:0:{}'),
	('abqvob4c9jk3cmju4420i3lti0',1439402718,'modx.user.contextTokens|a:0:{}'),
	('avg4tijpv714vljubs2ssfpcd6',1439401661,'modx.user.contextTokens|a:0:{}'),
	('9bj5n2bv0k6ptpco6mj2qlkh45',1439400253,'modx.user.contextTokens|a:0:{}'),
	('q9j1tnj3c0246npe2hsergfgh2',1439400001,'modx.user.contextTokens|a:0:{}'),
	('llcbg6nulqqpk115nf9o5h4i54',1439398997,'modx.user.contextTokens|a:0:{}'),
	('832v2p79tul2ktnsl17mtoqrs7',1439397823,'modx.user.contextTokens|a:0:{}'),
	('qb0abt2mjq45ep53k9s3nqo3v4',1439397302,'modx.user.contextTokens|a:0:{}'),
	('6v4kn6g6bltss72q9lfdd2cke4',1439397221,'modx.user.contextTokens|a:0:{}'),
	('i9fetaaqcm00d77ga5jqbaubo2',1439396059,'modx.user.contextTokens|a:0:{}'),
	('kmq0p3qp815sd5g3ho4ogjjk01',1439396057,'modx.user.contextTokens|a:0:{}'),
	('u3g6cj48ja71gap9tdh7ns5b20',1439395777,'modx.user.contextTokens|a:0:{}'),
	('updt5dp9vg62p309m7jpo9afk2',1439394092,'modx.user.contextTokens|a:0:{}'),
	('bpi3e8dd0171mpkphkeksj1760',1439391657,'modx.user.contextTokens|a:0:{}'),
	('mmd1ihrq0a899l21faqcihgo16',1439391612,'modx.user.contextTokens|a:0:{}'),
	('l5jn401r8gskqlse0oq659cm51',1439391608,'modx.user.contextTokens|a:0:{}'),
	('a4i44tjs9dd0nvm8u4ago1sak3',1439391606,'modx.user.contextTokens|a:0:{}'),
	('j6adgsgal2b6sd9t5pj6mi3i36',1439391604,'modx.user.contextTokens|a:0:{}'),
	('1qkg6a5af8t02ngvgi0ch4qo87',1439391454,'modx.user.contextTokens|a:0:{}'),
	('emvbdil2pijvs5sagv2n9qnu53',1439390241,'modx.user.contextTokens|a:0:{}'),
	('1g3bq1oo5cmtk41fl5f1ljf8u2',1439390147,'modx.user.contextTokens|a:0:{}'),
	('i0n0ev5mogkls5en7qek86p7k3',1439387887,'modx.user.contextTokens|a:0:{}'),
	('v5tvn9jeneq6qmf9i0bnn9om80',1439386727,'modx.user.contextTokens|a:0:{}'),
	('c5slefgk0tg85ecqmcshmo1310',1439386514,'modx.user.contextTokens|a:0:{}'),
	('t7of0isebvha2756e9ii98o5e4',1439386173,'modx.user.contextTokens|a:0:{}'),
	('h5n5d6kg6tkmuvbtvr3fdgd1m7',1439384526,'modx.user.contextTokens|a:0:{}'),
	('ciqk5rdsq20qba43ong4unvba7',1439384525,'modx.user.contextTokens|a:0:{}'),
	('nvqq6ju89ih3ngc1pc3v1ddb30',1439384525,'modx.user.contextTokens|a:0:{}'),
	('g231cf4g8505nr04ts17trho61',1439382525,'modx.user.contextTokens|a:0:{}'),
	('vdfs8r72otgg2mkjit94i3hde5',1439381494,'modx.user.contextTokens|a:0:{}'),
	('gsblmovv8nd0vbunptinv9dqf7',1439377341,'modx.user.contextTokens|a:0:{}'),
	('glg8m73j8kl7ia8o5ifvduf5h6',1439377341,'modx.user.contextTokens|a:0:{}'),
	('2a3q30015phqrvsrc7v4u4dqg0',1439377102,'modx.user.contextTokens|a:0:{}'),
	('anop1t9hcet3hfbdgu55dgf640',1439375812,'modx.user.contextTokens|a:0:{}'),
	('55jl9f7q115k6mpanegs3jn8j1',1439374986,'modx.user.contextTokens|a:0:{}'),
	('0ocj8rvub1k4paf50e9c0ilvf0',1439373907,'modx.user.contextTokens|a:0:{}'),
	('vmpcfjaemh2nfpqeja6s64o2l4',1439373274,'modx.user.contextTokens|a:0:{}'),
	('8vtlssdives700t5kq3ijntlj2',1439372429,'modx.user.contextTokens|a:0:{}'),
	('41fau2umse400rts7qtat84071',1439372269,'modx.user.contextTokens|a:0:{}'),
	('ffdhv3fat42hkbku2uone1tmv3',1439371141,'modx.user.contextTokens|a:0:{}'),
	('16oslu3pe4m4q8qkbvt59oqk24',1439371105,'modx.user.contextTokens|a:0:{}'),
	('06pl97vsn4ean8ad7aq7ii3sh4',1439368423,'modx.user.contextTokens|a:0:{}'),
	('gc28jhefivog2tqhafia3u3qq4',1439368422,'modx.user.contextTokens|a:0:{}'),
	('27pl4g5qmo0qtls6qi38h969f0',1439367660,'modx.user.contextTokens|a:0:{}'),
	('r3aggmi7jb7trvgkhfmcc29dn7',1439366716,'modx.user.contextTokens|a:0:{}'),
	('k2slimc0be4siblprt2emqql76',1439366417,'modx.user.contextTokens|a:0:{}'),
	('jb4d9ekeer72rdlac1sra17915',1439366060,'modx.user.contextTokens|a:0:{}'),
	('g66d92600bgs8rlkbb0a6m7ot1',1439365677,'modx.user.contextTokens|a:0:{}'),
	('m8rr9dro3un5c0vkftnknmdfp0',1439364753,'modx.user.contextTokens|a:0:{}'),
	('8jopvonh73ir3mgmaodgg4i7j3',1439363886,'modx.user.contextTokens|a:0:{}'),
	('u6i8289lvsdud8ll6dtbhf0kf4',1439363886,'modx.user.contextTokens|a:0:{}'),
	('bccfjtninnu2cnbgbe8bob1l61',1439360350,'modx.user.contextTokens|a:0:{}'),
	('qpsmsvkr8vdtclabn0poer6bv2',1439360348,'modx.user.contextTokens|a:0:{}'),
	('4fittn7mu802aasmi7donirqo4',1439360346,'modx.user.contextTokens|a:0:{}'),
	('lsm5iubqecpgpirr8c6midu1s0',1439359984,'modx.user.contextTokens|a:0:{}'),
	('1s2gdtueq51paagdcasg4nujj5',1439358377,'modx.user.contextTokens|a:0:{}'),
	('p58hr4b4cdm2choncb42u6qfm6',1439358253,'modx.user.contextTokens|a:0:{}'),
	('iojnq0d0tjkf6tio0ech1o2ms4',1439357751,'modx.user.contextTokens|a:0:{}'),
	('4tatq2tiho69fan2ectmq5log2',1439357678,'modx.user.contextTokens|a:0:{}'),
	('9sjb12bgvbpa3tt4p2hukftls0',1439357678,'modx.user.contextTokens|a:0:{}'),
	('5lp9c7blr86ur46m4q6u9rpai1',1439354983,'modx.user.contextTokens|a:0:{}'),
	('cus69abio7hupjoenrmefk23b7',1439354981,'modx.user.contextTokens|a:0:{}'),
	('pm1n9qjuq48en6e4uau7qo0l84',1439354527,'modx.user.contextTokens|a:0:{}'),
	('2tctj3r6sg3840gbcm4l5gc0v0',1439353732,'modx.user.contextTokens|a:0:{}'),
	('3asaelv84lod31e67mn321enb4',1439353559,'modx.user.contextTokens|a:0:{}'),
	('lhmeddt9qqj5pm663m9tsucr81',1439351054,'modx.user.contextTokens|a:0:{}'),
	('dba06afcami4unkl54pr51tjn0',1439350794,'modx.user.contextTokens|a:0:{}'),
	('tq1voe8g4h4pvm10kkj5dtlqo7',1439350791,'modx.user.contextTokens|a:0:{}'),
	('fqn3rm9essqljhbi57qioaj1t2',1439348146,'modx.user.contextTokens|a:0:{}'),
	('h5m40gs3onoanibt7sgorfr8b0',1439347894,'modx.user.contextTokens|a:0:{}'),
	('dafvaf9ot3q23turme4lrl30e7',1439347710,'modx.user.contextTokens|a:0:{}'),
	('3h6ibeegqkinjrfk6pkkhchk14',1439347389,'modx.user.contextTokens|a:0:{}'),
	('6rvb4o0t5tss5g26sjsqbo3hk6',1439347092,'modx.user.contextTokens|a:0:{}'),
	('83lkvtplrade1m4pso3be834r4',1439347092,'modx.user.contextTokens|a:0:{}'),
	('21goimuuifnvs64nf38lv40eg4',1439346511,'modx.user.contextTokens|a:0:{}'),
	('uptppo1ma0cnk7e7dhnlecrnh4',1439345689,'modx.user.contextTokens|a:0:{}'),
	('qqvmitohjhl070g1fnekhc8172',1439344895,'modx.user.contextTokens|a:0:{}'),
	('8kvu7eicui7dek3qpuc13drjr0',1439344867,'modx.user.contextTokens|a:0:{}'),
	('skpf8addr1s10t92jgq5gn41m5',1439344464,'modx.user.contextTokens|a:0:{}'),
	('rmcnfdh842s7rbnl8f2plps3j7',1439344280,'modx.user.contextTokens|a:0:{}'),
	('6lbvk7epifp2m2lrcrb1a46bp6',1439344280,'modx.user.contextTokens|a:0:{}'),
	('fkf3sbs7b81fn6u3scjad22jh0',1439344280,'modx.user.contextTokens|a:0:{}'),
	('i477md2sf57jqff12eiavvkg05',1439344279,'modx.user.contextTokens|a:0:{}'),
	('le7ue25afqq3vebclhj7qrm9g7',1439343748,'modx.user.contextTokens|a:0:{}'),
	('03eocip37f618jqengt0r1tjo5',1439343748,'modx.user.contextTokens|a:0:{}'),
	('b45gadscilsk6g6nuabtf6d0k6',1439343747,'modx.user.contextTokens|a:0:{}'),
	('dgpnlaqs3tuds9o81v8ct23vt3',1439343736,'modx.user.contextTokens|a:0:{}'),
	('n0rhmq4ultoq2kdnobf0vt0ge1',1439343732,'modx.user.contextTokens|a:0:{}'),
	('a3pjfb411u8r18q1sn480ltl84',1439342796,'modx.user.contextTokens|a:0:{}'),
	('chkc8ltlfqjs2n7kcm5rk8k422',1439338651,'modx.user.contextTokens|a:0:{}'),
	('0ns92kls0ob45oq1b1ni6nb2t0',1439338651,'modx.user.contextTokens|a:0:{}'),
	('000gu820t715j0e4ul5i7pdre5',1439337146,'modx.user.contextTokens|a:0:{}'),
	('cspq79uir71nkjfl9odmao9ej0',1439337140,'modx.user.contextTokens|a:0:{}'),
	('5rt56n43vpuallt5gvlbshcf13',1439336161,'modx.user.contextTokens|a:0:{}'),
	('8ds485ji78qovi4rfeqjdtvh20',1439335687,'modx.user.contextTokens|a:0:{}'),
	('uc32o7ucti867go2j94d3tth16',1439335669,'modx.user.contextTokens|a:0:{}'),
	('t71opg7rokffu02i504m48ua02',1439335314,'modx.user.contextTokens|a:0:{}'),
	('h9h72i927mnjqt59rhfol6g7h3',1439334776,'modx.user.contextTokens|a:0:{}'),
	('1vsu9i8dlm1q7j5kmdp3jsv622',1439334065,'modx.user.contextTokens|a:0:{}'),
	('grus214fmq9uovq4dmjfj92cm5',1439333895,'modx.user.contextTokens|a:0:{}'),
	('thhur4q9o21kqlf485u0ons5r1',1439331775,'modx.user.contextTokens|a:0:{}'),
	('853g12c12gdtra4ianivr2alk3',1439331334,'modx.user.contextTokens|a:0:{}'),
	('819n2vtal0sd9pe6h4iqsv5jl0',1439330839,'modx.user.contextTokens|a:0:{}'),
	('kv292aljs0dcso3pk7v5622um3',1439330295,'modx.user.contextTokens|a:0:{}'),
	('md3mv2gai3qahrthaid1j6qnt2',1439327961,'modx.user.contextTokens|a:0:{}'),
	('7kokectkrhef05if1r6ekrovm6',1439327859,'modx.user.contextTokens|a:0:{}'),
	('a3a7evp9hrhhl6dbc7ktatd6e5',1439326139,'modx.user.contextTokens|a:0:{}'),
	('dnl4k2r0g5hekksbhf5afs8c81',1439326003,'modx.user.contextTokens|a:0:{}'),
	('l09g1tb2vh82bog2jep3pl6l74',1439325478,'modx.user.contextTokens|a:0:{}'),
	('9bu20vg3b0jqiai7jc2ubsa3a5',1439325231,'modx.user.contextTokens|a:0:{}'),
	('4oupn2kk96on6s1317k93jd3d5',1439325088,'modx.user.contextTokens|a:0:{}'),
	('s86p10646o92kt0d9hprgtcv94',1439323765,'modx.user.contextTokens|a:0:{}'),
	('npls3qo8l405oro228g83o7i41',1439322491,'modx.user.contextTokens|a:0:{}'),
	('qghto1ng47hbtei5hqf5349ba0',1439320141,'modx.user.contextTokens|a:0:{}'),
	('fanvs7q5t2gme7995q489h6uc6',1438808005,'modx.user.contextTokens|a:0:{}'),
	('2lbspr6ovs3qsk5upop0cao1n7',1438809305,'modx.user.contextTokens|a:0:{}'),
	('8grbahfmotkv02ed66o94uu105',1438809699,'modx.user.contextTokens|a:0:{}'),
	('7f7686837qb6v987ikve6bm7a3',1438810171,'modx.user.contextTokens|a:0:{}'),
	('d8peu6045edtm40srlji6t8ur5',1438810332,'modx.user.contextTokens|a:0:{}'),
	('bl2gu9kgt517sbkfg827167sp1',1438810334,'modx.user.contextTokens|a:0:{}'),
	('j608noguiv2bnmmdm7nafgsg67',1438814522,'modx.user.contextTokens|a:0:{}'),
	('le5jj4vbbltus95eq3576qqcg6',1438817044,'modx.user.contextTokens|a:0:{}'),
	('546no80o34hiv2bkelgf1mi3b2',1438822246,'modx.user.contextTokens|a:0:{}'),
	('8o12is9n1pla252f311jbsq371',1438823332,'modx.user.contextTokens|a:0:{}'),
	('chgcuaeuf78dh153lb788uod54',1438824125,'modx.user.contextTokens|a:0:{}'),
	('hbu3bn0osdn92ri7nn5h4kcp11',1438824130,'modx.user.contextTokens|a:0:{}'),
	('n26cbubsnlmc991ie9sgln9mu1',1438824132,'modx.user.contextTokens|a:0:{}'),
	('lqc1rbbk68shk4ul7vodfb4ni1',1438824612,'modx.user.contextTokens|a:0:{}'),
	('lm0o08ggo0ujrt88ak4taamsk6',1438824639,'modx.user.contextTokens|a:0:{}'),
	('ammqnu0go8pftj81e2f6ottb05',1438824721,'modx.user.contextTokens|a:0:{}'),
	('n7opqi95u04heov0kb12kahca7',1438824723,'modx.user.contextTokens|a:0:{}'),
	('efvdj0kfcvv4hb6pkmq5g6cvl0',1438824852,'modx.user.contextTokens|a:0:{}'),
	('1uvcvv14jrrdhlv0ol74edt783',1438827693,'modx.user.contextTokens|a:0:{}'),
	('p44k0hup9gca3t4uejpgnv7hc1',1438831187,'modx.user.contextTokens|a:0:{}'),
	('vegk20oepl0vo9ga33gpv6vlg0',1438833149,'modx.user.contextTokens|a:0:{}'),
	('easekieu9el2hqs8hhrbcivtc1',1438833462,'modx.user.contextTokens|a:0:{}'),
	('7dsg2s6vjj5v62bc005gpsd2k4',1438833605,'modx.user.contextTokens|a:0:{}'),
	('p3j4lq0hb772gbjs4093baq4p6',1438833713,'modx.user.contextTokens|a:0:{}'),
	('fr8d27icpv3j9r2nn7n8k7ul47',1438834300,'modx.user.contextTokens|a:0:{}'),
	('5aaj32jlv8h99f2korl6oude41',1438836136,'modx.user.contextTokens|a:0:{}'),
	('94d6sedkb3p037k5sjf734d6m1',1438837056,'modx.user.contextTokens|a:0:{}'),
	('852ne0a1bin378ps4jeui7ch46',1438837069,'modx.user.contextTokens|a:0:{}'),
	('151nulfg4pn5rf648s1ill0kj4',1438837699,'modx.user.contextTokens|a:0:{}'),
	('k45af6nsslbji049pf8sjdljs5',1438839504,'modx.user.contextTokens|a:0:{}'),
	('8gesnic83eusofbg2mmfrf5sk2',1438839504,'modx.user.contextTokens|a:0:{}'),
	('0d0gdil58koeqr3dmskmtja8g5',1438840730,'modx.user.contextTokens|a:0:{}'),
	('ece2if88kic2l3e8tfc8m3n9o0',1438841553,'modx.user.contextTokens|a:0:{}'),
	('o80agh7ummf10q99l61rnf73s4',1438841865,'modx.user.contextTokens|a:0:{}'),
	('ipa1ehq14572akgjmtl248k8p4',1438841874,'modx.user.contextTokens|a:0:{}'),
	('5kv5ra80ko9hakrul4ip0gt5q5',1438841877,'modx.user.contextTokens|a:0:{}'),
	('0g9hg7ok0r4ha1unna9d1c17b7',1438841891,'modx.user.contextTokens|a:0:{}'),
	('oidfl1vfpd9gi0a39jck50cl57',1438844004,'modx.user.contextTokens|a:0:{}'),
	('s008ghm41mvs9nu4k9druc3sp6',1438844006,'modx.user.contextTokens|a:0:{}'),
	('qsjl9f12u2d4d96lmkl3tu93s5',1438844007,'modx.user.contextTokens|a:0:{}'),
	('g93hgmqpraglf5dnse534u2rq0',1438845370,'modx.user.contextTokens|a:0:{}'),
	('iq39gb3e2k1to2e6pcpo40d155',1438846201,'modx.user.contextTokens|a:0:{}'),
	('eg4vb2vaup1v18j2hf079f8e62',1438848002,'modx.user.contextTokens|a:0:{}'),
	('h3e97s1206martp3bqh6gik157',1438850076,'modx.user.contextTokens|a:0:{}'),
	('m140q5t8do9er97qisfuroaub0',1438851528,'modx.user.contextTokens|a:0:{}'),
	('m8priklcknd0ll7l6lupm0u607',1438855545,'modx.user.contextTokens|a:0:{}'),
	('a6650upn7ttnkr7fvpsar7lqn7',1438855549,'modx.user.contextTokens|a:0:{}'),
	('u6ihil21eon6qo6o8cu1uof0r2',1438855549,'modx.user.contextTokens|a:0:{}'),
	('ek5cf9k57qsicv2pc13jdrelq5',1438857561,'modx.user.contextTokens|a:0:{}'),
	('pldk8v5rikl57kihnp3pbmp314',1438858745,'modx.user.contextTokens|a:0:{}'),
	('7jt5im60nel63ngu331clq9st7',1438858745,'modx.user.contextTokens|a:0:{}'),
	('v8n3ounsbuvk1dc65ddj4hi0m7',1438858985,'modx.user.contextTokens|a:0:{}'),
	('ra1sr2v8mf054rr2md72p6m025',1438858986,'modx.user.contextTokens|a:0:{}'),
	('pumts6gb52vou99b39qul3dlv5',1438859487,'modx.user.contextTokens|a:0:{}'),
	('bvb6btit8q4mqq5oidu55q45p2',1438859493,'modx.user.contextTokens|a:0:{}'),
	('e2b6d5odb1dde9l22f0kcatg21',1438859931,'modx.user.contextTokens|a:0:{}'),
	('ot2vlfp7a8dt0fedvkue7ki5j6',1438861247,'modx.user.contextTokens|a:0:{}'),
	('olc5r0bnmolc150jjur9pr6pi3',1438861913,'modx.user.contextTokens|a:0:{}'),
	('l4ud7cp7qli7miaci2c32v1ot6',1438863219,'modx.user.contextTokens|a:0:{}'),
	('fc389pj538fh09h5tvjgv4ldc3',1438864768,'modx.user.contextTokens|a:0:{}'),
	('9jcsqrimh75e0ieiadu3u9fo37',1438864775,'modx.user.contextTokens|a:0:{}'),
	('msa0g635nev3jgieid116ee070',1438865754,'modx.user.contextTokens|a:0:{}'),
	('kc575fsdfbbkonqo2d3edn5ms3',1438866772,'modx.user.contextTokens|a:0:{}'),
	('h0eketh48o2ho0l3u1lm1edjh4',1438868304,'modx.user.contextTokens|a:0:{}'),
	('dqllkh85u246cbc767nev2mu20',1438868730,'modx.user.contextTokens|a:0:{}'),
	('a53mr45k5ag6p0h8mhl4umi5p4',1438871470,'modx.user.contextTokens|a:0:{}'),
	('nskokq2m5d1t0um5nun66aa423',1438871542,'modx.user.contextTokens|a:0:{}'),
	('2rt394st4a3qnkt04g1drb8pr1',1438871746,'modx.user.contextTokens|a:0:{}'),
	('1chm5qjodctdqnedqqmmdd3fk4',1438872178,'modx.user.contextTokens|a:0:{}'),
	('44k35agkv6t73m8v3csficivg6',1438872180,'modx.user.contextTokens|a:0:{}'),
	('jjje6p4g72jp09ksocgakv1794',1438872738,'modx.user.contextTokens|a:0:{}'),
	('07vmu0l5n61s63127tr5lipgg7',1438873395,'modx.user.contextTokens|a:0:{}'),
	('3stbonpdnd983kcn5mic1egbu0',1438873395,'modx.user.contextTokens|a:0:{}'),
	('prr7sa274er91cri1pcuo9cei5',1438873783,'modx.user.contextTokens|a:0:{}'),
	('64plb4kul9stgorne1infa0476',1438874741,'modx.user.contextTokens|a:0:{}'),
	('a61i8ile62890atksmimlf9si5',1438875285,'modx.user.contextTokens|a:0:{}'),
	('gbkkj71cogsqj4smiu7b1sdro0',1438877135,'modx.user.contextTokens|a:0:{}'),
	('9s19aqebpt402v2rg3713lcsn0',1438877568,'modx.user.contextTokens|a:0:{}'),
	('k97e4tnb9bttgo5utudnu4cnu0',1438877794,'modx.user.contextTokens|a:0:{}'),
	('1ftgbpl51bu47kdcftfc0k9la5',1438878171,'modx.user.contextTokens|a:0:{}'),
	('5rauv4u1q6f06kmpgk1ssn1120',1438880267,'modx.user.contextTokens|a:0:{}'),
	('ba62i6gk5kparm8ovasf9ft3v3',1438880269,'modx.user.contextTokens|a:0:{}'),
	('mvpfnkk8vp4ap6oc9ebvprgd12',1438880269,'modx.user.contextTokens|a:0:{}'),
	('p9jaji6h2mks7hsmeb15s9cps4',1438880271,'modx.user.contextTokens|a:0:{}'),
	('f4qnu6343bdfhdhs266utpko43',1438880271,'modx.user.contextTokens|a:0:{}'),
	('1h0glpgjs3qfb6i5h43idbfm91',1438880434,'modx.user.contextTokens|a:0:{}'),
	('9drnnn58lauuteaepqutnqqoc6',1438880480,'modx.user.contextTokens|a:0:{}'),
	('f7cu3lbth8j664s19p4g71b1r6',1438881628,'modx.user.contextTokens|a:0:{}'),
	('ketvqh99o1avn3lftgb6slk2q3',1438881682,'modx.user.contextTokens|a:0:{}'),
	('k81o3psrppl67548oih07qtu71',1438881932,'modx.user.contextTokens|a:0:{}'),
	('gs9vj8u9va7fp6sjqcvug6s2h5',1438882397,'modx.user.contextTokens|a:0:{}'),
	('iuf6b8u8rmgsk5ohpv4erkafs2',1438882398,'modx.user.contextTokens|a:0:{}'),
	('fsnvt9qpsdt8ot9fn1hqri6a21',1438882413,'modx.user.contextTokens|a:0:{}'),
	('dir4oe0gh45pu22iaf8k03f2p7',1438882968,'modx.user.contextTokens|a:0:{}'),
	('sp5ncpjbosjkpeirc8vv26bnk4',1438883463,'modx.user.contextTokens|a:0:{}'),
	('9sdvld36vr6i31mrcg3mlaufc7',1438883986,'modx.user.contextTokens|a:0:{}'),
	('frhifkeafpi04896a6a6o7njh4',1438884107,'modx.user.contextTokens|a:0:{}'),
	('p8hjls7genngmmjh9i4le5aae5',1438884108,'modx.user.contextTokens|a:0:{}'),
	('65olmedasnubahomsugq6vnc87',1438884397,'modx.user.contextTokens|a:0:{}'),
	('m83ga978snot5j02f1rvpqnta2',1438884868,'modx.user.contextTokens|a:0:{}'),
	('hej70vcl9q6p4ji1e5o7vgieg1',1438885040,'modx.user.contextTokens|a:0:{}'),
	('5hh6nfsgcbofeqk6522ditevr0',1438885538,'modx.user.contextTokens|a:0:{}'),
	('a80pjp5mnv8vn9krd7d54l1475',1438885669,'modx.user.contextTokens|a:0:{}'),
	('ijaapg1k0asmd4kh2qkp8bdhv4',1438885681,'modx.user.contextTokens|a:0:{}'),
	('f578oo7g89qlkod5lf2tfq7ri7',1438885683,'modx.user.contextTokens|a:0:{}'),
	('12ntmsq29brqk36mn8m4jq3e30',1438885964,'modx.user.contextTokens|a:0:{}'),
	('66v0o1s91s7l9305qjegmnrjs6',1438886229,'modx.user.contextTokens|a:0:{}'),
	('cn0bmb11s84a5o4mc36npav573',1438887287,'modx.user.contextTokens|a:0:{}'),
	('50uaol005j0gtdfgfe2v11plm5',1438889735,'modx.user.contextTokens|a:0:{}'),
	('t842nrqeop9d0qjejkfu3eci55',1438890455,'modx.user.contextTokens|a:0:{}'),
	('2j8qq8j25pged0h493k8t2dod5',1438890776,'modx.user.contextTokens|a:0:{}'),
	('gan3t0v29gd03hgk8vpfg8suh5',1438891780,'modx.user.contextTokens|a:0:{}'),
	('p9ah3frpo7gul265etbo85vvn7',1438892573,'modx.user.contextTokens|a:0:{}'),
	('8nh8irb26iqgqk52raskpqm977',1438892574,'modx.user.contextTokens|a:0:{}'),
	('dp13vcaigddv5ruu2c6rusjn56',1438893192,'modx.user.contextTokens|a:0:{}'),
	('up967auh4bst4bh5edu624nki6',1438894452,'modx.user.contextTokens|a:0:{}'),
	('7q1p48ki2s5u4qrm1m24edk7k2',1438895071,'modx.user.contextTokens|a:0:{}'),
	('sft94c6105gbgcka52rumnddi6',1438895323,'modx.user.contextTokens|a:0:{}'),
	('v12eov90ue127vi5uia9iplo90',1438895333,'modx.user.contextTokens|a:0:{}'),
	('kk9c0egdppm30oke0lu8h10d76',1438896388,'modx.user.contextTokens|a:0:{}'),
	('4i9igve1eaochqjjbhfq7pcru5',1438896591,'modx.user.contextTokens|a:0:{}'),
	('c5087p2h612ltf6sr9bcbj30p2',1438897446,'modx.user.contextTokens|a:0:{}'),
	('2cn5adq42gtf8faeb999fg3mg5',1438897876,'modx.user.contextTokens|a:0:{}'),
	('ilrlca2rt5jivcmao1oam4jie2',1438900952,'modx.user.contextTokens|a:0:{}'),
	('7c3uvll4o9o1lnj6o6ajba8d73',1438901006,'modx.user.contextTokens|a:0:{}'),
	('hej4f2dimklm4hbk33iatcibi2',1438804530,'modx.user.contextTokens|a:0:{}'),
	('crl71f4vhe9344e7e03tp0eft0',1438803426,'modx.user.contextTokens|a:0:{}'),
	('9dmgc7e2bnvj660hob1j0167j1',1438803202,'modx.user.contextTokens|a:0:{}'),
	('sja3m8fc53sfbe3vcvetajqhq5',1438803200,'modx.user.contextTokens|a:0:{}'),
	('q1g2km2b8eeusblms2hrjhpsh2',1438803198,'modx.user.contextTokens|a:0:{}'),
	('1dnpht9ejlrc4boo2k0psajs05',1438802112,'modx.user.contextTokens|a:0:{}'),
	('elqh15g3gd9ck2b6oupngncge1',1438799980,'modx.user.contextTokens|a:0:{}'),
	('mctt75rsdki1fh4ip0ieqv6jq6',1438799784,'modx.user.contextTokens|a:0:{}'),
	('eoequdrsfe39tees0kmjo5llo1',1438799783,'modx.user.contextTokens|a:0:{}'),
	('qqouu76l08m1epuktofns7c9h5',1438799782,'modx.user.contextTokens|a:0:{}'),
	('0t71hqgr9oi1646kl2u5ge2gn5',1438797517,'modx.user.contextTokens|a:0:{}'),
	('vamlai7sml8ij80iscsjr4pfn5',1438797346,'modx.user.contextTokens|a:0:{}'),
	('c4f5dtq6jhm9ksjtf0j9gf11f6',1438797124,'modx.user.contextTokens|a:0:{}'),
	('lddkbpakqhalo6g6oaln33anr5',1438795103,'modx.user.contextTokens|a:0:{}'),
	('eqp77cenrle0tfpo67ti0cfg60',1438795103,'modx.user.contextTokens|a:0:{}'),
	('icvqn6uq7avj1803164fotq081',1438795103,'modx.user.contextTokens|a:0:{}'),
	('e5ntkgnejs33ji8i0tj9rmbo76',1438795102,'modx.user.contextTokens|a:0:{}'),
	('csd5tdad0rh1d25041nqf2v0e1',1438795101,'modx.user.contextTokens|a:0:{}'),
	('doa6dct2vj5jkusvofrq7k7rq6',1438794715,'modx.user.contextTokens|a:0:{}'),
	('1tuorrl9hb8ciibd5d4tbugq25',1438794604,'modx.user.contextTokens|a:0:{}'),
	('sussnk09c4sflgi93v2bjvebu1',1438794604,'modx.user.contextTokens|a:0:{}'),
	('5lp5q37a3uv4rd7e2mr4pl8cf4',1438794581,'modx.user.contextTokens|a:0:{}'),
	('8mb0mk7l87mseo4ss3f3mb5f62',1438794581,'modx.user.contextTokens|a:0:{}'),
	('7bgh9o9c0ncrcia541bfj2ose0',1438794581,'modx.user.contextTokens|a:0:{}'),
	('00rkc3igofgrkladcd5clgor03',1438794581,'modx.user.contextTokens|a:0:{}'),
	('ip4tgvp2paq7s1301n784t4mt7',1438794581,'modx.user.contextTokens|a:0:{}'),
	('h5gm9of8jag0krqsqcbd070a41',1438794580,'modx.user.contextTokens|a:0:{}'),
	('0084u6c8aihcjk8b3p3kudaod0',1438794545,'modx.user.contextTokens|a:0:{}'),
	('v50bu6gqo8outhoi1gm397q282',1438794426,'modx.user.contextTokens|a:0:{}'),
	('mbppe1haiov4ctq1ssg7d89uo1',1438794064,'modx.user.contextTokens|a:0:{}'),
	('s98dmuqf1n9kltoj2452cg0hg3',1438791686,'modx.user.contextTokens|a:0:{}'),
	('oei11jj76aj0dmb2qjoa5dr8q2',1438791029,'modx.user.contextTokens|a:0:{}'),
	('pd9igulbqp5em0gbsbph5gpks7',1438791028,'modx.user.contextTokens|a:0:{}'),
	('54fvdc032e0a9hfju0u3snqo26',1438790485,'modx.user.contextTokens|a:0:{}'),
	('v3ccuum91n1rvu4bpnc594o562',1438790207,'modx.user.contextTokens|a:0:{}'),
	('8vldi75fgrlrtjj6tfubn7gl20',1438790078,'modx.user.contextTokens|a:0:{}'),
	('d04qlbt8bd0051umipj7pl7fv0',1438789820,'modx.user.contextTokens|a:0:{}'),
	('m7v6snaneevm795k8s7a194m43',1438789768,'modx.user.contextTokens|a:0:{}'),
	('k7lkeon4spiupfvtj4jndrjas4',1438789733,'modx.user.contextTokens|a:0:{}'),
	('fboj7p9hc3ll5vjd2824blsf46',1438788964,'modx.user.contextTokens|a:0:{}'),
	('gonsv5c61q18t8reinmvv6cko1',1438784944,'modx.user.contextTokens|a:0:{}'),
	('d85u8shc75k3gm7ucq7ltbnl57',1438783834,'modx.user.contextTokens|a:0:{}'),
	('74iosfqp3rodeh55h40f139dm4',1438783460,'modx.user.contextTokens|a:0:{}'),
	('md1f4irl4b7ekmkqeri9741sb6',1438783418,'modx.user.contextTokens|a:0:{}'),
	('fdblhvmfko9i091u4nnlq1pki0',1438783279,'modx.user.contextTokens|a:0:{}'),
	('ohm9bh8cgjof45oe9ldac1crp0',1438782485,'modx.user.contextTokens|a:0:{}'),
	('uu3i0q9fdats66164073mvlbf2',1438782485,'modx.user.contextTokens|a:0:{}'),
	('81c5l4q2koc452bin8nljotsn0',1438782467,'modx.user.contextTokens|a:0:{}'),
	('2colhrh9175rvi3596eqgk5jf0',1438781867,'modx.user.contextTokens|a:0:{}'),
	('t61jk5t4g1ui8ce91jrgl6snq1',1438780446,'modx.user.contextTokens|a:0:{}'),
	('97trqv1cffcfe1qcstk98feb24',1438778063,'modx.user.contextTokens|a:0:{}'),
	('jn8u40j3ig1jvlogrcv37j3cu0',1438777610,'modx.user.contextTokens|a:0:{}'),
	('proho2jqketleu06do0vul17r5',1438777568,'modx.user.contextTokens|a:0:{}'),
	('1ml8gr8cd5puaru25mjmnd6ss7',1438777287,'modx.user.contextTokens|a:0:{}'),
	('r9j0v1hsga3jnbe209j4c444f7',1438776907,'modx.user.contextTokens|a:0:{}'),
	('p6kr9uadd63ckul7bqs6gltod3',1438776815,'modx.user.contextTokens|a:0:{}'),
	('7natspt74vb9np9votpq0170s6',1438776806,'modx.user.contextTokens|a:0:{}'),
	('hg7qun8d7ilce7qi9kuli31k84',1438776779,'modx.user.contextTokens|a:0:{}'),
	('s5ov5tlfqketm3si22t1j27sm7',1438776777,'modx.user.contextTokens|a:0:{}'),
	('k80adonl8f0tjh4urp53guf0k6',1438776772,'modx.user.contextTokens|a:0:{}'),
	('5ao2nngg3enp52ud7qmdbu1ec6',1438776767,'modx.user.contextTokens|a:0:{}');

/*!40000 ALTER TABLE `modx_session` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_content`;

CREATE TABLE `modx_site_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL DEFAULT 'document',
  `contentType` varchar(50) NOT NULL DEFAULT 'text/html',
  `pagetitle` varchar(255) NOT NULL DEFAULT '',
  `longtitle` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) DEFAULT '',
  `link_attributes` varchar(255) NOT NULL DEFAULT '',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pub_date` int(20) NOT NULL DEFAULT '0',
  `unpub_date` int(20) NOT NULL DEFAULT '0',
  `parent` int(10) NOT NULL DEFAULT '0',
  `isfolder` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `introtext` text,
  `content` mediumtext,
  `richtext` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` int(10) NOT NULL DEFAULT '0',
  `menuindex` int(10) NOT NULL DEFAULT '0',
  `searchable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `cacheable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `createdby` int(10) NOT NULL DEFAULT '0',
  `createdon` int(20) NOT NULL DEFAULT '0',
  `editedby` int(10) NOT NULL DEFAULT '0',
  `editedon` int(20) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deletedon` int(20) NOT NULL DEFAULT '0',
  `deletedby` int(10) NOT NULL DEFAULT '0',
  `publishedon` int(20) NOT NULL DEFAULT '0',
  `publishedby` int(10) NOT NULL DEFAULT '0',
  `menutitle` varchar(255) NOT NULL DEFAULT '',
  `donthit` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `privateweb` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `privatemgr` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `content_dispo` tinyint(1) NOT NULL DEFAULT '0',
  `hidemenu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `class_key` varchar(100) NOT NULL DEFAULT 'modDocument',
  `context_key` varchar(100) NOT NULL DEFAULT 'web',
  `content_type` int(11) unsigned NOT NULL DEFAULT '1',
  `uri` text,
  `uri_override` tinyint(1) NOT NULL DEFAULT '0',
  `hide_children_in_tree` tinyint(1) NOT NULL DEFAULT '0',
  `show_in_tree` tinyint(1) NOT NULL DEFAULT '1',
  `properties` mediumtext,
  PRIMARY KEY (`id`),
  KEY `alias` (`alias`),
  KEY `published` (`published`),
  KEY `pub_date` (`pub_date`),
  KEY `unpub_date` (`unpub_date`),
  KEY `parent` (`parent`),
  KEY `isfolder` (`isfolder`),
  KEY `template` (`template`),
  KEY `menuindex` (`menuindex`),
  KEY `searchable` (`searchable`),
  KEY `cacheable` (`cacheable`),
  KEY `hidemenu` (`hidemenu`),
  KEY `class_key` (`class_key`),
  KEY `context_key` (`context_key`),
  KEY `uri` (`uri`(333)),
  KEY `uri_override` (`uri_override`),
  KEY `hide_children_in_tree` (`hide_children_in_tree`),
  KEY `show_in_tree` (`show_in_tree`),
  KEY `cache_refresh_idx` (`parent`,`menuindex`,`id`),
  FULLTEXT KEY `content_ft_idx` (`pagetitle`,`longtitle`,`description`,`introtext`,`content`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_content` WRITE;
/*!40000 ALTER TABLE `modx_site_content` DISABLE KEYS */;

INSERT INTO `modx_site_content` (`id`, `type`, `contentType`, `pagetitle`, `longtitle`, `description`, `alias`, `link_attributes`, `published`, `pub_date`, `unpub_date`, `parent`, `isfolder`, `introtext`, `content`, `richtext`, `template`, `menuindex`, `searchable`, `cacheable`, `createdby`, `createdon`, `editedby`, `editedon`, `deleted`, `deletedon`, `deletedby`, `publishedon`, `publishedby`, `menutitle`, `donthit`, `privateweb`, `privatemgr`, `content_dispo`, `hidemenu`, `class_key`, `context_key`, `content_type`, `uri`, `uri_override`, `hide_children_in_tree`, `show_in_tree`, `properties`)
VALUES
	(1,'document','text/html','Home','','','index','',1,0,0,0,1,'','',1,2,0,1,1,1,1412548428,2,1428032470,0,0,0,0,0,'',0,0,0,0,0,'modDocument','web',1,'index/',0,0,1,NULL),
	(2,'document','text/html','About Us','','','about','',1,0,0,0,0,'','<p>Heartland Heating &amp; Cooling began servicing the Des Moines area in 2003. Heartland was founded by Mike Nelson and Dave Volante whom both spent most of their lives working in the construction industry. They both were dedicated in providing outstanding customer service and a higher quality of workmanship. This helped their business grow, within the first 6 months their number of employees more than doubled which seemed to be the trend for the first years of business. Mike and Dave had to continue to grow to meet the needs of their customers and are currently 47 employees strong.</p>\r\n<p>In December of 2014 Mike retired and Dave took over as the sole owner of Heartland Heating &amp; Cooling. The company is still thriving, specializing in new construction installation, replacements, repair and maintenance of HVAC systems. Heartland Heating &amp; Cooling works hard every day to earn your trust and provide the maximum comfort in your home. Heartland believes in continually training staff to meet your needs.</p>\r\n<p>Rest easy knowing your comfort is our #1 priority</p>\r\n<p><img style=\"float: left; margin: 10px;\" src=\"resources/img/NATE_logo5.JPG\" alt=\"NATE\" width=\"109\" height=\"109\" /><img style=\"float: left; margin: 10px;\" src=\"resources/img/EPA.png\" alt=\"epa certified\" width=\"102\" height=\"102\" /></p>\r\n<p><img style=\"float: left; margin: 20px;\" src=\"resources/img/cbbb-badge-horz.png\" alt=\"bbb logo\" width=\"149\" height=\"56\" /></p>',1,3,1,1,1,1,1412552423,3,1429892674,0,0,0,1418008680,1,'About',0,0,0,0,0,'modDocument','web',1,'about',0,0,1,NULL),
	(13,'document','text/html','Comfort Team Membership','Heartland Heating and Cooling Comfort Team','','comfort-team','',1,0,0,2,0,'','<p>A Comfort Team Membership is an agreement that guarantees that Heartland Heating &amp; Cooling will have a tune-up specialist at your home twice a year making sure each piece of your HVAC system will be professionally cleaned and maintained annually. Our tune-up specialist will clean the outdoor coil, change your filter and ensure your equipment is operating safely and at its maximum efficiency. These appointments are scheduled Monday through Friday between the hours of 8 AM and 4 PM throughout the year.</p>\r\n<h3>The Comfort Team Membership is more than just tune-ups!</h3>\r\n<p><strong style=\"line-height: 1.5em;\">Convenience</strong><span style=\"line-height: 1.5em;\">- Don’t want to think about your home comfort system? Let us do that for you! We schedule your appointment ahead of time and we are happy to reschedule to meet your needs. Our Comfort Team Membership includes priority scheduling for emergency service, which guaranties service response within 24 hours.</span></p>\r\n<p><strong>Peace of Mind</strong>- Do you want to spend your hard earned money on equipment repairs? Having your equipment professionally cleaned and maintained may prevent the inconvenience of major repairs. Many manufacture warranties require annual maintenance. This maintenance can also extend the life of your equipment. Not only could annual maintenance reduce potential breakdowns it can also help save on your utility bills. Cleaning and maintenance will help the units run more efficiently. Thus minimizing your utility bills.</p>\r\n<p><strong>Discounts</strong>- No more paying emergency service rates! When you are a Comfort Team Member you are charged our normal rate even when it’s an afterhours emergency service call. You will also receive 10% on any parts you may need throughout the year, which includes filters.</p>\r\n<p>So the next time you have a Heartland Heating and Cooling team member in your home ask them about getting signed up for our Comfort Team Membership, <a href=\"mailto:jennifer@heartlandheating.com\" target=\"_blank\">email</a> or call us today at 515-986-5007 to schedule your first tune-up to get signed up!</p>\r\n<p> </p>\r\n<p><img style=\"float: left; margin: 20px;\" src=\"resources/img/CTM%20Coupon.jpg\" alt=\"comfort team membership coupon\" width=\"279\" height=\"214\" /></p>\r\n<p> </p>\r\n<p> </p>\r\n<p> </p>\r\n<p> </p>\r\n<p> </p>\r\n<p> </p>\r\n<p> </p>\r\n<p> </p>\r\n<p> </p>',1,3,0,1,1,2,1422210064,2,1428033458,0,0,0,1422210120,2,'',0,0,0,0,0,'modDocument','web',1,'about/comfort-team',0,0,1,NULL),
	(3,'document','text/html','Product Info','','','services','',1,0,0,0,1,'','<p>Heartland Heating &amp; Cooling is committed to keeping our customers comfortable even when Iowa weather is out of control.  </p>\r\n<p>Heartland offers many options to make your home a place that is comforting, as well as providing energy saving solutions to help keep energy cost down. Whether that means helping you expertly choose the perfect system for your current new construction project, replace an old existing unit with new, or just maintain your current system Heartland Heating and Cooling is here for you and your comfort.  </p>',1,4,2,1,1,1,1418009743,2,1427849117,0,0,0,1418009700,1,'Product Info',0,0,0,0,0,'modDocument','web',1,'services/',0,0,1,NULL),
	(4,'document','text/html','Order Filter','','','order-filter','',1,0,0,0,0,'','',1,3,6,1,1,1,1418009767,1,1418009956,0,0,0,1418009760,1,'',0,0,0,0,1,'modDocument','web',1,'order-filter',0,0,1,NULL),
	(5,'document','text/html','Employment','','','employment-opportunities','',1,0,0,0,0,'','<p>\r\n<script type=\"text/javascript\">// <![CDATA[\r\nvar headingText = \"Current Openings\";\r\n//var categoryTitlePrefix = \" Active \";\r\nvar categoryTitleSuffix = \" Jobs \";\r\nvar returnURL = \"http://www.heartlandheating.com/employment.html\";\r\n// ]]></script>\r\n<script type=\"text/javascript\" language=\"javascript\" src=\"http://HeartlandHeatingandAir.ourcareerpages.com/Resources/js/ccp_widget.aspx?GroupBy=label&amp;ccpLoc=bottom\"></script>\r\n</p>',1,3,8,1,1,1,1418009892,2,1427932309,0,0,0,1418009880,1,'',0,0,0,0,0,'modDocument','web',1,'employment-opportunities',0,0,1,NULL),
	(6,'document','text/html','Financing Options','','','financing-options','',1,0,0,0,0,'','<p>Heartland Heating &amp; Cooling wants to do all we can to provide comfort in your home. We always provide the best possible pricing but sometimes people need a little extra help pulling the funds together. HHC offers a financing program through <strong><a href=\"resources/img/Financing.pdf\" target=\"_blank\">Synchrony</a></strong>. There are several different options to choose from and the application process is simple and fast. This option allows you to finance while still receiving energy company rebates.</p>\r\n<p>Many times your energy company will also offer financing. For example, MidAmerican Energy has a financing program with a sub-prime rate, usually close to a point under. This can really make a difference in your payment as well as the total finance charge. However, if you finance through Mid Am you will not receive the energy company rebate.</p>\r\n<p><strong><a href=\"[[~7]]\">Contact us</a></strong> to determine the best solution for your financing needs.</p>',1,3,7,1,1,1,1418009917,2,1427852505,0,0,0,1418009880,1,'Financing',0,0,0,0,0,'modDocument','web',1,'financing-options',0,0,1,NULL),
	(7,'document','text/html','Contact','','','contact','',1,0,0,0,0,'','<p><strong>Please contact Heartland Heating &amp; Cooling:</strong></p>\r\n<p>515.986.5007 | 515.986.5007<br />3155 SE Miehe Drive, Suite 1, Grimes, IA 50111</p>\r\n<p><strong>24 Hour EMERGENCY Service</strong></p>\r\n<p>During Business Hours:<br />7:30 a.m. to 4:00 p.m.<br />Dial 515.986.5007 or 1.800.986.5007</p>\r\n<p><strong>After hours - EMERGENCY Service</strong><br />Dial 515.559.3746</p>',1,5,9,1,1,1,1418009975,2,1427853269,0,0,0,1418010000,1,'',0,0,0,0,0,'modDocument','web',1,'contact',0,0,1,NULL),
	(8,'document','text/html','Heating','','','heating','',1,0,0,3,0,'','<p><img style=\"margin-right: 10px; margin-bottom: 10px; float: left;\" src=\"resources/img/DaveFurnace.jpg\" alt=\"dave furnace lennox\" width=\"244\" height=\"350\" />Heartland Heating &amp; Cooling can take care of all your heating needs. We offer solutions for your home, office, garage, seasonal porches and even your outside living spaces. Our goal is to provide a comfortable environment just about anywhere you want to spend time. There are many different applications and price points available. We also offer rebates and financing options. Heartland is a “one stop shop” for all of your home comfort needs.</p>\r\n<p><strong><a href=\"resources/img/Furnaces.pdf\" target=\"_blank\">Click here</a></strong> for more information.</p>\r\n<p>All of our heating products have the <a href=\"resources/img/Good Housekeeping.pdf\" target=\"_blank\">Good Housekeeping Seal of Approval</a>.</p>',1,6,0,1,1,1,1418010044,2,1427849882,0,0,0,1418010000,1,'',0,0,0,0,0,'modDocument','web',1,'services/heating',0,0,1,NULL),
	(15,'document','text/html','Products','','','products','',1,0,0,0,1,'','',1,4,5,1,1,2,1422332848,2,1427060677,0,0,0,1422332820,2,'',0,0,0,0,1,'modDocument','web',1,'products/',0,0,1,NULL),
	(16,'document','text/html','Heating','','','heating','',1,0,0,15,0,'','',1,6,0,1,1,2,1422333035,1,1422358814,1,1427060586,2,1422333000,2,'',0,0,0,0,0,'modDocument','web',1,'products/heating',0,0,1,NULL),
	(17,'document','text/html','Cooling','','','cooling','',1,0,0,15,0,'','',1,6,1,1,1,2,1422333069,1,1422358823,1,1427060610,2,1422333060,2,'',0,0,0,0,0,'modDocument','web',1,'products/cooling',0,0,1,NULL),
	(18,'document','text/html','Fireplaces','','','fireplaces','',1,0,0,15,0,'','',1,6,2,1,1,2,1422333115,1,1422358831,1,1427059789,2,1422333120,2,'',0,0,0,0,0,'modDocument','web',1,'products/fireplaces',0,0,1,NULL),
	(19,'document','text/html','Specialty Equipment','','','in-floor','',1,0,0,15,0,'','',1,6,4,1,1,2,1422333174,2,1422719655,1,1427060121,2,1422333120,2,'',0,0,0,0,0,'modDocument','web',1,'products/in-floor',0,0,1,NULL),
	(20,'document','text/html','Thermostats','','','climate-control','',1,0,0,15,0,'','',1,6,3,1,1,2,1422333222,2,1422719625,1,1427060100,2,1422333180,2,'',0,0,0,0,0,'modDocument','web',1,'products/climate-control',0,0,1,NULL),
	(9,'document','text/html','Cooling','','','cooling','',1,0,0,3,0,'','<p><img style=\"float: left; margin-right: 10px; margin-bottom: 10px;\" src=\"resources/img/AC.jpg\" alt=\"air conditioner and dave\" width=\"250\" height=\"347\" />We all love being comfortable and typical Iowa summer can make that a challenge. Let Heartland Heating &amp; Cooling help keep that heat and humidity under control. We want to help ensure the comfort level you want and desire with one of our Air Conditioners.</p>\r\n<p><strong><a href=\"resources/img/Air.pdf\" target=\"_blank\">Click here</a></strong> for more information.</p>\r\n<p>All of our cooling products have the<strong> </strong><a href=\"resources/img/Good Housekeeping.pdf\" target=\"_blank\">Good Housekeeping Seal of Approval</a>.</p>',1,6,1,1,1,1,1418010078,2,1427932365,0,0,0,1418010060,1,'',0,0,0,0,0,'modDocument','web',1,'services/cooling',0,0,1,NULL),
	(10,'document','text/html','Geo-Thermal','','','geo-thermal','',1,0,0,3,0,'','<p dir=\"ltr\"><span>Want to save money and the environment? Look no further Heartland Heating &amp; Cooling has your answer with a new Geothermal System.</span></p>\r\n<p dir=\"ltr\"><span>Geothermal heating and cooling systems take advantage of the stable temperature underground using a piping system, commonly referred to as a “loop.”   Water circulates in the loop to exchange heat between your home, the ground source heat pump, and the earth, providing heating, cooling, and hot water at remarkably high efficiencies.</span></p>\r\n<p dir=\"ltr\"><span>According to the US Department of Energy, the typical American family spends at least $2,000 a year on home electric bills. Heating, cooling and hot water costs combined make up over 70% of the typical electric bill. With the ENERGY STAR® qualified Geothermal systems that Heartland Heating &amp; Cooling install you can save up to 80% of those cost. </span></p>\r\n<p dir=\"ltr\"><span>There’s even more savings! Until December 21, 2016, you can receive a 30% federal tax credit on the installation cost of a qualifying geothermal system. This tax credit must be claimed within the year of installation. </span></p>\r\n<p><strong><span id=\"docs-internal-guid-8d2636a3-225e-fee5-e204-fdb60da6a035\"><a href=\"resources/img/Geothermal.pdf\" target=\"_blank\">Click here</a> to see how Geothermal heating and coolying systems work.</span></strong></p>',1,6,2,1,1,1,1418010161,2,1427851108,0,0,0,1418010120,1,'',0,0,0,0,0,'modDocument','web',1,'services/geo-thermal',0,0,1,NULL),
	(14,'document','text/html','Indoor Air Quality','','','indoor-air-quality','',1,0,0,3,0,'','<p>Heartland Heating &amp; Cooling’s # 1 priority is your comfort. Which starts with Indoor Air Quality. Keeping your family comfortable, healthy and breathing easy is stress free with Heartland’s many solutions to your indoor air quality needs.</p>\r\n<p><br /><strong>Humidifier</strong>: By adding humidity to heated, dry air, the air feels warmer, helps reduce dry skin and static shock plus you can lower the thermostat and still be comfortable while saving money.</p>\r\n<p><br /><strong>Dehumidifiers</strong>: By removing moisture from sticky indoor air, dehumidifiers can help balance indoor humidity level and minimize the potential for mold and other pollutants.</p>\r\n<p><br /><strong>Air Purification</strong>: Air purification systems are attached to the furnace or air handler, where contaminants are removed and destroyed before air is recirculated into the home.</p>\r\n<p><br /><strong>Germicidal Lights</strong>: Power with maximum- intensity ultraviolet light, these products help to dramatically reduce concentrations of microorganisms such as mildew, bacteria and mold.</p>\r\n<p><br /><strong>Filtration Systems</strong>: Attach to your furnace or air handler, these systems combat indoor pollution by capturing allergy aggravating particles like pollen, mold and pet dander.</p>\r\n<p><br /><strong>View a list of our Indoor Air Quality products and their benefits in <a href=\"resources/img/IAQ.pdf\">this document</a>.</strong></p>\r\n<p>All of our indoor air quality products have the <a href=\"resources/img/Good Housekeeping.pdf\" target=\"_blank\">Good Housekeeping Seal of Approval</a>.</p>',1,6,3,1,1,2,1422212307,2,1427932868,0,0,0,1422212280,2,'',0,0,0,0,0,'modDocument','web',1,'services/indoor-air-quality',0,0,1,NULL),
	(11,'document','text/html','Promotions','','','promotions','',1,0,0,0,1,'','',1,3,10,1,1,1,1418034835,1,1418034929,0,0,0,1418034780,1,'',0,0,0,0,1,'modDocument','web',1,'promotions/',0,0,1,NULL),
	(12,'document','text/html','Rebates','','','test-promotion','',1,0,0,11,0,'','<p>Up to a $1,300 rebate on qualifying home comfort systems Savings on qualifying home comfort systems that include furnace or blower coil, air conditioner or heat pump and thermostat.</p>\r\n<p>$100 Rebate on iHarmony Zoning System Add-on rebate when your customers purchase an iHarmony Zoning System with a qualifying home comfort system.</p>\r\n<p>$300 Rebate on Solar Panels Add-on rebate when your customers purchase four or more solar panels with a qualifying home comfort system.</p>\r\n<p><em>*$1,700 offer includes Ultimate Comfort System with the iHarmony and Solar Panel Add-Ons. Rebate or Financing Eligibility: Each homeowner may qualify for one offer, rebate or financing. Financing reimbursement only available when Service Finance used for financing of qualifying products.</em></p>\r\n<p><strong><em>Promotion runs through June 12, 2015.</em></strong></p>',1,3,0,1,1,1,1418035030,2,1427766125,0,0,0,1418035020,1,'',0,0,0,0,0,'modDocument','web',1,'promotions/test-promotion',0,0,1,NULL),
	(21,'document','text/html','Replacements','','','replacements','',1,0,0,0,0,'','<p>Our goal at HHC is to build a relationship with our homeowners. We want to insure that your home is comfortable. The best way to do this is through good communication. Our staff will talk to you about your issues with your current system. They will ask you about allergies, air flow and energy bills. This information is all used to develop and construct the appropriate heating and cooling system for your home.</p>\n<p>We typically offer 3-4 different system options. These options vary due to budget, potential rebates, equipment efficiency and accessory additions. Accessory additions would include items such as humidifiers, additional air filtration or germicidal lights.</p>\n<p>We understand that budget is a deciding issue when changing out the 4th most expensive modification to your home. Our staff will work with you to find the equipment that best meets your needs both financially and for home comfort. We work with financing programs that can assist with very low credit scores to the very highest scores.</p>\n<p>If you need to replace your equipment, we will find a way to help you get it done. All of our full system replacement costs include a 2 year Comfort Team Membership. This means that twice each year for two years we will come to your home. Once to clean and maintain the furnace and once to clean and maintain your air-conditioner. We find this important to not only keep your system in good working order but it allows time for questions and good communication to continue building our relationship. Please <strong>fill out the form below</strong> to schedule a time to chat about your home comfort system.</p>\n<p><img style=\"float: left; margin: 20px;\" src=\"resources/img/Replacment%20Coupon.jpg\" alt=\"Replacements coupon\" width=\"276\" height=\"217\" /></p>\n<p> </p>\n<p> </p>\n<p> </p>\n<p> </p>\n<p> </p>\n<p> </p>\n<p> </p>\n<p> </p>\n<p> </p>',1,8,4,1,1,2,1422719722,1,1428498639,0,0,0,1422719820,2,'',0,0,0,0,0,'modDocument','web',1,'replacements',0,0,1,NULL),
	(22,'document','text/html','New Construction','','','new-construction','',1,0,0,0,0,'','<p><img style=\"float: left; margin: 0px 20px 20px 0px;\" src=\"resources/img/newconstruction_incontent_300px.jpg\" alt=\"new construction couple\" width=\"300\" height=\"200\" />Last year HHC provided new, energy efficient heating and cooling systems to almost 700 new construction homes. These systems include such options as fireplaces, in-floor heating and zoning.</p>\n<p>Zoning a mid to large size home gives us the ability to better balance the airflow within the home, insuring maximum comfortability in every room. Humidifiers, dehumidifiers and other indoor air quality options are added to combat allergies and the varying degrees of moisture in the air.</p>\n<p>We also offer garage heaters and mini-split systems to add to the comfort of the entire home.</p>\n<p>If you are interested in building a new home, <strong>please upload your plan using the form below</strong>. We would appreciate the opportunity to provide a quote for your new home. Heartland Heating &amp; Cooling provides an outstanding installation and unbeatable customer service. We truly want our customers to be comfortable in their homes.</p>',1,8,3,1,1,2,1422720111,1,1428498628,0,0,0,1422720060,2,'',0,0,0,0,0,'modDocument','web',1,'new-construction',0,0,1,NULL),
	(23,'document','text/html','Commercial HVAC','','','commercial','',1,0,0,3,0,'','<p><img style=\"float: left;\" src=\"resources/img/Commercial_incontent_300px.jpg\" alt=\"rooftop heating unit\" width=\"300\" height=\"208\" /></p>\r\n<p>Are you renovating or building a commercial building? HHC can provide improvements and replacements in most small to mid-size commercial units or buildings. We offer outstanding leasing and financing programs that include not only your HVAC equipment but can be inclusive for your total commercial project.</p>\r\n<p><strong><a href=\"[[~7]]\">Contact us</a></strong> for more information on how we can meet your commercial HVAC needs.</p>',1,6,5,1,1,2,1422720486,2,1427844693,0,0,0,1422720480,2,'Commercial',0,0,0,0,0,'modDocument','web',1,'services/commercial',0,0,1,NULL),
	(24,'document','text/html','UP TO <strong>$1,700 </br>IN SAVINGS</strong>','','','up-to-$1,700-in-savings','',1,0,0,1,0,'','<p>Homeowners can get up to $1,300 in rebates on an Ultimate Comfort System™. Plus rebates of $100 on an iHarmony™ Zoning System and $300 on qualifying Solar Panels for a total of “Up to $1,700 in Rebates!”*</p>',1,7,0,1,1,1,1427308894,2,1427765382,0,0,0,1427308860,1,'',0,0,0,0,0,'modDocument','web',1,'index/up-to-$1,700-in-savings',0,0,1,NULL),
	(25,'document','text/html','Replacing Equipment?','','','slide-2','',1,0,0,1,0,'','<p><span>Replacing your equipment is easier and more affordable than you think, with Heartland.</span></p>',1,7,1,1,1,1,1427308917,2,1427844943,0,0,0,1427308860,1,'',0,0,0,0,0,'modDocument','web',1,'index/slide-2',0,0,1,NULL),
	(26,'document','text/html','Thermostats','','','thermostats','',1,0,0,3,0,'','<p><img style=\"float: left;\" src=\"resources/img/iComfort.jpg\" alt=\"iComfort Thermostat\" width=\"300\" height=\"296\" />Your thermostat may be small, but it is in complete control of your heating and cooling units. It is important to keep your thermostat updated to insure the most cost savings.</p>\r\n<p>Whether you are a person that gets excited about new technology or a person that just wants to \"Set it and Forget it\" we have a thermostat for you. <span style=\"line-height: 1.5em;\"> </span></p>\r\n<p>The latest and greatest thermostats can be run from a cell phone or tablet from anywhere you have internet service. They can control all of your home comfort and indoor air-quality equipment all from one central locations. For example, if it gets extremely hot unexpectedly, you can turn your air-conditioner on from your phone and it will be cool and comfortable when you get home. If you are a person that just wants it to stay 72 degrees in your home or you don\'t want any fancy bells and whistles, we have a great option for you as well.</p>\r\n<p><strong>View a list of our thermostats and their benefits in <a href=\"resources/img/t-stats.pdf\">this document</a>.</strong></p>\r\n<p>All of our thermostats have the <a href=\"resources/img/Good Housekeeping.pdf\" target=\"_blank\">Good Housekeeping Seal of Approval</a>.</p>',1,6,4,1,1,2,1427590550,2,1427852121,0,0,0,1427590980,2,'',0,0,0,0,0,'modDocument','web',1,'services/thermostats',0,0,1,NULL),
	(27,'document','text/html','Heating','','','heating','',1,0,0,22,0,'','<p><img style=\"float: left; margin-bottom: 10px; margin-right: 10px;\" src=\"resources/img/DaveFurnace.jpg\" alt=\"dave furnace lennox\" width=\"244\" height=\"350\" />Heartland Heating &amp; Cooling can take care of all your heating needs. We offer solutions for your home, office, garage, seasonal porches and even your outside living spaces. Our goal is to provide a comfortable environment just about anywhere you want to spend time. There are many different applications and price points available. We also offer rebates and financing options. Heartland is a “one stop shop” for all of your home comfort needs.</p>\r\n<p><strong><a href=\"resources/img/Furnaces.pdf\" target=\"_blank\">Click here</a></strong> for more information.</p>\r\n<p>All of our heating products have the <a href=\"resources/img/Good Housekeeping.pdf\" target=\"_blank\">Good Housekeeping Seal of Approval</a>.</p>',1,6,0,1,1,2,1427842866,2,1427854541,0,0,0,1418010000,1,'',0,0,0,0,0,'modDocument','web',1,'new-construction/heating',0,0,1,NULL),
	(28,'document','text/html','Cooling','','','cooling','',1,0,0,22,0,'','<p><img style=\"float: left; margin-right: 10px; margin-bottom: 10px;\" src=\"resources/img/AC.jpg\" alt=\"air conditioner and dave\" width=\"250\" height=\"347\" />We all love being comfortable and typical Iowa summer can make that a challenge. Let Heartland Heating &amp; Cooling help keep that heat and humidity under control. We want to help ensure the comfort level you want and desire with one of our Air Conditioners.</p>\r\n<p><strong><a href=\"resources/img/Air.pdf\" target=\"_blank\">Click here</a></strong> for more information.</p>\r\n<p>All of our cooling products have the<strong> </strong><a href=\"resources/img/Good Housekeeping.pdf\" target=\"_blank\">Good Housekeeping Seal of Approval</a>.</p>',1,6,1,1,1,2,1427842889,2,1427932384,0,0,0,1418010060,1,'',0,0,0,0,0,'modDocument','web',1,'new-construction/cooling',0,0,1,NULL),
	(29,'document','text/html','Geo-Thermal','','','geo-thermal','',1,0,0,22,0,'','<p dir=\"ltr\"><span>Want to save money and the environment? Look no further Heartland Heating &amp; Cooling has your answer with a new Geothermal System.</span></p>\r\n<p dir=\"ltr\"><span>Geothermal heating and cooling systems take advantage of the stable temperature underground using a piping system, commonly referred to as a “loop.” Water circulates in the loop to exchange heat between your home, the ground source heat pump, and the earth, providing heating, cooling, and hot water at remarkably high efficiencies.</span></p>\r\n<p dir=\"ltr\"><span>According to the US Department of Energy, the typical American family spends at least $2,000 a year on home electric bills. Heating, cooling and hot water costs combined make up over 70% of the typical electric bill. With the ENERGY STAR® qualified Geothermal systems that Heartland Heating &amp; Cooling install you can save up to 80% of those cost. </span></p>\r\n<p dir=\"ltr\"><span>There’s even more savings! Until December 21, 2016, you can receive a 30% federal tax credit on the installation cost of a qualifying geothermal system. This tax credit must be claimed within the year of installation. </span></p>\r\n<p><strong><span id=\"docs-internal-guid-8d2636a3-225e-fee5-e204-fdb60da6a035\"><a href=\"resources/img/Geothermal.pdf\" target=\"_blank\">Click here</a> to see how Geothermal heating and coolying systems work.</span></strong></p>',1,6,2,1,1,2,1427842909,2,1427851593,0,0,0,1418010120,1,'',0,0,0,0,0,'modDocument','web',1,'new-construction/geo-thermal',0,0,1,NULL),
	(30,'document','text/html','Indoor Air Quality','','','indoor-air-quality','',1,0,0,22,0,'','<p>Heartland Heating &amp; Cooling’s # 1 priority is your comfort. Which starts with Indoor Air Quality. Keeping your family comfortable, healthy and breathing easy is stress free with Heartland’s many solutions to your indoor air quality needs.</p>\r\n<p><br /><strong>Humidifier</strong>: By adding humidity to heated, dry air, the air feels warmer, helps reduce dry skin and static shock plus you can lower the thermostat and still be comfortable while saving money.</p>\r\n<p><br /><strong>Dehumidifiers</strong>: By removing moisture from sticky indoor air, dehumidifiers can help balance indoor humidity level and minimize the potential for mold and other pollutants.</p>\r\n<p><br /><strong>Air Purification</strong>: Air purification systems are attached to the furnace or air handler, where contaminants are removed and destroyed before air is recirculated into the home.</p>\r\n<p><br /><strong>Germicidal Lights</strong>: Power with maximum- intensity ultraviolet light, these products help to dramatically reduce concentrations of microorganisms such as mildew, bacteria and mold.</p>\r\n<p><br /><strong>Filtration Systems</strong>: Attach to your furnace or air handler, these systems combat indoor pollution by capturing allergy aggravating particles like pollen, mold and pet dander.</p>\r\n<p><br /><strong>View a list of our Indoor Air Quality products and their benefits in <a href=\"resources/img/IAQ.pdf\">this document</a>.</strong></p>\r\n<p>All of our indoor air quality products have the <a href=\"resources/img/Good Housekeeping.pdf\" target=\"_blank\">Good Housekeeping Seal of Approval</a>.</p>',1,6,3,1,1,2,1427842933,2,1427932882,0,0,0,1422212280,2,'',0,0,0,0,0,'modDocument','web',1,'new-construction/indoor-air-quality',0,0,1,NULL),
	(31,'document','text/html','Thermostats','','','thermostats','',1,0,0,22,0,'','<p><img style=\"float: left;\" src=\"resources/img/iComfort.jpg\" alt=\"iComfort Thermostat\" width=\"300\" height=\"296\" />Your thermostat may be small, but it is in complete control of your heating and cooling units. It is important to keep your thermostat updated to insure the most cost savings.</p>\r\n<p>Whether you are a person that gets excited about new technology or a person that just wants to \"Set it and Forget it\" we have a thermostat for you. <span style=\"line-height: 1.5em;\"> </span></p>\r\n<p>The latest and greatest thermostats can be run from a cell phone or tablet from anywhere you have internet service. They can control all of your home comfort and indoor air-quality equipment all from one central locations. For example, if it gets extremely hot unexpectedly, you can turn your air-conditioner on from your phone and it will be cool and comfortable when you get home. If you are a person that just wants it to stay 72 degrees in your home or you don\'t want any fancy bells and whistles, we have a great option for you as well.</p>\r\n<p><strong>View a list of our thermostats and their benefits in <a href=\"resources/img/t-stats.pdf\">this document</a>.</strong></p>\r\n<p>All of our thermostats have the <a href=\"resources/img/Good Housekeeping.pdf\" target=\"_blank\">Good Housekeeping Seal of Approval</a>.</p>',1,6,4,1,1,2,1427854402,0,0,0,0,0,1427590980,2,'',0,0,0,0,0,'modDocument','web',1,'new-construction/thermostats',0,0,1,NULL),
	(32,'document','text/html','Commercial HVAC','','','commercial-hvac','',1,0,0,22,0,'','<p><img style=\"float: left;\" src=\"resources/img/Commercial_incontent_300px.jpg\" alt=\"rooftop heating unit\" width=\"300\" height=\"208\" /></p>\r\n<p>Are you renovating or building a commercial building? HHC can provide improvements and replacements in most small to mid-size commercial units or buildings. We offer outstanding leasing and financing programs that include not only your HVAC equipment but can be inclusive for your total commercial project.</p>\r\n<p><strong><a href=\"[[~7]]\">Contact us</a></strong> for more information on how we can meet your commercial HVAC needs.</p>',1,6,5,1,1,2,1427854422,0,0,0,0,0,1422720480,2,'Commercial',0,0,0,0,0,'modDocument','web',1,'new-construction/commercial-hvac',0,0,1,NULL),
	(33,'document','text/html','Heating','','','','',1,0,0,21,0,'','<p><img style=\"float: left; margin-bottom: 10px; margin-right: 10px;\" src=\"resources/img/DaveFurnace.jpg\" alt=\"dave furnace lennox\" width=\"244\" height=\"350\" />Heartland Heating &amp; Cooling can take care of all your heating needs. We offer solutions for your home, office, garage, seasonal porches and even your outside living spaces. Our goal is to provide a comfortable environment just about anywhere you want to spend time. There are many different applications and price points available. We also offer rebates and financing options. Heartland is a “one stop shop” for all of your home comfort needs.</p>\r\n<p><strong><a href=\"resources/img/Furnaces.pdf\" target=\"_blank\">Click here</a></strong> for more information.</p>\r\n<p>All of our heating products have the <a href=\"resources/img/Good Housekeeping.pdf\" target=\"_blank\">Good Housekeeping Seal of Approval</a>.</p>',1,6,0,1,1,2,1427854451,0,0,0,0,0,1418010000,1,'',0,0,0,0,0,'modDocument','web',1,'replacements/heating',0,0,1,NULL),
	(34,'document','text/html','Cooling','','','cooling','',1,0,0,21,0,'','<p><img style=\"float: left; margin-right: 10px; margin-bottom: 10px;\" src=\"resources/img/AC.jpg\" alt=\"air conditioner and dave\" width=\"250\" height=\"347\" />We all love being comfortable and typical Iowa summer can make that a challenge. Let Heartland Heating &amp; Cooling help keep that heat and humidity under control. We want to help ensure the comfort level you want and desire with one of our Air Conditioners.</p>\r\n<p><strong><a href=\"resources/img/Air.pdf\" target=\"_blank\">Click here</a></strong> for more information.</p>\r\n<p>All of our cooling products have the<strong> </strong><a href=\"resources/img/Good Housekeeping.pdf\" target=\"_blank\">Good Housekeeping Seal of Approval</a>.</p>',1,6,1,1,1,2,1427854464,2,1427932400,0,0,0,1418010060,1,'',0,0,0,0,0,'modDocument','web',1,'replacements/cooling',0,0,1,NULL),
	(35,'document','text/html','Geo-Thermal','','','','',1,0,0,21,0,'','<p dir=\"ltr\"><span>Want to save money and the environment? Look no further Heartland Heating &amp; Cooling has your answer with a new Geothermal System.</span></p>\r\n<p dir=\"ltr\"><span>Geothermal heating and cooling systems take advantage of the stable temperature underground using a piping system, commonly referred to as a “loop.” Water circulates in the loop to exchange heat between your home, the ground source heat pump, and the earth, providing heating, cooling, and hot water at remarkably high efficiencies.</span></p>\r\n<p dir=\"ltr\"><span>According to the US Department of Energy, the typical American family spends at least $2,000 a year on home electric bills. Heating, cooling and hot water costs combined make up over 70% of the typical electric bill. With the ENERGY STAR® qualified Geothermal systems that Heartland Heating &amp; Cooling install you can save up to 80% of those cost. </span></p>\r\n<p dir=\"ltr\"><span>There’s even more savings! Until December 21, 2016, you can receive a 30% federal tax credit on the installation cost of a qualifying geothermal system. This tax credit must be claimed within the year of installation. </span></p>\r\n<p><strong><span id=\"docs-internal-guid-8d2636a3-225e-fee5-e204-fdb60da6a035\"><a href=\"resources/img/Geothermal.pdf\" target=\"_blank\">Click here</a> to see how Geothermal heating and coolying systems work.</span></strong></p>',1,6,2,1,1,2,1427854478,0,0,0,0,0,1418010120,1,'',0,0,0,0,0,'modDocument','web',1,'replacements/geo-thermal',0,0,1,NULL),
	(36,'document','text/html','Indoor Air Quality','','','indoor-air-quality','',1,0,0,21,0,'','<p>Heartland Heating &amp; Cooling’s # 1 priority is your comfort. Which starts with Indoor Air Quality. Keeping your family comfortable, healthy and breathing easy is stress free with Heartland’s many solutions to your indoor air quality needs.</p>\r\n<p><br /><strong>Humidifier</strong>: By adding humidity to heated, dry air, the air feels warmer, helps reduce dry skin and static shock plus you can lower the thermostat and still be comfortable while saving money.</p>\r\n<p><br /><strong>Dehumidifiers</strong>: By removing moisture from sticky indoor air, dehumidifiers can help balance indoor humidity level and minimize the potential for mold and other pollutants.</p>\r\n<p><br /><strong>Air Purification</strong>: Air purification systems are attached to the furnace or air handler, where contaminants are removed and destroyed before air is recirculated into the home.</p>\r\n<p><br /><strong>Germicidal Lights</strong>: Power with maximum- intensity ultraviolet light, these products help to dramatically reduce concentrations of microorganisms such as mildew, bacteria and mold.</p>\r\n<p><br /><strong>Filtration Systems</strong>: Attach to your furnace or air handler, these systems combat indoor pollution by capturing allergy aggravating particles like pollen, mold and pet dander.</p>\r\n<p><br /><strong>View a list of our Indoor Air Quality products and their benefits in <a href=\"resources/img/IAQ.pdf\">this document</a>.</strong></p>\r\n<p>All of our indoor air quality products have the <a href=\"resources/img/Good Housekeeping.pdf\" target=\"_blank\">Good Housekeeping Seal of Approval</a>.</p>',1,6,3,1,1,2,1427854496,2,1427932897,0,0,0,1422212280,2,'',0,0,0,0,0,'modDocument','web',1,'replacements/indoor-air-quality',0,0,1,NULL),
	(37,'document','text/html','Thermostats','','','','',1,0,0,21,0,'','<p><img style=\"float: left;\" src=\"resources/img/iComfort.jpg\" alt=\"iComfort Thermostat\" width=\"300\" height=\"296\" />Your thermostat may be small, but it is in complete control of your heating and cooling units. It is important to keep your thermostat updated to insure the most cost savings.</p>\r\n<p>Whether you are a person that gets excited about new technology or a person that just wants to \"Set it and Forget it\" we have a thermostat for you. <span style=\"line-height: 1.5em;\"> </span></p>\r\n<p>The latest and greatest thermostats can be run from a cell phone or tablet from anywhere you have internet service. They can control all of your home comfort and indoor air-quality equipment all from one central locations. For example, if it gets extremely hot unexpectedly, you can turn your air-conditioner on from your phone and it will be cool and comfortable when you get home. If you are a person that just wants it to stay 72 degrees in your home or you don\'t want any fancy bells and whistles, we have a great option for you as well.</p>\r\n<p><strong>View a list of our thermostats and their benefits in <a href=\"resources/img/t-stats.pdf\">this document</a>.</strong></p>\r\n<p>All of our thermostats have the <a href=\"resources/img/Good Housekeeping.pdf\" target=\"_blank\">Good Housekeeping Seal of Approval</a>.</p>',1,6,4,1,1,2,1427854510,0,0,0,0,0,1427590980,2,'',0,0,0,0,0,'modDocument','web',1,'replacements/thermostats',0,0,1,NULL),
	(38,'document','text/html','Commercial','','','commercial','',1,0,0,21,0,'','<p><img style=\"float: left;\" src=\"resources/img/Commercial_incontent_300px.jpg\" alt=\"rooftop heating unit\" width=\"300\" height=\"208\" /></p>\r\n<p>Are you renovating or building a commercial building? HHC can provide improvements and replacements in most small to mid-size commercial units or buildings. We offer outstanding leasing and financing programs that include not only your HVAC equipment but can be inclusive for your total commercial project.</p>\r\n<p><strong><a href=\"[[~7]]\">Contact us</a></strong> for more information on how we can meet your commercial HVAC needs.</p>',1,6,5,1,1,2,1427854530,0,0,0,0,0,1422720480,2,'Commercial',0,0,0,0,0,'modDocument','web',1,'replacements/commercial',0,0,1,NULL),
	(39,'document','text/html','How to use your site','','','how-to-use-your-site','',1,0,0,0,0,'','<p><strong>Accessing the manager:</strong></p>\r\n<p><a href=\"manager\" target=\"_blank\">[[++site_url]]manager</a></p>\r\n<p><strong>Your CMS:<br /><br /></strong>Content pages and articles are located under the Resources tab to the left.</p>\r\n<p>Right click a navigation item for the admin menu to appear.</p>\r\n<p>Image and PDF assets are located under Files.</p>\r\n<p>Right click an asset item for the admin menu to appear.</p>\r\n<p id=\"video\"><strong>Video Tutorials:</strong><br />I reccomend walking through some of these. These will brush up on what I talked about during the training we went through. These videos show the basics of modx and how to access a lot of various thing. This playlist contains 12 videos, it is not required to know everything within each video to properly use your website, but it helps for general knowledge. Again, watching these videos is not required, but reccomended. I have also extracted which videos I deemed as most important from this playlist and I have listed them below.</p>\r\n<p><strong>Each video runs between 2 - 8 minutes long</strong></p>\r\n<p><iframe style=\"line-height: 1.5em;\" src=\"http://www.youtube.com/embed/videoseries?list=PL5652FA1EA27BC5A0\" frameborder=\"0\" width=\"560\" height=\"315\"></iframe></p>\r\n<p><strong>Below is the list of important videos that I feel you should be familiar with. I highly reccomend watching those videos.</strong></p>\r\n<p id=\"intro\"><strong style=\"line-height: 1.5em;\">MODX Introduction:</strong></p>\r\n<p><iframe src=\"http://www.youtube.com/embed/M-qTFHBkINE\" frameborder=\"0\" width=\"560\" height=\"315\"></iframe></p>\r\n<p id=\"resources\"><strong>Resources Tab</strong> (the pages of your website)<strong>:</strong><br />This video goes over the basics of navigating the resource tab. It does cover thing like adding dropdowns and moving pages within the resource tree, I do not suggest you try that. If you need a new navigation item, please contact me through email at <a href=\"mailto:justin.lobaito@weloideas.com\">justin.lobaito@weloideas.com</a></p>\r\n<p><iframe src=\"http://www.youtube.com/embed/S0VJiHSlBjc\" frameborder=\"0\" width=\"560\" height=\"315\"></iframe></p>\r\n<p><strong>Resources Tab</strong> <strong>Part 2 - </strong>Right click navigation:<br />This video goes over using the right click navigation on a resource item, this is handy for quick editing and navigating to various pages of your website.</p>\r\n<p><iframe src=\"http://www.youtube.com/embed/K7oIDxXgGqQ\" frameborder=\"0\" width=\"560\" height=\"315\"></iframe></p>\r\n<p> </p>\r\n<p id=\"content\"><strong>Editing Content:</strong></p>\r\n<p>Very important videos on how to edit content on the website</p>\r\n<p><iframe src=\"http://www.youtube.com/embed/zJvhbmLXyj8?list=PL5652FA1EA27BC5A0\" frameborder=\"0\" width=\"560\" height=\"315\"></iframe> <iframe src=\"http://www.youtube.com/embed/4ahuRzfFL1Q?list=PL5652FA1EA27BC5A0\" frameborder=\"0\" width=\"560\" height=\"315\"></iframe></p>\r\n<p id=\"linking\"><strong>Adding Links to Conent:</strong></p>\r\n<p>Video that shows how to add links. This video covers how to link to a document, webpage, internal webpage, etc...</p>\r\n<p><iframe src=\"http://www.youtube.com/embed/skhhoJY-97E?list=PL5652FA1EA27BC5A0\" frameborder=\"0\" width=\"560\" height=\"315\"></iframe></p>\r\n<p id=\"images\"><strong>Adding Images:</strong></p>\r\n<p><strong>VERY IMPORTANT:</strong> these videos show how to upload images, insert images into content, etc... <strong>This is a very important feature to understand</strong></p>\r\n<p>Be sure to size your images to the proper size before uploading to content management site. A great resource is <a href=\"http://www.picmonkey.com/\" target=\"_blank\">picmonkey.com</a> <iframe src=\"http://www.youtube.com/embed/FrKdb9sNoFA?list=PL5652FA1EA27BC5A0\" frameborder=\"0\" width=\"560\" height=\"315\"></iframe> <iframe src=\"http://www.youtube.com/embed/j681YvTPN08?list=PL5652FA1EA27BC5A0\" frameborder=\"0\" width=\"560\" height=\"315\"></iframe></p>\r\n<p>To edit content within a page navigate to and click on the name of the page within your resources tree or right click on the page name and select Edit Resource</p>\r\n<p>Type or copy and paste from txt document (such as Notepad). You do have to reformat all text in this editor (bullets, italics, etc.)</p>\r\n<p>Clicking SAVE WILL send your content live if the Published box in the Document tab is already checked.</p>\r\n<p>If you are creating a new weblink to an external site or document in your navigation (i.e. COF Reports) in the Link Attributes section type target=\"_blank\" in order to have the external or document open in a new window.</p>\r\n<p>You do have the ability to create tables in your editor but be sure to keep them small in width as mobile devices will break large tables.</p>\r\n<p>Shift+Enter will do a soft return (one line instead of two).</p>\r\n<p><strong>If in doubt as to where a piece of editable content resides - try the Template Variables tab</strong></p>\r\n<p>Best practices to always have a document or external website open in a new window (in the link window pulldown Target: Open in a New Window (_blank)</p>\r\n<p><strong>Assets (Documents and Images) - Found under the Files tab</strong></p>\r\n<p>The Create Directory Icon will allow you to create new folders for your assets.</p>\r\n<p>Be sure to size your images to the proper size before uploading to content management site. A great resource is <a href=\"http://www.picmonkey.com/\" target=\"_blank\">picmonkey.com</a>. Images for the inside pages, are best to be at most 600px wide.</p>\r\n<p>To upload a file first navigate to the folder where you wish the asset to reside and click on the Upload Files (document with an arrow) icon. You will then be able to browse to and upload your assets one at a time.</p>\r\n<p>When uploading assets be sure the filename does not contain any spaces or strange characters.</p>\r\n<p>To insert an image into a page or article simply click on the insert/edit image button and then select the Browse icon to browse to and select the image.</p>\r\n<p>To link a document asset in a page or article simply select the text you wish to link, click on the insert/edit link icon and then select the Browse icon to browse to and select the asset. Note: Once selected pulldown Target: Open in a New Window (_blank)</p>\r\n<p> </p>\r\n<p> </p>',1,3,11,1,1,1,1429097799,1,1429098023,0,0,0,1429097760,1,'',0,0,0,0,1,'modDocument','web',1,'how-to-use-your-site',0,0,1,NULL),
	(40,'document','text/html','Test slide 1','','','test-slide-1','',1,0,0,1,0,'','<p>this is a test.</p>',1,7,12,1,1,3,1429893336,3,1429893527,1,1429893544,3,1429893420,3,'',0,0,0,0,0,'modDocument','web',1,'test-slide-1',0,0,1,NULL);

/*!40000 ALTER TABLE `modx_site_content` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_htmlsnippets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_htmlsnippets`;

CREATE TABLE `modx_site_htmlsnippets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` int(10) unsigned NOT NULL DEFAULT '0',
  `property_preprocess` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Chunk',
  `editor_type` int(11) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0',
  `snippet` mediumtext,
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `properties` text,
  `static` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `static_file` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category` (`category`),
  KEY `locked` (`locked`),
  KEY `static` (`static`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_htmlsnippets` WRITE;
/*!40000 ALTER TABLE `modx_site_htmlsnippets` DISABLE KEYS */;

INSERT INTO `modx_site_htmlsnippets` (`id`, `source`, `property_preprocess`, `name`, `description`, `editor_type`, `category`, `cache_type`, `snippet`, `locked`, `properties`, `static`, `static_file`)
VALUES
	(1,1,0,'nav-inner','',0,2,0,'<li><a href=\"[[~[[+id]]]]\" title=\"[[+menutitle]]\">[[+menutitle]]</a></li>',0,'a:0:{}',0,''),
	(2,1,0,'snt_from','',0,4,0,'[[+first_name]] [[+last_name]] left you the message below from [[++site_name]]: <br />\n<br />\nPhone Number: [[+phone]]<br />\nAddress: [[+address]]<br />\nCity: [[+city]]<br />\nState: [[+state]]<br />\nZip: [[+zip]]<br />\n<br />\n\n\n<strong>[[+comments]]</strong><br /><br />\n\nYou can respond to [[+first_name]] [[+last_name]] by email at <a href=\"mailto:[[+email]]\">[[+email]]</a> or by phone at [[+phone]]',0,'a:0:{}',0,''),
	(3,1,0,'nav-outer','',0,2,0,'<ul>\n[[+wf.wrapper]]\n</ul>',0,'a:0:{}',0,''),
	(4,1,0,'doc_head','',0,5,0,'<!DOCTYPE html>\n<!--[if lt IE 7]>      <html class=\"no-js lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->\n<!--[if IE 7]>         <html class=\"no-js lt-ie9 lt-ie8\"> <![endif]-->\n<!--[if IE 8]>         <html class=\"no-js lt-ie9\"> <![endif]-->\n<!--[if gt IE 8]><!--> <html class=\"no-js\"> <!--<![endif]-->\n<head>\n    <meta charset=\"utf-8\">\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">\n    <title>[[*pagetitle]] | [[++site_name]]</title>\n    <meta name=\"description\" content=\"[[*seo_description]]\">\n    <meta name=\"keywords\" content=\"[[*seo_keywords]]\">\n    <meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0\">\n\n    <!-- THIS IS NEEDED FOR FURL -->\n    <base href=\"[[++site_url]]\" />\n\n    <meta name=\"description\" content=\"\">\n    <meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0\">\n\n    <!-- Facebook OpenGraph Data -->\n    <meta property=\"og:title\" content=\"[[++site_name]]\" />\n    <meta property=\"og:type\" content=\"website\" />\n        <!-- If it\'s an article:\n        <meta property=\"og:type\" content=\"article\" />\n        <meta property=\"article:published_time\" content = \"\" />\n    -->\n    <meta property=\"og:description\" content=\"[[*seo_description]]\" />        \n    <meta property=\"og:url\" content=\"[[++site_url]]\"/>\n    <meta property=\"og:image\" content=\"[[++social_image]]\" />\n    <meta property=\"og:site_name\" content=\"[[++site_name]]\" />\n\n    <!-- Canonical URL for CMS SEO -->\n    <link rel=\"canonical\" href=\"[[++site_url]]\" />\n\n    <link rel=\"stylesheet\" href=\"/resources/css/bootstrap.css\">\n    <link rel=\"stylesheet\" href=\"/resources/css/bootstrap-theme.css\">\n    <link rel=\"stylesheet\" href=\"/resources/css/base.css\">    \n        <!--[if lte IE 8]>\n        <link rel=\"stylesheet\" href=\"/resources/css/fallback.css\">\n        <link rel=\"stylesheet\" href=\"/resources/css/ie8.css\">\n        <![endif]-->\n        <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js\"></script>\n        <script src=\"/resources/js/vendor/modernizr.js\"></script>\n        <script src=\"/resources/js/nav.js\"></script>\n        <!-- FAVICON -->\n        <link rel=\"icon\" href=\"/resources/img/favicon.png\">\n        <link href=\'http://fonts.googleapis.com/css?family=Raleway:700,400,200\' rel=\'stylesheet\' type=\'text/css\'>\n        <link rel=\"stylesheet\" href=\"https://s3.amazonaws.com/icomoon.io/29117/Heartland/style.css\">\n\n        <!-- GOOGLE ANALYTICS -->\n        <script>\n        (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n      })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n\n      ga(\'create\', \'[[++google_analytics]]\', \'auto\');\n      ga(\'send\', \'pageview\');\n\n      </script>        \n  </head>',0,'a:0:{}',1,'assets/chunks/doc_head.html'),
	(5,1,0,'doc_footer','',0,5,0,'<script src=\"/resources/js/libs/respond.min.js\"></script>\n        <script src=\"/resources/js/libs/bootstrap.min.js\"></script>\n        <script src=\"/resources/js/plugins.js\"></script>\n        <script src=\"/resources/js/main.js\"></script>\n        <script src=\"/resources/js/event-tracker.js\"></script>\n        <script>\n	      var navigation = responsiveNav(\".nav-collapse\");\n	    </script>\n	    <script>\n	    $(window).load(function() {\n		  $(\'.flexslider\').flexslider({\n		    animation: \"slide\",\n		    directionNav: false, \n		  });\n		});\n	    </script>\n    </body>\n</html>',0,'a:0:{}',1,'assets/chunks/doc_footer.html'),
	(6,1,0,'header','',0,5,0,'<div class=\"heroContain\">\n  <div class=\"flexslider\">\n    <ul class=\"slides\">\n        [[getResources?\n          &parents=`1`\n          &tpl=`slides`\n          &showHidden=`1`\n          &limit=`0`\n          &includeContent=`1`\n          &includeTVs=`1` \n          &processTVs=`1` \n          &tvPrefix=``\n        ]]\n        \n    </ul>\n</div>\n</div>',0,'a:0:{}',1,'assets/chunks/header.html'),
	(7,1,0,'footer','',0,5,0,'<section class=\"footer\">\n    <div class=\"row-fluid wrap\">\n        [[Wayfinder? \n        &level=`0`\n        &startId=`0`\n        &innerTpl=`nav_inner`\n        &outerTpl=`nav_outer`\n        &level=`1`\n        &sortBy=`menuindex`\n        \n\n        ]]\n        <p>Copyright © 2014 | Website by <a href=\"\">Avidity Creative</a></p>\n    </div>\n</section>      	',0,'a:0:{}',1,'assets/chunks/footer.html'),
	(8,1,0,'navigation','',0,2,0,'[[$estimate]]\n<div class=\"navContain\">\n<div class=\"tophat\">\n	<div class=\"wrap\">\n		<p class=\"telephone\"> Call us! 515-986-5007</p>\n		<a href=\"#estimate\" data-toggle=\"modal\" class=\"button quote\" >Free Estimate</a>\n	</div>\n</div>\n<nav class=\"mainnav\">\n	<div class=\"wrap\">\n		<div class=\"logo-elipse\"><a href=\"/\"><img src=\"/resources/img/logo-elipse.png\" alt=\"\"></a></div>\n		<div class=\"logo\"><a href=\"/\"><img src=\"/resources/img/logo.jpg\" alt=\"\"></a></div>\n		<div class=\"nav-collapse\">\n		[[Wayfinder? \n        &level=`0`\n        &startId=`0`\n        &innerTpl=`nav_inner`\n        &outerTpl=`nav_outer`\n        &level=`1`\n        &sortBy=`menuindex`\n        &excludeDocs=`21,22`\n        ]]\n    </div>\n	</div>\n</nav>\n</div>',0,'a:0:{}',1,'assets/chunks/navigation.html'),
	(9,1,0,'header-interior','',0,5,0,'<div class=\"heroContain\">\n<section class=\"hero\" style=\"background-image:url(\'[[*background-image]]\');\">\n	\n	<div class=\"center\">\n		<div class=\"overlay\">\n		</div>\n		<div class=\"wrap\">\n			<div class=\"hero-content\">\n				<h2><strong>[[*pagetitle]]</strong></h2>\n				<div class=\"intro-text\">[[*intro-text]]</div>\n			</div>\n		</div>\n	</div>  \n</section>\n</div>',0,'a:0:{}',1,'assets/chunks/header-interior.html'),
	(10,1,0,'contact_form','',0,4,0,' [[!FormIt?\n       &hooks=`spam, sendGrid, email`\n       &emailFrom=`[[++primary_email]]`\n       &emailTpl=`snt_from`\n       &submitVar=`contactSubmit`\n       &emailTo=`[[++primary_email]]`\n       &emailSubject=`Contact Form Submission`\n       &validate=`phone:required, state:required, first_name:required, last_name:required, city:required, address:required, email:required, email:email, comments:required:stripTags, nospam:blank`\n      &successMessage=`<div class=\"alert alert-success\">\n      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n      <h3>Thank you for getting in contact with us.</h3>\n      <p>A [[++site_name]] representative will get in contact with you shortly</p>\n    </div>`\n    ]]\n\n   [[!+fi.validation_error_message:!empty=`\n\n\n<div class=\"alert alert-error\">\n      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n		<div>\n		      <strong><h3>Please review the following errors:</h3></strong>\n		            [[!+fi.error.first_name:!empty=`<p>First Name is required</p>`]]\n		            [[!+fi.error.last_name:!empty=`<p>Last Name is required</p>`]]\n		            [[!+fi.error.email:!empty=`<p>Email Address is required</p>`]]\n		            [[!+fi.error.phone:!empty=`<p>Phone Number is required</p>`]]\n		            [[!+fi.error.address:!empty=`<p>Address is required</p>`]]\n		            [[!+fi.error.city:!empty=`<p>City is required</p>`]]\n		            [[!+fi.error.state:!empty=`<p>State is required</p>`]]\n					[[!+fi.error.comments:!empty=`<p>Additional Comments is required</p>`]]\n\n		</div>\n</div>\n\n\n`]]\n    [[!+fi.successMessage]]\n\n\n                   <form class=\"main-form row-fluid\" action=\"[[~[[*id]]]]#error\" method=\"post\" enctype=\"multipart/form-data\">\n                        \n                            <div class=\"row-fluid\">\n                                <div class=\"span6\">\n                                    <label for=\"first_name\">First Name<sup>*</sup></label>\n                                    <input type=\"text\" value=\"[[!+fi.first_name]]\" id=\"first_name\" name=\"first_name\"  placeholder=\"Jonathan\">\n                                </div>\n                                <div class=\"span6\">\n                                    <label for=\"last_name\">Last Name<sup>*</sup></label>\n                                    <input type=\"text\" value=\"[[!+fi.last_name]]\" id=\"last_name\" name=\"last_name\"  placeholder=\"Doe\">\n                                </div>\n                            </div>\n                            <label for=\"email\">Email Address<sup>*</sup></label>\n                            <input type=\"text\" value=\"[[!+fi.email]]\" id=\"email\" name=\"email\"   placeholder=\"jon@doe.com\">\n                            <label for=\"phone\">Phone<sup>*</sup></label>\n                            <input type=\"text\" value=\"[[!+fi.phone]]\" id=\"phone\" name=\"phone\"   placeholder=\"999-999-9999\">\n                            <label for=\"address\">Address<sup>*</sup></label>\n                            <input type=\"text\" value=\"[[!+fi.address]]\" id=\"address\" name=\"address\"  >\n                            <div class=\"row-fluid\">\n                                <div class=\"span4\">\n                                    <label for=\"city\">City<sup>*</sup></label>\n                                    <input value=\"[[!+fi.city]]\" type=\"text\" id=\"city\" name=\"city\"  >\n                                </div>\n                                \n                                <div class=\"span4\">\n                            <label for=\"state\">State<sup>*</sup></label>\n                                    <select value=\"[[!+fi.state]]\" id=\"state\" name=\"state\">\n                                        <option value=\"AL\">Alabama</option>\n                                        <option value=\"AK\">Alaska</option>\n                                        <option value=\"AZ\">Arizona</option>\n                                        <option value=\"AR\">Arkansas</option>\n                                        <option value=\"CA\">California</option>\n                                        <option value=\"CO\">Colorado</option>\n                                        <option value=\"CT\">Connecticut</option>\n                                        <option value=\"DE\">Delaware</option>\n                                        <option value=\"DC\">District of Columbia</option>\n                                        <option value=\"FL\">Florida</option>\n                                        <option value=\"GA\">Georgia</option>\n                                        <option value=\"HI\">Hawaii</option>\n                                        <option value=\"ID\">Idaho</option>\n                                        <option value=\"IL\">Illinois</option>\n                                        <option value=\"IN\">Indiana</option>\n                                        <option value=\"IA\">Iowa</option>\n                                        <option value=\"KS\">Kansas</option>\n                                        <option value=\"KY\">Kentucky</option>\n                                        <option value=\"LA\">Louisiana</option>\n                                        <option value=\"ME\">Maine</option>\n                                        <option value=\"MD\">Maryland</option>\n                                        <option value=\"MA\">Massachusetts</option>\n                                        <option value=\"MI\">Michigan</option>\n                                        <option value=\"MN\">Minnesota</option>\n                                        <option value=\"MS\">Mississippi</option>\n                                        <option value=\"MO\">Missouri</option>\n                                        <option value=\"MT\">Montana</option>\n                                        <option value=\"NE\">Nebraska</option>\n                                        <option value=\"NV\">Nevada</option>\n                                        <option value=\"NH\">New Hampshire</option>\n                                        <option value=\"NJ\">New Jersey</option>\n                                        <option value=\"NM\">New Mexico</option>\n                                        <option value=\"NY\">New York</option>\n                                        <option value=\"NC\">North Carolina</option>\n                                        <option value=\"ND\">North Dakota</option>\n                                        <option value=\"OH\">Ohio</option>\n                                        <option value=\"OK\">Oklahoma</option>\n                                        <option value=\"OR\">Oregon</option>\n                                        <option value=\"PA\">Pennsylvania</option>\n                                        <option value=\"RI\">Rhode Island</option>\n                                        <option value=\"SC\">South Carolina</option>\n                                        <option value=\"SD\">South Dakota</option>\n                                        <option value=\"TN\">Tennessee</option>\n                                        <option value=\"TX\">Texas</option>\n                                        <option value=\"UT\">Utah</option>\n                                        <option value=\"VT\">Vermont</option>\n                                        <option value=\"VA\">Virginia</option>\n                                        <option value=\"WA\">Washington</option>\n                                        <option value=\"WV\">West Virginia</option>\n                                        <option value=\"WI\">Wisconsin</option>\n                                        <option value=\"WY\">Wyoming</option>\n                                      </select>\n                                </div>\n                                <div class=\"span4\">\n                                    <label for=\"zip\">Zip</label>\n                            <input value=\"[[!+fi.zip]]\" type=\"text\" id=\"zip\" name=\"zip\"  >\n<input value=\"[[!+fi.nospam]]\" type=\"text\" id=\"nospam\" name=\"nospam\" class=\"nospam\">\n                                </div>\n                            </div>\n                            <div class=\"row-fluid\">\n                                <label for=\"attachment\">Upload a floorplan:</label>\n                                <div class=\"span6\"><input type=\"file\" name=\"contact_images1\" value=\"[[!+fi.contact_images1]]\" /></div>\n                            </div>\n                            <label for=\"comments\">Additional Comments<sup>*</sup></label>\n                            <textarea value=\"[[!+fi.comments]]\" id=\"comments\" name=\"comments\"></textarea>\n                        <input class=\"button\" type=\"submit\" value=\"Send our way\"  name=\"contactSubmit\" />\n                    </form>\n                  ',0,'a:0:{}',1,'assets/chunks/contact_form.html'),
	(11,1,0,'estimate','',0,5,0,'<div id=\"estimate\" class=\"modal hide fade\">\n  <div class=\"modal-header\">\n    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n    <h3>Free Estimate</h3>\n  </div>\n  <div class=\"modal-body\">\n    [[$contact_form_estimate]]\n  </div>\n</div>',0,'a:0:{}',1,'assets/chunks/estimate.html'),
	(12,1,0,'contact_form_estimate','',0,4,0,' [[!FormIt?\n       &hooks=`spam, sendGrid, email`\n       &emailFrom=`[[++primary_email]]`\n       &emailTpl=`snt_from_quote`\n       &submitVar=`quote`\n       &emailTo=`[[++primary_email]]`\n       &emailSubject=`Free Quote Request`\n       &validate=`phone:required, state:required, first_name:required, last_name:required, city:required, address:required, email:required, email:email, comments:required:stripTags, nospam:blank`\n      &successMessage=`<div class=\"alert alert-success\">\n      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n      <h3>Thank you for getting in contact with us.</h3>\n      <p>A [[++site_name]] representative will get in contact with you shortly</p>\n    </div>`\n    ]]\n\n   [[!+fi.validation_error_message:!empty=`\n\n\n<div class=\"alert alert-error\">\n      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n    <div>\n          <strong><h3>Please review the following errors:</h3></strong>\n                [[!+fi.error.first_name:!empty=`<p>First Name is required</p>`]]\n                [[!+fi.error.last_name:!empty=`<p>Last Name is required</p>`]]\n                [[!+fi.error.email:!empty=`<p>Email Address is required</p>`]]\n                [[!+fi.error.phone:!empty=`<p>Phone Number is required</p>`]]\n                [[!+fi.error.address:!empty=`<p>Address is required</p>`]]\n                [[!+fi.error.city:!empty=`<p>City is required</p>`]]\n                [[!+fi.error.state:!empty=`<p>State is required</p>`]]\n          [[!+fi.error.comments:!empty=`<p>Additional Comments is required</p>`]]\n\n    </div>\n</div>\n\n\n`]]\n    [[!+fi.successMessage]]\n\n\n                   <form class=\"main-form row-fluid\" action=\"[[~[[*id]]]]\" method=\"post\" enctype=\"multipart/form-data\" >\n                        \n                            <div class=\"row-fluid\">\n                                <div class=\"span6\">\n                                    <label for=\"first_name\">First Name<sup>*</sup></label>\n                                    <input type=\"text\" value=\"[[!+fi.first_name]]\" id=\"first_name\" name=\"first_name\"  placeholder=\"Jonathan\">\n                                </div>\n                                <div class=\"span6\">\n                                    <label for=\"last_name\">Last Name<sup>*</sup></label>\n                                    <input type=\"text\" value=\"[[!+fi.last_name]]\" id=\"last_name\" name=\"last_name\"  placeholder=\"Doe\">\n                                </div>\n                            </div>\n                            <label for=\"email\">Email Address<sup>*</sup></label>\n                            <input type=\"text\" value=\"[[!+fi.email]]\" id=\"email\" name=\"email\"   placeholder=\"jon@doe.com\">\n                            <label for=\"phone\">Phone<sup>*</sup></label>\n                            <input type=\"text\" value=\"[[!+fi.phone]]\" id=\"phone\" name=\"phone\"   placeholder=\"999-999-9999\">\n                            <label for=\"address\">Address<sup>*</sup></label>\n                            <input type=\"text\" value=\"[[!+fi.address]]\" id=\"address\" name=\"address\"  >\n                            <div class=\"row-fluid\">\n                                <div class=\"span4\">\n                                    <label for=\"city\">City<sup>*</sup></label>\n                                    <input value=\"[[!+fi.city]]\" type=\"text\" id=\"city\" name=\"city\"  >\n                                </div>\n                                \n                                <div class=\"span4\">\n                            <label for=\"state\">State<sup>*</sup></label>\n                                    <select value=\"[[!+fi.state]]\" id=\"state\" name=\"state\">\n                                        <option value=\"AL\">Alabama</option>\n                                        <option value=\"AK\">Alaska</option>\n                                        <option value=\"AZ\">Arizona</option>\n                                        <option value=\"AR\">Arkansas</option>\n                                        <option value=\"CA\">California</option>\n                                        <option value=\"CO\">Colorado</option>\n                                        <option value=\"CT\">Connecticut</option>\n                                        <option value=\"DE\">Delaware</option>\n                                        <option value=\"DC\">District of Columbia</option>\n                                        <option value=\"FL\">Florida</option>\n                                        <option value=\"GA\">Georgia</option>\n                                        <option value=\"HI\">Hawaii</option>\n                                        <option value=\"ID\">Idaho</option>\n                                        <option value=\"IL\">Illinois</option>\n                                        <option value=\"IN\">Indiana</option>\n                                        <option value=\"IA\">Iowa</option>\n                                        <option value=\"KS\">Kansas</option>\n                                        <option value=\"KY\">Kentucky</option>\n                                        <option value=\"LA\">Louisiana</option>\n                                        <option value=\"ME\">Maine</option>\n                                        <option value=\"MD\">Maryland</option>\n                                        <option value=\"MA\">Massachusetts</option>\n                                        <option value=\"MI\">Michigan</option>\n                                        <option value=\"MN\">Minnesota</option>\n                                        <option value=\"MS\">Mississippi</option>\n                                        <option value=\"MO\">Missouri</option>\n                                        <option value=\"MT\">Montana</option>\n                                        <option value=\"NE\">Nebraska</option>\n                                        <option value=\"NV\">Nevada</option>\n                                        <option value=\"NH\">New Hampshire</option>\n                                        <option value=\"NJ\">New Jersey</option>\n                                        <option value=\"NM\">New Mexico</option>\n                                        <option value=\"NY\">New York</option>\n                                        <option value=\"NC\">North Carolina</option>\n                                        <option value=\"ND\">North Dakota</option>\n                                        <option value=\"OH\">Ohio</option>\n                                        <option value=\"OK\">Oklahoma</option>\n                                        <option value=\"OR\">Oregon</option>\n                                        <option value=\"PA\">Pennsylvania</option>\n                                        <option value=\"RI\">Rhode Island</option>\n                                        <option value=\"SC\">South Carolina</option>\n                                        <option value=\"SD\">South Dakota</option>\n                                        <option value=\"TN\">Tennessee</option>\n                                        <option value=\"TX\">Texas</option>\n                                        <option value=\"UT\">Utah</option>\n                                        <option value=\"VT\">Vermont</option>\n                                        <option value=\"VA\">Virginia</option>\n                                        <option value=\"WA\">Washington</option>\n                                        <option value=\"WV\">West Virginia</option>\n                                        <option value=\"WI\">Wisconsin</option>\n                                        <option value=\"WY\">Wyoming</option>\n                                      </select>\n                                </div>\n                                <div class=\"span4\">\n                                    <label for=\"zip\">Zip</label>\n                            <input value=\"[[!+fi.zip]]\" type=\"text\" id=\"zip\" name=\"zip\"  >\n<input value=\"[[!+fi.nospam]]\" type=\"text\" id=\"nospam\" name=\"nospam\" class=\"nospam\">\n                                </div>\n                            </div>\n                            <div class=\"row-fluid\">\n                            <label for=\"attachment\">Upload a floorplan:</label>\n                                <div class=\"span6\"><input type=\"file\" name=\"contact_images1\" value=\"[[!+fi.contact_images1]]\" /></div>\n                            </div>\n                            <label for=\"comments\">Additional Comments<sup>*</sup></label>\n                            <textarea value=\"[[!+fi.comments]]\" id=\"comments\" name=\"comments\"></textarea>\n                        <input class=\"button\" type=\"submit\" name=\"quote\" value=\"Send our way\" />\n                    </form>\n                  ',0,'a:0:{}',1,'assets/chunks/contact_form_estimate.html'),
	(13,1,0,'slides','',0,5,0,'<li class=\"hero\" style=\"background-image:url(\'[[+background-image]]\');\">\n            <div class=\"center\">\n            	<div class=\"overlay\">\n        		</div>\n                <div class=\"wrap\">\n                    <div class=\"hero-content\">\n                        <h2>[[+pagetitle]]</h2>\n                        [[+content]]\n                        [[+promo-link-text:notempty=`<a href=\"[[~[[+promo-link]]]]\" class=\"button\">[[+promo-link-text]]</a>`]]\n                    </div>\n                </div>\n            </div>  \n        </li>',0,'a:0:{}',1,'assets/chunks/slides.html'),
	(14,1,0,'snt_from_quote','',0,4,0,'[[+first_name]] [[+last_name]] is requesting a free quote from [[++site_name]]: <br />\n<br />\nPhone Number: [[+phone]]<br />\nAddress: [[+address]]<br />\nCity: [[+city]]<br />\nState: [[+state]]<br />\nZip: [[+zip]]<br />\n<br />\n\n\n<strong>[[+comments]]</strong><br /><br />\n\nYou can respond to [[+first_name]] [[+last_name]] by email at <a href=\"mailto:[[+email]]\">[[+email]]</a> or by phone at [[+phone]]',0,'a:0:{}',0,'');

/*!40000 ALTER TABLE `modx_site_htmlsnippets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_plugin_events
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_plugin_events`;

CREATE TABLE `modx_site_plugin_events` (
  `pluginid` int(10) NOT NULL DEFAULT '0',
  `event` varchar(255) NOT NULL DEFAULT '',
  `priority` int(10) NOT NULL DEFAULT '0',
  `propertyset` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`pluginid`,`event`),
  KEY `priority` (`priority`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_plugin_events` WRITE;
/*!40000 ALTER TABLE `modx_site_plugin_events` DISABLE KEYS */;

INSERT INTO `modx_site_plugin_events` (`pluginid`, `event`, `priority`, `propertyset`)
VALUES
	(1,'OnRichTextBrowserInit',0,0),
	(1,'OnRichTextEditorRegister',0,0),
	(1,'OnRichTextEditorInit',0,0);

/*!40000 ALTER TABLE `modx_site_plugin_events` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_plugins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_plugins`;

CREATE TABLE `modx_site_plugins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` int(10) unsigned NOT NULL DEFAULT '0',
  `property_preprocess` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0',
  `plugincode` mediumtext NOT NULL,
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `properties` text,
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `moduleguid` varchar(32) NOT NULL DEFAULT '',
  `static` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `static_file` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category` (`category`),
  KEY `locked` (`locked`),
  KEY `disabled` (`disabled`),
  KEY `static` (`static`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_plugins` WRITE;
/*!40000 ALTER TABLE `modx_site_plugins` DISABLE KEYS */;

INSERT INTO `modx_site_plugins` (`id`, `source`, `property_preprocess`, `name`, `description`, `editor_type`, `category`, `cache_type`, `plugincode`, `locked`, `properties`, `disabled`, `moduleguid`, `static`, `static_file`)
VALUES
	(1,0,0,'TinyMCE','TinyMCE 4.3.3-pl plugin for MODx Revolution',0,0,0,'/**\n * TinyMCE RichText Editor Plugin\n *\n * Events: OnRichTextEditorInit, OnRichTextEditorRegister,\n * OnBeforeManagerPageInit, OnRichTextBrowserInit\n *\n * @author Jeff Whitfield <jeff@collabpad.com>\n * @author Shaun McCormick <shaun@collabpad.com>\n *\n * @var modX $modx\n * @var array $scriptProperties\n *\n * @package tinymce\n * @subpackage build\n */\nif ($modx->event->name == \'OnRichTextEditorRegister\') {\n    $modx->event->output(\'TinyMCE\');\n    return;\n}\nrequire_once $modx->getOption(\'tiny.core_path\',null,$modx->getOption(\'core_path\').\'components/tinymce/\').\'tinymce.class.php\';\n$tiny = new TinyMCE($modx,$scriptProperties);\n\n$useEditor = $tiny->context->getOption(\'use_editor\',false);\n$whichEditor = $tiny->context->getOption(\'which_editor\',\'\');\n\n/* Handle event */\nswitch ($modx->event->name) {\n    case \'OnRichTextEditorInit\':\n        if ($useEditor && $whichEditor == \'TinyMCE\') {\n            unset($scriptProperties[\'chunk\']);\n            if (isset($forfrontend) || $modx->context->get(\'key\') != \'mgr\') {\n                $def = $tiny->context->getOption(\'cultureKey\',$tiny->context->getOption(\'manager_language\',\'en\'));\n                $tiny->properties[\'language\'] = $modx->getOption(\'fe_editor_lang\',array(),$def);\n                $tiny->properties[\'frontend\'] = true;\n                unset($def);\n            }\n            /* commenting these out as it causes problems with richtext tvs */\n            //if (isset($scriptProperties[\'resource\']) && !$resource->get(\'richtext\')) return;\n            //if (!isset($scriptProperties[\'resource\']) && !$modx->getOption(\'richtext_default\',null,false)) return;\n            $tiny->setProperties($scriptProperties);\n            $html = $tiny->initialize();\n            $modx->event->output($html);\n            unset($html);\n        }\n        break;\n    case \'OnRichTextBrowserInit\':\n        if ($useEditor && $whichEditor == \'TinyMCE\') {\n            $inRevo20 = (boolean)version_compare($modx->version[\'full_version\'],\'2.1.0-rc1\',\'<\');\n            $modx->getVersionData();\n            $source = $tiny->context->getOption(\'default_media_source\',null,1);\n            \n            $modx->controller->addHtml(\'<script type=\"text/javascript\">var inRevo20 = \'.($inRevo20 ? 1 : 0).\';MODx.source = \"\'.$source.\'\";</script>\');\n            \n            $modx->controller->addJavascript($tiny->config[\'assetsUrl\'].\'jscripts/tiny_mce/tiny_mce_popup.js\');\n            if (file_exists($tiny->config[\'assetsPath\'].\'jscripts/tiny_mce/langs/\'.$tiny->properties[\'language\'].\'.js\')) {\n                $modx->controller->addJavascript($tiny->config[\'assetsUrl\'].\'jscripts/tiny_mce/langs/\'.$tiny->properties[\'language\'].\'.js\');\n            } else {\n                $modx->controller->addJavascript($tiny->config[\'assetsUrl\'].\'jscripts/tiny_mce/langs/en.js\');\n            }\n            $modx->controller->addJavascript($tiny->config[\'assetsUrl\'].\'tiny.browser.js\');\n            $modx->event->output(\'Tiny.browserCallback\');\n        }\n        return \'\';\n        break;\n\n   default: break;\n}\nreturn;',0,'a:39:{s:22:\"accessibility_warnings\";a:7:{s:4:\"name\";s:22:\"accessibility_warnings\";s:4:\"desc\";s:315:\"If this option is set to true some accessibility warnings will be presented to the user if they miss specifying that information. This option is set to true by default, since we should all try to make this world a better place for disabled people. But if you are annoyed with the warnings, set this option to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:23:\"apply_source_formatting\";a:7:{s:4:\"name\";s:23:\"apply_source_formatting\";s:4:\"desc\";s:229:\"This option enables you to tell TinyMCE to apply some source formatting to the output HTML code. With source formatting, the output HTML code is indented and formatted. Without source formatting, the output HTML is more compact. \";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:15:\"button_tile_map\";a:7:{s:4:\"name\";s:15:\"button_tile_map\";s:4:\"desc\";s:338:\"If this option is set to true TinyMCE will use tiled images instead of individual images for most of the editor controls. This produces faster loading time since only one GIF image needs to be loaded instead of a GIF for each individual button. This option is set to false by default since it doesn\'t work with some DOCTYPE declarations. \";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:7:\"cleanup\";a:7:{s:4:\"name\";s:7:\"cleanup\";s:4:\"desc\";s:331:\"This option enables or disables the built-in clean up functionality. TinyMCE is equipped with powerful clean up functionality that enables you to specify what elements and attributes are allowed and how HTML contents should be generated. This option is set to true by default, but if you want to disable it you may set it to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:18:\"cleanup_on_startup\";a:7:{s:4:\"name\";s:18:\"cleanup_on_startup\";s:4:\"desc\";s:135:\"If you set this option to true, TinyMCE will perform a HTML cleanup call when the editor loads. This option is set to false by default.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:22:\"convert_fonts_to_spans\";a:7:{s:4:\"name\";s:22:\"convert_fonts_to_spans\";s:4:\"desc\";s:348:\"If you set this option to true, TinyMCE will convert all font elements to span elements and generate span elements instead of font elements. This option should be used in order to get more W3C compatible code, since font elements are deprecated. How sizes get converted can be controlled by the font_size_classes and font_size_style_values options.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:23:\"convert_newlines_to_brs\";a:7:{s:4:\"name\";s:23:\"convert_newlines_to_brs\";s:4:\"desc\";s:128:\"If you set this option to true, newline characters codes get converted into br elements. This option is set to false by default.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:12:\"convert_urls\";a:7:{s:4:\"name\";s:12:\"convert_urls\";s:4:\"desc\";s:495:\"This option enables you to control whether TinyMCE is to be clever and restore URLs to their original values. URLs are automatically converted (messed up) by default because the built-in browser logic works this way. There is no way to get the real URL unless you store it away. If you set this option to false it will try to keep these URLs intact. This option is set to true by default, which means URLs will be forced to be either absolute or relative depending on the state of relative_urls.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:11:\"dialog_type\";a:7:{s:4:\"name\";s:11:\"dialog_type\";s:4:\"desc\";s:246:\"This option enables you to specify how dialogs/popups should be opened. Possible values are \"window\" and \"modal\", where the window option opens a normal window and the dialog option opens a modal dialog. This option is set to \"window\" by default.\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:2:{i:0;a:2:{i:0;s:6:\"window\";s:4:\"text\";s:6:\"Window\";}i:1;a:2:{i:0;s:5:\"modal\";s:4:\"text\";s:5:\"Modal\";}}s:5:\"value\";s:6:\"window\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:14:\"directionality\";a:7:{s:4:\"name\";s:14:\"directionality\";s:4:\"desc\";s:261:\"This option specifies the default writing direction. Some languages (Like Hebrew, Arabic, Urdu...) write from right to left instead of left to right. The default value of this option is \"ltr\" but if you want to use from right to left mode specify \"rtl\" instead.\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:2:{i:0;a:2:{s:5:\"value\";s:3:\"ltr\";s:4:\"text\";s:13:\"Left to Right\";}i:1;a:2:{s:5:\"value\";s:3:\"rtl\";s:4:\"text\";s:13:\"Right to Left\";}}s:5:\"value\";s:3:\"ltr\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:14:\"element_format\";a:7:{s:4:\"name\";s:14:\"element_format\";s:4:\"desc\";s:210:\"This option enables control if elements should be in html or xhtml mode. xhtml is the default state for this option. This means that for example &lt;br /&gt; will be &lt;br&gt; if you set this option to \"html\".\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:2:{i:0;a:2:{s:5:\"value\";s:5:\"xhtml\";s:4:\"text\";s:5:\"XHTML\";}i:1;a:2:{s:5:\"value\";s:4:\"html\";s:4:\"text\";s:4:\"HTML\";}}s:5:\"value\";s:5:\"xhtml\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:15:\"entity_encoding\";a:7:{s:4:\"name\";s:15:\"entity_encoding\";s:4:\"desc\";s:70:\"This option controls how entities/characters get processed by TinyMCE.\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:4:{i:0;a:2:{s:5:\"value\";s:0:\"\";s:4:\"text\";s:4:\"None\";}i:1;a:2:{s:5:\"value\";s:5:\"named\";s:4:\"text\";s:5:\"Named\";}i:2;a:2:{s:5:\"value\";s:7:\"numeric\";s:4:\"text\";s:7:\"Numeric\";}i:3;a:2:{s:5:\"value\";s:3:\"raw\";s:4:\"text\";s:3:\"Raw\";}}s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:16:\"force_p_newlines\";a:7:{s:4:\"name\";s:16:\"force_p_newlines\";s:4:\"desc\";s:147:\"This option enables you to disable/enable the creation of paragraphs on return/enter in Mozilla/Firefox. The default value of this option is true. \";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:22:\"force_hex_style_colors\";a:7:{s:4:\"name\";s:22:\"force_hex_style_colors\";s:4:\"desc\";s:277:\"This option enables you to control TinyMCE to force the color format to use hexadecimal instead of rgb strings. It converts for example \"color: rgb(255, 255, 0)\" to \"#FFFF00\". This option is set to true by default since otherwice MSIE and Firefox would differ in this behavior.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:6:\"height\";a:7:{s:4:\"name\";s:6:\"height\";s:4:\"desc\";s:38:\"Sets the height of the TinyMCE editor.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:5:\"400px\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:11:\"indentation\";a:7:{s:4:\"name\";s:11:\"indentation\";s:4:\"desc\";s:139:\"This option allows specification of the indentation level for indent/outdent buttons in the UI. This defaults to 30px but can be any value.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:4:\"30px\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:16:\"invalid_elements\";a:7:{s:4:\"name\";s:16:\"invalid_elements\";s:4:\"desc\";s:163:\"This option should contain a comma separated list of element names to exclude from the content. Elements in this list will removed when TinyMCE executes a cleanup.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:6:\"nowrap\";a:7:{s:4:\"name\";s:6:\"nowrap\";s:4:\"desc\";s:212:\"This nowrap option enables you to control how whitespace is to be wordwrapped within the editor. This option is set to false by default, but if you enable it by setting it to true editor contents will never wrap.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:15:\"object_resizing\";a:7:{s:4:\"name\";s:15:\"object_resizing\";s:4:\"desc\";s:148:\"This option gives you the ability to turn on/off the inline resizing controls of tables and images in Firefox/Mozilla. These are enabled by default.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:12:\"path_options\";a:7:{s:4:\"name\";s:12:\"path_options\";s:4:\"desc\";s:119:\"Sets a group of options. Note: This will override the relative_urls, document_base_url and remove_script_host settings.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";a:3:{i:0;a:2:{s:5:\"value\";s:11:\"docrelative\";s:4:\"text\";s:17:\"Document Relative\";}i:1;a:2:{s:5:\"value\";s:12:\"rootrelative\";s:4:\"text\";s:13:\"Root Relative\";}i:2;a:2:{s:5:\"value\";s:11:\"fullpathurl\";s:4:\"text\";s:13:\"Full Path URL\";}}s:5:\"value\";s:11:\"docrelative\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:28:\"plugin_insertdate_dateFormat\";a:7:{s:4:\"name\";s:28:\"plugin_insertdate_dateFormat\";s:4:\"desc\";s:53:\"Formatting of dates when using the InsertDate plugin.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:8:\"%Y-%m-%d\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:28:\"plugin_insertdate_timeFormat\";a:7:{s:4:\"name\";s:28:\"plugin_insertdate_timeFormat\";s:4:\"desc\";s:53:\"Formatting of times when using the InsertDate plugin.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:8:\"%H:%M:%S\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:12:\"preformatted\";a:7:{s:4:\"name\";s:12:\"preformatted\";s:4:\"desc\";s:231:\"If you enable this feature, whitespace such as tabs and spaces will be preserved. Much like the behavior of a &lt;pre&gt; element. This can be handy when integrating TinyMCE with webmail clients. This option is disabled by default.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:13:\"relative_urls\";a:7:{s:4:\"name\";s:13:\"relative_urls\";s:4:\"desc\";s:231:\"If this option is set to true, all URLs returned from the file manager will be relative from the specified document_base_url. If it is set to false all URLs will be converted to absolute URLs. This option is set to true by default.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:17:\"remove_linebreaks\";a:7:{s:4:\"name\";s:17:\"remove_linebreaks\";s:4:\"desc\";s:531:\"This option controls whether line break characters should be removed from output HTML. This option is enabled by default because there are differences between browser implementations regarding what to do with white space in the DOM. Gecko and Safari place white space in text nodes in the DOM. IE and Opera remove them from the DOM and therefore the line breaks will automatically be removed in those. This option will normalize this behavior when enabled (true) and all browsers will have a white-space-stripped DOM serialization.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:18:\"remove_script_host\";a:7:{s:4:\"name\";s:18:\"remove_script_host\";s:4:\"desc\";s:221:\"If this option is enabled the protocol and host part of the URLs returned from the file manager will be removed. This option is only used if the relative_urls option is set to false. This option is set to true by default.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:20:\"remove_trailing_nbsp\";a:7:{s:4:\"name\";s:20:\"remove_trailing_nbsp\";s:4:\"desc\";s:392:\"This option enables you to specify that TinyMCE should remove any traling &nbsp; characters in block elements if you start to write inside them. Paragraphs are default padded with a &nbsp; and if you write text into such paragraphs the space will remain. Setting this option to true will remove the space. This option is set to false by default since the cursor jumps a bit in Gecko browsers.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:4:\"skin\";a:7:{s:4:\"name\";s:4:\"skin\";s:4:\"desc\";s:330:\"This option enables you to specify what skin you want to use with your theme. A skin is basically a CSS file that gets loaded from the skins directory inside the theme. The advanced theme that TinyMCE comes with has two skins, these are called \"default\" and \"o2k7\". We added another skin named \"cirkuit\" that is chosen by default.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:7:\"cirkuit\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:12:\"skin_variant\";a:7:{s:4:\"name\";s:12:\"skin_variant\";s:4:\"desc\";s:403:\"This option enables you to specify a variant for the skin, for example \"silver\" or \"black\". \"default\" skin does not offer any variant, whereas \"o2k7\" default offers \"silver\" or \"black\" variants to the default one. For the \"cirkuit\" skin there\'s one variant named \"silver\". When creating a skin, additional variants may also be created, by adding ui_[variant_name].css files alongside the default ui.css.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:20:\"table_inline_editing\";a:7:{s:4:\"name\";s:20:\"table_inline_editing\";s:4:\"desc\";s:231:\"This option gives you the ability to turn on/off the inline table editing controls in Firefox/Mozilla. According to the TinyMCE documentation, these controls are somewhat buggy and not redesignable, so they are disabled by default.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:22:\"theme_advanced_disable\";a:7:{s:4:\"name\";s:22:\"theme_advanced_disable\";s:4:\"desc\";s:111:\"This option should contain a comma separated list of controls to disable from any toolbar row/panel in TinyMCE.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:19:\"theme_advanced_path\";a:7:{s:4:\"name\";s:19:\"theme_advanced_path\";s:4:\"desc\";s:331:\"This option gives you the ability to enable/disable the element path. This option is only useful if the theme_advanced_statusbar_location option is set to \"top\" or \"bottom\". This option is set to \"true\" by default. Setting this option to \"false\" will effectively hide the path tool, though it still takes up room in the Status Bar.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:32:\"theme_advanced_resize_horizontal\";a:7:{s:4:\"name\";s:32:\"theme_advanced_resize_horizontal\";s:4:\"desc\";s:319:\"This option gives you the ability to enable/disable the horizontal resizing. This option is only useful if the theme_advanced_statusbar_location option is set to \"top\" or \"bottom\" and when the theme_advanced_resizing is set to true. This option is set to true by default, allowing both resizing horizontal and vertical.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:23:\"theme_advanced_resizing\";a:7:{s:4:\"name\";s:23:\"theme_advanced_resizing\";s:4:\"desc\";s:216:\"This option gives you the ability to enable/disable the resizing button. This option is only useful if the theme_advanced_statusbar_location option is set to \"top\" or \"bottom\". This option is set to false by default.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:33:\"theme_advanced_statusbar_location\";a:7:{s:4:\"name\";s:33:\"theme_advanced_statusbar_location\";s:4:\"desc\";s:257:\"This option enables you to specify where the element statusbar with the path and resize tool should be located. This option can be set to \"top\" or \"bottom\". The default value is set to \"top\". This option can only be used when the theme is set to \"advanced\".\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:2:{i:0;a:2:{s:5:\"value\";s:3:\"top\";s:4:\"text\";s:3:\"Top\";}i:1;a:2:{s:5:\"value\";s:6:\"bottom\";s:4:\"text\";s:6:\"Bottom\";}}s:5:\"value\";s:6:\"bottom\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:28:\"theme_advanced_toolbar_align\";a:7:{s:4:\"name\";s:28:\"theme_advanced_toolbar_align\";s:4:\"desc\";s:187:\"This option enables you to specify the alignment of the toolbar, this value can be \"left\", \"right\" or \"center\" (the default). This option can only be used when theme is set to \"advanced\".\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";a:3:{i:0;a:2:{s:5:\"value\";s:6:\"center\";s:4:\"text\";s:6:\"Center\";}i:1;a:2:{s:5:\"value\";s:4:\"left\";s:4:\"text\";s:4:\"Left\";}i:2;a:2:{s:5:\"value\";s:5:\"right\";s:4:\"text\";s:5:\"Right\";}}s:5:\"value\";s:4:\"left\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:31:\"theme_advanced_toolbar_location\";a:7:{s:4:\"name\";s:31:\"theme_advanced_toolbar_location\";s:4:\"desc\";s:191:\"\nThis option enables you to specify where the toolbar should be located. This option can be set to \"top\" or \"bottom\" (the defualt). This option can only be used when theme is set to advanced.\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:2:{i:0;a:2:{s:5:\"value\";s:3:\"top\";s:4:\"text\";s:3:\"Top\";}i:1;a:2:{s:5:\"value\";s:6:\"bottom\";s:4:\"text\";s:6:\"Bottom\";}}s:5:\"value\";s:3:\"top\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:5:\"width\";a:7:{s:4:\"name\";s:5:\"width\";s:4:\"desc\";s:32:\"The width of the TinyMCE editor.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:3:\"95%\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:33:\"template_selected_content_classes\";a:7:{s:4:\"name\";s:33:\"template_selected_content_classes\";s:4:\"desc\";s:234:\"Specify a list of CSS class names for the template plugin. They must be separated by spaces. Any template element with one of the specified CSS classes will have its content replaced by the selected editor content when first inserted.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}}',0,'',0,'');

/*!40000 ALTER TABLE `modx_site_plugins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_snippets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_snippets`;

CREATE TABLE `modx_site_snippets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` int(10) unsigned NOT NULL DEFAULT '0',
  `property_preprocess` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  `cache_type` tinyint(1) NOT NULL DEFAULT '0',
  `snippet` mediumtext,
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `properties` text,
  `moduleguid` varchar(32) NOT NULL DEFAULT '',
  `static` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `static_file` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category` (`category`),
  KEY `locked` (`locked`),
  KEY `moduleguid` (`moduleguid`),
  KEY `static` (`static`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_snippets` WRITE;
/*!40000 ALTER TABLE `modx_site_snippets` DISABLE KEYS */;

INSERT INTO `modx_site_snippets` (`id`, `source`, `property_preprocess`, `name`, `description`, `editor_type`, `category`, `cache_type`, `snippet`, `locked`, `properties`, `moduleguid`, `static`, `static_file`)
VALUES
	(1,0,0,'FormIt','A dynamic form processing snippet.',0,7,0,'/**\n * FormIt\n *\n * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>\n *\n * FormIt is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package formit\n */\n/**\n * FormIt\n *\n * A dynamic form processing Snippet for MODx Revolution.\n *\n * @package formit\n */\nrequire_once $modx->getOption(\'formit.core_path\',null,$modx->getOption(\'core_path\',null,MODX_CORE_PATH).\'components/formit/\').\'model/formit/formit.class.php\';\n$fi = new FormIt($modx,$scriptProperties);\n$fi->initialize($modx->context->get(\'key\'));\n$fi->loadRequest();\n\n$fields = $fi->request->prepare();\nreturn $fi->request->handle($fields);',0,'a:57:{s:5:\"hooks\";a:7:{s:4:\"name\";s:5:\"hooks\";s:4:\"desc\";s:22:\"prop_formit.hooks_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:8:\"preHooks\";a:7:{s:4:\"name\";s:8:\"preHooks\";s:4:\"desc\";s:25:\"prop_formit.prehooks_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:9:\"submitVar\";a:7:{s:4:\"name\";s:9:\"submitVar\";s:4:\"desc\";s:26:\"prop_formit.submitvar_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:8:\"validate\";a:7:{s:4:\"name\";s:8:\"validate\";s:4:\"desc\";s:25:\"prop_formit.validate_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:6:\"errTpl\";a:7:{s:4:\"name\";s:6:\"errTpl\";s:4:\"desc\";s:23:\"prop_formit.errtpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:37:\"<span class=\"error\">[[+error]]</span>\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:22:\"validationErrorMessage\";a:7:{s:4:\"name\";s:22:\"validationErrorMessage\";s:4:\"desc\";s:39:\"prop_formit.validationerrormessage_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:96:\"<p class=\"error\">A form validation error occurred. Please check the values you have entered.</p>\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:22:\"validationErrorBulkTpl\";a:7:{s:4:\"name\";s:22:\"validationErrorBulkTpl\";s:4:\"desc\";s:39:\"prop_formit.validationerrorbulktpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:19:\"<li>[[+error]]</li>\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:26:\"trimValuesBeforeValidation\";a:7:{s:4:\"name\";s:26:\"trimValuesBeforeValidation\";s:4:\"desc\";s:43:\"prop_formit.trimvaluesdeforevalidation_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:16:\"customValidators\";a:7:{s:4:\"name\";s:16:\"customValidators\";s:4:\"desc\";s:33:\"prop_formit.customvalidators_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:20:\"clearFieldsOnSuccess\";a:7:{s:4:\"name\";s:20:\"clearFieldsOnSuccess\";s:4:\"desc\";s:37:\"prop_formit.clearfieldsonsuccess_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:14:\"successMessage\";a:7:{s:4:\"name\";s:14:\"successMessage\";s:4:\"desc\";s:31:\"prop_formit.successmessage_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:25:\"successMessagePlaceholder\";a:7:{s:4:\"name\";s:25:\"successMessagePlaceholder\";s:4:\"desc\";s:42:\"prop_formit.successmessageplaceholder_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:17:\"fi.successMessage\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:5:\"store\";a:7:{s:4:\"name\";s:5:\"store\";s:4:\"desc\";s:22:\"prop_formit.store_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:17:\"placeholderPrefix\";a:7:{s:4:\"name\";s:17:\"placeholderPrefix\";s:4:\"desc\";s:34:\"prop_formit.placeholderprefix_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:3:\"fi.\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:9:\"storeTime\";a:7:{s:4:\"name\";s:9:\"storeTime\";s:4:\"desc\";s:26:\"prop_formit.storetime_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";i:300;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:10:\"allowFiles\";a:7:{s:4:\"name\";s:10:\"allowFiles\";s:4:\"desc\";s:27:\"prop_formit.allowfiles_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:15:\"spamEmailFields\";a:7:{s:4:\"name\";s:15:\"spamEmailFields\";s:4:\"desc\";s:32:\"prop_formit.spamemailfields_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:5:\"email\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"spamCheckIp\";a:7:{s:4:\"name\";s:11:\"spamCheckIp\";s:4:\"desc\";s:28:\"prop_formit.spamcheckip_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"recaptchaJs\";a:7:{s:4:\"name\";s:11:\"recaptchaJs\";s:4:\"desc\";s:28:\"prop_formit.recaptchajs_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:2:\"{}\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:14:\"recaptchaTheme\";a:7:{s:4:\"name\";s:14:\"recaptchaTheme\";s:4:\"desc\";s:31:\"prop_formit.recaptchatheme_desc\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:4:{i:0;a:2:{s:4:\"text\";s:14:\"formit.opt_red\";s:5:\"value\";s:3:\"red\";}i:1;a:2:{s:4:\"text\";s:16:\"formit.opt_white\";s:5:\"value\";s:5:\"white\";}i:2;a:2:{s:4:\"text\";s:16:\"formit.opt_clean\";s:5:\"value\";s:5:\"clean\";}i:3;a:2:{s:4:\"text\";s:21:\"formit.opt_blackglass\";s:5:\"value\";s:10:\"blackglass\";}}s:5:\"value\";s:5:\"clean\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:10:\"redirectTo\";a:7:{s:4:\"name\";s:10:\"redirectTo\";s:4:\"desc\";s:27:\"prop_formit.redirectto_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:14:\"redirectParams\";a:7:{s:4:\"name\";s:14:\"redirectParams\";s:4:\"desc\";s:31:\"prop_formit.redirectparams_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:7:\"emailTo\";a:7:{s:4:\"name\";s:7:\"emailTo\";s:4:\"desc\";s:24:\"prop_formit.emailto_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"emailToName\";a:7:{s:4:\"name\";s:11:\"emailToName\";s:4:\"desc\";s:28:\"prop_formit.emailtoname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:9:\"emailFrom\";a:7:{s:4:\"name\";s:9:\"emailFrom\";s:4:\"desc\";s:26:\"prop_formit.emailfrom_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:13:\"emailFromName\";a:7:{s:4:\"name\";s:13:\"emailFromName\";s:4:\"desc\";s:30:\"prop_formit.emailfromname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"emailReplyTo\";a:7:{s:4:\"name\";s:12:\"emailReplyTo\";s:4:\"desc\";s:29:\"prop_formit.emailreplyto_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:16:\"emailReplyToName\";a:7:{s:4:\"name\";s:16:\"emailReplyToName\";s:4:\"desc\";s:33:\"prop_formit.emailreplytoname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:7:\"emailCC\";a:7:{s:4:\"name\";s:7:\"emailCC\";s:4:\"desc\";s:24:\"prop_formit.emailcc_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"emailCCName\";a:7:{s:4:\"name\";s:11:\"emailCCName\";s:4:\"desc\";s:28:\"prop_formit.emailccname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:8:\"emailBCC\";a:7:{s:4:\"name\";s:8:\"emailBCC\";s:4:\"desc\";s:25:\"prop_formit.emailbcc_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"emailBCCName\";a:7:{s:4:\"name\";s:12:\"emailBCCName\";s:4:\"desc\";s:29:\"prop_formit.emailbccname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:15:\"emailReturnPath\";a:7:{s:4:\"name\";s:15:\"emailReturnPath\";s:4:\"desc\";s:32:\"prop_formit.emailreturnpath_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"emailSubject\";a:7:{s:4:\"name\";s:12:\"emailSubject\";s:4:\"desc\";s:29:\"prop_formit.emailsubject_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:23:\"emailUseFieldForSubject\";a:7:{s:4:\"name\";s:23:\"emailUseFieldForSubject\";s:4:\"desc\";s:40:\"prop_formit.emailusefieldforsubject_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:9:\"emailHtml\";a:7:{s:4:\"name\";s:9:\"emailHtml\";s:4:\"desc\";s:26:\"prop_formit.emailhtml_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:20:\"emailConvertNewlines\";a:7:{s:4:\"name\";s:20:\"emailConvertNewlines\";s:4:\"desc\";s:37:\"prop_formit.emailconvertnewlines_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:17:\"emailMultiWrapper\";a:7:{s:4:\"name\";s:17:\"emailMultiWrapper\";s:4:\"desc\";s:34:\"prop_formit.emailmultiwrapper_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:10:\"[[+value]]\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:19:\"emailMultiSeparator\";a:7:{s:4:\"name\";s:19:\"emailMultiSeparator\";s:4:\"desc\";s:36:\"prop_formit.emailmultiseparator_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:7:\"fiarTpl\";a:7:{s:4:\"name\";s:7:\"fiarTpl\";s:4:\"desc\";s:22:\"prop_fiar.fiartpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"fiarToField\";a:7:{s:4:\"name\";s:11:\"fiarToField\";s:4:\"desc\";s:26:\"prop_fiar.fiartofield_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:5:\"email\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"fiarSubject\";a:7:{s:4:\"name\";s:11:\"fiarSubject\";s:4:\"desc\";s:26:\"prop_fiar.fiarsubject_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:30:\"[[++site_name]] Auto-Responder\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:8:\"fiarFrom\";a:7:{s:4:\"name\";s:8:\"fiarFrom\";s:4:\"desc\";s:23:\"prop_fiar.fiarfrom_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"fiarFromName\";a:7:{s:4:\"name\";s:12:\"fiarFromName\";s:4:\"desc\";s:27:\"prop_fiar.fiarfromname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"fiarReplyTo\";a:7:{s:4:\"name\";s:11:\"fiarReplyTo\";s:4:\"desc\";s:26:\"prop_fiar.fiarreplyto_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:15:\"fiarReplyToName\";a:7:{s:4:\"name\";s:15:\"fiarReplyToName\";s:4:\"desc\";s:30:\"prop_fiar.fiarreplytoname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:6:\"fiarCC\";a:7:{s:4:\"name\";s:6:\"fiarCC\";s:4:\"desc\";s:21:\"prop_fiar.fiarcc_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:10:\"fiarCCName\";a:7:{s:4:\"name\";s:10:\"fiarCCName\";s:4:\"desc\";s:25:\"prop_fiar.fiarccname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:7:\"fiarBCC\";a:7:{s:4:\"name\";s:7:\"fiarBCC\";s:4:\"desc\";s:22:\"prop_fiar.fiarbcc_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"fiarBCCName\";a:7:{s:4:\"name\";s:11:\"fiarBCCName\";s:4:\"desc\";s:26:\"prop_fiar.fiarbccname_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:8:\"fiarHtml\";a:7:{s:4:\"name\";s:8:\"fiarHtml\";s:4:\"desc\";s:23:\"prop_fiar.fiarhtml_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"mathMinRange\";a:7:{s:4:\"name\";s:12:\"mathMinRange\";s:4:\"desc\";s:27:\"prop_math.mathminrange_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";i:10;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"mathMaxRange\";a:7:{s:4:\"name\";s:12:\"mathMaxRange\";s:4:\"desc\";s:27:\"prop_math.mathmaxrange_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";i:100;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:9:\"mathField\";a:7:{s:4:\"name\";s:9:\"mathField\";s:4:\"desc\";s:24:\"prop_math.mathfield_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:4:\"math\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"mathOp1Field\";a:7:{s:4:\"name\";s:12:\"mathOp1Field\";s:4:\"desc\";s:27:\"prop_math.mathop1field_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:3:\"op1\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"mathOp2Field\";a:7:{s:4:\"name\";s:12:\"mathOp2Field\";s:4:\"desc\";s:27:\"prop_math.mathop2field_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:3:\"op2\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:17:\"mathOperatorField\";a:7:{s:4:\"name\";s:17:\"mathOperatorField\";s:4:\"desc\";s:32:\"prop_math.mathoperatorfield_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:8:\"operator\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}}','',0,''),
	(2,0,0,'FormItAutoResponder','Custom hook for FormIt to handle Auto-Response emails.',0,7,0,'/**\n * FormIt\n *\n * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>\n *\n * FormIt is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package formit\n */\n/**\n * A custom FormIt hook for auto-responders.\n *\n * @var modX $modx\n * @var array $scriptProperties\n * @var FormIt $formit\n * @var fiHooks $hook\n * \n * @package formit\n */\n/* setup default properties */\n$tpl = $modx->getOption(\'fiarTpl\',$scriptProperties,\'fiarTpl\');\n$mailFrom = $modx->getOption(\'fiarFrom\',$scriptProperties,$modx->getOption(\'emailsender\'));\n$mailFromName = $modx->getOption(\'fiarFromName\',$scriptProperties,$modx->getOption(\'site_name\'));\n$mailSender = $modx->getOption(\'fiarSender\',$scriptProperties,$modx->getOption(\'emailsender\'));\n$mailSubject = $modx->getOption(\'fiarSubject\',$scriptProperties,\'[[++site_name]] Auto-Responder\');\n$mailSubject = str_replace(array(\'[[++site_name]]\',\'[[++emailsender]]\'),array($modx->getOption(\'site_name\'),$modx->getOption(\'emailsender\')),$mailSubject);\n$fiarFiles = $modx->getOption(\'fiarFiles\',$scriptProperties,false);\n$isHtml = $modx->getOption(\'fiarHtml\',$scriptProperties,true);\n$toField = $modx->getOption(\'fiarToField\',$scriptProperties,\'email\');\n$multiSeparator = $modx->getOption(\'fiarMultiSeparator\',$formit->config,\"\\n\");\n$multiWrapper = $modx->getOption(\'fiarMultiWrapper\',$formit->config,\"[[+value]]\");\nif (empty($fields[$toField])) {\n    $modx->log(modX::LOG_LEVEL_ERROR,\'[FormIt] Auto-responder could not find field `\'.$toField.\'` in form submission.\');\n    return false;\n}\n\n/* handle checkbox and array fields */\nforeach ($fields as $k => &$v) {\n    if (is_array($v)) {\n        $vOpts = array();\n        foreach ($v as $vKey => $vValue) {\n            if (is_string($vKey) && !empty($vKey)) {\n                $vKey = $k.\'.\'.$vKey;\n                $fields[$vKey] = $vValue;\n            } else {\n                $vOpts[] = str_replace(\'[[+value]]\',$vValue,$multiWrapper);\n            }\n        }\n        $newValue = implode($multiSeparator,$vOpts);\n        if (!empty($vOpts)) {\n            $fields[$k] = $newValue;\n        }\n    }\n}\n\n/* setup placeholders */\n$placeholders = $fields;\n$mailTo= $fields[$toField];\n\n$message = $formit->getChunk($tpl,$placeholders);\n$modx->parser->processElementTags(\'\',$message,true,false);\n\n$modx->getService(\'mail\', \'mail.modPHPMailer\');\n$modx->mail->reset();\n$modx->mail->set(modMail::MAIL_BODY,$message);\n$modx->mail->set(modMail::MAIL_FROM,$hook->_process($mailFrom,$placeholders));\n$modx->mail->set(modMail::MAIL_FROM_NAME,$hook->_process($mailFromName,$placeholders));\n$modx->mail->set(modMail::MAIL_SENDER,$hook->_process($mailSender,$placeholders));\n$modx->mail->set(modMail::MAIL_SUBJECT,$hook->_process($mailSubject,$placeholders));\n$modx->mail->address(\'to\',$mailTo);\n$modx->mail->setHTML($isHtml);\n\n/* add attachments */\nif($fiarFiles){\n    $fiarFiles = explode(\',\', $fiarFiles);\n    foreach($fiarFiles AS $file){\n        $modx->mail->mailer->AddAttachment($file);\n    }\n}\n\n/* reply to */\n$emailReplyTo = $modx->getOption(\'fiarReplyTo\',$scriptProperties,$mailFrom);\n$emailReplyTo = $hook->_process($emailReplyTo,$fields);\n$emailReplyToName = $modx->getOption(\'fiarReplyToName\',$scriptProperties,$mailFromName);\n$emailReplyToName = $hook->_process($emailReplyToName,$fields);\nif (!empty($emailReplyTo)) {\n    $modx->mail->address(\'reply-to\',$emailReplyTo,$emailReplyToName);\n}\n\n/* cc */\n$emailCC = $modx->getOption(\'fiarCC\',$scriptProperties,\'\');\nif (!empty($emailCC)) {\n    $emailCCName = $modx->getOption(\'fiarCCName\',$scriptProperties,\'\');\n    $emailCC = explode(\',\',$emailCC);\n    $emailCCName = explode(\',\',$emailCCName);\n    $numAddresses = count($emailCC);\n    for ($i=0;$i<$numAddresses;$i++) {\n        $etn = !empty($emailCCName[$i]) ? $emailCCName[$i] : \'\';\n        if (!empty($etn)) $etn = $hook->_process($etn,$fields);\n        $emailCC[$i] = $hook->_process($emailCC[$i],$fields);\n        if (!empty($emailCC[$i])) {\n            $modx->mail->address(\'cc\',$emailCC[$i],$etn);\n        }\n    }\n}\n\n/* bcc */\n$emailBCC = $modx->getOption(\'fiarBCC\',$scriptProperties,\'\');\nif (!empty($emailBCC)) {\n    $emailBCCName = $modx->getOption(\'fiarBCCName\',$scriptProperties,\'\');\n    $emailBCC = explode(\',\',$emailBCC);\n    $emailBCCName = explode(\',\',$emailBCCName);\n    $numAddresses = count($emailBCC);\n    for ($i=0;$i<$numAddresses;$i++) {\n        $etn = !empty($emailBCCName[$i]) ? $emailBCCName[$i] : \'\';\n        if (!empty($etn)) $etn = $hook->_process($etn,$fields);\n        $emailBCC[$i] = $hook->_process($emailBCC[$i],$fields);\n        if (!empty($emailBCC[$i])) {\n            $modx->mail->address(\'bcc\',$emailBCC[$i],$etn);\n        }\n    }\n}\n\nif (!$formit->inTestMode) {\n    if (!$modx->mail->send()) {\n        $modx->log(modX::LOG_LEVEL_ERROR,\'[FormIt] An error occurred while trying to send the auto-responder email: \'.$modx->mail->mailer->ErrorInfo);\n        return false;\n    }\n}\n$modx->mail->reset();\nreturn true;',0,NULL,'',0,''),
	(3,0,0,'FormItRetriever','Fetches a form submission for a user for displaying on a thank you page.',0,7,0,'/**\n * FormIt\n *\n * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>\n *\n * FormIt is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package formit\n */\n/**\n * Retrieves a prior form submission that was stored with the &store property\n * in a FormIt call.\n *\n * @var modX $modx\n * @var array $scriptProperties\n * \n * @package formit\n */\nrequire_once $modx->getOption(\'formit.core_path\',null,$modx->getOption(\'core_path\').\'components/formit/\').\'model/formit/formit.class.php\';\n$fi = new FormIt($modx,$scriptProperties);\n\n/* setup properties */\n$placeholderPrefix = $modx->getOption(\'placeholderPrefix\',$scriptProperties,\'fi.\');\n$eraseOnLoad = $modx->getOption(\'eraseOnLoad\',$scriptProperties,false);\n$redirectToOnNotFound = $modx->getOption(\'redirectToOnNotFound\',$scriptProperties,false);\n\n/* fetch data from cache and set to placeholders */\n$fi->loadRequest();\n$fi->request->loadDictionary();\n$data = $fi->request->dictionary->retrieve();\nif (!empty($data)) {\n    /* set data to placeholders */\n    foreach ($data as $k=>$v) {\n        /*checkboxes & other multi-values are stored as arrays, must be imploded*/\n        if (is_array($v)) {\n            $data[$k] = implode(\',\',$v);\n        }\n    }\n    $modx->toPlaceholders($data,$placeholderPrefix,\'\');\n    \n    /* if set, erase the data on load, otherwise depend on cache expiry time */\n    if ($eraseOnLoad) {\n        $fi->request->dictionary->erase();\n    }\n/* if the data\'s not found, and we want to redirect somewhere if so, do here */\n} else if (!empty($redirectToOnNotFound)) {\n    $url = $modx->makeUrl($redirectToOnNotFound);\n    $modx->sendRedirect($url);\n}\nreturn \'\';',0,'a:3:{s:17:\"placeholderPrefix\";a:7:{s:4:\"name\";s:17:\"placeholderPrefix\";s:4:\"desc\";s:31:\"prop_fir.placeholderprefix_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:3:\"fi.\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:20:\"redirectToOnNotFound\";a:7:{s:4:\"name\";s:20:\"redirectToOnNotFound\";s:4:\"desc\";s:34:\"prop_fir.redirecttoonnotfound_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"eraseOnLoad\";a:7:{s:4:\"name\";s:11:\"eraseOnLoad\";s:4:\"desc\";s:25:\"prop_fir.eraseonload_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}}','',0,''),
	(4,0,0,'FormItIsChecked','A custom output filter used with checkboxes/radios for checking checked status.',0,7,0,'/**\n * FormIt\n *\n * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>\n *\n * FormIt is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package formit\n */\n/**\n * Custom output filter that returns checked=\"checked\" if the value is set\n *\n * @var string $input\n * @var string $options\n * @package formit\n */\n$output = \' \';\nif ($input == $options) {\n    $output = \' checked=\"checked\"\';\n}\n$input = $modx->fromJSON($input);\nif (!empty($input) && is_array($input) && in_array($options,$input)) {\n  $output = \' checked=\"checked\"\';\n}\nreturn $output;',0,NULL,'',0,''),
	(5,0,0,'FormItIsSelected','A custom output filter used with dropdowns for checking selected status.',0,7,0,'/**\n * FormIt\n *\n * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>\n *\n * FormIt is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package formit\n */\n/**\n * Custom output filter that returns checked=\"checked\" if the value is set\n *\n * @var string $input\n * @var string $options\n * @package formit\n */\n$output = \' \';\nif ($input == $options) {\n    $output = \' selected=\"selected\"\';\n}\n$input = $modx->fromJSON($input);\nif(is_array($input)){\n    if (!empty($input) && is_array($input) && in_array($options,$input)) {\n      $output = \' selected=\"selected\"\';\n    }\n}\nreturn $output;',0,NULL,'',0,''),
	(6,0,0,'FormItCountryOptions','A utility snippet for generating a dropdown list of countries.',0,7,0,'/**\n * FormIt\n *\n * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>\n *\n * FormIt is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package formit\n */\n/**\n * Automatically generates and outputs a country list for usage in forms\n *\n * @var modX $modx\n * @var array $scriptProperties\n * @package formit\n */\nrequire_once $modx->getOption(\'formit.core_path\',null,$modx->getOption(\'core_path\').\'components/formit/\').\'model/formit/formit.class.php\';\n$fi = new FormIt($modx,$scriptProperties);\n\n/** @var fiCountryOptions $co */\n$co = $fi->loadModule(\'fiCountryOptions\',\'countryOptions\',$scriptProperties);\n$co->initialize();\n$co->getData();\n$co->loadPrioritized();\n$co->iterate();\nreturn $co->output();',0,'a:10:{s:8:\"selected\";a:7:{s:4:\"name\";s:8:\"selected\";s:4:\"desc\";s:23:\"prop_fico.selected_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:3:\"tpl\";a:7:{s:4:\"name\";s:3:\"tpl\";s:4:\"desc\";s:18:\"prop_fico.tpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:6:\"option\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:10:\"useIsoCode\";a:7:{s:4:\"name\";s:10:\"useIsoCode\";s:4:\"desc\";s:25:\"prop_fico.useisocode_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"prioritized\";a:7:{s:4:\"name\";s:11:\"prioritized\";s:4:\"desc\";s:26:\"prop_fico.prioritized_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:11:\"optGroupTpl\";a:7:{s:4:\"name\";s:11:\"optGroupTpl\";s:4:\"desc\";s:26:\"prop_fico.optgrouptpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:8:\"optgroup\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:20:\"prioritizedGroupText\";a:7:{s:4:\"name\";s:20:\"prioritizedGroupText\";s:4:\"desc\";s:35:\"prop_fico.prioritizedgrouptext_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:12:\"allGroupText\";a:7:{s:4:\"name\";s:12:\"allGroupText\";s:4:\"desc\";s:27:\"prop_fico.allgrouptext_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:17:\"selectedAttribute\";a:7:{s:4:\"name\";s:17:\"selectedAttribute\";s:4:\"desc\";s:32:\"prop_fico.selectedattribute_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:20:\" selected=\"selected\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:13:\"toPlaceholder\";a:7:{s:4:\"name\";s:13:\"toPlaceholder\";s:4:\"desc\";s:28:\"prop_fico.toplaceholder_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:7:\"country\";a:7:{s:4:\"name\";s:7:\"country\";s:4:\"desc\";s:22:\"prop_fico.country_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:16:\"[[++cultureKey]]\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}}','',0,''),
	(7,0,0,'FormItStateOptions','A utility snippet for generating a dropdown list of U.S. states.',0,7,0,'/**\n * FormIt\n *\n * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>\n *\n * FormIt is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package formit\n */\n/**\n * Automatically generates and outputs a U.S. state list for usage in forms\n * \n * @package formit\n */\nrequire_once $modx->getOption(\'formit.core_path\',null,$modx->getOption(\'core_path\').\'components/formit/\').\'model/formit/formit.class.php\';\n$fi = new FormIt($modx,$scriptProperties);\n\n/** @var fiStateOptions $so */\n$so = $fi->loadModule(\'fiStateOptions\',\'stateOptions\',$scriptProperties);\n$so->initialize();\n$so->getData();\n$so->iterate();\nreturn $so->output();',0,'a:6:{s:8:\"selected\";a:7:{s:4:\"name\";s:8:\"selected\";s:4:\"desc\";s:23:\"prop_fiso.selected_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:3:\"tpl\";a:7:{s:4:\"name\";s:3:\"tpl\";s:4:\"desc\";s:18:\"prop_fiso.tpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:6:\"option\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:7:\"useAbbr\";a:7:{s:4:\"name\";s:7:\"useAbbr\";s:4:\"desc\";s:22:\"prop_fiso.useabbr_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:17:\"selectedAttribute\";a:7:{s:4:\"name\";s:17:\"selectedAttribute\";s:4:\"desc\";s:32:\"prop_fiso.selectedattribute_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:20:\" selected=\"selected\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:7:\"country\";a:7:{s:4:\"name\";s:7:\"country\";s:4:\"desc\";s:22:\"prop_fiso.country_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:16:\"[[++cultureKey]]\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}s:13:\"toPlaceholder\";a:7:{s:4:\"name\";s:13:\"toPlaceholder\";s:4:\"desc\";s:28:\"prop_fiso.toplaceholder_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:17:\"formit:properties\";s:4:\"area\";s:0:\"\";}}','',0,''),
	(8,0,0,'getResources','<strong>1.6.1-pl</strong> A general purpose Resource listing and summarization snippet for MODX Revolution',0,0,0,'/**\n * getResources\n *\n * A general purpose Resource listing and summarization snippet for MODX 2.x.\n *\n * @author Jason Coward\n * @copyright Copyright 2010-2013, Jason Coward\n *\n * TEMPLATES\n *\n * tpl - Name of a chunk serving as a resource template\n * [NOTE: if not provided, properties are dumped to output for each resource]\n *\n * tplOdd - (Opt) Name of a chunk serving as resource template for resources with an odd idx value\n * (see idx property)\n * tplFirst - (Opt) Name of a chunk serving as resource template for the first resource (see first\n * property)\n * tplLast - (Opt) Name of a chunk serving as resource template for the last resource (see last\n * property)\n * tpl_{n} - (Opt) Name of a chunk serving as resource template for the nth resource\n *\n * tplCondition - (Opt) Defines a field of the resource to evaluate against keys defined in the\n * conditionalTpls property. Must be a resource field; does not work with Template Variables.\n * conditionalTpls - (Opt) A JSON object defining a map of field values and the associated tpl to\n * use when the field defined by tplCondition matches the value. [NOTE: tplOdd, tplFirst, tplLast,\n * and tpl_{n} will take precedence over any defined conditionalTpls]\n *\n * tplWrapper - (Opt) Name of a chunk serving as a wrapper template for the output\n * [NOTE: Does not work with toSeparatePlaceholders]\n *\n * SELECTION\n *\n * parents - Comma-delimited list of ids serving as parents\n *\n * context - (Opt) Comma-delimited list of context keys to limit results by; if empty, contexts for all specified\n * parents will be used (all contexts if 0 is specified) [default=]\n *\n * depth - (Opt) Integer value indicating depth to search for resources from each parent [default=10]\n *\n * tvFilters - (Opt) Delimited-list of TemplateVar values to filter resources by. Supports two\n * delimiters and two value search formats. The first delimiter || represents a logical OR and the\n * primary grouping mechanism.  Within each group you can provide a comma-delimited list of values.\n * These values can be either tied to a specific TemplateVar by name, e.g. myTV==value, or just the\n * value, indicating you are searching for the value in any TemplateVar tied to the Resource. An\n * example would be &tvFilters=`filter2==one,filter1==bar%||filter1==foo`\n * [NOTE: filtering by values uses a LIKE query and % is considered a wildcard.]\n * [NOTE: this only looks at the raw value set for specific Resource, i. e. there must be a value\n * specifically set for the Resource and it is not evaluated.]\n *\n * tvFiltersAndDelimiter - (Opt) Custom delimiter for logical AND, default \',\', in case you want to\n * match a literal comma in the tvFilters. E.g. &tvFiltersAndDelimiter=`&&`\n * &tvFilters=`filter1==foo,bar&&filter2==baz` [default=,]\n *\n * tvFiltersOrDelimiter - (Opt) Custom delimiter for logical OR, default \'||\', in case you want to\n * match a literal \'||\' in the tvFilters. E.g. &tvFiltersOrDelimiter=`|OR|`\n * &tvFilters=`filter1==foo||bar|OR|filter2==baz` [default=||]\n *\n * where - (Opt) A JSON expression of criteria to build any additional where clauses from. An example would be\n * &where=`{{\"alias:LIKE\":\"foo%\", \"OR:alias:LIKE\":\"%bar\"},{\"OR:pagetitle:=\":\"foobar\", \"AND:description:=\":\"raboof\"}}`\n *\n * sortby - (Opt) Field to sort by or a JSON array, e.g. {\"publishedon\":\"ASC\",\"createdon\":\"DESC\"} [default=publishedon]\n * sortbyTV - (opt) A Template Variable name to sort by (if supplied, this precedes the sortby value) [default=]\n * sortbyTVType - (Opt) A data type to CAST a TV Value to in order to sort on it properly [default=string]\n * sortbyAlias - (Opt) Query alias for sortby field [default=]\n * sortbyEscaped - (Opt) Escapes the field name(s) specified in sortby [default=0]\n * sortdir - (Opt) Order which to sort by [default=DESC]\n * sortdirTV - (Opt) Order which to sort by a TV [default=DESC]\n * limit - (Opt) Limits the number of resources returned [default=5]\n * offset - (Opt) An offset of resources returned by the criteria to skip [default=0]\n * dbCacheFlag - (Opt) Controls caching of db queries; 0|false = do not cache result set; 1 = cache result set\n * according to cache settings, any other integer value = number of seconds to cache result set [default=0]\n *\n * OPTIONS\n *\n * includeContent - (Opt) Indicates if the content of each resource should be returned in the\n * results [default=0]\n * includeTVs - (Opt) Indicates if TemplateVar values should be included in the properties available\n * to each resource template [default=0]\n * includeTVList - (Opt) Limits the TemplateVars that are included if includeTVs is true to those specified\n * by name in a comma-delimited list [default=]\n * prepareTVs - (Opt) Prepares media-source dependent TemplateVar values [default=1]\n * prepareTVList - (Opt) Limits the TVs that are prepared to those specified by name in a comma-delimited\n * list [default=]\n * processTVs - (Opt) Indicates if TemplateVar values should be rendered as they would on the\n * resource being summarized [default=0]\n * processTVList - (opt) Limits the TemplateVars that are processed if included to those specified\n * by name in a comma-delimited list [default=]\n * tvPrefix - (Opt) The prefix for TemplateVar properties [default=tv.]\n * idx - (Opt) You can define the starting idx of the resources, which is an property that is\n * incremented as each resource is rendered [default=1]\n * first - (Opt) Define the idx which represents the first resource (see tplFirst) [default=1]\n * last - (Opt) Define the idx which represents the last resource (see tplLast) [default=# of\n * resources being summarized + first - 1]\n * outputSeparator - (Opt) An optional string to separate each tpl instance [default=\"\\n\"]\n * wrapIfEmpty - (Opt) Indicates if the tplWrapper should be applied if the output is empty [default=0]\n *\n */\n$output = array();\n$outputSeparator = isset($outputSeparator) ? $outputSeparator : \"\\n\";\n\n/* set default properties */\n$tpl = !empty($tpl) ? $tpl : \'\';\n$includeContent = !empty($includeContent) ? true : false;\n$includeTVs = !empty($includeTVs) ? true : false;\n$includeTVList = !empty($includeTVList) ? explode(\',\', $includeTVList) : array();\n$processTVs = !empty($processTVs) ? true : false;\n$processTVList = !empty($processTVList) ? explode(\',\', $processTVList) : array();\n$prepareTVs = !empty($prepareTVs) ? true : false;\n$prepareTVList = !empty($prepareTVList) ? explode(\',\', $prepareTVList) : array();\n$tvPrefix = isset($tvPrefix) ? $tvPrefix : \'tv.\';\n$parents = (!empty($parents) || $parents === \'0\') ? explode(\',\', $parents) : array($modx->resource->get(\'id\'));\narray_walk($parents, \'trim\');\n$parents = array_unique($parents);\n$depth = isset($depth) ? (integer) $depth : 10;\n\n$tvFiltersOrDelimiter = isset($tvFiltersOrDelimiter) ? $tvFiltersOrDelimiter : \'||\';\n$tvFiltersAndDelimiter = isset($tvFiltersAndDelimiter) ? $tvFiltersAndDelimiter : \',\';\n$tvFilters = !empty($tvFilters) ? explode($tvFiltersOrDelimiter, $tvFilters) : array();\n\n$where = !empty($where) ? $modx->fromJSON($where) : array();\n$showUnpublished = !empty($showUnpublished) ? true : false;\n$showDeleted = !empty($showDeleted) ? true : false;\n\n$sortby = isset($sortby) ? $sortby : \'publishedon\';\n$sortbyTV = isset($sortbyTV) ? $sortbyTV : \'\';\n$sortbyAlias = isset($sortbyAlias) ? $sortbyAlias : \'modResource\';\n$sortbyEscaped = !empty($sortbyEscaped) ? true : false;\n$sortdir = isset($sortdir) ? $sortdir : \'DESC\';\n$sortdirTV = isset($sortdirTV) ? $sortdirTV : \'DESC\';\n$limit = isset($limit) ? (integer) $limit : 5;\n$offset = isset($offset) ? (integer) $offset : 0;\n$totalVar = !empty($totalVar) ? $totalVar : \'total\';\n\n$dbCacheFlag = !isset($dbCacheFlag) ? false : $dbCacheFlag;\nif (is_string($dbCacheFlag) || is_numeric($dbCacheFlag)) {\n    if ($dbCacheFlag == \'0\') {\n        $dbCacheFlag = false;\n    } elseif ($dbCacheFlag == \'1\') {\n        $dbCacheFlag = true;\n    } else {\n        $dbCacheFlag = (integer) $dbCacheFlag;\n    }\n}\n\n/* multiple context support */\n$contextArray = array();\n$contextSpecified = false;\nif (!empty($context)) {\n    $contextArray = explode(\',\',$context);\n    array_walk($contextArray, \'trim\');\n    $contexts = array();\n    foreach ($contextArray as $ctx) {\n        $contexts[] = $modx->quote($ctx);\n    }\n    $context = implode(\',\',$contexts);\n    $contextSpecified = true;\n    unset($contexts,$ctx);\n} else {\n    $context = $modx->quote($modx->context->get(\'key\'));\n}\n\n$pcMap = array();\n$pcQuery = $modx->newQuery(\'modResource\', array(\'id:IN\' => $parents), $dbCacheFlag);\n$pcQuery->select(array(\'id\', \'context_key\'));\nif ($pcQuery->prepare() && $pcQuery->stmt->execute()) {\n    foreach ($pcQuery->stmt->fetchAll(PDO::FETCH_ASSOC) as $pcRow) {\n        $pcMap[(integer) $pcRow[\'id\']] = $pcRow[\'context_key\'];\n    }\n}\n\n$children = array();\n$parentArray = array();\nforeach ($parents as $parent) {\n    $parent = (integer) $parent;\n    if ($parent === 0) {\n        $pchildren = array();\n        if ($contextSpecified) {\n            foreach ($contextArray as $pCtx) {\n                if (!in_array($pCtx, $contextArray)) {\n                    continue;\n                }\n                $options = $pCtx !== $modx->context->get(\'key\') ? array(\'context\' => $pCtx) : array();\n                $pcchildren = $modx->getChildIds($parent, $depth, $options);\n                if (!empty($pcchildren)) $pchildren = array_merge($pchildren, $pcchildren);\n            }\n        } else {\n            $cQuery = $modx->newQuery(\'modContext\', array(\'key:!=\' => \'mgr\'));\n            $cQuery->select(array(\'key\'));\n            if ($cQuery->prepare() && $cQuery->stmt->execute()) {\n                foreach ($cQuery->stmt->fetchAll(PDO::FETCH_COLUMN) as $pCtx) {\n                    $options = $pCtx !== $modx->context->get(\'key\') ? array(\'context\' => $pCtx) : array();\n                    $pcchildren = $modx->getChildIds($parent, $depth, $options);\n                    if (!empty($pcchildren)) $pchildren = array_merge($pchildren, $pcchildren);\n                }\n            }\n        }\n        $parentArray[] = $parent;\n    } else {\n        $pContext = array_key_exists($parent, $pcMap) ? $pcMap[$parent] : false;\n        if ($debug) $modx->log(modX::LOG_LEVEL_ERROR, \"context for {$parent} is {$pContext}\");\n        if ($pContext && $contextSpecified && !in_array($pContext, $contextArray, true)) {\n            $parent = next($parents);\n            continue;\n        }\n        $parentArray[] = $parent;\n        $options = !empty($pContext) && $pContext !== $modx->context->get(\'key\') ? array(\'context\' => $pContext) : array();\n        $pchildren = $modx->getChildIds($parent, $depth, $options);\n    }\n    if (!empty($pchildren)) $children = array_merge($children, $pchildren);\n    $parent = next($parents);\n}\n$parents = array_merge($parentArray, $children);\n\n/* build query */\n$criteria = array(\"modResource.parent IN (\" . implode(\',\', $parents) . \")\");\nif ($contextSpecified) {\n    $contextResourceTbl = $modx->getTableName(\'modContextResource\');\n    $criteria[] = \"(modResource.context_key IN ({$context}) OR EXISTS(SELECT 1 FROM {$contextResourceTbl} ctx WHERE ctx.resource = modResource.id AND ctx.context_key IN ({$context})))\";\n}\nif (empty($showDeleted)) {\n    $criteria[\'deleted\'] = \'0\';\n}\nif (empty($showUnpublished)) {\n    $criteria[\'published\'] = \'1\';\n}\nif (empty($showHidden)) {\n    $criteria[\'hidemenu\'] = \'0\';\n}\nif (!empty($hideContainers)) {\n    $criteria[\'isfolder\'] = \'0\';\n}\n$criteria = $modx->newQuery(\'modResource\', $criteria);\nif (!empty($tvFilters)) {\n    $tmplVarTbl = $modx->getTableName(\'modTemplateVar\');\n    $tmplVarResourceTbl = $modx->getTableName(\'modTemplateVarResource\');\n    $conditions = array();\n    $operators = array(\n        \'<=>\' => \'<=>\',\n        \'===\' => \'=\',\n        \'!==\' => \'!=\',\n        \'<>\' => \'<>\',\n        \'==\' => \'LIKE\',\n        \'!=\' => \'NOT LIKE\',\n        \'<<\' => \'<\',\n        \'<=\' => \'<=\',\n        \'=<\' => \'=<\',\n        \'>>\' => \'>\',\n        \'>=\' => \'>=\',\n        \'=>\' => \'=>\'\n    );\n    foreach ($tvFilters as $fGroup => $tvFilter) {\n        $filterGroup = array();\n        $filters = explode($tvFiltersAndDelimiter, $tvFilter);\n        $multiple = count($filters) > 0;\n        foreach ($filters as $filter) {\n            $operator = \'==\';\n            $sqlOperator = \'LIKE\';\n            foreach ($operators as $op => $opSymbol) {\n                if (strpos($filter, $op, 1) !== false) {\n                    $operator = $op;\n                    $sqlOperator = $opSymbol;\n                    break;\n                }\n            }\n            $tvValueField = \'tvr.value\';\n            $tvDefaultField = \'tv.default_text\';\n            $f = explode($operator, $filter);\n            if (count($f) >= 2) {\n                if (count($f) > 2) {\n                    $k = array_shift($f);\n                    $b = join($operator, $f);\n                    $f = array($k, $b);\n                }\n                $tvName = $modx->quote($f[0]);\n                if (is_numeric($f[1]) && !in_array($sqlOperator, array(\'LIKE\', \'NOT LIKE\'))) {\n                    $tvValue = $f[1];\n                    if ($f[1] == (integer)$f[1]) {\n                        $tvValueField = \"CAST({$tvValueField} AS SIGNED INTEGER)\";\n                        $tvDefaultField = \"CAST({$tvDefaultField} AS SIGNED INTEGER)\";\n                    } else {\n                        $tvValueField = \"CAST({$tvValueField} AS DECIMAL)\";\n                        $tvDefaultField = \"CAST({$tvDefaultField} AS DECIMAL)\";\n                    }\n                } else {\n                    $tvValue = $modx->quote($f[1]);\n                }\n                if ($multiple) {\n                    $filterGroup[] =\n                        \"(EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.name = {$tvName} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id) \" .\n                        \"OR EXISTS (SELECT 1 FROM {$tmplVarTbl} tv WHERE tv.name = {$tvName} AND {$tvDefaultField} {$sqlOperator} {$tvValue} AND tv.id NOT IN (SELECT tmplvarid FROM {$tmplVarResourceTbl} WHERE contentid = modResource.id)) \" .\n                        \")\";\n                } else {\n                    $filterGroup =\n                        \"(EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.name = {$tvName} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id) \" .\n                        \"OR EXISTS (SELECT 1 FROM {$tmplVarTbl} tv WHERE tv.name = {$tvName} AND {$tvDefaultField} {$sqlOperator} {$tvValue} AND tv.id NOT IN (SELECT tmplvarid FROM {$tmplVarResourceTbl} WHERE contentid = modResource.id)) \" .\n                        \")\";\n                }\n            } elseif (count($f) == 1) {\n                $tvValue = $modx->quote($f[0]);\n                if ($multiple) {\n                    $filterGroup[] = \"EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id)\";\n                } else {\n                    $filterGroup = \"EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id)\";\n                }\n            }\n        }\n        $conditions[] = $filterGroup;\n    }\n    if (!empty($conditions)) {\n        $firstGroup = true;\n        foreach ($conditions as $cGroup => $c) {\n            if (is_array($c)) {\n                $first = true;\n                foreach ($c as $cond) {\n                    if ($first && !$firstGroup) {\n                        $criteria->condition($criteria->query[\'where\'][0][1], $cond, xPDOQuery::SQL_OR, null, $cGroup);\n                    } else {\n                        $criteria->condition($criteria->query[\'where\'][0][1], $cond, xPDOQuery::SQL_AND, null, $cGroup);\n                    }\n                    $first = false;\n                }\n            } else {\n                $criteria->condition($criteria->query[\'where\'][0][1], $c, $firstGroup ? xPDOQuery::SQL_AND : xPDOQuery::SQL_OR, null, $cGroup);\n            }\n            $firstGroup = false;\n        }\n    }\n}\n/* include/exclude resources, via &resources=`123,-456` prop */\nif (!empty($resources)) {\n    $resourceConditions = array();\n    $resources = explode(\',\',$resources);\n    $include = array();\n    $exclude = array();\n    foreach ($resources as $resource) {\n        $resource = (int)$resource;\n        if ($resource == 0) continue;\n        if ($resource < 0) {\n            $exclude[] = abs($resource);\n        } else {\n            $include[] = $resource;\n        }\n    }\n    if (!empty($include)) {\n        $criteria->where(array(\'OR:modResource.id:IN\' => $include), xPDOQuery::SQL_OR);\n    }\n    if (!empty($exclude)) {\n        $criteria->where(array(\'modResource.id:NOT IN\' => $exclude), xPDOQuery::SQL_AND, null, 1);\n    }\n}\nif (!empty($where)) {\n    $criteria->where($where);\n}\n\n$total = $modx->getCount(\'modResource\', $criteria);\n$modx->setPlaceholder($totalVar, $total);\n\n$fields = array_keys($modx->getFields(\'modResource\'));\nif (empty($includeContent)) {\n    $fields = array_diff($fields, array(\'content\'));\n}\n$columns = $includeContent ? $modx->getSelectColumns(\'modResource\', \'modResource\') : $modx->getSelectColumns(\'modResource\', \'modResource\', \'\', array(\'content\'), true);\n$criteria->select($columns);\nif (!empty($sortbyTV)) {\n    $criteria->leftJoin(\'modTemplateVar\', \'tvDefault\', array(\n        \"tvDefault.name\" => $sortbyTV\n    ));\n    $criteria->leftJoin(\'modTemplateVarResource\', \'tvSort\', array(\n        \"tvSort.contentid = modResource.id\",\n        \"tvSort.tmplvarid = tvDefault.id\"\n    ));\n    if (empty($sortbyTVType)) $sortbyTVType = \'string\';\n    if ($modx->getOption(\'dbtype\') === \'mysql\') {\n        switch ($sortbyTVType) {\n            case \'integer\':\n                $criteria->select(\"CAST(IFNULL(tvSort.value, tvDefault.default_text) AS SIGNED INTEGER) AS sortTV\");\n                break;\n            case \'decimal\':\n                $criteria->select(\"CAST(IFNULL(tvSort.value, tvDefault.default_text) AS DECIMAL) AS sortTV\");\n                break;\n            case \'datetime\':\n                $criteria->select(\"CAST(IFNULL(tvSort.value, tvDefault.default_text) AS DATETIME) AS sortTV\");\n                break;\n            case \'string\':\n            default:\n                $criteria->select(\"IFNULL(tvSort.value, tvDefault.default_text) AS sortTV\");\n                break;\n        }\n    } elseif ($modx->getOption(\'dbtype\') === \'sqlsrv\') {\n        switch ($sortbyTVType) {\n            case \'integer\':\n                $criteria->select(\"CAST(ISNULL(tvSort.value, tvDefault.default_text) AS BIGINT) AS sortTV\");\n                break;\n            case \'decimal\':\n                $criteria->select(\"CAST(ISNULL(tvSort.value, tvDefault.default_text) AS DECIMAL) AS sortTV\");\n                break;\n            case \'datetime\':\n                $criteria->select(\"CAST(ISNULL(tvSort.value, tvDefault.default_text) AS DATETIME) AS sortTV\");\n                break;\n            case \'string\':\n            default:\n                $criteria->select(\"ISNULL(tvSort.value, tvDefault.default_text) AS sortTV\");\n                break;\n        }\n    }\n    $criteria->sortby(\"sortTV\", $sortdirTV);\n}\nif (!empty($sortby)) {\n    if (strpos($sortby, \'{\') === 0) {\n        $sorts = $modx->fromJSON($sortby);\n    } else {\n        $sorts = array($sortby => $sortdir);\n    }\n    if (is_array($sorts)) {\n        while (list($sort, $dir) = each($sorts)) {\n            if ($sortbyEscaped) $sort = $modx->escape($sort);\n            if (!empty($sortbyAlias)) $sort = $modx->escape($sortbyAlias) . \".{$sort}\";\n            $criteria->sortby($sort, $dir);\n        }\n    }\n}\nif (!empty($limit)) $criteria->limit($limit, $offset);\n\nif (!empty($debug)) {\n    $criteria->prepare();\n    $modx->log(modX::LOG_LEVEL_ERROR, $criteria->toSQL());\n}\n$collection = $modx->getCollection(\'modResource\', $criteria, $dbCacheFlag);\n\n$idx = !empty($idx) || $idx === \'0\' ? (integer) $idx : 1;\n$first = empty($first) && $first !== \'0\' ? 1 : (integer) $first;\n$last = empty($last) ? (count($collection) + $idx - 1) : (integer) $last;\n\n/* include parseTpl */\ninclude_once $modx->getOption(\'getresources.core_path\',null,$modx->getOption(\'core_path\').\'components/getresources/\').\'include.parsetpl.php\';\n\n$templateVars = array();\nif (!empty($includeTVs) && !empty($includeTVList)) {\n    $templateVars = $modx->getCollection(\'modTemplateVar\', array(\'name:IN\' => $includeTVList));\n}\n/** @var modResource $resource */\nforeach ($collection as $resourceId => $resource) {\n    $tvs = array();\n    if (!empty($includeTVs)) {\n        if (empty($includeTVList)) {\n            $templateVars = $resource->getMany(\'TemplateVars\');\n        }\n        /** @var modTemplateVar $templateVar */\n        foreach ($templateVars as $tvId => $templateVar) {\n            if (!empty($includeTVList) && !in_array($templateVar->get(\'name\'), $includeTVList)) continue;\n            if ($processTVs && (empty($processTVList) || in_array($templateVar->get(\'name\'), $processTVList))) {\n                $tvs[$tvPrefix . $templateVar->get(\'name\')] = $templateVar->renderOutput($resource->get(\'id\'));\n            } else {\n                $value = $templateVar->getValue($resource->get(\'id\'));\n                if ($prepareTVs && method_exists($templateVar, \'prepareOutput\') && (empty($prepareTVList) || in_array($templateVar->get(\'name\'), $prepareTVList))) {\n                    $value = $templateVar->prepareOutput($value);\n                }\n                $tvs[$tvPrefix . $templateVar->get(\'name\')] = $value;\n            }\n        }\n    }\n    $odd = ($idx & 1);\n    $properties = array_merge(\n        $scriptProperties\n        ,array(\n            \'idx\' => $idx\n            ,\'first\' => $first\n            ,\'last\' => $last\n            ,\'odd\' => $odd\n        )\n        ,$includeContent ? $resource->toArray() : $resource->get($fields)\n        ,$tvs\n    );\n    $resourceTpl = false;\n    if ($idx == $first && !empty($tplFirst)) {\n        $resourceTpl = parseTpl($tplFirst, $properties);\n    }\n    if ($idx == $last && empty($resourceTpl) && !empty($tplLast)) {\n        $resourceTpl = parseTpl($tplLast, $properties);\n    }\n    $tplidx = \'tpl_\' . $idx;\n    if (empty($resourceTpl) && !empty($$tplidx)) {\n        $resourceTpl = parseTpl($$tplidx, $properties);\n    }\n    if ($idx > 1 && empty($resourceTpl)) {\n        $divisors = getDivisors($idx);\n        if (!empty($divisors)) {\n            foreach ($divisors as $divisor) {\n                $tplnth = \'tpl_n\' . $divisor;\n                if (!empty($$tplnth)) {\n                    $resourceTpl = parseTpl($$tplnth, $properties);\n                    if (!empty($resourceTpl)) {\n                        break;\n                    }\n                }\n            }\n        }\n    }\n    if ($odd && empty($resourceTpl) && !empty($tplOdd)) {\n        $resourceTpl = parseTpl($tplOdd, $properties);\n    }\n    if (!empty($tplCondition) && !empty($conditionalTpls) && empty($resourceTpl)) {\n        $conTpls = $modx->fromJSON($conditionalTpls);\n        $subject = $properties[$tplCondition];\n        $tplOperator = !empty($tplOperator) ? $tplOperator : \'=\';\n        $tplOperator = strtolower($tplOperator);\n        $tplCon = \'\';\n        foreach ($conTpls as $operand => $conditionalTpl) {\n            switch ($tplOperator) {\n                case \'!=\':\n                case \'neq\':\n                case \'not\':\n                case \'isnot\':\n                case \'isnt\':\n                case \'unequal\':\n                case \'notequal\':\n                    $tplCon = (($subject != $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case \'<\':\n                case \'lt\':\n                case \'less\':\n                case \'lessthan\':\n                    $tplCon = (($subject < $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case \'>\':\n                case \'gt\':\n                case \'greater\':\n                case \'greaterthan\':\n                    $tplCon = (($subject > $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case \'<=\':\n                case \'lte\':\n                case \'lessthanequals\':\n                case \'lessthanorequalto\':\n                    $tplCon = (($subject <= $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case \'>=\':\n                case \'gte\':\n                case \'greaterthanequals\':\n                case \'greaterthanequalto\':\n                    $tplCon = (($subject >= $operand) ? $conditionalTpl : $tplCon);\n                    break;\n                case \'isempty\':\n                case \'empty\':\n                    $tplCon = empty($subject) ? $conditionalTpl : $tplCon;\n                    break;\n                case \'!empty\':\n                case \'notempty\':\n                case \'isnotempty\':\n                    $tplCon = !empty($subject) && $subject != \'\' ? $conditionalTpl : $tplCon;\n                    break;\n                case \'isnull\':\n                case \'null\':\n                    $tplCon = $subject == null || strtolower($subject) == \'null\' ? $conditionalTpl : $tplCon;\n                    break;\n                case \'inarray\':\n                case \'in_array\':\n                case \'ia\':\n                    $operand = explode(\',\', $operand);\n                    $tplCon = in_array($subject, $operand) ? $conditionalTpl : $tplCon;\n                    break;\n                case \'between\':\n                case \'range\':\n                case \'>=<\':\n                case \'><\':\n                    $operand = explode(\',\', $operand);\n                    $tplCon = ($subject >= min($operand) && $subject <= max($operand)) ? $conditionalTpl : $tplCon;\n                    break;\n                case \'==\':\n                case \'=\':\n                case \'eq\':\n                case \'is\':\n                case \'equal\':\n                case \'equals\':\n                case \'equalto\':\n                default:\n                    $tplCon = (($subject == $operand) ? $conditionalTpl : $tplCon);\n                    break;\n            }\n        }\n        if (!empty($tplCon)) {\n            $resourceTpl = parseTpl($tplCon, $properties);\n        }\n    }\n    if (!empty($tpl) && empty($resourceTpl)) {\n        $resourceTpl = parseTpl($tpl, $properties);\n    }\n    if ($resourceTpl === false && !empty($debug)) {\n        $chunk = $modx->newObject(\'modChunk\');\n        $chunk->setCacheable(false);\n        $output[]= $chunk->process(array(), \'<pre>\' . print_r($properties, true) .\'</pre>\');\n    } else {\n        $output[]= $resourceTpl;\n    }\n    $idx++;\n}\n\n/* output */\n$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);\nif (!empty($toSeparatePlaceholders)) {\n    $modx->setPlaceholders($output, $toSeparatePlaceholders);\n    return \'\';\n}\n\n$output = implode($outputSeparator, $output);\n\n$tplWrapper = $modx->getOption(\'tplWrapper\', $scriptProperties, false);\n$wrapIfEmpty = $modx->getOption(\'wrapIfEmpty\', $scriptProperties, false);\nif (!empty($tplWrapper) && ($wrapIfEmpty || !empty($output))) {\n    $output = parseTpl($tplWrapper, array_merge($scriptProperties, array(\'output\' => $output)));\n}\n\n$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);\nif (!empty($toPlaceholder)) {\n    $modx->setPlaceholder($toPlaceholder, $output);\n    return \'\';\n}\nreturn $output;',0,'a:44:{s:3:\"tpl\";a:7:{s:4:\"name\";s:3:\"tpl\";s:4:\"desc\";s:121:\"Name of a chunk serving as a resource template. NOTE: if not provided, properties are dumped to output for each resource.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:6:\"tplOdd\";a:7:{s:4:\"name\";s:6:\"tplOdd\";s:4:\"desc\";s:100:\"Name of a chunk serving as resource template for resources with an odd idx value (see idx property).\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:8:\"tplFirst\";a:7:{s:4:\"name\";s:8:\"tplFirst\";s:4:\"desc\";s:89:\"Name of a chunk serving as resource template for the first resource (see first property).\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:7:\"tplLast\";a:7:{s:4:\"name\";s:7:\"tplLast\";s:4:\"desc\";s:87:\"Name of a chunk serving as resource template for the last resource (see last property).\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:10:\"tplWrapper\";a:7:{s:4:\"name\";s:10:\"tplWrapper\";s:4:\"desc\";s:115:\"Name of a chunk serving as wrapper template for the Snippet output. This does not work with toSeparatePlaceholders.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:11:\"wrapIfEmpty\";a:7:{s:4:\"name\";s:11:\"wrapIfEmpty\";s:4:\"desc\";s:95:\"Indicates if empty output should be wrapped by the tplWrapper, if specified. Defaults to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:6:\"sortby\";a:7:{s:4:\"name\";s:6:\"sortby\";s:4:\"desc\";s:153:\"A field name to sort by or JSON object of field names and sortdir for each field, e.g. {\"publishedon\":\"ASC\",\"createdon\":\"DESC\"}. Defaults to publishedon.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:11:\"publishedon\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:8:\"sortbyTV\";a:7:{s:4:\"name\";s:8:\"sortbyTV\";s:4:\"desc\";s:65:\"Name of a Template Variable to sort by. Defaults to empty string.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:12:\"sortbyTVType\";a:7:{s:4:\"name\";s:12:\"sortbyTVType\";s:4:\"desc\";s:72:\"An optional type to indicate how to sort on the Template Variable value.\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:4:{i:0;a:2:{s:4:\"text\";s:6:\"string\";s:5:\"value\";s:6:\"string\";}i:1;a:2:{s:4:\"text\";s:7:\"integer\";s:5:\"value\";s:7:\"integer\";}i:2;a:2:{s:4:\"text\";s:7:\"decimal\";s:5:\"value\";s:7:\"decimal\";}i:3;a:2:{s:4:\"text\";s:8:\"datetime\";s:5:\"value\";s:8:\"datetime\";}}s:5:\"value\";s:6:\"string\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:11:\"sortbyAlias\";a:7:{s:4:\"name\";s:11:\"sortbyAlias\";s:4:\"desc\";s:58:\"Query alias for sortby field. Defaults to an empty string.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:13:\"sortbyEscaped\";a:7:{s:4:\"name\";s:13:\"sortbyEscaped\";s:4:\"desc\";s:82:\"Determines if the field name specified in sortby should be escaped. Defaults to 0.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\"0\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:7:\"sortdir\";a:7:{s:4:\"name\";s:7:\"sortdir\";s:4:\"desc\";s:41:\"Order which to sort by. Defaults to DESC.\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:2:{i:0;a:2:{s:4:\"text\";s:3:\"ASC\";s:5:\"value\";s:3:\"ASC\";}i:1;a:2:{s:4:\"text\";s:4:\"DESC\";s:5:\"value\";s:4:\"DESC\";}}s:5:\"value\";s:4:\"DESC\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:9:\"sortdirTV\";a:7:{s:4:\"name\";s:9:\"sortdirTV\";s:4:\"desc\";s:61:\"Order which to sort a Template Variable by. Defaults to DESC.\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:2:{i:0;a:2:{s:4:\"text\";s:3:\"ASC\";s:5:\"value\";s:3:\"ASC\";}i:1;a:2:{s:4:\"text\";s:4:\"DESC\";s:5:\"value\";s:4:\"DESC\";}}s:5:\"value\";s:4:\"DESC\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:5:\"limit\";a:7:{s:4:\"name\";s:5:\"limit\";s:4:\"desc\";s:55:\"Limits the number of resources returned. Defaults to 5.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\"5\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:6:\"offset\";a:7:{s:4:\"name\";s:6:\"offset\";s:4:\"desc\";s:56:\"An offset of resources returned by the criteria to skip.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\"0\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:9:\"tvFilters\";a:7:{s:4:\"name\";s:9:\"tvFilters\";s:4:\"desc\";s:778:\"Delimited-list of TemplateVar values to filter resources by. Supports two delimiters and two value search formats. THe first delimiter || represents a logical OR and the primary grouping mechanism.  Within each group you can provide a comma-delimited list of values. These values can be either tied to a specific TemplateVar by name, e.g. myTV==value, or just the value, indicating you are searching for the value in any TemplateVar tied to the Resource. An example would be &tvFilters=`filter2==one,filter1==bar%||filter1==foo`. <br />NOTE: filtering by values uses a LIKE query and % is considered a wildcard. <br />ANOTHER NOTE: This only looks at the raw value set for specific Resource, i. e. there must be a value specifically set for the Resource and it is not evaluated.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:21:\"tvFiltersAndDelimiter\";a:7:{s:4:\"name\";s:21:\"tvFiltersAndDelimiter\";s:4:\"desc\";s:83:\"The delimiter to use to separate logical AND expressions in tvFilters. Default is ,\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\",\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:20:\"tvFiltersOrDelimiter\";a:7:{s:4:\"name\";s:20:\"tvFiltersOrDelimiter\";s:4:\"desc\";s:83:\"The delimiter to use to separate logical OR expressions in tvFilters. Default is ||\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:2:\"||\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:5:\"depth\";a:7:{s:4:\"name\";s:5:\"depth\";s:4:\"desc\";s:88:\"Integer value indicating depth to search for resources from each parent. Defaults to 10.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:2:\"10\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:7:\"parents\";a:7:{s:4:\"name\";s:7:\"parents\";s:4:\"desc\";s:57:\"Optional. Comma-delimited list of ids serving as parents.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:14:\"includeContent\";a:7:{s:4:\"name\";s:14:\"includeContent\";s:4:\"desc\";s:95:\"Indicates if the content of each resource should be returned in the results. Defaults to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:10:\"includeTVs\";a:7:{s:4:\"name\";s:10:\"includeTVs\";s:4:\"desc\";s:124:\"Indicates if TemplateVar values should be included in the properties available to each resource template. Defaults to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:13:\"includeTVList\";a:7:{s:4:\"name\";s:13:\"includeTVList\";s:4:\"desc\";s:96:\"Limits included TVs to those specified as a comma-delimited list of TV names. Defaults to empty.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:10:\"showHidden\";a:7:{s:4:\"name\";s:10:\"showHidden\";s:4:\"desc\";s:85:\"Indicates if Resources that are hidden from menus should be shown. Defaults to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:15:\"showUnpublished\";a:7:{s:4:\"name\";s:15:\"showUnpublished\";s:4:\"desc\";s:79:\"Indicates if Resources that are unpublished should be shown. Defaults to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:11:\"showDeleted\";a:7:{s:4:\"name\";s:11:\"showDeleted\";s:4:\"desc\";s:75:\"Indicates if Resources that are deleted should be shown. Defaults to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:9:\"resources\";a:7:{s:4:\"name\";s:9:\"resources\";s:4:\"desc\";s:177:\"A comma-separated list of resource IDs to exclude or include. IDs with a - in front mean to exclude. Ex: 123,-456 means to include Resource 123, but always exclude Resource 456.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:10:\"processTVs\";a:7:{s:4:\"name\";s:10:\"processTVs\";s:4:\"desc\";s:117:\"Indicates if TemplateVar values should be rendered as they would on the resource being summarized. Defaults to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:13:\"processTVList\";a:7:{s:4:\"name\";s:13:\"processTVList\";s:4:\"desc\";s:166:\"Limits processed TVs to those specified as a comma-delimited list of TV names; note only includedTVs will be available for processing if specified. Defaults to empty.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:10:\"prepareTVs\";a:7:{s:4:\"name\";s:10:\"prepareTVs\";s:4:\"desc\";s:120:\"Indicates if TemplateVar values that are not processed fully should be prepared before being returned. Defaults to true.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:13:\"prepareTVList\";a:7:{s:4:\"name\";s:13:\"prepareTVList\";s:4:\"desc\";s:164:\"Limits prepared TVs to those specified as a comma-delimited list of TV names; note only includedTVs will be available for preparing if specified. Defaults to empty.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:8:\"tvPrefix\";a:7:{s:4:\"name\";s:8:\"tvPrefix\";s:4:\"desc\";s:55:\"The prefix for TemplateVar properties. Defaults to: tv.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:3:\"tv.\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:3:\"idx\";a:7:{s:4:\"name\";s:3:\"idx\";s:4:\"desc\";s:120:\"You can define the starting idx of the resources, which is an property that is incremented as each resource is rendered.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:5:\"first\";a:7:{s:4:\"name\";s:5:\"first\";s:4:\"desc\";s:81:\"Define the idx which represents the first resource (see tplFirst). Defaults to 1.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:4:\"last\";a:7:{s:4:\"name\";s:4:\"last\";s:4:\"desc\";s:129:\"Define the idx which represents the last resource (see tplLast). Defaults to the number of resources being summarized + first - 1\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:13:\"toPlaceholder\";a:7:{s:4:\"name\";s:13:\"toPlaceholder\";s:4:\"desc\";s:85:\"If set, will assign the result to this placeholder instead of outputting it directly.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:22:\"toSeparatePlaceholders\";a:7:{s:4:\"name\";s:22:\"toSeparatePlaceholders\";s:4:\"desc\";s:130:\"If set, will assign EACH result to a separate placeholder named by this param suffixed with a sequential number (starting from 0).\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:5:\"debug\";a:7:{s:4:\"name\";s:5:\"debug\";s:4:\"desc\";s:68:\"If true, will send the SQL query to the MODX log. Defaults to false.\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:5:\"where\";a:7:{s:4:\"name\";s:5:\"where\";s:4:\"desc\";s:193:\"A JSON expression of criteria to build any additional where clauses from, e.g. &where=`{{\"alias:LIKE\":\"foo%\", \"OR:alias:LIKE\":\"%bar\"},{\"OR:pagetitle:=\":\"foobar\", \"AND:description:=\":\"raboof\"}}`\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:11:\"dbCacheFlag\";a:7:{s:4:\"name\";s:11:\"dbCacheFlag\";s:4:\"desc\";s:218:\"Determines how result sets are cached if cache_db is enabled in MODX. 0|false = do not cache result set; 1 = cache result set according to cache settings, any other integer value = number of seconds to cache result set\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\"0\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:7:\"context\";a:7:{s:4:\"name\";s:7:\"context\";s:4:\"desc\";s:116:\"A comma-delimited list of context keys for limiting results. Default is empty, i.e. do not limit results by context.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:12:\"tplCondition\";a:7:{s:4:\"name\";s:12:\"tplCondition\";s:4:\"desc\";s:129:\"A condition to compare against the conditionalTpls property to map Resources to different tpls based on custom conditional logic.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:11:\"tplOperator\";a:7:{s:4:\"name\";s:11:\"tplOperator\";s:4:\"desc\";s:125:\"An optional operator to use for the tplCondition when comparing against the conditionalTpls operands. Default is == (equals).\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:10:{i:0;a:2:{s:4:\"text\";s:11:\"is equal to\";s:5:\"value\";s:2:\"==\";}i:1;a:2:{s:4:\"text\";s:15:\"is not equal to\";s:5:\"value\";s:2:\"!=\";}i:2;a:2:{s:4:\"text\";s:9:\"less than\";s:5:\"value\";s:1:\"<\";}i:3;a:2:{s:4:\"text\";s:21:\"less than or equal to\";s:5:\"value\";s:2:\"<=\";}i:4;a:2:{s:4:\"text\";s:24:\"greater than or equal to\";s:5:\"value\";s:2:\">=\";}i:5;a:2:{s:4:\"text\";s:8:\"is empty\";s:5:\"value\";s:5:\"empty\";}i:6;a:2:{s:4:\"text\";s:12:\"is not empty\";s:5:\"value\";s:6:\"!empty\";}i:7;a:2:{s:4:\"text\";s:7:\"is null\";s:5:\"value\";s:4:\"null\";}i:8;a:2:{s:4:\"text\";s:11:\"is in array\";s:5:\"value\";s:7:\"inarray\";}i:9;a:2:{s:4:\"text\";s:10:\"is between\";s:5:\"value\";s:7:\"between\";}}s:5:\"value\";s:2:\"==\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}s:15:\"conditionalTpls\";a:7:{s:4:\"name\";s:15:\"conditionalTpls\";s:4:\"desc\";s:121:\"A JSON map of conditional operands and tpls to compare against the tplCondition property using the specified tplOperator.\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";N;s:4:\"area\";s:0:\"\";}}','',0,''),
	(9,0,0,'Wayfinder','Wayfinder for MODx Revolution 2.0.0-beta-5 and later.',0,0,0,'/**\n * Wayfinder Snippet to build site navigation menus\n *\n * Totally refactored from original DropMenu nav builder to make it easier to\n * create custom navigation by using chunks as output templates. By using\n * templates, many of the paramaters are no longer needed for flexible output\n * including tables, unordered- or ordered-lists (ULs or OLs), definition lists\n * (DLs) or in any other format you desire.\n *\n * @version 2.1.1-beta5\n * @author Garry Nutting (collabpad.com)\n * @author Kyle Jaebker (muddydogpaws.com)\n * @author Ryan Thrash (modx.com)\n * @author Shaun McCormick (modx.com)\n * @author Jason Coward (modx.com)\n *\n * @example [[Wayfinder? &startId=`0`]]\n *\n * @var modX $modx\n * @var array $scriptProperties\n * \n * @package wayfinder\n */\n$wayfinder_base = $modx->getOption(\'wayfinder.core_path\',$scriptProperties,$modx->getOption(\'core_path\').\'components/wayfinder/\');\n\n/* include a custom config file if specified */\nif (isset($scriptProperties[\'config\'])) {\n    $scriptProperties[\'config\'] = str_replace(\'../\',\'\',$scriptProperties[\'config\']);\n    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/\'.$scriptProperties[\'config\'].\'.config.php\';\n} else {\n    $scriptProperties[\'config\'] = $wayfinder_base.\'configs/default.config.php\';\n}\nif (file_exists($scriptProperties[\'config\'])) {\n    include $scriptProperties[\'config\'];\n}\n\n/* include wayfinder class */\ninclude_once $wayfinder_base.\'wayfinder.class.php\';\nif (!$modx->loadClass(\'Wayfinder\',$wayfinder_base,true,true)) {\n    return \'error: Wayfinder class not found\';\n}\n$wf = new Wayfinder($modx,$scriptProperties);\n\n/* get user class definitions\n * TODO: eventually move these into config parameters */\n$wf->_css = array(\n    \'first\' => isset($firstClass) ? $firstClass : \'\',\n    \'last\' => isset($lastClass) ? $lastClass : \'last\',\n    \'here\' => isset($hereClass) ? $hereClass : \'active\',\n    \'parent\' => isset($parentClass) ? $parentClass : \'\',\n    \'row\' => isset($rowClass) ? $rowClass : \'\',\n    \'outer\' => isset($outerClass) ? $outerClass : \'\',\n    \'inner\' => isset($innerClass) ? $innerClass : \'\',\n    \'level\' => isset($levelClass) ? $levelClass: \'\',\n    \'self\' => isset($selfClass) ? $selfClass : \'\',\n    \'weblink\' => isset($webLinkClass) ? $webLinkClass : \'\'\n);\n\n/* get user templates\n * TODO: eventually move these into config parameters */\n$wf->_templates = array(\n    \'outerTpl\' => isset($outerTpl) ? $outerTpl : \'\',\n    \'rowTpl\' => isset($rowTpl) ? $rowTpl : \'\',\n    \'parentRowTpl\' => isset($parentRowTpl) ? $parentRowTpl : \'\',\n    \'parentRowHereTpl\' => isset($parentRowHereTpl) ? $parentRowHereTpl : \'\',\n    \'hereTpl\' => isset($hereTpl) ? $hereTpl : \'\',\n    \'innerTpl\' => isset($innerTpl) ? $innerTpl : \'\',\n    \'innerRowTpl\' => isset($innerRowTpl) ? $innerRowTpl : \'\',\n    \'innerHereTpl\' => isset($innerHereTpl) ? $innerHereTpl : \'\',\n    \'activeParentRowTpl\' => isset($activeParentRowTpl) ? $activeParentRowTpl : \'\',\n    \'categoryFoldersTpl\' => isset($categoryFoldersTpl) ? $categoryFoldersTpl : \'\',\n    \'startItemTpl\' => isset($startItemTpl) ? $startItemTpl : \'\'\n);\n\n/* process Wayfinder */\n$output = $wf->run();\nif ($wf->_config[\'debug\']) {\n    $output .= $wf->renderDebugOutput();\n}\n\n/* output results */\nif ($wf->_config[\'ph\']) {\n    $modx->setPlaceholder($wf->_config[\'ph\'],$output);\n} else {\n    return $output;\n}',0,'a:48:{s:5:\"level\";a:6:{s:4:\"name\";s:5:\"level\";s:4:\"desc\";s:25:\"prop_wayfinder.level_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\"0\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:11:\"includeDocs\";a:6:{s:4:\"name\";s:11:\"includeDocs\";s:4:\"desc\";s:31:\"prop_wayfinder.includeDocs_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\"0\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:11:\"excludeDocs\";a:6:{s:4:\"name\";s:11:\"excludeDocs\";s:4:\"desc\";s:31:\"prop_wayfinder.excludeDocs_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\"0\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:8:\"contexts\";a:6:{s:4:\"name\";s:8:\"contexts\";s:4:\"desc\";s:28:\"prop_wayfinder.contexts_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"cacheResults\";a:6:{s:4:\"name\";s:12:\"cacheResults\";s:4:\"desc\";s:32:\"prop_wayfinder.cacheResults_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:9:\"cacheTime\";a:6:{s:4:\"name\";s:9:\"cacheTime\";s:4:\"desc\";s:29:\"prop_wayfinder.cacheTime_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";i:3600;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:2:\"ph\";a:6:{s:4:\"name\";s:2:\"ph\";s:4:\"desc\";s:22:\"prop_wayfinder.ph_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:5:\"debug\";a:6:{s:4:\"name\";s:5:\"debug\";s:4:\"desc\";s:25:\"prop_wayfinder.debug_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"ignoreHidden\";a:6:{s:4:\"name\";s:12:\"ignoreHidden\";s:4:\"desc\";s:32:\"prop_wayfinder.ignoreHidden_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"hideSubMenus\";a:6:{s:4:\"name\";s:12:\"hideSubMenus\";s:4:\"desc\";s:32:\"prop_wayfinder.hideSubMenus_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:13:\"useWeblinkUrl\";a:6:{s:4:\"name\";s:13:\"useWeblinkUrl\";s:4:\"desc\";s:33:\"prop_wayfinder.useWeblinkUrl_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:8:\"fullLink\";a:6:{s:4:\"name\";s:8:\"fullLink\";s:4:\"desc\";s:28:\"prop_wayfinder.fullLink_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:6:\"scheme\";a:6:{s:4:\"name\";s:6:\"scheme\";s:4:\"desc\";s:26:\"prop_wayfinder.scheme_desc\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:3:{i:0;a:2:{s:4:\"text\";s:23:\"prop_wayfinder.relative\";s:5:\"value\";s:0:\"\";}i:1;a:2:{s:4:\"text\";s:23:\"prop_wayfinder.absolute\";s:5:\"value\";s:3:\"abs\";}i:2;a:2:{s:4:\"text\";s:19:\"prop_wayfinder.full\";s:5:\"value\";s:4:\"full\";}}s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:9:\"sortOrder\";a:6:{s:4:\"name\";s:9:\"sortOrder\";s:4:\"desc\";s:29:\"prop_wayfinder.sortOrder_desc\";s:4:\"type\";s:4:\"list\";s:7:\"options\";a:2:{i:0;a:2:{s:4:\"text\";s:24:\"prop_wayfinder.ascending\";s:5:\"value\";s:3:\"ASC\";}i:1;a:2:{s:4:\"text\";s:25:\"prop_wayfinder.descending\";s:5:\"value\";s:4:\"DESC\";}}s:5:\"value\";s:3:\"ASC\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:6:\"sortBy\";a:6:{s:4:\"name\";s:6:\"sortBy\";s:4:\"desc\";s:26:\"prop_wayfinder.sortBy_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:9:\"menuindex\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:5:\"limit\";a:6:{s:4:\"name\";s:5:\"limit\";s:4:\"desc\";s:25:\"prop_wayfinder.limit_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:1:\"0\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:6:\"cssTpl\";a:6:{s:4:\"name\";s:6:\"cssTpl\";s:4:\"desc\";s:26:\"prop_wayfinder.cssTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:5:\"jsTpl\";a:6:{s:4:\"name\";s:5:\"jsTpl\";s:4:\"desc\";s:25:\"prop_wayfinder.jsTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:11:\"rowIdPrefix\";a:6:{s:4:\"name\";s:11:\"rowIdPrefix\";s:4:\"desc\";s:31:\"prop_wayfinder.rowIdPrefix_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:11:\"textOfLinks\";a:6:{s:4:\"name\";s:11:\"textOfLinks\";s:4:\"desc\";s:31:\"prop_wayfinder.textOfLinks_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:9:\"menutitle\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"titleOfLinks\";a:6:{s:4:\"name\";s:12:\"titleOfLinks\";s:4:\"desc\";s:32:\"prop_wayfinder.titleOfLinks_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:9:\"pagetitle\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"displayStart\";a:6:{s:4:\"name\";s:12:\"displayStart\";s:4:\"desc\";s:32:\"prop_wayfinder.displayStart_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:0;s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:10:\"firstClass\";a:6:{s:4:\"name\";s:10:\"firstClass\";s:4:\"desc\";s:30:\"prop_wayfinder.firstClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:5:\"first\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:9:\"lastClass\";a:6:{s:4:\"name\";s:9:\"lastClass\";s:4:\"desc\";s:29:\"prop_wayfinder.lastClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:4:\"last\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:9:\"hereClass\";a:6:{s:4:\"name\";s:9:\"hereClass\";s:4:\"desc\";s:29:\"prop_wayfinder.hereClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:6:\"active\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:11:\"parentClass\";a:6:{s:4:\"name\";s:11:\"parentClass\";s:4:\"desc\";s:31:\"prop_wayfinder.parentClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:8:\"rowClass\";a:6:{s:4:\"name\";s:8:\"rowClass\";s:4:\"desc\";s:28:\"prop_wayfinder.rowClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:10:\"outerClass\";a:6:{s:4:\"name\";s:10:\"outerClass\";s:4:\"desc\";s:30:\"prop_wayfinder.outerClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:10:\"innerClass\";a:6:{s:4:\"name\";s:10:\"innerClass\";s:4:\"desc\";s:30:\"prop_wayfinder.innerClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:10:\"levelClass\";a:6:{s:4:\"name\";s:10:\"levelClass\";s:4:\"desc\";s:30:\"prop_wayfinder.levelClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:9:\"selfClass\";a:6:{s:4:\"name\";s:9:\"selfClass\";s:4:\"desc\";s:29:\"prop_wayfinder.selfClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"webLinkClass\";a:6:{s:4:\"name\";s:12:\"webLinkClass\";s:4:\"desc\";s:32:\"prop_wayfinder.webLinkClass_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:8:\"outerTpl\";a:6:{s:4:\"name\";s:8:\"outerTpl\";s:4:\"desc\";s:28:\"prop_wayfinder.outerTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:6:\"rowTpl\";a:6:{s:4:\"name\";s:6:\"rowTpl\";s:4:\"desc\";s:26:\"prop_wayfinder.rowTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"parentRowTpl\";a:6:{s:4:\"name\";s:12:\"parentRowTpl\";s:4:\"desc\";s:32:\"prop_wayfinder.parentRowTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:16:\"parentRowHereTpl\";a:6:{s:4:\"name\";s:16:\"parentRowHereTpl\";s:4:\"desc\";s:36:\"prop_wayfinder.parentRowHereTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:7:\"hereTpl\";a:6:{s:4:\"name\";s:7:\"hereTpl\";s:4:\"desc\";s:27:\"prop_wayfinder.hereTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:8:\"innerTpl\";a:6:{s:4:\"name\";s:8:\"innerTpl\";s:4:\"desc\";s:28:\"prop_wayfinder.innerTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:11:\"innerRowTpl\";a:6:{s:4:\"name\";s:11:\"innerRowTpl\";s:4:\"desc\";s:31:\"prop_wayfinder.innerRowTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"innerHereTpl\";a:6:{s:4:\"name\";s:12:\"innerHereTpl\";s:4:\"desc\";s:32:\"prop_wayfinder.innerHereTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:18:\"activeParentRowTpl\";a:6:{s:4:\"name\";s:18:\"activeParentRowTpl\";s:4:\"desc\";s:38:\"prop_wayfinder.activeParentRowTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:18:\"categoryFoldersTpl\";a:6:{s:4:\"name\";s:18:\"categoryFoldersTpl\";s:4:\"desc\";s:38:\"prop_wayfinder.categoryFoldersTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:12:\"startItemTpl\";a:6:{s:4:\"name\";s:12:\"startItemTpl\";s:4:\"desc\";s:32:\"prop_wayfinder.startItemTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:11:\"permissions\";a:6:{s:4:\"name\";s:11:\"permissions\";s:4:\"desc\";s:31:\"prop_wayfinder.permissions_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:4:\"list\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:6:\"hereId\";a:6:{s:4:\"name\";s:6:\"hereId\";s:4:\"desc\";s:26:\"prop_wayfinder.hereId_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:5:\"where\";a:6:{s:4:\"name\";s:5:\"where\";s:4:\"desc\";s:25:\"prop_wayfinder.where_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:9:\"templates\";a:6:{s:4:\"name\";s:9:\"templates\";s:4:\"desc\";s:29:\"prop_wayfinder.templates_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}s:18:\"previewUnpublished\";a:6:{s:4:\"name\";s:18:\"previewUnpublished\";s:4:\"desc\";s:38:\"prop_wayfinder.previewunpublished_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"wayfinder:properties\";}}','',0,''),
	(11,1,0,'GetPageTitle','',0,0,0,'/**\n * MY_GetPageTitle\n *\n * DESCRIPTION\n *\n * This snippet returns the title of a page matching an id\n *\n * USAGE:\n *\n * [[!MY_GetPageTitle? &id=`[[+parent]]`]]\n */\n \n$page = $modx->getObject(\'modResource\', $id);\nif(!empty($page)) {\n    return $page->get(\'pagetitle\');\n}\nreturn \'\';',0,'a:0:{}','',0,''),
	(12,0,0,'FormItSaveForm','Save any form and export them to csv.',0,7,0,'/**\n * FormIt\n *\n * Copyright 2011-12 by SCHERP Ontwikkeling <info@scherpontwikkeling.nl>\n * Copyright 2015 by Wieger Sloot <modx@sterc.nl>\n *\n * FormIt is free software; you can redistribute it and/or modify it\n * under the terms of the GNU General Public License as published by the Free\n * Software Foundation; either version 2 of the License, or (at your option) any\n * later version.\n *\n * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,\n * Suite 330, Boston, MA 02111-1307 USA\n *\n * @package formit\n */\n/**\n * A custom FormIt hook for saving filled-in forms. - Based on FormSave\n *\n * @var modX $modx\n * @var array $scriptProperties\n * @var FormIt $formit\n * @var fiHooks $hook\n * \n * @package formit\n */\n/* setup default properties */\n$values = $hook->getValues();\n$formName = $modx->getOption(\'formName\', $formit->config, \'form-\'.$modx->resource->get(\'id\'));\n$formEncrypt = $modx->getOption(\'formEncrypt\', $formit->config, false);\n$formFields = $modx->getOption(\'formFields\', $formit->config, false);\n$fieldNames = $modx->getOption(\'fieldNames\', $formit->config, false);\nif ($formFields) {\n    $formFields = explode(\',\', $formFields);\n    foreach($formFields as $k => $v) {\n        $formFields[$k] = trim($v);\n    }\n}\n// Build the data array\n$dataArray = array();\nif($formFields){\n    foreach($formFields as $field) {\n        $dataArray[$field] = (!isset($values[$field])) ? \'\' : $values[$field];\n    }\n}else{\n    $dataArray = $values;\n}\n//Change the fieldnames\nif($fieldNames){\n    $newDataArray = array();\n    $fieldLabels = array();\n    $formFieldNames = explode(\',\', $fieldNames);\n    foreach($formFieldNames as $formFieldName){\n        list($name, $label) = explode(\'==\', $formFieldName);\n        $fieldLabels[trim($name)] = trim($label);\n    }\n    foreach ($dataArray as $key => $value) {\n        if($fieldLabels[$key]){\n            $newDataArray[$fieldLabels[$key]] = $value;\n        }else{\n            $newDataArray[$key] = $value;\n        }\n    }\n    $dataArray = $newDataArray;\n}\n// Create obj\n$newForm = $modx->newObject(\'FormItForm\');\nif($formEncrypt){\n    $dataArray = $newForm->encrypt($modx->toJSON($dataArray));\n}else{\n    $dataArray = $modx->toJSON($dataArray);\n}\n$newForm->fromArray(array(\n    \'form\' => $formName,\n    \'date\' => time(),\n    \'values\' => $dataArray,\n    \'ip\' => $modx->getOption(\'REMOTE_ADDR\', $_SERVER, \'\'),\n    \'context_key\' => $modx->resource->get(\'context_key\'),\n    \'encrypted\' => $formEncrypt\n));\n\nif (!$newForm->save()) {\n    $modx->log(modX::LOG_LEVEL_ERROR, \'[FormItSaveForm] An error occurred while trying to save the submitted form: \' . print_r($newForm->toArray(), true));\n    return false;\n}\n$hook->setValue(\'savedForm\', $newForm->toArray());\nreturn true;',0,NULL,'',0,''),
	(13,0,0,'sendGrid','Route mail through sendGrid',0,10,0,'/**\n * sendGrid snippet for sendGrid extra\n *\n * Copyright 2015 by Graeme Leighfield <http://www.gelstudios.co.uk>\n * Created on 02-22-2015\n *\n * sendGrid is free software; you can redistribute it and/or modify it under the\n * terms of the GNU General Public License as published by the Free Software\n * Foundation; either version 2 of the License, or (at your option) any later\n * version.\n *\n * sendGrid is distributed in the hope that it will be useful, but WITHOUT ANY\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n *\n * You should have received a copy of the GNU General Public License along with\n * sendGrid; if not, write to the Free Software Foundation, Inc., 59 Temple\n * Place, Suite 330, Boston, MA 02111-1307 USA\n *\n * @package sendgrid\n */\n\n/**\n * Description\n * -----------\n * Route mail through sendGrid\n *\n * Variables\n * ---------\n * @var $modx modX\n * @var $scriptProperties array\n *\n * @package sendgrid\n **/\n\n$tpl = $modx->getOption(\'sgTpl\', $scriptProperties);\n$mailSubject = $modx->getOption(\'sgSubject\', $scriptProperties);\n$mailFrom = $modx->getOption(\'sgFrom\', $scriptProperties, $modx->getOption(\'emailsender\'));\n$mailTo = $hook->getValue($modx->getOption(\'sgTo\', $scriptProperties, \'email\'));\n$mailToName = $hook->getValue($modx->getOption(\'sgToName\', $scriptProperties, \'name\'));\n$mailFromName = $modx->getOption(\'sgFromName\', $scriptProperties, $modx->getOption(\'site_name\'));\n$html = $modx->getChunk($tpl, $hook->getValues());\n\n$url = \'https://api.sendgrid.com/\';\n$user = $modx->getOption(\'sendgrid_username\');\n$pass = $modx->getOption(\'sendgrid_password\');\n\n$params = array(\n    \'api_user\'  => $user,\n    \'api_key\'   => $pass,\n    \'to\'        => $mailTo,\n	\'toname\'	=> $mailToName,\n	\'fromname\'	=> $mailFromName,\n    \'subject\'   => $mailSubject,\n    \'html\'      => $html,\n    \'from\'      => $mailFrom,\n);\n\n$request =  $url.\'api/mail.send.json\';\n\n// Generate curl request\n$session = curl_init($request);\n// Tell curl to use HTTP POST\ncurl_setopt ($session, CURLOPT_POST, true);\n// Tell curl that this is the body of the POST\ncurl_setopt ($session, CURLOPT_POSTFIELDS, $params);\n// Tell curl not to return headers, but do return the response\ncurl_setopt($session, CURLOPT_HEADER, false);\n// Tell PHP not to use SSLv3 (instead opting for TLS)\ncurl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);\ncurl_setopt($session, CURLOPT_RETURNTRANSFER, true);\n\n// obtain response\n$response = curl_exec($session);\ncurl_close($session);\n\n// print everything out\n// $modx->log(xPDO::LOG_LEVEL_ERROR, print_r($response, true));\n\nreturn true;',0,NULL,'',0,''),
	(14,0,0,'QuickEmail','Email sending and diagnostic snippet.',0,11,0,'/**\r\n * QuickEmail\r\n *\r\n * Copyright 2011-2013 Bob Ray\r\n *\r\n * @author Bob Ray\r\n * @date 1/15/11\r\n *\r\n * QuickEmail is free software; you can redistribute it and/or modify it\r\n * under the terms of the GNU General Public License as published by the Free\r\n * Software Foundation; either version 2 of the License, or (at your option) any\r\n * later version.\r\n *\r\n * QuickEmail is distributed in the hope that it will be useful, but WITHOUT ANY\r\n * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR\r\n * A PARTICULAR PURPOSE. See the GNU General Public License for more details.\r\n *\r\n * You should have received a copy of the GNU General Public License along with\r\n * QuickEmail; if not, write to the Free Software Foundation, Inc., 59 Temple\r\n * Place, Suite 330, Boston, MA 02111-1307 USA\r\n *\r\n * @package quickEmail\r\n */\r\n/**\r\n * MODX QuickEmail Snippet\r\n * @description A quick email sending and diagnostic snippet for MODX Revolution\r\n * @package quickemail\r\n * @version 1.0.4\r\n\r\n * @var $modx modX\r\n * @var $scriptProperties array\r\n *\r\n * @property string message - Message for the email body; default: `Default Message`.\r\n * @property string subject - Subject for the email message; default: `Default Subject`.\r\n * @property string to - Address the email message will be sent to; default: emailsender System Setting.\r\n * @property string toName - Value for message toName; default; emailsender System Setting.\r\n * @property string fromName - Value for message fromName; default; site_name System Setting.\r\n * @property string emailSender - Email address for from field of email; default: emailsender System Setting.\r\n * @property string replyTo - Value for replyTo field for email; default: emailsender System Setting.\r\n * @property boolean debug - Turn on debugging (still attempts to send email); default: `0`\r\n * @property boolean html - Allow HTML in message; default: `1`\r\n * @property string msgTpl - If sent, the specified chunk will be used for the message body and the &message parameter will be ignored.\r\n * @property boolean hideOutput - Stifle all output from the snippet; ignored if debug is set; default: `0`\r\n * @property string success - Message to display when send is successful\r\n * @property string failure - Message to display when send is successful\r\n * @property string errorHeader - Header for mail error message\r\n * @property string smtpErrorHeader - Header for smtp server error messages section\r\n */\r\n\r\n/* Bail if the user is not logged in to the Manager -- prevents bots\r\n   from triggering emails */\r\n\r\nif (! $modx->user->hasSessionContext(\'mgr\')) return \'Unauthorized\';\r\n/* save some typing */\r\n$sp = $scriptProperties;\r\n\r\n/* get the MODX mailer object */\r\n$modx->getService(\'mail\', \'mail.modPHPMailer\');\r\n\r\n/* set default values */\r\n$output = \'\';\r\n$debug = $modx->getOption(\'debug\',$sp,false);\r\nif (is_string($debug) && strlen($debug) > 1) {\r\n    $debug = stristr(\'no\',$debug)? false : true;\r\n}\r\n$tpl = $modx->getOption(\'msgTpl\',$scriptProperties,false);\r\n$message = $modx->getOption(\'message\',$sp,false);\r\n$message = empty($message)? \'Default Message\' : $message;\r\n$subject = $modx->getOption(\'subject\',$sp);\r\n$subject = empty($subject)? \'Default Subject\' : $subject;\r\n$to = $modx->getOption(\'to\',$sp);\r\n$to = empty($to)? $modx->getOption(\'emailsender\') : $to;\r\n$toName = $modx->getOption(\'toName\',$sp);\r\n$toName = empty($toName)? $modx->getOption(\'emailsender\') : $to;\r\n$fromName = $modx->getOption(\'fromName\',$sp);\r\n$fromName = empty($fromName)? \'QuickEmail\' : $fromName;\r\n$emailSender = $modx->getOption(\'emailSender\',$sp);\r\n$emailSender = empty($emailSender) ? $modx->getOption(\'emailsender\',null,false): $emailSender;\r\n$replyTo = $modx->getOption(\'replyTo\',$sp);\r\n$replyTo = empty($replyTo)? $modx->getOption(\'emailsender\'): $replyTo;\r\n$html = $modx->getOption(\'allowHtml\',$sp,false);\r\nif (is_string($html) && strlen($html) > 1) {\r\n    $html = stristr(\'no\',$html)? false : true;\r\n}\r\n$hideOutput = $modx->getOption(\'hideOutput\',$sp,false);\r\nif (is_string($hideOutput) && strlen($hideOutput) > 1) {\r\n    $hideOutput = stristr(\'yes\',$hideOutput)? true : false;\r\n}\r\n$failureMessage = $modx->getOption(\'failureMessage\',$sp,false);\r\n$successMessage = $modx->getOption(\'successMessage\',$sp,false);\r\n$errorHeader = $modx->getOption(\'errorHeader\',$sp,false);\r\n\r\nif (! empty ($tpl) ) {\r\n    $msg = $modx->getChunk($tpl);\r\n    if (empty($msg) && $debug) {\r\n        $output .= \'<br />Error: Cannot find Tpl chunk: \' . $tpl;\r\n    }\r\n} else {\r\n    $msg = $message;\r\n}\r\n\r\nif (! $msg) {\r\n   $msg = \'Default Message\';\r\n}\r\n\r\nif ($debug) {\r\n    $output .= \'<h3>System Settings (used if property is missing):</h3>\';\r\n    $output .= \'<b>emailsender System Setting:</b> \' .$modx->getOption(\'emailsender\',$sp);\r\n    $output .= \'<br /><b>site_name System Setting:</b> \' .$modx->getOption(\'site_name\',$sp);\r\n\r\n    $output .= \'<h3>Properties (from parameters, property set, or snippet default properties:</h3>\';\r\n    $output .= \'<b>Tpl chunk name:</b> \' . $modx->getOption(\'msgTpl\',$sp);\r\n    $output .= \'<br /><b>subject:</b> \' . $modx->getOption(\'subject\',$sp);\r\n    $output .= \'<br /><b>to:</b> \' . $modx->getOption(\'to\',$sp,\'empty\');\r\n    $output .= \'<br /><b>fromName:</b> \' . $modx->getOption(\'fromName\',$sp);\r\n    $output .= \'<br /><b>replyTo:</b> \' . $modx->getOption(\'replyTo\',$sp);\r\n    $output .= \'<br /><b>emailSender:</b> \' . $modx->getOption(\'emailSender\',$sp);\r\n    $output .= \'<br /><b>allowHtml:</b> \' . $modx->getOption(\'allowHtml\',$sp);\r\n    $output .= \'<br /><b>message:</b> \' . $modx->getOption(\'message\',$sp);\r\n\r\n\r\n    $output .= \'<h3>Final Values (actually used when sending email):</h3>\';\r\n    $output .= \'<b>subject:</b> \' .$subject;\r\n    $output .= \'<br /><b>to:</b> \' .$to;\r\n    $output .= \'<br /><b>fromName:</b> \' .$fromName;\r\n    $output .= \'<br /><b>replyTo:</b> \' .$replyTo;\r\n    $output .= \'<br /><b>emailSender:</b> \' .$emailSender;\r\n    $output .= \'<br /><b>allowHtml:</b> \' . $html;\r\n    $output .= \'<br /><b>Message Body:</b> \' . $msg;\r\n\r\n}\r\n\r\n$modx->mail->set(modMail::MAIL_BODY, $msg);\r\n$modx->mail->set(modMail::MAIL_FROM, $emailSender);\r\n$modx->mail->set(modMail::MAIL_FROM_NAME, $fromName);\r\n$modx->mail->set(modMail::MAIL_SENDER, $emailSender);\r\n$modx->mail->set(modMail::MAIL_SUBJECT, $subject);\r\n$modx->mail->address(\'to\', $to, $toName);\r\n$modx->mail->address(\'reply-to\', $replyTo);\r\n$modx->mail->setHTML($html);\r\nif ($debug) {\r\n    ob_start();\r\n    echo \'<pre>\';\r\n\r\n    if ($modx->getOption(\'mail_use_smtp\') ) {\r\n        $modx->mail->mailer->SMTPDebug = 2;\r\n    }\r\n}\r\n$sent = $modx->mail->send();\r\n\r\nif ($debug) {\r\n    echo \'</pre>\';\r\n    $ob = ob_get_contents();\r\n    ob_end_clean();\r\n}\r\n\r\n$modx->mail->reset();\r\n\r\nif ($sent) {\r\n    $output .= $successMessage;\r\n    if ($debug) {\r\n       $output .= $ob;\r\n    }\r\n} else  {\r\n\r\n    $output .= $failureMessage;\r\n    $output .= $errorHeader;\r\n    $output .= $modx->mail->mailer->ErrorInfo;\r\n    if (!empty($ob)) {\r\n        $output .= $smtpErrorHeader;\r\n    }\r\n    $output .= $ob;\r\n\r\n\r\n}\r\n$output = $hideOutput && (! $debug )? \'\' : $output . \"<br /><br />\";\r\nreturn $output;',0,'a:15:{s:9:\"allowHtml\";a:7:{s:4:\"name\";s:9:\"allowHtml\";s:4:\"desc\";s:25:\"prop_quickemail.html_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";b:1;s:7:\"lexicon\";s:21:\"quickemail:properties\";s:4:\"area\";s:0:\"\";}s:5:\"debug\";a:7:{s:4:\"name\";s:5:\"debug\";s:4:\"desc\";s:26:\"prop_quickemail.debug_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:21:\"quickemail:properties\";s:4:\"area\";s:0:\"\";}s:11:\"emailSender\";a:7:{s:4:\"name\";s:11:\"emailSender\";s:4:\"desc\";s:32:\"prop_quickemail.emailSender_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:21:\"quickemail:properties\";s:4:\"area\";s:0:\"\";}s:11:\"errorHeader\";a:7:{s:4:\"name\";s:11:\"errorHeader\";s:4:\"desc\";s:32:\"prop_quickemail.errorHeader_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:26:\"<b>Mailer error info: </b>\";s:7:\"lexicon\";s:21:\"quickemail:properties\";s:4:\"area\";s:0:\"\";}s:14:\"failureMessage\";a:7:{s:4:\"name\";s:14:\"failureMessage\";s:4:\"desc\";s:28:\"prop_quickemail.failure_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:40:\"<h3 style = \"color:red\">Send Failed</h3>\";s:7:\"lexicon\";s:21:\"quickemail:properties\";s:4:\"area\";s:0:\"\";}s:8:\"fromName\";a:7:{s:4:\"name\";s:8:\"fromName\";s:4:\"desc\";s:29:\"prop_quickemail.fromName_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:21:\"quickemail:properties\";s:4:\"area\";s:0:\"\";}s:10:\"hideOutput\";a:7:{s:4:\"name\";s:10:\"hideOutput\";s:4:\"desc\";s:31:\"prop_quickemail.hideOutput_desc\";s:4:\"type\";s:13:\"combo-boolean\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:21:\"quickemail:properties\";s:4:\"area\";s:0:\"\";}s:7:\"message\";a:7:{s:4:\"name\";s:7:\"message\";s:4:\"desc\";s:28:\"prop_quickemail.message_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:21:\"quickemail:properties\";s:4:\"area\";s:0:\"\";}s:6:\"msgTpl\";a:7:{s:4:\"name\";s:6:\"msgTpl\";s:4:\"desc\";s:27:\"prop_quickemail.msgTpl_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:20:\"quickedit:properties\";s:4:\"area\";s:0:\"\";}s:7:\"replyTo\";a:7:{s:4:\"name\";s:7:\"replyTo\";s:4:\"desc\";s:28:\"prop_quickemail.replyTo_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:21:\"quickemail:properties\";s:4:\"area\";s:0:\"\";}s:15:\"smtpErrorHeader\";a:7:{s:4:\"name\";s:15:\"smtpErrorHeader\";s:4:\"desc\";s:36:\"prop_quickemail.smtpErrorHeader_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:34:\"<h3>Server Debug Information:</h3>\";s:7:\"lexicon\";s:21:\"quickemail:properties\";s:4:\"area\";s:0:\"\";}s:7:\"subject\";a:7:{s:4:\"name\";s:7:\"subject\";s:4:\"desc\";s:28:\"prop_quickemail.subject_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:21:\"quickemail:properties\";s:4:\"area\";s:0:\"\";}s:14:\"successMessage\";a:7:{s:4:\"name\";s:14:\"successMessage\";s:4:\"desc\";s:28:\"prop_quickemail.success_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:55:\"<h3 style = \"color:green\">Send reported successful</h3>\";s:7:\"lexicon\";s:21:\"quickemail:properties\";s:4:\"area\";s:0:\"\";}s:2:\"to\";a:7:{s:4:\"name\";s:2:\"to\";s:4:\"desc\";s:23:\"prop_quickemail.to_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:21:\"quickemail:properties\";s:4:\"area\";s:0:\"\";}s:6:\"toName\";a:7:{s:4:\"name\";s:6:\"toName\";s:4:\"desc\";s:27:\"prop_quickemail.toName_desc\";s:4:\"type\";s:9:\"textfield\";s:7:\"options\";s:0:\"\";s:5:\"value\";s:0:\"\";s:7:\"lexicon\";s:21:\"quickemail:properties\";s:4:\"area\";s:0:\"\";}}','',0,'');

/*!40000 ALTER TABLE `modx_site_snippets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_templates`;

CREATE TABLE `modx_site_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` int(10) unsigned NOT NULL DEFAULT '0',
  `property_preprocess` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `templatename` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT 'Template',
  `editor_type` int(11) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  `icon` varchar(255) NOT NULL DEFAULT '',
  `template_type` int(11) NOT NULL DEFAULT '0',
  `content` mediumtext NOT NULL,
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `properties` text,
  `static` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `static_file` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `templatename` (`templatename`),
  KEY `category` (`category`),
  KEY `locked` (`locked`),
  KEY `static` (`static`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_templates` WRITE;
/*!40000 ALTER TABLE `modx_site_templates` DISABLE KEYS */;

INSERT INTO `modx_site_templates` (`id`, `source`, `property_preprocess`, `templatename`, `description`, `editor_type`, `category`, `icon`, `template_type`, `content`, `locked`, `properties`, `static`, `static_file`)
VALUES
	(1,0,0,'BaseTemplate','Template',0,0,'',0,'<html>\n<head>\n<title>[[++site_name]] - [[*pagetitle]]</title>\n<base href=\"[[++site_url]]\" />\n</head>\n<body>\n[[*content]]\n</body>\n</html>',0,NULL,0,''),
	(2,1,0,'Homepage','',0,0,'',0,'[[$doc_head]]\n  <body>\n    [[$navigation]]\n    [[$header]]\n    <section class=\"home-main\">\n        <div class=\"row-fluid wrap\">\n            <div class=\"col3 red\">\n                <div class=\"headline\">\n                    <img src=\"[[*column-1-image]]\" alt=\"\">\n                    <h3>[[*column-1-headline]]</h3>\n                </div>\n                [[*column-1-content]]\n                <a href=\"[[~[[*column-1-link]]]]\" class=\"button\">[[*column-1-link-text]]</a>\n            </div>\n            <div class=\"col3 red\">\n                <div class=\"headline\">\n                    <img src=\"[[*column-2-image]]\" alt=\"\">\n                    <h3>[[*column-2-headline]]</h3>\n                </div>\n                [[*column-2-content]]\n                <a href=\"[[~[[*column-2-link]]]]\" class=\"button\">[[*column-2-link-text]]</a>\n            </div>\n            <div class=\"col3 red\">\n                <div class=\"headline\">\n                    <img src=\"[[*column-3-image]]\" alt=\"\">\n                    <h3>[[*column-3-headline]]</h3>\n                </div>\n                [[*column-3-content]]\n                <a href=\"[[~[[*column-3-link]]]]\" class=\"button\">[[*column-3-link-text]]</a>\n            </div>\n        </div>\n    </section>\n    [[$footer]]\n    [[$doc_footer]]\n',0,'a:0:{}',1,'assets/templates/homepage.html'),
	(6,1,0,'Sidebar Inner','',0,0,'',0,'[[$doc_head]]\n<body class=\"interior\">\n    [[$navigation]]\n    [[$header-interior]]\n    <section class=\"content\">\n        <div class=\"row-fluid wrap\">\n            <div class=\"span3 sidebar\">\n                \n                [[Wayfinder? \n                &level=`0`\n                &startId=`[[*parent]]`\n                &innerTpl=`nav_inner`\n                &outerTpl=`nav_outer`\n                &level=`1`\n                &sortBy=`menuindex`\n                ]]\n            </div>\n            <div class=\"span9\">\n                [[*content]]\n            </div>\n        </div>\n    </section>\n    [[$footer]]\n    [[$doc_footer]]',0,'a:0:{}',1,'assets/templates/sidebar-inner.html'),
	(3,1,0,'Interior','',0,0,'',0,'[[$doc_head]]\n<body class=\"interior\">\n    [[$navigation]]\n    [[$header-interior]]\n    <section class=\"content\">\n        <div class=\"row-fluid wrap\">\n            [[*content]]\n        </div>\n    </section>\n    [[$footer]]\n    [[$doc_footer]]\n',0,'a:0:{}',1,'assets/templates/interior.html'),
	(4,1,0,'Sidebar','',0,0,'',0,'[[$doc_head]]\n<body class=\"interior\">\n    [[$navigation]]\n    [[$header-interior]]\n    <section class=\"content\">\n        <div class=\"row-fluid wrap\">\n            <div class=\"span3 sidebar\">\n               \n                [[Wayfinder? \n                &level=`0`\n                &startId=`[[*id]]`\n                &innerTpl=`nav_inner`\n                &outerTpl=`nav_outer`\n                &level=`1`\n                &sortBy=`menuindex`\n                ]]\n            </div>\n            <div class=\"span9\">\n                [[*content]]\n            </div>\n        </div>\n    </section>\n    [[$footer]]\n    [[$doc_footer]]\n',0,'a:0:{}',1,'assets/templates/services.html'),
	(5,1,0,'Contact','',0,0,'',0,'[[$doc_head]]\n<body class=\"interior\">\n    [[$navigation]]\n    [[$header-interior]]\n    <section class=\"content\">\n        <div class=\"row-fluid wrap\">\n        	<div class=\"span5\">\n        		[[*content]]\n        	</div>\n        	<div class=\"span7\">\n        		[[$contact_form]]\n        	</div>\n            \n        </div>\n    </section>\n    [[$footer]]\n    [[$doc_footer]]\n',0,'a:0:{}',1,'assets/templates/contact.html'),
	(7,1,0,'Slides','Use for slider slides',0,0,'',0,'',0,'a:0:{}',0,''),
	(8,1,0,'Sidebar - form','',0,0,'',0,'[[$doc_head]]\n<body class=\"interior\">\n    [[$navigation]]\n    [[$header-interior]]\n    <section class=\"content\">\n        <div class=\"row-fluid wrap\">\n            <div class=\"span3 sidebar\">\n               \n                [[Wayfinder? \n                &level=`0`\n                &startId=`[[*id]]`\n                &innerTpl=`nav_inner`\n                &outerTpl=`nav_outer`\n                &level=`1`\n                &sortBy=`menuindex`\n                ]]\n            </div>\n            <div class=\"span9\">\n                [[*content]]\n                <hr>\n                [[*contact-form-text]]\n                [[$contact_form]]\n            </div>\n        </div>\n    </section>\n    [[$footer]]\n    [[$doc_footer]]\n',0,'a:0:{}',1,'assets/templates/services2.html');

/*!40000 ALTER TABLE `modx_site_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_tmplvar_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_tmplvar_access`;

CREATE TABLE `modx_site_tmplvar_access` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tmplvarid` int(10) NOT NULL DEFAULT '0',
  `documentgroup` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_site_tmplvar_contentvalues
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_tmplvar_contentvalues`;

CREATE TABLE `modx_site_tmplvar_contentvalues` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tmplvarid` int(10) NOT NULL DEFAULT '0',
  `contentid` int(10) NOT NULL DEFAULT '0',
  `value` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tmplvarid` (`tmplvarid`),
  KEY `contentid` (`contentid`),
  KEY `tv_cnt` (`tmplvarid`,`contentid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_tmplvar_contentvalues` WRITE;
/*!40000 ALTER TABLE `modx_site_tmplvar_contentvalues` DISABLE KEYS */;

INSERT INTO `modx_site_tmplvar_contentvalues` (`id`, `tmplvarid`, `contentid`, `value`)
VALUES
	(1,3,1,'9'),
	(2,4,1,'<h2>STAY COOL IN THE HEAT OF SUMMER</h2>'),
	(3,5,1,'<p>Hot and humid, that is a typical Iowa summer. We can help keep the heat under control this year with regular maintenance on your air conditioner.</p>'),
	(63,8,22,'resources/img/newconstruction_hero.jpg'),
	(64,8,23,'resources/img/commercial_hero.jpg'),
	(65,8,21,'resources/img/replacements_hero.jpg'),
	(5,7,1,'Learn More'),
	(6,6,7,'<p><span>Have a question for us or would like to set up a time for a free estimate? Please call or fill out the quick form below and we will be in touch soon.</span></p>'),
	(61,8,24,'resources/img/rebates_promo_hero.jpg'),
	(8,6,2,'<p><span>Heartland Heating and Cooling has a rich history of helping customers.</span></p>'),
	(14,6,13,'<p>Take maintenance off your mind and let our tune-up specialist take care of you!</p>'),
	(24,6,15,'<p>Not only can we install and maintain your units, you can purchase them right from us as well.</p>'),
	(13,2,2,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(15,9,1,'New Construction'),
	(16,10,1,'<p>Building a new property? Get in touch with us to learn how we can help with your HVAC needs.</p>'),
	(29,7,25,'Learn More'),
	(17,11,1,'resources/img/house_icon.png'),
	(18,12,1,'21'),
	(19,16,1,'resources/img/sun_snow_icon_gray.png'),
	(20,17,1,'Replacements'),
	(21,18,1,'<p>Is your heating or cooling unit old and needing replacement? Take advantage of our products and services to replace it.</p>'),
	(26,3,24,'12'),
	(27,7,24,'Learn More'),
	(28,3,25,'21'),
	(22,14,1,'22'),
	(30,21,1,'resources/img/tools_icon_gray.png'),
	(31,20,1,'Tune-up'),
	(32,19,1,'<p>Take maintenance off your mind and let our tune-up specialist take care of you!</p>'),
	(33,22,1,'13'),
	(34,1,1,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(35,1,2,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa offering heating and cooling services for residential, commercial and new construction properties.'),
	(36,1,13,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(37,1,3,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(38,1,8,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(39,1,9,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(40,1,10,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(41,1,14,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(42,1,22,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(43,1,23,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(44,1,21,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(45,1,26,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(46,1,6,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa with special financing to make for cheap and affordable service options.'),
	(47,1,5,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(48,1,7,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(49,2,1,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(50,2,13,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(51,2,3,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(52,2,8,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(53,2,9,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(54,2,10,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(55,2,14,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(56,2,22,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(57,2,23,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(58,2,21,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(59,2,26,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(60,2,7,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(62,8,12,'resources/img/rebates_promo_hero.jpg'),
	(66,8,13,'resources/img/comfort_team_hero.jpg'),
	(67,8,25,'resources/img/replacements_promo_hero.jpg'),
	(68,1,27,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(69,2,27,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(70,1,28,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(71,2,28,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(72,1,29,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(73,2,29,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(74,1,30,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(75,2,30,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(76,8,2,'resources/img/about_hero.jpg'),
	(77,8,3,'resources/img/product_info_hero.jpg'),
	(78,8,8,'resources/img/heating_hero.jpg'),
	(79,8,27,'resources/img/heating_hero.jpg'),
	(80,8,10,'resources/img/geothermal_hero.jpg'),
	(81,8,29,'resources/img/geothermal_hero.jpg'),
	(82,8,26,'resources/img/thermostats_hero.jpg'),
	(83,8,6,'resources/img/financing_hero.jpg'),
	(84,8,5,'resources/img/employment_hero.jpg'),
	(85,8,7,'resources/img/contact_hero.jpg'),
	(86,1,31,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(87,2,31,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(88,8,31,'resources/img/thermostats_hero.jpg'),
	(89,8,32,'resources/img/commercial_hero.jpg'),
	(90,1,32,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(91,2,32,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(92,1,33,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(93,2,33,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(94,8,33,'resources/img/heating_hero.jpg'),
	(95,1,34,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(96,2,34,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(97,1,35,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(98,2,35,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(99,8,35,'resources/img/geothermal_hero.jpg'),
	(100,1,36,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(101,2,36,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(102,1,37,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(103,2,37,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(104,8,37,'resources/img/thermostats_hero.jpg'),
	(105,8,38,'resources/img/commercial_hero.jpg'),
	(106,1,38,'Heartland Heating & Cooling is a full-service heating and cooling service company in Des Moines, Iowa repairing & replacing heating and cooling services for residential, commercial and new construction properties.'),
	(107,2,38,'heating, cooling, hvac, des moines, service, commercial, residential'),
	(108,8,9,'resources/img/cooling_hero.jpg'),
	(109,8,28,'resources/img/cooling_hero.jpg'),
	(110,8,34,'resources/img/cooling_hero.jpg'),
	(111,8,14,'resources/img/iaq_hero.jpg'),
	(112,8,30,'resources/img/iaq_hero.jpg'),
	(113,8,36,'resources/img/iaq_hero.jpg'),
	(114,6,39,'<p>This page contains information on how to login to your site, how to edit content and how to do some basic controls. </p>'),
	(115,8,39,'resources/img/contact_hero.jpg'),
	(116,3,40,'2'),
	(117,7,40,'learn about us'),
	(118,8,40,'resources/img/geothermal_hero.jpg');

/*!40000 ALTER TABLE `modx_site_tmplvar_contentvalues` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_tmplvar_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_tmplvar_templates`;

CREATE TABLE `modx_site_tmplvar_templates` (
  `tmplvarid` int(10) NOT NULL DEFAULT '0',
  `templateid` int(11) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tmplvarid`,`templateid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_tmplvar_templates` WRITE;
/*!40000 ALTER TABLE `modx_site_tmplvar_templates` DISABLE KEYS */;

INSERT INTO `modx_site_tmplvar_templates` (`tmplvarid`, `templateid`, `rank`)
VALUES
	(22,2,14),
	(21,2,11),
	(20,2,12),
	(6,3,0),
	(6,4,0),
	(1,2,0),
	(1,3,0),
	(1,4,0),
	(2,2,0),
	(2,3,0),
	(2,4,0),
	(19,2,13),
	(8,3,0),
	(8,4,0),
	(1,5,0),
	(2,5,0),
	(6,5,0),
	(8,5,0),
	(9,2,2),
	(10,2,3),
	(11,2,1),
	(12,2,9),
	(13,2,5),
	(14,2,4),
	(15,2,10),
	(16,2,6),
	(17,2,7),
	(18,2,8),
	(6,6,0),
	(1,6,0),
	(2,6,0),
	(8,6,0),
	(3,7,0),
	(7,7,0),
	(8,7,0),
	(23,2,15),
	(24,8,0),
	(8,8,0),
	(6,8,0),
	(1,8,0),
	(2,8,0);

/*!40000 ALTER TABLE `modx_site_tmplvar_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_site_tmplvars
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_site_tmplvars`;

CREATE TABLE `modx_site_tmplvars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source` int(10) unsigned NOT NULL DEFAULT '0',
  `property_preprocess` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `type` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `caption` varchar(80) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `editor_type` int(11) NOT NULL DEFAULT '0',
  `category` int(11) NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `elements` text,
  `rank` int(11) NOT NULL DEFAULT '0',
  `display` varchar(20) NOT NULL DEFAULT '',
  `default_text` mediumtext,
  `properties` text,
  `input_properties` text,
  `output_properties` text,
  `static` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `static_file` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category` (`category`),
  KEY `locked` (`locked`),
  KEY `rank` (`rank`),
  KEY `static` (`static`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_site_tmplvars` WRITE;
/*!40000 ALTER TABLE `modx_site_tmplvars` DISABLE KEYS */;

INSERT INTO `modx_site_tmplvars` (`id`, `source`, `property_preprocess`, `type`, `name`, `caption`, `description`, `editor_type`, `category`, `locked`, `elements`, `rank`, `display`, `default_text`, `properties`, `input_properties`, `output_properties`, `static`, `static_file`)
VALUES
	(1,1,0,'textarea','seo-description','Page Descprition','',0,6,0,'',0,'default','','a:0:{}','a:1:{s:10:\"allowBlank\";s:4:\"true\";}','a:0:{}',0,''),
	(2,1,0,'text','seo-keyword','SEO Keywords','Comma separated list',0,6,0,'',0,'default','','a:0:{}','a:3:{s:10:\"allowBlank\";s:4:\"true\";s:9:\"maxLength\";s:0:\"\";s:9:\"minLength\";s:0:\"\";}','a:0:{}',0,''),
	(3,1,0,'resourcelist','promo-link','Promo Link','Link to current promotion',0,8,0,'',0,'default','','a:0:{}','a:8:{s:10:\"allowBlank\";s:1:\"1\";s:8:\"showNone\";s:1:\"1\";s:7:\"parents\";s:0:\"\";s:5:\"depth\";s:2:\"10\";s:13:\"includeParent\";s:1:\"1\";s:19:\"limitRelatedContext\";s:1:\"0\";s:5:\"where\";s:0:\"\";s:5:\"limit\";s:1:\"0\";}','a:0:{}',0,''),
	(4,1,0,'richtext','promo-headline','Promotion Headline','Headline for promotion (format as h2)',0,8,0,'',0,'default','','a:0:{}','a:1:{s:10:\"allowBlank\";s:4:\"true\";}','a:0:{}',0,''),
	(5,1,0,'richtext','promo-text','Promotion description','Brief description of promotion',0,8,0,'',0,'default','','a:0:{}','a:0:{}','a:0:{}',0,''),
	(6,1,0,'richtext','intro-text','Intro text','Brief description of page contents',0,8,0,'',0,'default','','a:0:{}','a:1:{s:10:\"allowBlank\";s:4:\"true\";}','a:0:{}',1,''),
	(7,1,0,'text','promo-link-text','','',0,8,0,'',0,'default','','a:0:{}','a:3:{s:10:\"allowBlank\";s:4:\"true\";s:9:\"maxLength\";s:0:\"\";s:9:\"minLength\";s:0:\"\";}','a:0:{}',0,''),
	(8,1,0,'image','background-image','Header image for page','Upload or link to header image',0,8,0,'',0,'default','/resources/img/hero.jpg','a:0:{}','a:3:{s:10:\"allowBlank\";s:4:\"true\";s:9:\"maxLength\";s:0:\"\";s:9:\"minLength\";s:0:\"\";}','a:0:{}',0,''),
	(9,1,0,'text','column-1-headline','','',0,9,0,'',0,'default','','a:0:{}','a:3:{s:10:\"allowBlank\";s:4:\"true\";s:9:\"maxLength\";s:0:\"\";s:9:\"minLength\";s:0:\"\";}','a:0:{}',0,''),
	(10,1,0,'richtext','column-1-content','','',0,9,0,'',0,'default','','a:0:{}','a:3:{s:10:\"allowBlank\";s:4:\"true\";s:9:\"maxLength\";s:0:\"\";s:9:\"minLength\";s:0:\"\";}','a:0:{}',0,''),
	(11,1,0,'image','column-1-image','','',0,9,0,'',0,'default','','a:0:{}','a:8:{s:10:\"allowBlank\";s:1:\"1\";s:8:\"showNone\";s:1:\"1\";s:7:\"parents\";s:0:\"\";s:5:\"depth\";s:2:\"10\";s:13:\"includeParent\";s:1:\"1\";s:19:\"limitRelatedContext\";s:1:\"0\";s:5:\"where\";s:0:\"\";s:5:\"limit\";s:1:\"0\";}','a:0:{}',0,''),
	(12,1,0,'resourcelist','column-2-link','','',0,9,0,'',0,'default','','a:0:{}','a:8:{s:10:\"allowBlank\";s:1:\"1\";s:8:\"showNone\";s:1:\"1\";s:7:\"parents\";s:0:\"\";s:5:\"depth\";s:2:\"10\";s:13:\"includeParent\";s:1:\"1\";s:19:\"limitRelatedContext\";s:1:\"0\";s:5:\"where\";s:0:\"\";s:5:\"limit\";s:1:\"0\";}','a:0:{}',0,''),
	(13,1,0,'text','column-1-link-text','','',0,9,0,'',0,'default','Learn More','a:0:{}','a:3:{s:10:\"allowBlank\";s:4:\"true\";s:9:\"maxLength\";s:0:\"\";s:9:\"minLength\";s:0:\"\";}','a:0:{}',0,''),
	(14,1,0,'resourcelist','column-1-link','','',0,9,0,'',0,'default','','a:0:{}','a:8:{s:10:\"allowBlank\";s:1:\"1\";s:8:\"showNone\";s:1:\"1\";s:7:\"parents\";s:0:\"\";s:5:\"depth\";s:2:\"10\";s:13:\"includeParent\";s:1:\"1\";s:19:\"limitRelatedContext\";s:1:\"0\";s:5:\"where\";s:0:\"\";s:5:\"limit\";s:1:\"0\";}','a:0:{}',0,''),
	(15,1,0,'text','column-2-link-text','','',0,9,0,'',0,'default','Learn More','a:0:{}','a:3:{s:10:\"allowBlank\";s:4:\"true\";s:9:\"maxLength\";s:0:\"\";s:9:\"minLength\";s:0:\"\";}','a:0:{}',0,''),
	(16,1,0,'image','column-2-image','','',0,9,0,'',0,'default','','a:0:{}','a:8:{s:10:\"allowBlank\";s:1:\"1\";s:8:\"showNone\";s:1:\"1\";s:7:\"parents\";s:0:\"\";s:5:\"depth\";s:2:\"10\";s:13:\"includeParent\";s:1:\"1\";s:19:\"limitRelatedContext\";s:1:\"0\";s:5:\"where\";s:0:\"\";s:5:\"limit\";s:1:\"0\";}','a:0:{}',0,''),
	(17,1,0,'text','column-2-headline','','',0,9,0,'',0,'default','','a:0:{}','a:3:{s:10:\"allowBlank\";s:4:\"true\";s:9:\"maxLength\";s:0:\"\";s:9:\"minLength\";s:0:\"\";}','a:0:{}',0,''),
	(18,1,0,'richtext','column-2-content','','',0,9,0,'',0,'default','','a:0:{}','a:3:{s:10:\"allowBlank\";s:4:\"true\";s:9:\"maxLength\";s:0:\"\";s:9:\"minLength\";s:0:\"\";}','a:0:{}',0,''),
	(19,1,0,'richtext','column-3-content','','',0,9,0,'',0,'default','','a:0:{}','a:3:{s:10:\"allowBlank\";s:4:\"true\";s:9:\"maxLength\";s:0:\"\";s:9:\"minLength\";s:0:\"\";}','a:0:{}',0,''),
	(20,1,0,'text','column-3-headline','','',0,9,0,'',0,'default','','a:0:{}','a:3:{s:10:\"allowBlank\";s:4:\"true\";s:9:\"maxLength\";s:0:\"\";s:9:\"minLength\";s:0:\"\";}','a:0:{}',0,''),
	(21,1,0,'image','column-3-image','','',0,9,0,'',0,'default','','a:0:{}','a:8:{s:10:\"allowBlank\";s:1:\"1\";s:8:\"showNone\";s:1:\"1\";s:7:\"parents\";s:0:\"\";s:5:\"depth\";s:2:\"10\";s:13:\"includeParent\";s:1:\"1\";s:19:\"limitRelatedContext\";s:1:\"0\";s:5:\"where\";s:0:\"\";s:5:\"limit\";s:1:\"0\";}','a:0:{}',0,''),
	(22,1,0,'resourcelist','column-3-link','','',0,9,0,'',0,'default','','a:0:{}','a:8:{s:10:\"allowBlank\";s:1:\"1\";s:8:\"showNone\";s:1:\"1\";s:7:\"parents\";s:0:\"\";s:5:\"depth\";s:2:\"10\";s:13:\"includeParent\";s:1:\"1\";s:19:\"limitRelatedContext\";s:1:\"0\";s:5:\"where\";s:0:\"\";s:5:\"limit\";s:1:\"0\";}','a:0:{}',0,''),
	(23,1,0,'text','column-3-link-text','','',0,9,0,'',0,'default','Learn More','a:0:{}','a:3:{s:10:\"allowBlank\";s:4:\"true\";s:9:\"maxLength\";s:0:\"\";s:9:\"minLength\";s:0:\"\";}','a:0:{}',0,''),
	(24,1,0,'richtext','contact-form-text','Contact Form Text','Briefly describe the action the user is supposed to take.',0,0,0,'',0,'default','','a:0:{}','a:0:{}','a:0:{}',0,'');

/*!40000 ALTER TABLE `modx_site_tmplvars` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_system_eventnames
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_system_eventnames`;

CREATE TABLE `modx_system_eventnames` (
  `name` varchar(50) NOT NULL,
  `service` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `groupname` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_system_eventnames` WRITE;
/*!40000 ALTER TABLE `modx_system_eventnames` DISABLE KEYS */;

INSERT INTO `modx_system_eventnames` (`name`, `service`, `groupname`)
VALUES
	('OnPluginEventBeforeSave',1,'Plugin Events'),
	('OnPluginEventSave',1,'Plugin Events'),
	('OnPluginEventBeforeRemove',1,'Plugin Events'),
	('OnPluginEventRemove',1,'Plugin Events'),
	('OnResourceGroupSave',1,'Security'),
	('OnResourceGroupBeforeSave',1,'Security'),
	('OnResourceGroupRemove',1,'Security'),
	('OnResourceGroupBeforeRemove',1,'Security'),
	('OnSnippetBeforeSave',1,'Snippets'),
	('OnSnippetSave',1,'Snippets'),
	('OnSnippetBeforeRemove',1,'Snippets'),
	('OnSnippetRemove',1,'Snippets'),
	('OnSnipFormPrerender',1,'Snippets'),
	('OnSnipFormRender',1,'Snippets'),
	('OnBeforeSnipFormSave',1,'Snippets'),
	('OnSnipFormSave',1,'Snippets'),
	('OnBeforeSnipFormDelete',1,'Snippets'),
	('OnSnipFormDelete',1,'Snippets'),
	('OnTemplateBeforeSave',1,'Templates'),
	('OnTemplateSave',1,'Templates'),
	('OnTemplateBeforeRemove',1,'Templates'),
	('OnTemplateRemove',1,'Templates'),
	('OnTempFormPrerender',1,'Templates'),
	('OnTempFormRender',1,'Templates'),
	('OnBeforeTempFormSave',1,'Templates'),
	('OnTempFormSave',1,'Templates'),
	('OnBeforeTempFormDelete',1,'Templates'),
	('OnTempFormDelete',1,'Templates'),
	('OnTemplateVarBeforeSave',1,'Template Variables'),
	('OnTemplateVarSave',1,'Template Variables'),
	('OnTemplateVarBeforeRemove',1,'Template Variables'),
	('OnTemplateVarRemove',1,'Template Variables'),
	('OnTVFormPrerender',1,'Template Variables'),
	('OnTVFormRender',1,'Template Variables'),
	('OnBeforeTVFormSave',1,'Template Variables'),
	('OnTVFormSave',1,'Template Variables'),
	('OnBeforeTVFormDelete',1,'Template Variables'),
	('OnTVFormDelete',1,'Template Variables'),
	('OnTVInputRenderList',1,'Template Variables'),
	('OnTVInputPropertiesList',1,'Template Variables'),
	('OnTVOutputRenderList',1,'Template Variables'),
	('OnTVOutputRenderPropertiesList',1,'Template Variables'),
	('OnUserGroupBeforeSave',1,'User Groups'),
	('OnUserGroupSave',1,'User Groups'),
	('OnUserGroupBeforeRemove',1,'User Groups'),
	('OnUserGroupRemove',1,'User Groups'),
	('OnBeforeUserGroupFormSave',1,'User Groups'),
	('OnUserGroupFormSave',1,'User Groups'),
	('OnBeforeUserGroupFormRemove',1,'User Groups'),
	('OnDocFormPrerender',1,'Resources'),
	('OnDocFormRender',1,'Resources'),
	('OnBeforeDocFormSave',1,'Resources'),
	('OnDocFormSave',1,'Resources'),
	('OnBeforeDocFormDelete',1,'Resources'),
	('OnDocFormDelete',1,'Resources'),
	('OnDocPublished',5,'Resources'),
	('OnDocUnPublished',5,'Resources'),
	('OnBeforeEmptyTrash',1,'Resources'),
	('OnEmptyTrash',1,'Resources'),
	('OnResourceTVFormPrerender',1,'Resources'),
	('OnResourceTVFormRender',1,'Resources'),
	('OnResourceAutoPublish',1,'Resources'),
	('OnResourceDelete',1,'Resources'),
	('OnResourceUndelete',1,'Resources'),
	('OnResourceBeforeSort',1,'Resources'),
	('OnResourceSort',1,'Resources'),
	('OnResourceDuplicate',1,'Resources'),
	('OnResourceToolbarLoad',1,'Resources'),
	('OnResourceRemoveFromResourceGroup',1,'Resources'),
	('OnResourceAddToResourceGroup',1,'Resources'),
	('OnRichTextEditorRegister',1,'RichText Editor'),
	('OnRichTextEditorInit',1,'RichText Editor'),
	('OnRichTextBrowserInit',1,'RichText Editor'),
	('OnWebLogin',3,'Security'),
	('OnBeforeWebLogout',3,'Security'),
	('OnWebLogout',3,'Security'),
	('OnManagerLogin',2,'Security'),
	('OnBeforeManagerLogout',2,'Security'),
	('OnManagerLogout',2,'Security'),
	('OnBeforeWebLogin',3,'Security'),
	('OnWebAuthentication',3,'Security'),
	('OnBeforeManagerLogin',2,'Security'),
	('OnManagerAuthentication',2,'Security'),
	('OnManagerLoginFormRender',2,'Security'),
	('OnManagerLoginFormPrerender',2,'Security'),
	('OnPageUnauthorized',1,'Security'),
	('OnUserFormPrerender',1,'Users'),
	('OnUserFormRender',1,'Users'),
	('OnBeforeUserFormSave',1,'Users'),
	('OnUserFormSave',1,'Users'),
	('OnBeforeUserFormDelete',1,'Users'),
	('OnUserFormDelete',1,'Users'),
	('OnUserNotFound',1,'Users'),
	('OnBeforeUserActivate',1,'Users'),
	('OnUserActivate',1,'Users'),
	('OnBeforeUserDeactivate',1,'Users'),
	('OnUserDeactivate',1,'Users'),
	('OnBeforeUserDuplicate',1,'Users'),
	('OnUserDuplicate',1,'Users'),
	('OnUserChangePassword',1,'Users'),
	('OnUserBeforeRemove',1,'Users'),
	('OnUserBeforeSave',1,'Users'),
	('OnUserSave',1,'Users'),
	('OnUserRemove',1,'Users'),
	('OnUserBeforeAddToGroup',1,'User Groups'),
	('OnUserAddToGroup',1,'User Groups'),
	('OnUserBeforeRemoveFromGroup',1,'User Groups'),
	('OnUserRemoveFromGroup',1,'User Groups'),
	('OnWebPagePrerender',5,'System'),
	('OnBeforeCacheUpdate',4,'System'),
	('OnCacheUpdate',4,'System'),
	('OnLoadWebPageCache',4,'System'),
	('OnBeforeSaveWebPageCache',4,'System'),
	('OnSiteRefresh',1,'System'),
	('OnFileManagerDirCreate',1,'System'),
	('OnFileManagerDirRemove',1,'System'),
	('OnFileManagerDirRename',1,'System'),
	('OnFileManagerFileRename',1,'System'),
	('OnFileManagerFileRemove',1,'System'),
	('OnFileManagerFileUpdate',1,'System'),
	('OnFileManagerFileCreate',1,'System'),
	('OnFileManagerBeforeUpload',1,'System'),
	('OnFileManagerUpload',1,'System'),
	('OnFileManagerMoveObject',1,'System'),
	('OnFileCreateFormPrerender',1,'System'),
	('OnFileEditFormPrerender',1,'System'),
	('OnManagerPageInit',2,'System'),
	('OnManagerPageBeforeRender',2,'System'),
	('OnManagerPageAfterRender',2,'System'),
	('OnWebPageInit',5,'System'),
	('OnLoadWebDocument',5,'System'),
	('OnParseDocument',5,'System'),
	('OnWebPageComplete',5,'System'),
	('OnBeforeManagerPageInit',2,'System'),
	('OnPageNotFound',1,'System'),
	('OnHandleRequest',5,'System'),
	('OnMODXInit',5,'System'),
	('OnElementNotFound',1,'System'),
	('OnSiteSettingsRender',1,'Settings'),
	('OnInitCulture',1,'Internationalization'),
	('OnCategorySave',1,'Categories'),
	('OnCategoryBeforeSave',1,'Categories'),
	('OnCategoryRemove',1,'Categories'),
	('OnCategoryBeforeRemove',1,'Categories'),
	('OnChunkSave',1,'Chunks'),
	('OnChunkBeforeSave',1,'Chunks'),
	('OnChunkRemove',1,'Chunks'),
	('OnChunkBeforeRemove',1,'Chunks'),
	('OnChunkFormPrerender',1,'Chunks'),
	('OnChunkFormRender',1,'Chunks'),
	('OnBeforeChunkFormSave',1,'Chunks'),
	('OnChunkFormSave',1,'Chunks'),
	('OnBeforeChunkFormDelete',1,'Chunks'),
	('OnChunkFormDelete',1,'Chunks'),
	('OnContextSave',1,'Contexts'),
	('OnContextBeforeSave',1,'Contexts'),
	('OnContextRemove',1,'Contexts'),
	('OnContextBeforeRemove',1,'Contexts'),
	('OnContextFormPrerender',2,'Contexts'),
	('OnContextFormRender',2,'Contexts'),
	('OnPluginSave',1,'Plugins'),
	('OnPluginBeforeSave',1,'Plugins'),
	('OnPluginRemove',1,'Plugins'),
	('OnPluginBeforeRemove',1,'Plugins'),
	('OnPluginFormPrerender',1,'Plugins'),
	('OnPluginFormRender',1,'Plugins'),
	('OnBeforePluginFormSave',1,'Plugins'),
	('OnPluginFormSave',1,'Plugins'),
	('OnBeforePluginFormDelete',1,'Plugins'),
	('OnPluginFormDelete',1,'Plugins'),
	('OnPropertySetSave',1,'Property Sets'),
	('OnPropertySetBeforeSave',1,'Property Sets'),
	('OnPropertySetRemove',1,'Property Sets'),
	('OnPropertySetBeforeRemove',1,'Property Sets'),
	('OnMediaSourceBeforeFormDelete',1,'Media Sources'),
	('OnMediaSourceBeforeFormSave',1,'Media Sources'),
	('OnMediaSourceGetProperties',1,'Media Sources'),
	('OnMediaSourceFormDelete',1,'Media Sources'),
	('OnMediaSourceFormSave',1,'Media Sources'),
	('OnMediaSourceDuplicate',1,'Media Sources');

/*!40000 ALTER TABLE `modx_system_eventnames` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_system_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_system_settings`;

CREATE TABLE `modx_system_settings` (
  `key` varchar(50) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `xtype` varchar(75) NOT NULL DEFAULT 'textfield',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `area` varchar(255) NOT NULL DEFAULT '',
  `editedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_system_settings` WRITE;
/*!40000 ALTER TABLE `modx_system_settings` DISABLE KEYS */;

INSERT INTO `modx_system_settings` (`key`, `value`, `xtype`, `namespace`, `area`, `editedon`)
VALUES
	('access_category_enabled','1','combo-boolean','core','authentication','0000-00-00 00:00:00'),
	('access_context_enabled','1','combo-boolean','core','authentication','0000-00-00 00:00:00'),
	('access_resource_group_enabled','1','combo-boolean','core','authentication','0000-00-00 00:00:00'),
	('allow_forward_across_contexts','','combo-boolean','core','system','0000-00-00 00:00:00'),
	('allow_manager_login_forgot_password','1','combo-boolean','core','authentication','0000-00-00 00:00:00'),
	('allow_multiple_emails','1','combo-boolean','core','authentication','0000-00-00 00:00:00'),
	('allow_tags_in_post','','combo-boolean','core','system','0000-00-00 00:00:00'),
	('archive_with','','combo-boolean','core','system','0000-00-00 00:00:00'),
	('auto_menuindex','1','combo-boolean','core','site','0000-00-00 00:00:00'),
	('auto_check_pkg_updates','1','combo-boolean','core','system','0000-00-00 00:00:00'),
	('auto_check_pkg_updates_cache_expire','15','textfield','core','system','0000-00-00 00:00:00'),
	('automatic_alias','1','combo-boolean','core','furls','0000-00-00 00:00:00'),
	('base_help_url','//rtfm.modx.com/display/revolution20/','textfield','core','manager','0000-00-00 00:00:00'),
	('blocked_minutes','60','textfield','core','authentication','0000-00-00 00:00:00'),
	('cache_action_map','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_alias_map','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_context_settings','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_db','0','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_db_expires','0','textfield','core','caching','0000-00-00 00:00:00'),
	('cache_db_session','0','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_db_session_lifetime','','textfield','core','caching','0000-00-00 00:00:00'),
	('cache_default','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_disabled','0','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_expires','0','textfield','core','caching','0000-00-00 00:00:00'),
	('cache_format','0','textfield','core','caching','0000-00-00 00:00:00'),
	('cache_handler','xPDOFileCache','textfield','core','caching','0000-00-00 00:00:00'),
	('cache_lang_js','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_lexicon_topics','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_noncore_lexicon_topics','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_resource','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_resource_expires','0','textfield','core','caching','0000-00-00 00:00:00'),
	('cache_scripts','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('cache_system_settings','1','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('clear_cache_refresh_trees','0','combo-boolean','core','caching','0000-00-00 00:00:00'),
	('compress_css','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('compress_js','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('compress_js_max_files','10','textfield','core','manager','0000-00-00 00:00:00'),
	('compress_js_groups','','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('confirm_navigation','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('container_suffix','/','textfield','core','furls','0000-00-00 00:00:00'),
	('context_tree_sort','','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('context_tree_sortby','rank','textfield','core','manager','0000-00-00 00:00:00'),
	('context_tree_sortdir','ASC','textfield','core','manager','0000-00-00 00:00:00'),
	('cultureKey','en','modx-combo-language','core','language','0000-00-00 00:00:00'),
	('date_timezone','','textfield','core','system','0000-00-00 00:00:00'),
	('debug','','textfield','core','system','0000-00-00 00:00:00'),
	('default_duplicate_publish_option','preserve','textfield','core','manager','0000-00-00 00:00:00'),
	('default_media_source','1','modx-combo-source','core','manager','0000-00-00 00:00:00'),
	('default_per_page','20','textfield','core','manager','0000-00-00 00:00:00'),
	('default_context','web','modx-combo-context','core','site','0000-00-00 00:00:00'),
	('default_template','1','modx-combo-template','core','site','0000-00-00 00:00:00'),
	('default_content_type','1','modx-combo-content-type','core','site','0000-00-00 00:00:00'),
	('editor_css_path','','textfield','core','editor','0000-00-00 00:00:00'),
	('editor_css_selectors','','textfield','core','editor','0000-00-00 00:00:00'),
	('emailsender','justin.lobaito@weloideas.com','textfield','core','authentication','2014-10-05 17:33:48'),
	('emailsubject','Your login details','textfield','core','authentication','0000-00-00 00:00:00'),
	('enable_dragdrop','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('error_page','1','textfield','core','site','0000-00-00 00:00:00'),
	('failed_login_attempts','5','textfield','core','authentication','0000-00-00 00:00:00'),
	('fe_editor_lang','en','modx-combo-language','core','language','0000-00-00 00:00:00'),
	('feed_modx_news','http://feeds.feedburner.com/modx-announce','textfield','core','system','0000-00-00 00:00:00'),
	('feed_modx_news_enabled','1','combo-boolean','core','system','0000-00-00 00:00:00'),
	('feed_modx_security','http://forums.modx.com/board.xml?board=294','textfield','core','system','0000-00-00 00:00:00'),
	('feed_modx_security_enabled','1','combo-boolean','core','system','0000-00-00 00:00:00'),
	('filemanager_path','','textfield','core','file','0000-00-00 00:00:00'),
	('filemanager_path_relative','1','combo-boolean','core','file','0000-00-00 00:00:00'),
	('filemanager_url','','textfield','core','file','0000-00-00 00:00:00'),
	('filemanager_url_relative','1','combo-boolean','core','file','0000-00-00 00:00:00'),
	('forgot_login_email','<p>Hello [[+username]],</p>\n<p>A request for a password reset has been issued for your MODX user. If you sent this, you may follow this link and use this password to login. If you did not send this request, please ignore this email.</p>\n\n<p>\n    <strong>Activation Link:</strong> [[+url_scheme]][[+http_host]][[+manager_url]]?modahsh=[[+hash]]<br />\n    <strong>Username:</strong> [[+username]]<br />\n    <strong>Password:</strong> [[+password]]<br />\n</p>\n\n<p>After you log into the MODX Manager, you can change your password again, if you wish.</p>\n\n<p>Regards,<br />Site Administrator</p>','textarea','core','authentication','0000-00-00 00:00:00'),
	('form_customization_use_all_groups','','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('forward_merge_excludes','type,published,class_key','textfield','core','system','0000-00-00 00:00:00'),
	('friendly_alias_lowercase_only','1','combo-boolean','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_max_length','0','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_restrict_chars','pattern','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_restrict_chars_pattern','/[\\0\\x0B\\t\\n\\r\\f\\a&=+%#<>\"~:`@\\?\\[\\]\\{\\}\\|\\^\'\\\\]/','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_strip_element_tags','1','combo-boolean','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_translit','none','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_translit_class','translit.modTransliterate','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_translit_class_path','{core_path}components/','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_trim_chars','/.-_','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_word_delimiter','-','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_alias_word_delimiters','-_','textfield','core','furls','0000-00-00 00:00:00'),
	('friendly_urls','1','combo-boolean','core','furls','2014-10-05 17:42:33'),
	('friendly_urls_strict','0','combo-boolean','core','furls','0000-00-00 00:00:00'),
	('global_duplicate_uri_check','0','combo-boolean','core','furls','0000-00-00 00:00:00'),
	('hidemenu_default','0','combo-boolean','core','site','0000-00-00 00:00:00'),
	('inline_help','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('locale','','textfield','core','language','0000-00-00 00:00:00'),
	('log_level','1','textfield','core','system','0000-00-00 00:00:00'),
	('log_target','FILE','textfield','core','system','0000-00-00 00:00:00'),
	('link_tag_scheme','-1','textfield','core','site','0000-00-00 00:00:00'),
	('lock_ttl','360','textfield','core','system','0000-00-00 00:00:00'),
	('mail_charset','UTF-8','modx-combo-charset','core','mail','0000-00-00 00:00:00'),
	('mail_encoding','8bit','textfield','core','mail','0000-00-00 00:00:00'),
	('mail_use_smtp','1','combo-boolean','core','mail','2015-04-14 11:01:38'),
	('mail_smtp_auth','1','combo-boolean','core','mail','2015-04-14 11:00:12'),
	('mail_smtp_helo','','textfield','core','mail','0000-00-00 00:00:00'),
	('mail_smtp_hosts','smtp.sendgrid.net','textfield','core','mail','2015-04-14 16:24:56'),
	('mail_smtp_keepalive','0','combo-boolean','core','mail','2015-04-14 13:37:04'),
	('mail_smtp_pass','Jpl@57006','text-password','core','mail','2015-04-14 11:01:21'),
	('mail_smtp_port','465','textfield','core','mail','2015-04-14 11:00:18'),
	('mail_smtp_prefix','ssl','textfield','core','mail','2015-04-14 16:25:03'),
	('mail_smtp_single_to','0','combo-boolean','core','mail','2015-04-14 11:01:30'),
	('mail_smtp_timeout','10','textfield','core','mail','0000-00-00 00:00:00'),
	('mail_smtp_user','jplobaito','textfield','core','mail','2015-04-14 16:25:12'),
	('manager_date_format','Y-m-d','textfield','core','manager','0000-00-00 00:00:00'),
	('manager_favicon_url','','textfield','core','manager','0000-00-00 00:00:00'),
	('manager_html5_cache','0','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('manager_js_cache_file_locking','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('manager_js_cache_max_age','3600','textfield','core','manager','0000-00-00 00:00:00'),
	('manager_js_document_root','','textfield','core','manager','0000-00-00 00:00:00'),
	('manager_js_zlib_output_compression','0','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('manager_time_format','g:i a','textfield','core','manager','0000-00-00 00:00:00'),
	('manager_direction','ltr','textfield','core','language','0000-00-00 00:00:00'),
	('manager_lang_attribute','en','textfield','core','language','0000-00-00 00:00:00'),
	('manager_language','en','modx-combo-language','core','language','0000-00-00 00:00:00'),
	('manager_login_url_alternate','','textfield','core','authentication','0000-00-00 00:00:00'),
	('manager_theme','default','modx-combo-manager-theme','core','manager','0000-00-00 00:00:00'),
	('manager_week_start','0','textfield','core','manager','0000-00-00 00:00:00'),
	('modx_browser_default_sort','name','textfield','core','manager','0000-00-00 00:00:00'),
	('modx_browser_default_viewmode','grid','textfield','core','manager','0000-00-00 00:00:00'),
	('modx_charset','UTF-8','modx-combo-charset','core','language','0000-00-00 00:00:00'),
	('principal_targets','modAccessContext,modAccessResourceGroup,modAccessCategory,sources.modAccessMediaSource','textfield','core','authentication','0000-00-00 00:00:00'),
	('proxy_auth_type','BASIC','textfield','core','proxy','0000-00-00 00:00:00'),
	('proxy_host','','textfield','core','proxy','0000-00-00 00:00:00'),
	('proxy_password','','text-password','core','proxy','0000-00-00 00:00:00'),
	('proxy_port','','textfield','core','proxy','0000-00-00 00:00:00'),
	('proxy_username','','textfield','core','proxy','0000-00-00 00:00:00'),
	('password_generated_length','8','textfield','core','authentication','0000-00-00 00:00:00'),
	('password_min_length','8','textfield','core','authentication','0000-00-00 00:00:00'),
	('phpthumb_allow_src_above_docroot','','combo-boolean','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_cache_maxage','30','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_cache_maxsize','100','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_cache_maxfiles','10000','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_cache_source_enabled','','combo-boolean','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_document_root','','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_error_bgcolor','CCCCFF','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_error_textcolor','FF0000','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_error_fontsize','1','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_far','C','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_imagemagick_path','','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nohotlink_enabled','1','combo-boolean','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nohotlink_erase_image','1','combo-boolean','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nohotlink_valid_domains','{http_host}','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nohotlink_text_message','Off-server thumbnailing is not allowed','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nooffsitelink_enabled','','combo-boolean','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nooffsitelink_erase_image','1','combo-boolean','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nooffsitelink_require_refer','','combo-boolean','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nooffsitelink_text_message','Off-server linking is not allowed','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nooffsitelink_valid_domains','{http_host}','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_nooffsitelink_watermark_src','','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('phpthumb_zoomcrop','0','textfield','core','phpthumb','0000-00-00 00:00:00'),
	('publish_default','','combo-boolean','core','site','0000-00-00 00:00:00'),
	('rb_base_dir','','textfield','core','file','0000-00-00 00:00:00'),
	('rb_base_url','','textfield','core','file','0000-00-00 00:00:00'),
	('request_controller','index.php','textfield','core','gateway','0000-00-00 00:00:00'),
	('request_method_strict','0','combo-boolean','core','gateway','0000-00-00 00:00:00'),
	('request_param_alias','q','textfield','core','gateway','0000-00-00 00:00:00'),
	('request_param_id','id','textfield','core','gateway','0000-00-00 00:00:00'),
	('resolve_hostnames','0','combo-boolean','core','system','0000-00-00 00:00:00'),
	('resource_tree_node_name','pagetitle','textfield','core','manager','0000-00-00 00:00:00'),
	('resource_tree_node_name_fallback','pagetitle','textfield','core','manager','0000-00-00 00:00:00'),
	('resource_tree_node_tooltip','','textfield','core','manager','0000-00-00 00:00:00'),
	('richtext_default','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('search_default','1','combo-boolean','core','site','0000-00-00 00:00:00'),
	('server_offset_time','0','textfield','core','system','0000-00-00 00:00:00'),
	('server_protocol','http','textfield','core','system','0000-00-00 00:00:00'),
	('session_cookie_domain','','textfield','core','session','0000-00-00 00:00:00'),
	('session_cookie_lifetime','604800','textfield','core','session','0000-00-00 00:00:00'),
	('session_cookie_path','','textfield','core','session','0000-00-00 00:00:00'),
	('session_cookie_secure','','combo-boolean','core','session','0000-00-00 00:00:00'),
	('session_cookie_httponly','1','combo-boolean','core','session','0000-00-00 00:00:00'),
	('session_gc_maxlifetime','604800','textfield','core','session','0000-00-00 00:00:00'),
	('session_handler_class','modSessionHandler','textfield','core','session','0000-00-00 00:00:00'),
	('session_name','','textfield','core','session','0000-00-00 00:00:00'),
	('set_header','1','combo-boolean','core','system','0000-00-00 00:00:00'),
	('show_tv_categories_header','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('signupemail_message','<p>Hello [[+uid]],</p>\n    <p>Here are your login details for the [[+sname]] MODX Manager:</p>\n\n    <p>\n        <strong>Username:</strong> [[+uid]]<br />\n        <strong>Password:</strong> [[+pwd]]<br />\n    </p>\n\n    <p>Once you log into the MODX Manager at [[+surl]], you can change your password.</p>\n\n    <p>Regards,<br />Site Administrator</p>','textarea','core','authentication','0000-00-00 00:00:00'),
	('site_name','Heartland Heating and Cooling','textfield','core','site','2014-12-08 05:01:48'),
	('site_start','1','textfield','core','site','0000-00-00 00:00:00'),
	('site_status','1','combo-boolean','core','site','0000-00-00 00:00:00'),
	('site_unavailable_message','The site is currently unavailable','textfield','core','site','0000-00-00 00:00:00'),
	('site_unavailable_page','0','textfield','core','site','0000-00-00 00:00:00'),
	('strip_image_paths','1','combo-boolean','core','file','0000-00-00 00:00:00'),
	('symlink_merge_fields','1','combo-boolean','core','site','0000-00-00 00:00:00'),
	('topmenu_show_descriptions','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('tree_default_sort','menuindex','textfield','core','manager','0000-00-00 00:00:00'),
	('tree_root_id','0','numberfield','core','manager','0000-00-00 00:00:00'),
	('tvs_below_content','0','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('udperms_allowroot','','combo-boolean','core','authentication','0000-00-00 00:00:00'),
	('unauthorized_page','1','textfield','core','site','0000-00-00 00:00:00'),
	('upload_files','txt,html,htm,xml,js,css,zip,gz,rar,z,tgz,tar,htaccess,mp3,mp4,aac,wav,au,wmv,avi,mpg,mpeg,pdf,doc,docx,xls,xlsx,ppt,pptx,jpg,jpeg,png,gif,psd,ico,bmp,odt,ods,odp,odb,odg,odf','textfield','core','file','0000-00-00 00:00:00'),
	('upload_flash','swf,fla','textfield','core','file','0000-00-00 00:00:00'),
	('upload_images','jpg,jpeg,png,gif,psd,ico,bmp','textfield','core','file','0000-00-00 00:00:00'),
	('upload_maxsize','33554432','textfield','core','file','2014-10-05 17:33:48'),
	('upload_media','mp3,wav,au,wmv,avi,mpg,mpeg','textfield','core','file','0000-00-00 00:00:00'),
	('use_alias_path','1','combo-boolean','core','furls','2014-10-05 17:42:52'),
	('use_browser','1','combo-boolean','core','file','0000-00-00 00:00:00'),
	('use_editor','1','combo-boolean','core','editor','0000-00-00 00:00:00'),
	('use_multibyte','1','combo-boolean','core','language','2014-10-05 17:33:48'),
	('use_weblink_target','0','combo-boolean','core','site','2014-10-05 17:43:06'),
	('webpwdreminder_message','<p>Hello [[+uid]],</p>\n\n    <p>To activate your new password click the following link:</p>\n\n    <p>[[+surl]]</p>\n\n    <p>If successful you can use the following password to login:</p>\n\n    <p><strong>Password:</strong> [[+pwd]]</p>\n\n    <p>If you did not request this email then please ignore it.</p>\n\n    <p>Regards,<br />\n    Site Administrator</p>','textarea','core','authentication','0000-00-00 00:00:00'),
	('websignupemail_message','<p>Hello [[+uid]],</p>\n\n    <p>Here are your login details for [[+sname]]:</p>\n\n    <p><strong>Username:</strong> [[+uid]]<br />\n    <strong>Password:</strong> [[+pwd]]</p>\n\n    <p>Once you log into [[+sname]] at [[+surl]], you can change your password.</p>\n\n    <p>Regards,<br />\n    Site Administrator</p>','textarea','core','authentication','0000-00-00 00:00:00'),
	('welcome_screen','','combo-boolean','core','manager','2014-10-05 17:34:00'),
	('welcome_screen_url','http://misc.modx.com/revolution/welcome.22.html','textfield','core','manager','0000-00-00 00:00:00'),
	('welcome_action','welcome','textfield','core','manager','0000-00-00 00:00:00'),
	('welcome_namespace','core','textfield','core','manager','0000-00-00 00:00:00'),
	('which_editor','TinyMCE','modx-combo-rte','core','editor','2014-12-07 20:58:32'),
	('which_element_editor','','modx-combo-rte','core','editor','0000-00-00 00:00:00'),
	('xhtml_urls','1','combo-boolean','core','site','0000-00-00 00:00:00'),
	('enable_gravatar','1','combo-boolean','core','manager','0000-00-00 00:00:00'),
	('settings_version','2.3.1-pl','textfield','core','system','0000-00-00 00:00:00'),
	('settings_distro','traditional','textfield','core','system','0000-00-00 00:00:00'),
	('primary_email','webform@heartlandheating.com','textfield','core','','2015-04-15 15:31:39'),
	('facebook','','textfield','core','','0000-00-00 00:00:00'),
	('linkedin','','textfield','core','','0000-00-00 00:00:00'),
	('twitter','','textfield','core','','0000-00-00 00:00:00'),
	('telephone',' 515-986-5007','textfield','core','','2015-03-22 16:53:45'),
	('formit.recaptcha_public_key','','textfield','formit','reCaptcha','0000-00-00 00:00:00'),
	('formit.recaptcha_private_key','','textfield','formit','reCaptcha','0000-00-00 00:00:00'),
	('formit.recaptcha_use_ssl','','combo-boolean','formit','reCaptcha','0000-00-00 00:00:00'),
	('tiny.base_url','','textfield','tinymce','general','0000-00-00 00:00:00'),
	('tiny.convert_fonts_to_spans','1','combo-boolean','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.convert_newlines_to_brs','','combo-boolean','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.css_selectors','','textfield','tinymce','advanced-theme','0000-00-00 00:00:00'),
	('tiny.custom_buttons1','undo,redo,selectall,separator,pastetext,pasteword,separator,search,replace,separator,nonbreaking,hr,charmap,separator,image,modxlink,unlink,anchor,media,separator,cleanup,removeformat,separator,fullscreen,print,code,help','textfield','tinymce','custom-buttons','0000-00-00 00:00:00'),
	('tiny.custom_buttons2','bold,italic,underline,strikethrough,sub,sup,separator,bullist,numlist,outdent,indent,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,styleselect,formatselect,separator,styleprops','textfield','tinymce','custom-buttons','0000-00-00 00:00:00'),
	('tiny.custom_buttons3','','textfield','tinymce','custom-buttons','0000-00-00 00:00:00'),
	('tiny.custom_buttons4','','textfield','tinymce','custom-buttons','0000-00-00 00:00:00'),
	('tiny.custom_buttons5','','textfield','tinymce','custom-buttons','0000-00-00 00:00:00'),
	('tiny.custom_plugins','style,advimage,advlink,modxlink,searchreplace,print,contextmenu,paste,fullscreen,noneditable,nonbreaking,xhtmlxtras,visualchars,media','textfield','tinymce','general','0000-00-00 00:00:00'),
	('tiny.editor_theme','advanced','textfield','tinymce','general','0000-00-00 00:00:00'),
	('tiny.element_format','xhtml','textfield','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.entity_encoding','named','textfield','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.fix_nesting','','combo-boolean','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.fix_table_elements','','combo-boolean','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.font_size_classes','','textfield','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.font_size_style_values','xx-small,x-small,small,medium,large,x-large,xx-large','textfield','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.forced_root_block','p','textfield','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.indentation','30px','textfield','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.invalid_elements','','textfield','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.nowrap','','combo-boolean','tinymce','general','0000-00-00 00:00:00'),
	('tiny.object_resizing','1','combo-boolean','tinymce','general','0000-00-00 00:00:00'),
	('tiny.path_options','','textfield','tinymce','general','0000-00-00 00:00:00'),
	('tiny.remove_linebreaks','','combo-boolean','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.remove_redundant_brs','1','combo-boolean','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.removeformat_selector','b,strong,em,i,span,ins','textfield','tinymce','cleanup-output','0000-00-00 00:00:00'),
	('tiny.skin','cirkuit','textfield','tinymce','general','0000-00-00 00:00:00'),
	('tiny.skin_variant','','textfield','tinymce','general','0000-00-00 00:00:00'),
	('tiny.table_inline_editing','','combo-boolean','tinymce','general','0000-00-00 00:00:00'),
	('tiny.template_list','','textarea','tinymce','general','0000-00-00 00:00:00'),
	('tiny.template_list_snippet','','textarea','tinymce','general','0000-00-00 00:00:00'),
	('tiny.template_selected_content_classes','','textfield','tinymce','general','0000-00-00 00:00:00'),
	('tiny.theme_advanced_blockformats','p,h1,h2,h3,h4,h5,h6,div,blockquote,code,pre,address','textfield','tinymce','advanced-theme','0000-00-00 00:00:00'),
	('tiny.theme_advanced_font_sizes','80%,90%,100%,120%,140%,160%,180%,220%,260%,320%,400%,500%,700%','textfield','tinymce','advanced-theme','0000-00-00 00:00:00'),
	('tiny.use_uncompressed_library','','combo-boolean','tinymce','general','0000-00-00 00:00:00'),
	('formit.exclude_contexts','mgr','textfield','formit','','0000-00-00 00:00:00'),
	('sendgrid_username','######','textfield','sendgrid','sendgrid','0000-00-00 00:00:00'),
	('sendgrid_password','######','textfield','sendgrid','sendgrid','0000-00-00 00:00:00'),
	('google_analytics','UA-61921636-1','textfield','core','','2015-04-15 07:02:46'),
	('analytics_accountId','','textfield','analytics','','0000-00-00 00:00:00'),
	('analytics_profileId','','textfield','analytics','','0000-00-00 00:00:00'),
	('analytics_sessionToken','1/QJwJlm44Av0Gr6Ri8rAMvY8vXlj3cHvKatwPBAsoc_M','textfield','analytics','','2015-04-15 07:09:33'),
	('analytics_webPropertyId','','textfield','analytics','','0000-00-00 00:00:00'),
	('analytics_days','7','textfield','analytics','','0000-00-00 00:00:00'),
	('analytics_cachingtime','3600','textfield','analytics','','0000-00-00 00:00:00'),
	('analytics_sitename','','textfield','analytics','','0000-00-00 00:00:00'),
	('analytics_activetabs','{\"visitors\":true,\"traffic-sources\":true,\"top-content\":true,\"goals\":true,\"keywords\":true,\"sitesearch\":true}','textfield','analytics','','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `modx_system_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_transport_packages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_transport_packages`;

CREATE TABLE `modx_transport_packages` (
  `signature` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `installed` datetime DEFAULT NULL,
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `workspace` int(10) unsigned NOT NULL DEFAULT '0',
  `provider` int(10) unsigned NOT NULL DEFAULT '0',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `source` tinytext,
  `manifest` text,
  `attributes` mediumtext,
  `package_name` varchar(255) NOT NULL,
  `metadata` text,
  `version_major` smallint(5) unsigned NOT NULL DEFAULT '0',
  `version_minor` smallint(5) unsigned NOT NULL DEFAULT '0',
  `version_patch` smallint(5) unsigned NOT NULL DEFAULT '0',
  `release` varchar(100) NOT NULL DEFAULT '',
  `release_index` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`signature`),
  KEY `workspace` (`workspace`),
  KEY `provider` (`provider`),
  KEY `disabled` (`disabled`),
  KEY `package_name` (`package_name`),
  KEY `version_major` (`version_major`),
  KEY `version_minor` (`version_minor`),
  KEY `version_patch` (`version_patch`),
  KEY `release` (`release`),
  KEY `release_index` (`release_index`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_transport_packages` WRITE;
/*!40000 ALTER TABLE `modx_transport_packages` DISABLE KEYS */;

INSERT INTO `modx_transport_packages` (`signature`, `created`, `updated`, `installed`, `state`, `workspace`, `provider`, `disabled`, `source`, `manifest`, `attributes`, `package_name`, `metadata`, `version_major`, `version_minor`, `version_patch`, `release`, `release_index`)
VALUES
	('tinymce-4.3.3-pl','2014-12-08 03:57:20','2014-12-07 20:58:33','2014-12-08 03:58:33',0,1,1,0,'tinymce-4.3.3-pl.transport.zip',NULL,'a:31:{s:7:\"license\";s:15218:\"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The \"Program\", below,\nrefers to any such program or work, and a \"work based on the Program\"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term \"modification\".)  Each licensee is addressed as \"you\".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and \"any\nlater version\", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS\";s:9:\"signature\";s:16:\"tinymce-4.3.3-pl\";s:13:\"initialConfig\";s:15:\"[object Object]\";s:4:\"text\";s:8:\"Continue\";s:2:\"id\";s:19:\"package-install-btn\";s:6:\"hidden\";s:5:\"false\";s:7:\"handler\";s:557:\"function (va){var g=Ext.getCmp(\'modx-package-grid\');if(!g)return false;var r=g.menu.record.data?g.menu.record.data:g.menu.record;var topic=\'/workspace/package/install/\'+r.signature+\'/\';g.loadConsole(Ext.getBody(),topic);va=va||{};Ext.apply(va,{action:\'workspace/packages/install\',signature:r.signature,register:\'mgr\',topic:topic});MODx.Ajax.request({url:MODx.config.connector_url,params:va,listeners:{\'success\':{fn:function(){this.activate();Ext.getCmp(\'modx-package-grid\').refresh();},scope:this},\'failure\':{fn:function(){this.activate();},scope:this}}});}\";s:5:\"scope\";s:15:\"[object Object]\";s:8:\"minWidth\";s:2:\"75\";s:10:\"removeMode\";s:9:\"container\";s:10:\"hideParent\";s:4:\"true\";s:6:\"events\";s:15:\"[object Object]\";s:7:\"ownerCt\";s:15:\"[object Object]\";s:9:\"container\";s:15:\"[object Object]\";s:8:\"rendered\";s:4:\"true\";s:8:\"template\";s:15:\"[object Object]\";s:5:\"btnEl\";s:15:\"[object Object]\";s:4:\"mons\";s:15:\"[object Object]\";s:2:\"el\";s:15:\"[object Object]\";s:4:\"icon\";s:0:\"\";s:7:\"iconCls\";s:0:\"\";s:8:\"boxReady\";s:4:\"true\";s:8:\"lastSize\";s:15:\"[object Object]\";s:11:\"useSetClass\";s:4:\"true\";s:6:\"oldCls\";s:12:\"x-btn-noicon\";s:3:\"doc\";s:15:\"[object Object]\";s:19:\"monitoringMouseOver\";s:4:\"true\";s:6:\"action\";s:26:\"workspace/packages/install\";s:8:\"register\";s:3:\"mgr\";s:5:\"topic\";s:44:\"/workspace/package/install/tinymce-4.3.3-pl/\";s:14:\"package_action\";i:0;}','TinyMCE','a:38:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4ff84cc3f245544fc100000a\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"package\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556bc5b2b083396d0007e9\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:12:\"display_name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:16:\"tinymce-4.3.3-pl\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:4:\"name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:7:\"TinyMCE\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"4.3.3\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:13:\"version_major\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"4\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:13:\"version_minor\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"3\";s:8:\"children\";a:0:{}}i:7;a:4:{s:4:\"name\";s:13:\"version_patch\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"3\";s:8:\"children\";a:0:{}}i:8;a:4:{s:4:\"name\";s:7:\"release\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:9;a:4:{s:4:\"name\";s:8:\"vrelease\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:10;a:4:{s:4:\"name\";s:14:\"vrelease_index\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"0\";s:8:\"children\";a:0:{}}i:11;a:4:{s:4:\"name\";s:6:\"author\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"splittingred\";s:8:\"children\";a:0:{}}i:12;a:4:{s:4:\"name\";s:11:\"description\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:102:\"<p>TinyMCE version 3.4.7 for MODx Revolution. Works with Revolution 2.2.x or later only.</p><ul>\n</ul>\";s:8:\"children\";a:0:{}}i:13;a:4:{s:4:\"name\";s:12:\"instructions\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:225:\"<p>Install via Package Management.</p>\n<p>If you\'re having issues installing, make sure you have the latest ZipArchive extension for PHP, and that it\'s properly configured, or set the \"archive_with\" System Setting to Yes.</p>\";s:8:\"children\";a:0:{}}i:14;a:4:{s:4:\"name\";s:9:\"changelog\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2332:\"<p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 4.3.3</b></p><ul><li>Change popup windows to more convenient modals</li><li>Have TinyMCE respect context settings of Resource being edited</li><li>Update TinyMCE to 3.5.4.1</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 4.3.2</b></p><ul><li>Update Czech/German translation</li><li>&#91;#74&#93; Fix inclusion of english as fallback for language</li><li>&#91;#80&#93; Make context menu use MODxLink plugin</li><li>Upgrade TinyMCE to 3.4.7</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 4.3.1</b></p><ul><li>Optimizations for MODX 2.2</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 4.3.0</b></p><ul><li>&#91;#71&#93; Update TinyMCE to v3.4.5</li><li>&#91;#70&#93; Fixes to cirkuit skin with missing CSS styles</li><li>&#91;#64&#93; Add tiny.template_list_snippet setting for grabbing template list from a Snippet</li><li>&#91;#66&#93; Fix issues with Revolution 2.2.0 code</li><li>&#91;#63&#93; Add tiny.base_url setting for managing the document_base_url tinymce setting</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 4.2.4</b></p><ul><li>Updated TinyMCE to 3.4.2</li><li>Fix issue where recursion detected xPDO error was showing in logs on chunk editing</li><li>&#91;#55&#93; Fix help for element_format and preformatted descriptions in plugin properties</li><li>&#91;#53&#93; Languages added/update: German, English, French, Indonesian, Japanese, Dutch, Russian, Ukrainian</li></ul><b>New in 4.2.3</b><p></p><ul><li>Fix issue that inserted wrong URL when using TinyMCE in Revolution 2.1 and later</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 4.2.2</b></p><ul><li>&#91;#49&#93; Added spellchecker files</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 4.2.1</b></p><ul><li>&#91;#45&#93; &#91;#47&#93; Fixes for front-end usage and compatibility with NewsPublisher</li><li>Add compressed JS for faster loading</li></ul>\";s:8:\"children\";a:0:{}}i:15;a:4:{s:4:\"name\";s:9:\"createdon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2012-07-07T14:50:43+0000\";s:8:\"children\";a:0:{}}i:16;a:4:{s:4:\"name\";s:9:\"createdby\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"splittingred\";s:8:\"children\";a:0:{}}i:17;a:4:{s:4:\"name\";s:8:\"editedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2014-12-08T01:29:19+0000\";s:8:\"children\";a:0:{}}i:18;a:4:{s:4:\"name\";s:10:\"releasedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2012-07-07T14:50:43+0000\";s:8:\"children\";a:0:{}}i:19;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6:\"231509\";s:8:\"children\";a:0:{}}i:20;a:4:{s:4:\"name\";s:8:\"approved\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:21;a:4:{s:4:\"name\";s:7:\"audited\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:22;a:4:{s:4:\"name\";s:8:\"featured\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:23;a:4:{s:4:\"name\";s:10:\"deprecated\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:24;a:4:{s:4:\"name\";s:7:\"license\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"GPLv2\";s:8:\"children\";a:0:{}}i:25;a:3:{s:4:\"name\";s:7:\"smf_url\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:26;a:4:{s:4:\"name\";s:10:\"repository\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d4c3fa6b2b0830da9000001\";s:8:\"children\";a:0:{}}i:27;a:4:{s:4:\"name\";s:8:\"supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"2.2\";s:8:\"children\";a:0:{}}i:28;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=4ff84cc6f245544fc100000c\";s:8:\"children\";a:0:{}}i:29;a:4:{s:4:\"name\";s:9:\"signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:16:\"tinymce-4.3.3-pl\";s:8:\"children\";a:0:{}}i:30;a:4:{s:4:\"name\";s:11:\"supports_db\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"mysql,sqlsrv\";s:8:\"children\";a:0:{}}i:31;a:4:{s:4:\"name\";s:16:\"minimum_supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"2.2\";s:8:\"children\";a:0:{}}i:32;a:4:{s:4:\"name\";s:9:\"breaks_at\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"10000000\";s:8:\"children\";a:0:{}}i:33;a:4:{s:4:\"name\";s:10:\"screenshot\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:54:\"http://modx.s3.amazonaws.com/extras/459/tinymce-ss.png\";s:8:\"children\";a:0:{}}i:34;a:3:{s:4:\"name\";s:4:\"file\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:7:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4ff84cc6f245544fc100000c\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4ff84cc3f245544fc100000a\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:8:\"filename\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:30:\"tinymce-4.3.3-pl.transport.zip\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6:\"104223\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:6:\"lastip\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:15:\"188.119.150.200\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:9:\"transport\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=4ff84cc6f245544fc100000c\";s:8:\"children\";a:0:{}}}}i:35;a:4:{s:4:\"name\";s:17:\"package-signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:16:\"tinymce-4.3.3-pl\";s:8:\"children\";a:0:{}}i:36;a:4:{s:4:\"name\";s:10:\"categories\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:23:\"content,richtexteditors\";s:8:\"children\";a:0:{}}i:37;a:4:{s:4:\"name\";s:4:\"tags\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:51:\"rte,richtext,wysiwyg,richtext editor,editor,content\";s:8:\"children\";a:0:{}}}',4,3,3,'pl',0),
	('wayfinder-2.3.3-pl','2014-12-08 03:57:31','2014-12-07 20:58:37','2014-12-08 03:58:37',0,1,1,0,'wayfinder-2.3.3-pl.transport.zip',NULL,'a:33:{s:7:\"license\";s:15218:\"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The \"Program\", below,\nrefers to any such program or work, and a \"work based on the Program\"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term \"modification\".)  Each licensee is addressed as \"you\".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and \"any\nlater version\", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS\";s:6:\"readme\";s:804:\"::::::::::::::::::::::::::::::::::::::::\n Snippet name: Wayfinder\n Short Desc: builds site navigation\n Version: 2.3.0 (Revolution compatible)\n Authors: \n    Kyle Jaebker (muddydogpaws.com)\n    Ryan Thrash (vertexworks.com)\n    Shaun McCormick (splittingred.com)\n ::::::::::::::::::::::::::::::::::::::::\nDescription:\n    Totally refactored from original DropMenu nav builder to make it easier to\n    create custom navigation by using chunks as output templates. By using templates,\n    many of the paramaters are no longer needed for flexible output including tables,\n    unordered- or ordered-lists (ULs or OLs), definition lists (DLs) or in any other\n    format you desire.\n::::::::::::::::::::::::::::::::::::::::\nExample Usage:\n    [[Wayfinder? &startId=`0`]]\n::::::::::::::::::::::::::::::::::::::::\";s:9:\"changelog\";s:2655:\"Changelog for Wayfinder (for Revolution).\n\nWayfinder 2.3.3\n====================================\n- [#40] Add wf.level placeholder to items for showing current depth\n- [#42] Allow authenticated mgr users with view_unpublished to use new previewUnpublished property to preview unpublished Resources in menus\n- [#41] Fix issue with Wayfinder and truncated result sets due to getIterator call\n\nWayfinder 2.3.2\n====================================\n- [#36] Fix issue with multiple Wayfinder calls using &config\n- [#35] Fix issues with TV bindings rendering\n- Add \"protected\" placeholder that is 1 if Resource is protected by a Resource Group\n- Updated documentation, snippet properties descriptions\n\nWayfinder 2.3.1\n====================================\n- [#31] Add &scheme property for specifying link schemes\n- [#27] Improve caching in Wayfinder to store cache files in resource cache so cache is synced with modx core caching\n\nWayfinder 2.3.0\n====================================\n- [#14] Fix issue with hideSubMenus when using it with a non-zero startId\n- Add all fields of a Resource to the rowTpl placeholder set, such as menutitle, published, etc\n- Properly optimize TV value grabbing to properly parse and cache TVs to improve load times when using TVs in a result set\n- Ensure that caching also caches by user ID to persist access permissions through cached result sets\n\nWayfinder 2.2.0\n====================================\n- [#23] Fix issue that generated error message in error.log due to &contexts always being processed regardless of empty state\n- [#21] Fix issue with unnecessary groupby that was breaking sorting in older mysql versions\n- [#22] Add &cacheResults parameter, which caches queries for faster loading\n- [#8] Add &contexts parameter, and &startIdContext parameter if navigating across multiple contexts and using a non-0 &startId\n\nWayfinder 2.1.3\n====================================\n- [#14] Fix hideSubMenus property\n- Add templates parameter that accepts a comma-delimited list of template IDs to filter by\n- Add where parameter that accepts a JSON object for where conditions\n- Add hereId parameter for specifying the active location\n\nWayfinder 2.1.2\n====================================\n- Fixed bug with includeDocs parameter\n\nWayfinder 2.1.1\n====================================\n- Wayfinder now properly uses MODx parsing system\n- Fixed issue with includeDocs statement\n- Fixed issues with PDO statements\n- Added the missing permissions check\n- Added wayfinder parameter \"permissions\" - default to \"list\", empty to bypass permissions check\n- [#WAYFINDER-20] TemplateVariables not rendering in Wayfinder templates.\n- Added changelog.\";s:9:\"signature\";s:18:\"wayfinder-2.3.3-pl\";s:13:\"initialConfig\";s:15:\"[object Object]\";s:4:\"text\";s:8:\"Continue\";s:2:\"id\";s:19:\"package-install-btn\";s:6:\"hidden\";s:5:\"false\";s:7:\"handler\";s:557:\"function (va){var g=Ext.getCmp(\'modx-package-grid\');if(!g)return false;var r=g.menu.record.data?g.menu.record.data:g.menu.record;var topic=\'/workspace/package/install/\'+r.signature+\'/\';g.loadConsole(Ext.getBody(),topic);va=va||{};Ext.apply(va,{action:\'workspace/packages/install\',signature:r.signature,register:\'mgr\',topic:topic});MODx.Ajax.request({url:MODx.config.connector_url,params:va,listeners:{\'success\':{fn:function(){this.activate();Ext.getCmp(\'modx-package-grid\').refresh();},scope:this},\'failure\':{fn:function(){this.activate();},scope:this}}});}\";s:5:\"scope\";s:15:\"[object Object]\";s:8:\"minWidth\";s:2:\"75\";s:10:\"removeMode\";s:9:\"container\";s:10:\"hideParent\";s:4:\"true\";s:6:\"events\";s:15:\"[object Object]\";s:7:\"ownerCt\";s:15:\"[object Object]\";s:9:\"container\";s:15:\"[object Object]\";s:8:\"rendered\";s:4:\"true\";s:8:\"template\";s:15:\"[object Object]\";s:5:\"btnEl\";s:15:\"[object Object]\";s:4:\"mons\";s:15:\"[object Object]\";s:2:\"el\";s:15:\"[object Object]\";s:4:\"icon\";s:0:\"\";s:7:\"iconCls\";s:0:\"\";s:8:\"boxReady\";s:4:\"true\";s:8:\"lastSize\";s:15:\"[object Object]\";s:11:\"useSetClass\";s:4:\"true\";s:6:\"oldCls\";s:12:\"x-btn-noicon\";s:3:\"doc\";s:15:\"[object Object]\";s:19:\"monitoringMouseOver\";s:4:\"true\";s:6:\"action\";s:26:\"workspace/packages/install\";s:8:\"register\";s:3:\"mgr\";s:5:\"topic\";s:46:\"/workspace/package/install/wayfinder-2.3.3-pl/\";s:14:\"package_action\";i:0;}','Wayfinder','a:38:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4eaecb1ef24554127d0000b6\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"package\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556be8b2b083396d0008bd\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:12:\"display_name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:18:\"wayfinder-2.3.3-pl\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:4:\"name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:9:\"Wayfinder\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"2.3.3\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:13:\"version_major\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:13:\"version_minor\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"3\";s:8:\"children\";a:0:{}}i:7;a:4:{s:4:\"name\";s:13:\"version_patch\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"3\";s:8:\"children\";a:0:{}}i:8;a:4:{s:4:\"name\";s:7:\"release\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:9;a:4:{s:4:\"name\";s:8:\"vrelease\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:10;a:4:{s:4:\"name\";s:14:\"vrelease_index\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"0\";s:8:\"children\";a:0:{}}i:11;a:4:{s:4:\"name\";s:6:\"author\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"splittingred\";s:8:\"children\";a:0:{}}i:12;a:4:{s:4:\"name\";s:11:\"description\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:230:\"<p>Wayfinder is a highly flexible navigation builder for MODx Revolution.</p><p>See the official docs here:&nbsp;<a href=\"http://rtfm.modx.com/display/ADDON/Wayfinder\">http://rtfm.modx.com/display/ADDON/Wayfinder</a></p><ul>\n</ul>\";s:8:\"children\";a:0:{}}i:13;a:4:{s:4:\"name\";s:12:\"instructions\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:38:\"<p>Install via Package Management.</p>\";s:8:\"children\";a:0:{}}i:14;a:4:{s:4:\"name\";s:9:\"changelog\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2306:\"<p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 2.3.3</b></p><ul><li>&#91;#40&#93; Add wf.level placeholder to items for showing current depth</li><li>&#91;#42&#93; Allow authenticated mgr users with view_unpublished to use new previewUnpublished property to preview unpublished Resources in menus</li><li>&#91;#41&#93; Fix issue with Wayfinder and truncated result sets due to getIterator call</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 2.3.2</b></p><ul><li>&#91;#36&#93; Fix issue with multiple Wayfinder calls using &amp;config</li><li>&#91;#35&#93; Fix issues with TV bindings rendering</li><li>Add \"protected\" placeholder that is 1 if Resource is protected by a Resource Group</li><li>Updated documentation, snippet properties descriptions</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 2.3.1</b></p><ul><li>&#91;#31&#93; Add &amp;scheme property for specifying link schemes</li><li>&#91;#27&#93; Improve caching in Wayfinder to store cache files in resource cache so cache is synced with modx core caching</li></ul><p></p><p style=\"padding-top: 2px; padding-right: 2px; padding-bottom: 2px; padding-left: 2px; \"><b>New in 2.3.0</b></p><ul><li>&#91;#14&#93; Fix issue with hideSubMenus when using it with a non-zero startId</li><li>Add all fields of a Resource to the rowTpl placeholder set, such as menutitle, published, etc</li><li>Properly optimize TV value grabbing to properly parse and cache TVs to improve load times when using TVs in a result set</li><li>Ensure that caching also caches by user ID to persist access permissions through cached result sets</li></ul><p><b>New in 2.2.0</b></p><ul><li>&#91;#23&#93; Fix issue that generated error message in error.log due to &amp;contexts always being processed regardless of empty state</li><li>&#91;#21&#93; Fix issue with unnecessary groupby that was breaking sorting in older mysql versions</li><li>&#91;#22&#93; Add &amp;cacheResults parameter, which caches queries for faster loading</li><li>&#91;#8&#93; Add &amp;contexts parameter, and &amp;startIdContext parameter if navigating across multiple contexts and using a non-0 &amp;startId</li></ul>\";s:8:\"children\";a:0:{}}i:15;a:4:{s:4:\"name\";s:9:\"createdon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2011-10-31T16:21:50+0000\";s:8:\"children\";a:0:{}}i:16;a:4:{s:4:\"name\";s:9:\"createdby\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"splittingred\";s:8:\"children\";a:0:{}}i:17;a:4:{s:4:\"name\";s:8:\"editedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2014-12-08T01:31:06+0000\";s:8:\"children\";a:0:{}}i:18;a:4:{s:4:\"name\";s:10:\"releasedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2011-10-31T16:21:50+0000\";s:8:\"children\";a:0:{}}i:19;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6:\"211277\";s:8:\"children\";a:0:{}}i:20;a:4:{s:4:\"name\";s:8:\"approved\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:21;a:4:{s:4:\"name\";s:7:\"audited\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:22;a:4:{s:4:\"name\";s:8:\"featured\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:23;a:4:{s:4:\"name\";s:10:\"deprecated\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:24;a:4:{s:4:\"name\";s:7:\"license\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"GPLv2\";s:8:\"children\";a:0:{}}i:25;a:3:{s:4:\"name\";s:7:\"smf_url\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:26;a:4:{s:4:\"name\";s:10:\"repository\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d4c3fa6b2b0830da9000001\";s:8:\"children\";a:0:{}}i:27;a:4:{s:4:\"name\";s:8:\"supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:28;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=4eaecb20f24554127d0000b8\";s:8:\"children\";a:0:{}}i:29;a:4:{s:4:\"name\";s:9:\"signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:18:\"wayfinder-2.3.3-pl\";s:8:\"children\";a:0:{}}i:30;a:4:{s:4:\"name\";s:11:\"supports_db\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"mysql,sqlsrv\";s:8:\"children\";a:0:{}}i:31;a:4:{s:4:\"name\";s:16:\"minimum_supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:32;a:4:{s:4:\"name\";s:9:\"breaks_at\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"10000000\";s:8:\"children\";a:0:{}}i:33;a:3:{s:4:\"name\";s:10:\"screenshot\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:34;a:3:{s:4:\"name\";s:4:\"file\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:7:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4eaecb20f24554127d0000b8\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4eaecb1ef24554127d0000b6\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:8:\"filename\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:32:\"wayfinder-2.3.3-pl.transport.zip\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6:\"127729\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:6:\"lastip\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:15:\"188.119.150.200\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:9:\"transport\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=4eaecb20f24554127d0000b8\";s:8:\"children\";a:0:{}}}}i:35;a:4:{s:4:\"name\";s:17:\"package-signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:18:\"wayfinder-2.3.3-pl\";s:8:\"children\";a:0:{}}i:36;a:4:{s:4:\"name\";s:10:\"categories\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:15:\"menu,navigation\";s:8:\"children\";a:0:{}}i:37;a:4:{s:4:\"name\";s:4:\"tags\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:44:\"menus,flyover,navigation,structure,menu,site\";s:8:\"children\";a:0:{}}}',2,3,3,'pl',0),
	('getresources-1.6.1-pl','2014-12-08 03:57:50','2014-12-07 20:58:28','2014-12-08 03:58:28',0,1,1,0,'getresources-1.6.1-pl.transport.zip',NULL,'a:33:{s:7:\"license\";s:15218:\"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The \"Program\", below,\nrefers to any such program or work, and a \"work based on the Program\"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term \"modification\".)  Each licensee is addressed as \"you\".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and \"any\nlater version\", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS\";s:6:\"readme\";s:336:\"--------------------\nSnippet: getResources\n--------------------\nVersion: 1.6.0-pl\nReleased: December 30, 2013\nSince: December 28, 2009\nAuthor: Jason Coward <jason@opengeek.com>\n\nA general purpose Resource listing and summarization snippet for MODX Revolution.\n\nOfficial Documentation:\nhttp://docs.modxcms.com/display/ADDON/getResources\n\";s:9:\"changelog\";s:3492:\"Changelog for getResources.\n\ngetResources 1.6.1-pl (December 30, 2013)\n====================================\n- Allow tvFilter values to contain filter operators\n- Allow 0-based idx\n- Pass scriptProperties to wrapperTpl\n- [#30][#80] Only dump properties for invalid tpl when debug enabled\n\ngetResources 1.6.0-pl (February 19, 2013)\n====================================\n- Add tplWrapper for specifying a wrapper template\n\ngetResources 1.5.1-pl (August 23, 2012)\n====================================\n- Add tplOperator property to default properties\n- [#73] Add between tplOperator to conditionalTpls\n\ngetResources 1.5.0-pl (June 15, 2012)\n====================================\n- [#58] Add tplCondition/conditionalTpls support\n- [#67] Add odd property\n- [#60] Allow custom delimiters for tvFilters\n- [#63] Give tplFirst/tplLast precedence over tpl_X/tpl_nX\n- Automatically prepare TV values for media-source dependent TVs\n\ngetResources 1.4.2-pl (December 9, 2011)\n====================================\n- [#25] Add new operators to tvFilters\n- [#37] Consider default values with tvFilters\n- [#57] Fix tpl overrides and improve order\n\ngetResources 1.4.1-pl (December 8, 2011)\n====================================\n- [#57] Add support for factor-based tpls\n- [#54], [#55] Fix processTVList feature\n\ngetResources 1.4.0-pl (September 21, 2011)\n====================================\n- [#50] Use children of parents from other contexts\n- [#45] Add dbCacheFlag to control db caching of getCollection, default to false\n- [#49] Allow comma-delimited list of TV names as includeTVList or processTVList\n\ngetResources 1.3.1-pl (July 14, 2011)\n====================================\n- [#43] Allow 0 as idx property\n- [#9] Fix tvFilters grouping\n- [#46] Fix criteria issue with &resources property\n\ngetResources 1.3.0-pl (March 28, 2011)\n====================================\n- [#33] sortbyTVType: Allow numeric and datetime TV sorting via SQL CAST()\n- [#24] Fix typos in list property options\n- [#4] Support multiple sortby fields via JSON object\n- Use get() instead to toArray() if includeContent is false\n- [#22] Add &toSeparatePlaceholders property for splitting output\n\ngetResources 1.2.2-pl (October 18, 2010)\n====================================\n- [#19] Fix sortbyTV returning duplicate rows\n\ngetResources 1.2.1-pl (October 11, 2010)\n====================================\n- Remove inadvertent call to modX::setLogTarget(\'ECHO\')\n\ngetResources 1.2.0-pl (September 25, 2010)\n====================================\n- Fix error when &parents is not set\n- Allow empty &sortby\n- Add ability to sort by a single Template Variable value (or default value)\n\ngetResources 1.1.0-pl (July 30, 2010)\n====================================\n- Added &toPlaceholder property for assigning results to a placeholder\n- Added &resources property for including/excluding specific resources\n- Added &showDeleted property\n- Allow multiple contexts to be passed into &context\n- Added &showUnpublish property\n- Added getresources.core_path reference for easier development\n- [#ADDON-135] Make output separator configurable via outputSeparator property\n- Add where property to allow ad hoc criteria in JSON format\n\ngetResources 1.0.0-ga (December 29, 2009)\n====================================\n- [#ADDON-81] Allow empty tvPrefix property.\n- [#ADDON-89] Allow parents property to have a value of 0.\n- Changed default value of sortbyAlias to empty string and added sortbyEscaped property with default of 0.\n- Added changelog, license, and readme.\n\";s:9:\"signature\";s:21:\"getresources-1.6.1-pl\";s:13:\"initialConfig\";s:15:\"[object Object]\";s:4:\"text\";s:8:\"Continue\";s:2:\"id\";s:19:\"package-install-btn\";s:6:\"hidden\";s:5:\"false\";s:7:\"handler\";s:557:\"function (va){var g=Ext.getCmp(\'modx-package-grid\');if(!g)return false;var r=g.menu.record.data?g.menu.record.data:g.menu.record;var topic=\'/workspace/package/install/\'+r.signature+\'/\';g.loadConsole(Ext.getBody(),topic);va=va||{};Ext.apply(va,{action:\'workspace/packages/install\',signature:r.signature,register:\'mgr\',topic:topic});MODx.Ajax.request({url:MODx.config.connector_url,params:va,listeners:{\'success\':{fn:function(){this.activate();Ext.getCmp(\'modx-package-grid\').refresh();},scope:this},\'failure\':{fn:function(){this.activate();},scope:this}}});}\";s:5:\"scope\";s:15:\"[object Object]\";s:8:\"minWidth\";s:2:\"75\";s:10:\"removeMode\";s:9:\"container\";s:10:\"hideParent\";s:4:\"true\";s:6:\"events\";s:15:\"[object Object]\";s:7:\"ownerCt\";s:15:\"[object Object]\";s:9:\"container\";s:15:\"[object Object]\";s:8:\"rendered\";s:4:\"true\";s:8:\"template\";s:15:\"[object Object]\";s:5:\"btnEl\";s:15:\"[object Object]\";s:4:\"mons\";s:15:\"[object Object]\";s:2:\"el\";s:15:\"[object Object]\";s:4:\"icon\";s:0:\"\";s:7:\"iconCls\";s:0:\"\";s:8:\"boxReady\";s:4:\"true\";s:8:\"lastSize\";s:15:\"[object Object]\";s:11:\"useSetClass\";s:4:\"true\";s:6:\"oldCls\";s:12:\"x-btn-noicon\";s:3:\"doc\";s:15:\"[object Object]\";s:19:\"monitoringMouseOver\";s:4:\"true\";s:6:\"action\";s:26:\"workspace/packages/install\";s:8:\"register\";s:3:\"mgr\";s:5:\"topic\";s:49:\"/workspace/package/install/getresources-1.6.1-pl/\";s:14:\"package_action\";i:0;}','getResources','a:38:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"52c184b462cf240b35006e31\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"package\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556c3db2b083396d000abe\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:12:\"display_name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:21:\"getresources-1.6.1-pl\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:4:\"name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"getResources\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"1.6.1\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:13:\"version_major\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"1\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:13:\"version_minor\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"6\";s:8:\"children\";a:0:{}}i:7;a:4:{s:4:\"name\";s:13:\"version_patch\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"1\";s:8:\"children\";a:0:{}}i:8;a:4:{s:4:\"name\";s:7:\"release\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:9;a:4:{s:4:\"name\";s:8:\"vrelease\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:10;a:4:{s:4:\"name\";s:14:\"vrelease_index\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"0\";s:8:\"children\";a:0:{}}i:11;a:4:{s:4:\"name\";s:6:\"author\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"opengeek\";s:8:\"children\";a:0:{}}i:12;a:4:{s:4:\"name\";s:11:\"description\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:157:\"<p>This patch release fixes several bugs, including the dumping of properties to array if the output of a tpl Chunk is empty.</p><p></p><p></p><p></p><p></p>\";s:8:\"children\";a:0:{}}i:13;a:4:{s:4:\"name\";s:12:\"instructions\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:37:\"<p>Install via Package Management</p>\";s:8:\"children\";a:0:{}}i:14;a:4:{s:4:\"name\";s:9:\"changelog\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1742:\"<p></p><p>getResources 1.6.1-pl (December 30, 2013)</p><ul><li>Allow tvFilter values to contain filter operators</li><li><li>Allow 0-based idx</li><li>Pass scriptProperties to wrapperTpl</li><li>Only dump properties for invalid tpl when debug enabled</li></li></ul><p>getResources 1.6.0-pl (February 19, 2013)</p><p></p><ul><li>Add tplWrapper for specifying a wrapper template</li></ul><p></p><p>getResources 1.5.1-pl (August 23, 2012)</p><p></p><ul><li>Add tplOperator property to default properties</li><li>&#91;#73&#93; Add between tplOperator to conditionalTpls</li></ul><p></p><p>getResources 1.5.0-pl (June 15, 2012)</p><p></p><ul><li>&#91;#58&#93; Add tplCondition/conditionalTpls support</li><li>&#91;#67&#93; Add odd property</li><li>&#91;#60&#93; Allow custom delimiters for tvFilters</li><li>&#91;#63&#93; Give tplFirst/tplLast precedence over tpl_X/tpl_nX</li><li>Automatically prepare TV values for media-source dependent TVs</li></ul><p></p><p></p><p>getResources 1.4.2-pl (December 9, 2011)</p><p></p><ul><li>&#91;#25&#93; Add new operators to tvFilters</li><li>&#91;#37&#93; Consider default values with tvFilters</li><li>&#91;#57&#93; Fix tpl overrides and improve order</li></ul><p></p><p></p><p>getResources 1.4.1-pl (December 8, 2011)</p><p></p><ul><li>&#91;#57&#93; Add support for factor-based tpls</li><li>&#91;#54&#93;, &#91;#55&#93; Fix processTVList feature</li></ul><p></p><p></p><p>getResources 1.4.0-pl (September 21, 2011)</p><p></p><ul><li>&#91;#50&#93; Use children of parents from other contexts</li><li>&#91;#45&#93; Add dbCacheFlag to control db caching of getCollection, default to false</li><li>&#91;#49&#93; Allow comma-delimited list of TV names as includeTVList or processTVList</li></ul><p></p><p></p>\";s:8:\"children\";a:0:{}}i:15;a:4:{s:4:\"name\";s:9:\"createdon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2013-12-30T14:35:32+0000\";s:8:\"children\";a:0:{}}i:16;a:4:{s:4:\"name\";s:9:\"createdby\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"opengeek\";s:8:\"children\";a:0:{}}i:17;a:4:{s:4:\"name\";s:8:\"editedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2014-12-08T01:05:34+0000\";s:8:\"children\";a:0:{}}i:18;a:4:{s:4:\"name\";s:10:\"releasedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2013-12-30T14:35:32+0000\";s:8:\"children\";a:0:{}}i:19;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6:\"161142\";s:8:\"children\";a:0:{}}i:20;a:4:{s:4:\"name\";s:8:\"approved\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:21;a:4:{s:4:\"name\";s:7:\"audited\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:22;a:4:{s:4:\"name\";s:8:\"featured\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:23;a:4:{s:4:\"name\";s:10:\"deprecated\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:24;a:4:{s:4:\"name\";s:7:\"license\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"GPLv2\";s:8:\"children\";a:0:{}}i:25;a:3:{s:4:\"name\";s:7:\"smf_url\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:26;a:4:{s:4:\"name\";s:10:\"repository\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d4c3fa6b2b0830da9000001\";s:8:\"children\";a:0:{}}i:27;a:4:{s:4:\"name\";s:8:\"supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:28;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=52c184b562cf240b35006e33\";s:8:\"children\";a:0:{}}i:29;a:4:{s:4:\"name\";s:9:\"signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:21:\"getresources-1.6.1-pl\";s:8:\"children\";a:0:{}}i:30;a:4:{s:4:\"name\";s:11:\"supports_db\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"mysql,sqlsrv\";s:8:\"children\";a:0:{}}i:31;a:4:{s:4:\"name\";s:16:\"minimum_supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:32;a:4:{s:4:\"name\";s:9:\"breaks_at\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"10000000\";s:8:\"children\";a:0:{}}i:33;a:3:{s:4:\"name\";s:10:\"screenshot\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:34;a:3:{s:4:\"name\";s:4:\"file\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:7:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"52c184b562cf240b35006e33\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"52c184b462cf240b35006e31\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:8:\"filename\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:35:\"getresources-1.6.1-pl.transport.zip\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"36165\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:6:\"lastip\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:13:\"89.253.232.85\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:9:\"transport\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=52c184b562cf240b35006e33\";s:8:\"children\";a:0:{}}}}i:35;a:4:{s:4:\"name\";s:17:\"package-signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:21:\"getresources-1.6.1-pl\";s:8:\"children\";a:0:{}}i:36;a:4:{s:4:\"name\";s:10:\"categories\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:32:\"blogging,content,navigation,news\";s:8:\"children\";a:0:{}}i:37;a:4:{s:4:\"name\";s:4:\"tags\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:57:\"blog,blogging,resources,getr,getresource,resource,listing\";s:8:\"children\";a:0:{}}}',1,6,1,'pl',0),
	('formit-2.2.0-pl','2014-12-08 03:58:06','2014-12-07 20:58:21','2014-12-08 03:58:21',0,1,1,0,'formit-2.2.0-pl.transport.zip',NULL,'a:33:{s:7:\"license\";s:15218:\"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The \"Program\", below,\nrefers to any such program or work, and a \"work based on the Program\"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term \"modification\".)  Each licensee is addressed as \"you\".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and \"any\nlater version\", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS\";s:6:\"readme\";s:213:\"--------------------\nSnippet: FormIt\n--------------------\nAuthor: Shaun McCormick <shaun@modx.com>\n\nA form processing Snippet for MODx Revolution.\n\nOfficial Documentation:\nhttp://rtfm.modx.com/display/ADDON/FormIt\";s:9:\"changelog\";s:10330:\"Changelog for FormIt.\n\nFormIt 2.2.0\n====================================\n- [#8382] Prevent issue with checkboxes/radios causing text-parsing problems with required validator\n- Fixed issue with custom error message for vTextPasswordConfirm not respected\n- [#9457] Fixed issue with commas in values causing errors with FormItIsChecked & FormItIsSelected\n- [#9576] Add ability to translate country options\n- Add check for preHook errors before processing postHooks\n- Add option, defaulting true, to trim spaces from sides of values before validation\n- [#8785] Fix E_STRICT error in fiDictionary\n\nFormIt 2.1.2\n====================================\n- Various language updates\n- [#7250] Fix issue with 0 not passing :required filter\n\nFormIt 2.1.1\n====================================\n- [#8204] Fix issue with FormItAutoResponder and processing of MODX tags\n\nFormIt 2.1.0\n====================================\n- [#7620] Allow for MODX tags in email templates, as well as pass-through of snippet properties to tpl\n- [#7502] Add ability to find type of hook by using $hook->type\n- [#8151] More sanity checking for FormItAutoResponder and replyTo addresses\n- Fix useIsoCode issue in FormItCountryOptions\n- Update German translation\n- Enhance validation templating for validationErrorBulkTpl\n- Add &country option to FormItStateOptions to allow loading of non-US states (currently us/de)\n\nFormIt 2.0.3\n====================================\n- Update Czech translation\n- Fix issue with French accents in translation\n- [#6021] Refactor Russian reCaptcha translations\n- [#6618] Standardize XHTML in reCaptcha usage\n\nFormIt 2.0.2\n====================================\n- [#4864] Fix issue with isNumber not allowing blank fields\n- [#5404] Fix issues with checkboxes and array fields in FormItAutoResponder\n- [#5269] Fix issues with checkboxes in various forms in emails\n- [#5792] Update reCaptcha URLs\n\nFormIt 2.0.1\n====================================\n- [#5525] Add &allowFiles property, that when set to 0, prevents file submissions on form\n- [#5484] Fix issue with double validation error spans\n- Fix issue where config was not passed to hooks\n- Update German translation\n\nFormIt 2.0.0\n====================================\n- [#3514] Add ability to customize validator error messages per FormIt form and per field\n- [#4705] Add regexp validator\n- [#5454] Fix issue with customValidators property in 2.0.0-rc2\n- Major reworking of main FormIt script to be OOP\n- Add over 150 unit tests to prevent regression\n- [#5388], [#5240] Fix issue with FormItCountryOptions and &useIsoCode\n- Fix issue with FormItStateOptions and &useAbbr\n- [#5267] Fix issue with FormItRetriever and array fields\n\nFormIt 1.7.0\n====================================\n- Add ability to have \"Frequent Visitors\" optgroup in FormItCountryOptions, moving specified countries to the top of the list in an optgroup\n- Add missing property translations for FormItStateOptions snippet\n- Fix small issue with stored values after validation of fields\n- Add FormItStateOptions snippet for easy U.S. state dropdowns\n- Add FormItCountryOptions snippet for easy country dropdowns\n- [#5101] Fix issue with emailMultiSeparator and emailMultiWrapper default values\n- Fix issue with bracketed field names being added as extra fields post-validation with . prefix\n\nFormIt 1.6.0\n====================================\n- [#4708] Add support for bracketed fields, such as contact[name]\n- [#5038] Fix uninitialized variable warnings in reCaptcha service\n- [#4993] Add Italian translation and fix recaptcha links\n- Fix issue where fields could be removed via DOM from form and be bypassed\n- Add &emailMultiSeparator and &emailMultiWrapper for handling display of checkboxes/multi-selects in email hook\n\nFormIt 1.5.6\n====================================\n- [#4564] Fix redirectTo with non-web contexts\n\nFormIt 1.5.5\n====================================\n- [#4168] Add emailConvertNewlines property for handling newlines in HTML emails\n- [#4057] Prevent math hook from generating similar numbers\n- [#4302] Cleanups to FormItAutoResponder snippet\n- [#3991] Fix issue with checkbox values in emails\n\nFormIt 1.5.4\n====================================\n- Fix issue with math hook where error placeholders were incorrect\n- Fix issue where emailHtml property was not respected in email hook\n- Fix issue where hooks were not passed customProperties array\n- [#51] Allow blank fields to be passed with :email validator\n- [#55] Allow all fields to be accessed in custom validators\n\nFormIt 1.5.3\n====================================\n- [#40] Add ability to display all error messages in bulk at top, added validationErrorBulkTpl for templating each one\n- [#52] Add a general validation error message property, validationErrorMessage, that shows when validation fails\n- [#53] Fix bug that prevented recaptcha options from working\n- Add a generic validation error placeholder in FormIt to allow for general messages\n- [#50] Trim each hook specification in hooks calls\n- [#49] Ensure reCaptcha service instance is unique for each FormIt instance\n- [#47] Ensure email validator checks for empty string\n- [#42] Can now include field names in error strings via `field` placeholder\n- [#39] Fix issue with FormItIsChecked/Selected to prevent output from occurring if not checked\n- [#37] Fix allowTags validator to work, and work with parameters encapsulated by ^\n\nFormIt 1.5.2\n====================================\n- Fixed security vulnerability\n- Added math hook, allowing anti-spam math field measure\n- Added more debugging info to email hook\n\nFormIt 1.5.1\n====================================\n- Fixed issue where &store was not respecting values set in post-hooks\n- Redirect hook now redirects *after* all other hooks execute\n\nFormIt 1.5.0\n====================================\n- Fixed bug with redirectParams not parsing placeholders in the params\n- Added redirectParams property, which allows a JSON object of params to be passed when using redirect hook\n- Added spamCheckIp property, defaults to false, to check IP as well in spam hook\n- Fixed incorrect default param for fiarSender\n- Fixed error reporting for FormItAutoResponder\n- Added sanity checks to form attachments when dealing with missing names\n- Fixed invalid offset error in checkbox validation\n- Added recaptchaJS to allow for custom JS overriding of reCaptcha options var\n\nFormIt 1.4.1\n====================================\n- Added sanity check for emailHtml property on email hook\n- Added sanity check for replyto/cc/bcc emails on email hook\n- Added ability to change language via &language parameter\n\nFormIt 1.4.0\n====================================\n- Fixed bug with recaptcha and other hooks error display messages\n- Introduced &validate parameter for more secure validation parameters to prevent POST injection\n- Added FormItIsChecked and FormItIsSelected custom output filters for easier checkbox/radio/select handling of selected values\n- Added &placeholderPrefix for FormIt snippet, defaults to `fi.`\n\nFormIt 1.3.0\n====================================\n- Fixed issue with isNumber validator\n- Added FormItRetriever snippet to get data from a FormIt submission for thank you pages\n- Added extra API methods for custom hooks for easier data grabbing\n- Added FormItAutoResponder snippet to use as a custom hook for auto-responses\n- Added &successMessage and &successMessagePlaceholder properties for easier success message handling\n- Fixed ordering for &emailFrom property\n- Added width/height for reCaptcha, however, reCaptcha APIs prevent resizing via calls\n\nFormIt 1.2.1\n====================================\n- Added recaptchaTheme property, which allows theming of reCaptcha hook\n\nFormIt 1.2.0\n====================================\n- Added preHooks property to allow for custom snippets to pre-fill fields\n- Added clearFieldsOnSuccess property to clear fields after a successful form submission without a redirect\n- Allow placeholders of fields in all email properties\n- Added customValidators property to FormIt snippet to restrict custom validators to only specified validators to prevent brute force snippet loading\n- Added fiValidator::addError for easier error loading for custom validators\n- Added German translation\n\nFormIt 1.1.7\n====================================\n- Added bcc and cc properties for email hook\n\nFormIt 1.1.6\n====================================\n- i18n of Snippet properties\n- Added emailReplyToName and emailReplyTo properties for email hook\n- Removed SMTP settings as those are now in Revo\n- Fixed bug in html emails where linebreaks were being ignored\n- Added islowercase and isuppercase validators\n- Added multibyte support to validators\n\nFormIt 1.1.5\n====================================\n- Added Russian translation\n- Updated copyright information\n\nFormIt 1.1.4\n====================================\n- Fixed bug with isDate check\n- Migrated FormIt to Git\n- Fixed bug that caused validators to not fire\n- Fixed bug where custom validators were wonky, added \'errors\' references to custom hooks/validators\n- [#ADDON-147] Added support for validation and emailing of file fields\n- Added stripTags to all fields by default (unless \'allowTags\' hook is passed\') to prevent XSS\n- Added in missing settings\n- Added reCaptcha support via the recaptcha hook\n- Adjusted copyright information to reflect current year\n\nFormIt 1.0\n====================================\n- Fixed bug with emailFrom property getting overwritten\n- [#ADDON-122] Fixed incorrect message in spam lexicon item\n- Added \'spam\' hook that utilizes StopForumSpam spam filter. Will filter the fields in the property &spamEmailFields \n- Ensure hooks errors are set as placeholders\n- Aded fi.success placeholder to be set on a successful form submission if no redirect hook is specified \n- Added default to emailTpl property so that it is now no longer required. Will send out email with just field names and values.\n- Added Dutch translation\n- Added missing formit.contains lexicon entry\n- Fixed possible loophole with $this->fields and referencing in multiple hooks\n- Fixed bug on hooks due to !== and != difference\n- Added SMTP support to FormIt email hook\n- Fixed bug with emailFrom in email hook\n- Added emailUseFieldForSubject property to FormIt snippet\n- Fixed bug on email hook where if subject was passed through form, it wouldn\'t set it as email subject\n- Added changelog\";s:9:\"signature\";s:15:\"formit-2.2.0-pl\";s:13:\"initialConfig\";s:15:\"[object Object]\";s:4:\"text\";s:8:\"Continue\";s:2:\"id\";s:19:\"package-install-btn\";s:6:\"hidden\";s:5:\"false\";s:7:\"handler\";s:557:\"function (va){var g=Ext.getCmp(\'modx-package-grid\');if(!g)return false;var r=g.menu.record.data?g.menu.record.data:g.menu.record;var topic=\'/workspace/package/install/\'+r.signature+\'/\';g.loadConsole(Ext.getBody(),topic);va=va||{};Ext.apply(va,{action:\'workspace/packages/install\',signature:r.signature,register:\'mgr\',topic:topic});MODx.Ajax.request({url:MODx.config.connector_url,params:va,listeners:{\'success\':{fn:function(){this.activate();Ext.getCmp(\'modx-package-grid\').refresh();},scope:this},\'failure\':{fn:function(){this.activate();},scope:this}}});}\";s:5:\"scope\";s:15:\"[object Object]\";s:8:\"minWidth\";s:2:\"75\";s:10:\"removeMode\";s:9:\"container\";s:10:\"hideParent\";s:4:\"true\";s:6:\"events\";s:15:\"[object Object]\";s:7:\"ownerCt\";s:15:\"[object Object]\";s:9:\"container\";s:15:\"[object Object]\";s:8:\"rendered\";s:4:\"true\";s:8:\"template\";s:15:\"[object Object]\";s:5:\"btnEl\";s:15:\"[object Object]\";s:4:\"mons\";s:15:\"[object Object]\";s:2:\"el\";s:15:\"[object Object]\";s:4:\"icon\";s:0:\"\";s:7:\"iconCls\";s:0:\"\";s:8:\"boxReady\";s:4:\"true\";s:8:\"lastSize\";s:15:\"[object Object]\";s:11:\"useSetClass\";s:4:\"true\";s:6:\"oldCls\";s:12:\"x-btn-noicon\";s:3:\"doc\";s:15:\"[object Object]\";s:19:\"monitoringMouseOver\";s:4:\"true\";s:6:\"action\";s:26:\"workspace/packages/install\";s:8:\"register\";s:3:\"mgr\";s:5:\"topic\";s:43:\"/workspace/package/install/formit-2.2.0-pl/\";s:14:\"package_action\";i:0;}','FormIt','a:38:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"51472969f245540556000081\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"package\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556c62b2b083396d000b9c\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:12:\"display_name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:15:\"formit-2.2.0-pl\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:4:\"name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6:\"FormIt\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"2.2.0\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:13:\"version_major\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:13:\"version_minor\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:7;a:4:{s:4:\"name\";s:13:\"version_patch\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"0\";s:8:\"children\";a:0:{}}i:8;a:4:{s:4:\"name\";s:7:\"release\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:9;a:4:{s:4:\"name\";s:8:\"vrelease\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:10;a:4:{s:4:\"name\";s:14:\"vrelease_index\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"0\";s:8:\"children\";a:0:{}}i:11;a:4:{s:4:\"name\";s:6:\"author\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"splittingred\";s:8:\"children\";a:0:{}}i:12;a:4:{s:4:\"name\";s:11:\"description\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:400:\"<p>Automatically validate, parse and email forms. Redirect to thank you pages. Add your own hooks as Snippets to handle forms dynamically. Validate with custom Snippets. Spam protection. Auto-response options. Dynamic country/state dropdown lists.</p>\n<p>See the Official Documentation here:</p>\n<p><a href=\"http://rtfm.modx.com/display/ADDON/FormIt\">http://rtfm.modx.com/display/ADDON/FormIt</a></p>\";s:8:\"children\";a:0:{}}i:13;a:4:{s:4:\"name\";s:12:\"instructions\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:38:\"<p>Install via Package Management.</p>\";s:8:\"children\";a:0:{}}i:14;a:4:{s:4:\"name\";s:9:\"changelog\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6161:\"<p></p><p><b>New in 2.2.0</b></p><p></p><ul><li>&#91;#8382&#93; Prevent issue with checkboxes/radios causing text-parsing problems with required validator</li><li>Fixed issue with custom error message for vTextPasswordConfirm not respected</li><li>&#91;#9457&#93; Fixed issue with commas in values causing errors with FormItIsChecked &amp; FormItIsSelected</li><li>&#91;#9576&#93; Add ability to translate country options</li><li>Add check for preHook errors before processing postHooks</li><li>Add option, defaulting true, to trim spaces from sides of values before validation</li><li>&#91;#8785&#93; Fix E_STRICT error in fiDictionary</li></ul><p></p><p><b>New in 2.1.2</b></p><p></p><ul><li>Various language updates</li><li>&#91;#7250&#93; Fix issue with 0 not passing :required filter</li></ul><p></p><p><b>New in 2.1.1</b></p><p></p><ul><li>&#91;#8204&#93; Fix issue with FormItAutoResponder and processing of MODX tags</li></ul><p></p><p><b>New in 2.1.0</b></p><p></p><ul><li>&#91;#7620&#93; Allow for MODX tags in email templates, as well as pass-through of snippet properties to tpl</li><li>&#91;#7502&#93; Add ability to find type of hook by using $hook-&gt;type</li><li>&#91;#8151&#93; More sanity checking for FormItAutoResponder and replyTo addresses</li><li>Fix useIsoCode issue in FormItCountryOptions</li><li>Update German translation</li><li>Enhance validation templating for validationErrorBulkTpl</li><li>Add &amp;country option to FormItStateOptions to allow loading of non-US states (currently us/de)</li></ul><p></p><p><b>New in 2.0.3</b></p><p></p><ul><li>Update Czech translation</li><li>Fix issue with French accents in translation</li><li>&#91;#6021&#93; Refactor Russian reCaptcha translations</li><li>&#91;#6618&#93; Standardize XHTML in reCaptcha usage</li></ul><p></p><p><b>New in 2.0.2</b></p><p></p><ul><li>&#91;#4864&#93; Fix issue with isNumber not allowing blank fields</li><li>&#91;#5404&#93; Fix issues with checkboxes and array fields in FormItAutoResponder</li><li>&#91;#5269&#93; Fix issues with checkboxes in various forms in emails</li><li>&#91;#5792&#93; Update reCaptcha URLs</li></ul><p></p><p><b>New in 2.0.1</b></p><ul><li>&#91;#5525&#93; Add &amp;allowFiles property, that when set to 0, prevents file submissions on form</li><li>&#91;#5484&#93; Fix issue with double validation error spans</li><li>Fix issue where config was not passed to hooks</li><li>Update German translation</li></ul><p></p><p><b>New in 2.0.0</b></p><p></p><ul><li>&#91;#3514&#93; Add ability to customize validator error messages per FormIt form and per field</li><li>&#91;#4705&#93; Add regexp validator</li><li>&#91;#5454&#93; Fix issue with customValidators property in 2.0.0-rc2</li><li>Fix issue with reCaptcha loading in 2.0.0-rc1</li><li>Major reworking of main FormIt script to be OOP</li><li>Add over 150 unit tests to prevent regression</li><li>&#91;#5388&#93;, &#91;#5240&#93; Fix issue with FormItCountryOptions and &amp;useIsoCode</li><li>Fix issue with FormItStateOptions and &amp;useAbbr</li><li>&#91;#5267&#93; Fix issue with FormItRetriever and array fields</li></ul><p></p><p><b>New in 1.7.0</b></p><p></p><ul><li>Add ability to have \"Frequent Visitors\" optgroup in FormItCountryOptions, moving specified countries to the top of the list in an optgroup</li><li>Add missing property translations for FormItStateOptions snippet</li><li>Fix small issue with stored values after validation of fields</li><li>Add FormItStateOptions snippet for easy U.S. state dropdowns</li><li>Add FormItCountryOptions snippet for easy country dropdowns</li><li>&#91;#5101&#93; Fix issue with emailMultiSeparator and emailMultiWrapper default values</li><li>Fix issue with bracketed field names being added as extra fields post-validation with . prefix</li></ul><p></p><p><b>New in 1.6.0</b></p><p></p><ul><li>&#91;#4708&#93; Add support for bracketed fields, such as contact&#91;name&#93;</li><li>&#91;#5038&#93; Fix uninitialized variable warnings in reCaptcha service</li><li>&#91;#4993&#93; Add Italian translation and fix recaptcha links</li><li>Fix issue where fields could be removed via DOM from form and be bypassed</li><li>Add &amp;emailMultiSeparator and &amp;emailMultiWrapper properties for handling display of checkboxes/multi-selects in email hook</li></ul><p></p><p><b>New in 1.5.6</b></p><p></p><ul><li>&#91;#4564&#93; Fix redirectTo with non-web contexts</li></ul><p></p><p><b>New in 1.5.5</b></p><ul><li>&#91;#4168&#93; Add emailConvertNewlines property for handling newlines in HTML emails</li><li>&#91;#4057&#93; Prevent math hook from generating similar numbers</li><li>&#91;#4302&#93; Cleanups to FormItAutoResponder snippet</li><li>&#91;#3991&#93; Fix issue with checkbox values in emails</li></ul><p></p><p><b>New in 1.5.4</b></p><p></p>\n<ul>\n<li>Fix issue with math hook where error placeholders were incorrect</li><li>Fix issue where emailHtml property was not respected in email hook</li><li>Fix issue where hooks were not passed customProperties array</li><li>&#91;#51&#93; Allow blank fields to be passed with :email validator</li>\n<li>&#91;#55&#93; Allow all fields to be accessed in custom validators</li>\n</ul>\n<p><b>New in 1.5.3</b></p>\n<ul>\n<li>&#91;#40&#93; Add ability to display all error messages in bulk at top, added validationErrorBulkTpl for templating each one</li>\n<li>&#91;#52&#93; Add a general validation error message property, validationErrorMessage, that shows when validation fails</li>\n<li>&#91;#53&#93; Fix bug that prevented recaptcha options from working</li>\n<li>Add a generic validation error placeholder in FormIt to allow for general messages</li>\n<li>&#91;#50&#93; Trim each hook specification in hooks calls</li>\n<li>&#91;#49&#93; Ensure reCaptcha service instance is unique for each FormIt instance</li>\n<li>&#91;#47&#93; Ensure email validator checks for empty string</li>\n<li>&#91;#42&#93; Can now include field names in error strings via `field` placeholder</li>\n<li>&#91;#39&#93; Fix issue with FormItIsChecked/Selected to prevent output from occurring if not checked</li>\n<li>&#91;#37&#93; Fix allowTags validator to work, and work with parameters encapsulated by ^</li>\n</ul>\";s:8:\"children\";a:0:{}}i:15;a:4:{s:4:\"name\";s:9:\"createdon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2013-03-18T14:49:13+0000\";s:8:\"children\";a:0:{}}i:16;a:4:{s:4:\"name\";s:9:\"createdby\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"splittingred\";s:8:\"children\";a:0:{}}i:17;a:4:{s:4:\"name\";s:8:\"editedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2014-12-08T01:05:45+0000\";s:8:\"children\";a:0:{}}i:18;a:4:{s:4:\"name\";s:10:\"releasedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2013-03-18T14:49:13+0000\";s:8:\"children\";a:0:{}}i:19;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6:\"133798\";s:8:\"children\";a:0:{}}i:20;a:4:{s:4:\"name\";s:8:\"approved\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:21;a:4:{s:4:\"name\";s:7:\"audited\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:22;a:4:{s:4:\"name\";s:8:\"featured\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:23;a:4:{s:4:\"name\";s:10:\"deprecated\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:24;a:4:{s:4:\"name\";s:7:\"license\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"GPLv2\";s:8:\"children\";a:0:{}}i:25;a:3:{s:4:\"name\";s:7:\"smf_url\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:26;a:4:{s:4:\"name\";s:10:\"repository\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d4c3fa6b2b0830da9000001\";s:8:\"children\";a:0:{}}i:27;a:4:{s:4:\"name\";s:8:\"supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:28;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=5147296ff245540556000083\";s:8:\"children\";a:0:{}}i:29;a:4:{s:4:\"name\";s:9:\"signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:15:\"formit-2.2.0-pl\";s:8:\"children\";a:0:{}}i:30;a:4:{s:4:\"name\";s:11:\"supports_db\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"mysql,sqlsrv\";s:8:\"children\";a:0:{}}i:31;a:4:{s:4:\"name\";s:16:\"minimum_supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:32;a:4:{s:4:\"name\";s:9:\"breaks_at\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"10000000\";s:8:\"children\";a:0:{}}i:33;a:3:{s:4:\"name\";s:10:\"screenshot\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:34;a:3:{s:4:\"name\";s:4:\"file\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:7:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"5147296ff245540556000083\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"51472969f245540556000081\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:8:\"filename\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:29:\"formit-2.2.0-pl.transport.zip\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"50076\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:6:\"lastip\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:13:\"89.253.232.85\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:9:\"transport\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=5147296ff245540556000083\";s:8:\"children\";a:0:{}}}}i:35;a:4:{s:4:\"name\";s:17:\"package-signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:15:\"formit-2.2.0-pl\";s:8:\"children\";a:0:{}}i:36;a:4:{s:4:\"name\";s:10:\"categories\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"forms\";s:8:\"children\";a:0:{}}i:37;a:3:{s:4:\"name\";s:4:\"tags\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}}',2,2,0,'pl',0),
	('formit-2.2.4-pl','2015-04-14 10:05:21','2015-04-14 11:05:28','2015-04-14 10:05:28',0,1,1,0,'formit-2.2.4-pl.transport.zip',NULL,'a:33:{s:7:\"license\";s:15218:\"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The \"Program\", below,\nrefers to any such program or work, and a \"work based on the Program\"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term \"modification\".)  Each licensee is addressed as \"you\".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and \"any\nlater version\", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS\";s:6:\"readme\";s:213:\"--------------------\nSnippet: FormIt\n--------------------\nAuthor: Shaun McCormick <shaun@modx.com>\n\nA form processing Snippet for MODx Revolution.\n\nOfficial Documentation:\nhttp://rtfm.modx.com/display/ADDON/FormIt\";s:9:\"changelog\";s:11263:\"Changelog for FormIt.\n\nFormIt 2.2.4\n====================================\n- Fixed bug inside FormItSaveForm and PR #43\n\nFormIt 2.2.3\n====================================\n- Added encryption to saved forms\n- Added formname to grid\n- Saved form is now returned from the hook\n- Export unlimited items\n- Add RU translation\n\nFormIt 2.2.2\n====================================\n- Added CMP for the saved forms\n- Fixed whitespace PR on required checkboxes\n\nFormIt 2.2.1\n====================================\n- Updated numbers generation for math captcha #5\n- German translation #10\n- Added missing formit.not_regexp lexicon #15\n- Specify explicitely return-path fixes #19 #20\n- fix addAttachment() typo #23\n- Fixed typo in adding of the attachments #24\n- Add the possibility of redirectTo=`formfield` #26\n- Added attachments for auto-reply and Added ability to use @CODE as tpl #29\n- Update snippet.formitisselected.php #12\n- Canadian options for FormitStateOptions\n\nFormIt 2.2.0\n====================================\n- [#8382] Prevent issue with checkboxes/radios causing text-parsing problems with required validator\n- Fixed issue with custom error message for vTextPasswordConfirm not respected\n- [#9457] Fixed issue with commas in values causing errors with FormItIsChecked & FormItIsSelected\n- [#9576] Add ability to translate country options\n- Add check for preHook errors before processing postHooks\n- Add option, defaulting true, to trim spaces from sides of values before validation\n- [#8785] Fix E_STRICT error in fiDictionary\n\nFormIt 2.1.2\n====================================\n- Various language updates\n- [#7250] Fix issue with 0 not passing :required filter\n\nFormIt 2.1.1\n====================================\n- [#8204] Fix issue with FormItAutoResponder and processing of MODX tags\n\nFormIt 2.1.0\n====================================\n- [#7620] Allow for MODX tags in email templates, as well as pass-through of snippet properties to tpl\n- [#7502] Add ability to find type of hook by using $hook->type\n- [#8151] More sanity checking for FormItAutoResponder and replyTo addresses\n- Fix useIsoCode issue in FormItCountryOptions\n- Update German translation\n- Enhance validation templating for validationErrorBulkTpl\n- Add &country option to FormItStateOptions to allow loading of non-US states (currently us/de)\n\nFormIt 2.0.3\n====================================\n- Update Czech translation\n- Fix issue with French accents in translation\n- [#6021] Refactor Russian reCaptcha translations\n- [#6618] Standardize XHTML in reCaptcha usage\n\nFormIt 2.0.2\n====================================\n- [#4864] Fix issue with isNumber not allowing blank fields\n- [#5404] Fix issues with checkboxes and array fields in FormItAutoResponder\n- [#5269] Fix issues with checkboxes in various forms in emails\n- [#5792] Update reCaptcha URLs\n\nFormIt 2.0.1\n====================================\n- [#5525] Add &allowFiles property, that when set to 0, prevents file submissions on form\n- [#5484] Fix issue with double validation error spans\n- Fix issue where config was not passed to hooks\n- Update German translation\n\nFormIt 2.0.0\n====================================\n- [#3514] Add ability to customize validator error messages per FormIt form and per field\n- [#4705] Add regexp validator\n- [#5454] Fix issue with customValidators property in 2.0.0-rc2\n- Major reworking of main FormIt script to be OOP\n- Add over 150 unit tests to prevent regression\n- [#5388], [#5240] Fix issue with FormItCountryOptions and &useIsoCode\n- Fix issue with FormItStateOptions and &useAbbr\n- [#5267] Fix issue with FormItRetriever and array fields\n\nFormIt 1.7.0\n====================================\n- Add ability to have \"Frequent Visitors\" optgroup in FormItCountryOptions, moving specified countries to the top of the list in an optgroup\n- Add missing property translations for FormItStateOptions snippet\n- Fix small issue with stored values after validation of fields\n- Add FormItStateOptions snippet for easy U.S. state dropdowns\n- Add FormItCountryOptions snippet for easy country dropdowns\n- [#5101] Fix issue with emailMultiSeparator and emailMultiWrapper default values\n- Fix issue with bracketed field names being added as extra fields post-validation with . prefix\n\nFormIt 1.6.0\n====================================\n- [#4708] Add support for bracketed fields, such as contact[name]\n- [#5038] Fix uninitialized variable warnings in reCaptcha service\n- [#4993] Add Italian translation and fix recaptcha links\n- Fix issue where fields could be removed via DOM from form and be bypassed\n- Add &emailMultiSeparator and &emailMultiWrapper for handling display of checkboxes/multi-selects in email hook\n\nFormIt 1.5.6\n====================================\n- [#4564] Fix redirectTo with non-web contexts\n\nFormIt 1.5.5\n====================================\n- [#4168] Add emailConvertNewlines property for handling newlines in HTML emails\n- [#4057] Prevent math hook from generating similar numbers\n- [#4302] Cleanups to FormItAutoResponder snippet\n- [#3991] Fix issue with checkbox values in emails\n\nFormIt 1.5.4\n====================================\n- Fix issue with math hook where error placeholders were incorrect\n- Fix issue where emailHtml property was not respected in email hook\n- Fix issue where hooks were not passed customProperties array\n- [#51] Allow blank fields to be passed with :email validator\n- [#55] Allow all fields to be accessed in custom validators\n\nFormIt 1.5.3\n====================================\n- [#40] Add ability to display all error messages in bulk at top, added validationErrorBulkTpl for templating each one\n- [#52] Add a general validation error message property, validationErrorMessage, that shows when validation fails\n- [#53] Fix bug that prevented recaptcha options from working\n- Add a generic validation error placeholder in FormIt to allow for general messages\n- [#50] Trim each hook specification in hooks calls\n- [#49] Ensure reCaptcha service instance is unique for each FormIt instance\n- [#47] Ensure email validator checks for empty string\n- [#42] Can now include field names in error strings via `field` placeholder\n- [#39] Fix issue with FormItIsChecked/Selected to prevent output from occurring if not checked\n- [#37] Fix allowTags validator to work, and work with parameters encapsulated by ^\n\nFormIt 1.5.2\n====================================\n- Fixed security vulnerability\n- Added math hook, allowing anti-spam math field measure\n- Added more debugging info to email hook\n\nFormIt 1.5.1\n====================================\n- Fixed issue where &store was not respecting values set in post-hooks\n- Redirect hook now redirects *after* all other hooks execute\n\nFormIt 1.5.0\n====================================\n- Fixed bug with redirectParams not parsing placeholders in the params\n- Added redirectParams property, which allows a JSON object of params to be passed when using redirect hook\n- Added spamCheckIp property, defaults to false, to check IP as well in spam hook\n- Fixed incorrect default param for fiarSender\n- Fixed error reporting for FormItAutoResponder\n- Added sanity checks to form attachments when dealing with missing names\n- Fixed invalid offset error in checkbox validation\n- Added recaptchaJS to allow for custom JS overriding of reCaptcha options var\n\nFormIt 1.4.1\n====================================\n- Added sanity check for emailHtml property on email hook\n- Added sanity check for replyto/cc/bcc emails on email hook\n- Added ability to change language via &language parameter\n\nFormIt 1.4.0\n====================================\n- Fixed bug with recaptcha and other hooks error display messages\n- Introduced &validate parameter for more secure validation parameters to prevent POST injection\n- Added FormItIsChecked and FormItIsSelected custom output filters for easier checkbox/radio/select handling of selected values\n- Added &placeholderPrefix for FormIt snippet, defaults to `fi.`\n\nFormIt 1.3.0\n====================================\n- Fixed issue with isNumber validator\n- Added FormItRetriever snippet to get data from a FormIt submission for thank you pages\n- Added extra API methods for custom hooks for easier data grabbing\n- Added FormItAutoResponder snippet to use as a custom hook for auto-responses\n- Added &successMessage and &successMessagePlaceholder properties for easier success message handling\n- Fixed ordering for &emailFrom property\n- Added width/height for reCaptcha, however, reCaptcha APIs prevent resizing via calls\n\nFormIt 1.2.1\n====================================\n- Added recaptchaTheme property, which allows theming of reCaptcha hook\n\nFormIt 1.2.0\n====================================\n- Added preHooks property to allow for custom snippets to pre-fill fields\n- Added clearFieldsOnSuccess property to clear fields after a successful form submission without a redirect\n- Allow placeholders of fields in all email properties\n- Added customValidators property to FormIt snippet to restrict custom validators to only specified validators to prevent brute force snippet loading\n- Added fiValidator::addError for easier error loading for custom validators\n- Added German translation\n\nFormIt 1.1.7\n====================================\n- Added bcc and cc properties for email hook\n\nFormIt 1.1.6\n====================================\n- i18n of Snippet properties\n- Added emailReplyToName and emailReplyTo properties for email hook\n- Removed SMTP settings as those are now in Revo\n- Fixed bug in html emails where linebreaks were being ignored\n- Added islowercase and isuppercase validators\n- Added multibyte support to validators\n\nFormIt 1.1.5\n====================================\n- Added Russian translation\n- Updated copyright information\n\nFormIt 1.1.4\n====================================\n- Fixed bug with isDate check\n- Migrated FormIt to Git\n- Fixed bug that caused validators to not fire\n- Fixed bug where custom validators were wonky, added \'errors\' references to custom hooks/validators\n- [#ADDON-147] Added support for validation and emailing of file fields\n- Added stripTags to all fields by default (unless \'allowTags\' hook is passed\') to prevent XSS\n- Added in missing settings\n- Added reCaptcha support via the recaptcha hook\n- Adjusted copyright information to reflect current year\n\nFormIt 1.0\n====================================\n- Fixed bug with emailFrom property getting overwritten\n- [#ADDON-122] Fixed incorrect message in spam lexicon item\n- Added \'spam\' hook that utilizes StopForumSpam spam filter. Will filter the fields in the property &spamEmailFields \n- Ensure hooks errors are set as placeholders\n- Aded fi.success placeholder to be set on a successful form submission if no redirect hook is specified \n- Added default to emailTpl property so that it is now no longer required. Will send out email with just field names and values.\n- Added Dutch translation\n- Added missing formit.contains lexicon entry\n- Fixed possible loophole with $this->fields and referencing in multiple hooks\n- Fixed bug on hooks due to !== and != difference\n- Added SMTP support to FormIt email hook\n- Fixed bug with emailFrom in email hook\n- Added emailUseFieldForSubject property to FormIt snippet\n- Fixed bug on email hook where if subject was passed through form, it wouldn\'t set it as email subject\n- Added changelog\n\";s:9:\"signature\";s:15:\"formit-2.2.4-pl\";s:13:\"initialConfig\";s:15:\"[object Object]\";s:4:\"text\";s:8:\"Continue\";s:2:\"id\";s:19:\"package-install-btn\";s:6:\"hidden\";s:5:\"false\";s:7:\"handler\";s:557:\"function (va){var g=Ext.getCmp(\'modx-package-grid\');if(!g)return false;var r=g.menu.record.data?g.menu.record.data:g.menu.record;var topic=\'/workspace/package/install/\'+r.signature+\'/\';g.loadConsole(Ext.getBody(),topic);va=va||{};Ext.apply(va,{action:\'workspace/packages/install\',signature:r.signature,register:\'mgr\',topic:topic});MODx.Ajax.request({url:MODx.config.connector_url,params:va,listeners:{\'success\':{fn:function(){this.activate();Ext.getCmp(\'modx-package-grid\').refresh();},scope:this},\'failure\':{fn:function(){this.activate();},scope:this}}});}\";s:5:\"scope\";s:15:\"[object Object]\";s:8:\"minWidth\";s:2:\"75\";s:10:\"removeMode\";s:9:\"container\";s:10:\"hideParent\";s:4:\"true\";s:6:\"events\";s:15:\"[object Object]\";s:7:\"ownerCt\";s:15:\"[object Object]\";s:9:\"container\";s:15:\"[object Object]\";s:8:\"rendered\";s:4:\"true\";s:8:\"template\";s:15:\"[object Object]\";s:5:\"btnEl\";s:15:\"[object Object]\";s:4:\"mons\";s:15:\"[object Object]\";s:2:\"el\";s:15:\"[object Object]\";s:4:\"icon\";s:0:\"\";s:7:\"iconCls\";s:0:\"\";s:8:\"boxReady\";s:4:\"true\";s:8:\"lastSize\";s:15:\"[object Object]\";s:11:\"useSetClass\";s:4:\"true\";s:6:\"oldCls\";s:12:\"x-btn-noicon\";s:3:\"doc\";s:15:\"[object Object]\";s:19:\"monitoringMouseOver\";s:4:\"true\";s:6:\"action\";s:26:\"workspace/packages/install\";s:8:\"register\";s:3:\"mgr\";s:5:\"topic\";s:43:\"/workspace/package/install/formit-2.2.4-pl/\";s:14:\"package_action\";i:1;}','FormIt','a:38:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"55279f64dc532f210c042e2c\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"package\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556c62b2b083396d000b9c\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:12:\"display_name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:15:\"formit-2.2.4-pl\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:4:\"name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6:\"FormIt\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"2.2.4\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:13:\"version_major\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:13:\"version_minor\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:7;a:4:{s:4:\"name\";s:13:\"version_patch\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"4\";s:8:\"children\";a:0:{}}i:8;a:4:{s:4:\"name\";s:7:\"release\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:9;a:4:{s:4:\"name\";s:8:\"vrelease\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:10;a:4:{s:4:\"name\";s:14:\"vrelease_index\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"0\";s:8:\"children\";a:0:{}}i:11;a:4:{s:4:\"name\";s:6:\"author\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"sterc\";s:8:\"children\";a:0:{}}i:12;a:4:{s:4:\"name\";s:11:\"description\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:839:\"<p>Formit is an extra to create advanced web forms. Key features:</p><p></p><ul><li>Automatic validation and custom validator options</li><li>Auto reply to visitor + email to owner(s)</li><li>Multiple attachments</li><li>Submitted forms can be automatically saved and encrypted in the Formit component</li><li>Submitted forms can be exported to CSV, based on filters</li><li>Redirect to thank-you pages for optimal tracking in your analytics software (e.g. Google Analytics funnels)</li><li>Add your own hooks as Snippets to handle forms dynamically</li><li>Spam protection</li><li>Dynamic country/state dropdown lists</li></ul><p></p>\n<p>Official documentation here (still being updated to 2.2.2 version):&nbsp;<a href=\"http://rtfm.modx.com/display/ADDON/FormIt\" style=\"line-height: 1.5;\">http://rtfm.modx.com/display/ADDON/FormIt</a></p>\";s:8:\"children\";a:0:{}}i:13;a:4:{s:4:\"name\";s:12:\"instructions\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:38:\"<p>Install via Package Management.</p>\";s:8:\"children\";a:0:{}}i:14;a:4:{s:4:\"name\";s:9:\"changelog\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:7623:\"<p></p><p style=\"line-height: 17.7272720336914px;\"><b>New in 2.2.4</b></p><p style=\"line-height: 17.7272720336914px;\"></p><ul style=\"line-height: 17.7272720336914px;\"></ul><p style=\"line-height: 17.7272720336914px;\"></p><ul><li>Fixed bug inside FormItSaveForm and PR #43</li></ul><p style=\"line-height: 17.7272720336914px;\"><b>New in 2.2.3</b></p><p style=\"line-height: 17.7272720336914px;\"></p><ul style=\"line-height: 17.7272720336914px;\"></ul><p style=\"line-height: 17.7272720336914px;\"></p><ul><li><li>Added encryption to saved forms</li><li>Added formname to grid</li><li>Saved form is now returned from the hook</li><li>Export unlimited items</li><li>Add RU translation</li><li>Fixed some bugs</li></li></ul><p><b>New in 2.2.2</b></p><p></p><ul></ul><p></p><ul><li>Added CMP for the saved forms</li><li>Fixed whitespace PR on required checkboxes</li></ul><p><b>New in 2.2.1</b></p><p></p><ul></ul><p></p><ul><li>Updated numbers generation for math captcha #5</li><li>German translation #10</li><li>Added missing formit.not_regexp lexicon #15</li><li>Specify explicitely return-path fixes #19 #20</li><li>fix addAttachment() typo #23</li><li>Fixed typo in adding of the attachments #24</li><li>Add the possibility of redirectTo=`formfield` #26</li><li>Added attachments for auto-reply and Added ability to use @CODE as tpl #29</li><li>Update snippet.formitisselected.php #12</li><li>Canadian options for FormitStateOptions</li></ul><p></p><p><b style=\"line-height: 1.5;\">New in 2.2.0</b></p><p></p><ul><li>&#91;#8382&#93; Prevent issue with checkboxes/radios causing text-parsing problems with required validator</li><li>Fixed issue with custom error message for vTextPasswordConfirm not respected</li><li>&#91;#9457&#93; Fixed issue with commas in values causing errors with FormItIsChecked &amp; FormItIsSelected</li><li>&#91;#9576&#93; Add ability to translate country options</li><li>Add check for preHook errors before processing postHooks</li><li>Add option, defaulting true, to trim spaces from sides of values before validation</li><li>&#91;#8785&#93; Fix E_STRICT error in fiDictionary</li></ul><p></p><p><b>New in 2.1.2</b></p><p></p><ul><li>Various language updates</li><li>&#91;#7250&#93; Fix issue with 0 not passing :required filter</li></ul><p></p><p><b>New in 2.1.1</b></p><p></p><ul><li>&#91;#8204&#93; Fix issue with FormItAutoResponder and processing of MODX tags</li></ul><p></p><p><b>New in 2.1.0</b></p><p></p><ul><li>&#91;#7620&#93; Allow for MODX tags in email templates, as well as pass-through of snippet properties to tpl</li><li>&#91;#7502&#93; Add ability to find type of hook by using $hook-&gt;type</li><li>&#91;#8151&#93; More sanity checking for FormItAutoResponder and replyTo addresses</li><li>Fix useIsoCode issue in FormItCountryOptions</li><li>Update German translation</li><li>Enhance validation templating for validationErrorBulkTpl</li><li>Add &amp;country option to FormItStateOptions to allow loading of non-US states (currently us/de)</li></ul><p></p><p><b>New in 2.0.3</b></p><p></p><ul><li>Update Czech translation</li><li>Fix issue with French accents in translation</li><li>&#91;#6021&#93; Refactor Russian reCaptcha translations</li><li>&#91;#6618&#93; Standardize XHTML in reCaptcha usage</li></ul><p></p><p><b>New in 2.0.2</b></p><p></p><ul><li>&#91;#4864&#93; Fix issue with isNumber not allowing blank fields</li><li>&#91;#5404&#93; Fix issues with checkboxes and array fields in FormItAutoResponder</li><li>&#91;#5269&#93; Fix issues with checkboxes in various forms in emails</li><li>&#91;#5792&#93; Update reCaptcha URLs</li></ul><p></p><p><b>New in 2.0.1</b></p><ul><li>&#91;#5525&#93; Add &amp;allowFiles property, that when set to 0, prevents file submissions on form</li><li>&#91;#5484&#93; Fix issue with double validation error spans</li><li>Fix issue where config was not passed to hooks</li><li>Update German translation</li></ul><p></p><p><b>New in 2.0.0</b></p><p></p><ul><li>&#91;#3514&#93; Add ability to customize validator error messages per FormIt form and per field</li><li>&#91;#4705&#93; Add regexp validator</li><li>&#91;#5454&#93; Fix issue with customValidators property in 2.0.0-rc2</li><li>Fix issue with reCaptcha loading in 2.0.0-rc1</li><li>Major reworking of main FormIt script to be OOP</li><li>Add over 150 unit tests to prevent regression</li><li>&#91;#5388&#93;, &#91;#5240&#93; Fix issue with FormItCountryOptions and &amp;useIsoCode</li><li>Fix issue with FormItStateOptions and &amp;useAbbr</li><li>&#91;#5267&#93; Fix issue with FormItRetriever and array fields</li></ul><p></p><p><b>New in 1.7.0</b></p><p></p><ul><li>Add ability to have \"Frequent Visitors\" optgroup in FormItCountryOptions, moving specified countries to the top of the list in an optgroup</li><li>Add missing property translations for FormItStateOptions snippet</li><li>Fix small issue with stored values after validation of fields</li><li>Add FormItStateOptions snippet for easy U.S. state dropdowns</li><li>Add FormItCountryOptions snippet for easy country dropdowns</li><li>&#91;#5101&#93; Fix issue with emailMultiSeparator and emailMultiWrapper default values</li><li>Fix issue with bracketed field names being added as extra fields post-validation with . prefix</li></ul><p></p><p><b>New in 1.6.0</b></p><p></p><ul><li>&#91;#4708&#93; Add support for bracketed fields, such as contact&#91;name&#93;</li><li>&#91;#5038&#93; Fix uninitialized variable warnings in reCaptcha service</li><li>&#91;#4993&#93; Add Italian translation and fix recaptcha links</li><li>Fix issue where fields could be removed via DOM from form and be bypassed</li><li>Add &amp;emailMultiSeparator and &amp;emailMultiWrapper properties for handling display of checkboxes/multi-selects in email hook</li></ul><p></p><p><b>New in 1.5.6</b></p><p></p><ul><li>&#91;#4564&#93; Fix redirectTo with non-web contexts</li></ul><p></p><p><b>New in 1.5.5</b></p><ul><li>&#91;#4168&#93; Add emailConvertNewlines property for handling newlines in HTML emails</li><li>&#91;#4057&#93; Prevent math hook from generating similar numbers</li><li>&#91;#4302&#93; Cleanups to FormItAutoResponder snippet</li><li>&#91;#3991&#93; Fix issue with checkbox values in emails</li></ul><p></p><p><b>New in 1.5.4</b></p><p></p>\n<ul>\n<li>Fix issue with math hook where error placeholders were incorrect</li><li>Fix issue where emailHtml property was not respected in email hook</li><li>Fix issue where hooks were not passed customProperties array</li><li>&#91;#51&#93; Allow blank fields to be passed with :email validator</li>\n<li>&#91;#55&#93; Allow all fields to be accessed in custom validators</li>\n</ul>\n<p><b>New in 1.5.3</b></p>\n<ul>\n<li>&#91;#40&#93; Add ability to display all error messages in bulk at top, added validationErrorBulkTpl for templating each one</li>\n<li>&#91;#52&#93; Add a general validation error message property, validationErrorMessage, that shows when validation fails</li>\n<li>&#91;#53&#93; Fix bug that prevented recaptcha options from working</li>\n<li>Add a generic validation error placeholder in FormIt to allow for general messages</li>\n<li>&#91;#50&#93; Trim each hook specification in hooks calls</li>\n<li>&#91;#49&#93; Ensure reCaptcha service instance is unique for each FormIt instance</li>\n<li>&#91;#47&#93; Ensure email validator checks for empty string</li>\n<li>&#91;#42&#93; Can now include field names in error strings via `field` placeholder</li>\n<li>&#91;#39&#93; Fix issue with FormItIsChecked/Selected to prevent output from occurring if not checked</li>\n<li>&#91;#37&#93; Fix allowTags validator to work, and work with parameters encapsulated by ^</li>\n</ul>\";s:8:\"children\";a:0:{}}i:15;a:4:{s:4:\"name\";s:9:\"createdon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2015-04-10T10:01:08+0000\";s:8:\"children\";a:0:{}}i:16;a:4:{s:4:\"name\";s:9:\"createdby\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"sterc\";s:8:\"children\";a:0:{}}i:17;a:4:{s:4:\"name\";s:8:\"editedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2015-04-14T16:01:23+0000\";s:8:\"children\";a:0:{}}i:18;a:4:{s:4:\"name\";s:10:\"releasedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2015-04-10T10:01:08+0000\";s:8:\"children\";a:0:{}}i:19;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6:\"148804\";s:8:\"children\";a:0:{}}i:20;a:4:{s:4:\"name\";s:8:\"approved\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:21;a:4:{s:4:\"name\";s:7:\"audited\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:22;a:4:{s:4:\"name\";s:8:\"featured\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:23;a:4:{s:4:\"name\";s:10:\"deprecated\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:24;a:4:{s:4:\"name\";s:7:\"license\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"GPLv2\";s:8:\"children\";a:0:{}}i:25;a:3:{s:4:\"name\";s:7:\"smf_url\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:26;a:4:{s:4:\"name\";s:10:\"repository\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d4c3fa6b2b0830da9000001\";s:8:\"children\";a:0:{}}i:27;a:4:{s:4:\"name\";s:8:\"supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"2.2\";s:8:\"children\";a:0:{}}i:28;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=55279f66dc532f210c042e2e\";s:8:\"children\";a:0:{}}i:29;a:4:{s:4:\"name\";s:9:\"signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:15:\"formit-2.2.4-pl\";s:8:\"children\";a:0:{}}i:30;a:4:{s:4:\"name\";s:11:\"supports_db\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"mysql,sqlsrv\";s:8:\"children\";a:0:{}}i:31;a:4:{s:4:\"name\";s:16:\"minimum_supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"2.2\";s:8:\"children\";a:0:{}}i:32;a:4:{s:4:\"name\";s:9:\"breaks_at\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"10000000\";s:8:\"children\";a:0:{}}i:33;a:3:{s:4:\"name\";s:10:\"screenshot\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:34;a:3:{s:4:\"name\";s:4:\"file\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:7:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"55279f66dc532f210c042e2e\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"55279f64dc532f210c042e2c\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:8:\"filename\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:29:\"formit-2.2.4-pl.transport.zip\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"1050\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:6:\"lastip\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"5.101.156.77\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:9:\"transport\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=55279f66dc532f210c042e2e\";s:8:\"children\";a:0:{}}}}i:35;a:4:{s:4:\"name\";s:17:\"package-signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:15:\"formit-2.2.4-pl\";s:8:\"children\";a:0:{}}i:36;a:4:{s:4:\"name\";s:10:\"categories\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"forms\";s:8:\"children\";a:0:{}}i:37;a:3:{s:4:\"name\";s:4:\"tags\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}}',2,2,4,'pl',0),
	('sendgrid-1.0.1-pl','2015-04-14 10:05:36','2015-04-14 11:06:10','2015-04-14 10:06:10',0,1,1,0,'sendgrid-1.0.1-pl.transport.zip',NULL,'a:34:{s:7:\"license\";s:15218:\"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The \"Program\", below,\nrefers to any such program or work, and a \"work based on the Program\"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term \"modification\".)  Each licensee is addressed as \"you\".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and \"any\nlater version\", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS\";s:6:\"readme\";s:527:\"This will send emails via sendGrid. Get a free account to route up to 400 emails per day. \nThis is meant as a replacement for formItAutoResponder, albiet currently with less options.\n\nCreated due to certain mail services sending authorised/valid email to spam highly annoying! \nEnsure that your visitors get your autoresponders/emails by sending through sendGrid.\n\nFree to use by anyone - Donated to the MODX community by GEL Studios Ltd. \n\nWe love MODX, and we hope you do too!\n\nGraeme Leighfield \nhttp://www.gelstudios.co.uk/\";s:9:\"changelog\";s:244:\"Changelog for sendGrid\n\nsendGrid 1.0.1\n---------------------------------\nPackaged with my component\nAdded sendgrid username and password in as system settings\nMade snippet static\n\nsendGrid 1.0.0\n---------------------------------\nInitial Version\";s:13:\"setup-options\";a:0:{}s:9:\"signature\";s:17:\"sendgrid-1.0.1-pl\";s:13:\"initialConfig\";s:15:\"[object Object]\";s:4:\"text\";s:8:\"Continue\";s:2:\"id\";s:19:\"package-install-btn\";s:6:\"hidden\";s:5:\"false\";s:7:\"handler\";s:557:\"function (va){var g=Ext.getCmp(\'modx-package-grid\');if(!g)return false;var r=g.menu.record.data?g.menu.record.data:g.menu.record;var topic=\'/workspace/package/install/\'+r.signature+\'/\';g.loadConsole(Ext.getBody(),topic);va=va||{};Ext.apply(va,{action:\'workspace/packages/install\',signature:r.signature,register:\'mgr\',topic:topic});MODx.Ajax.request({url:MODx.config.connector_url,params:va,listeners:{\'success\':{fn:function(){this.activate();Ext.getCmp(\'modx-package-grid\').refresh();},scope:this},\'failure\':{fn:function(){this.activate();},scope:this}}});}\";s:5:\"scope\";s:15:\"[object Object]\";s:8:\"minWidth\";s:2:\"75\";s:10:\"removeMode\";s:9:\"container\";s:10:\"hideParent\";s:4:\"true\";s:6:\"events\";s:15:\"[object Object]\";s:7:\"ownerCt\";s:15:\"[object Object]\";s:9:\"container\";s:15:\"[object Object]\";s:8:\"rendered\";s:4:\"true\";s:8:\"template\";s:15:\"[object Object]\";s:5:\"btnEl\";s:15:\"[object Object]\";s:4:\"mons\";s:15:\"[object Object]\";s:2:\"el\";s:15:\"[object Object]\";s:4:\"icon\";s:0:\"\";s:7:\"iconCls\";s:0:\"\";s:8:\"boxReady\";s:4:\"true\";s:8:\"lastSize\";s:15:\"[object Object]\";s:11:\"useSetClass\";s:4:\"true\";s:6:\"oldCls\";s:12:\"x-btn-noicon\";s:3:\"doc\";s:15:\"[object Object]\";s:19:\"monitoringMouseOver\";s:4:\"true\";s:6:\"action\";s:26:\"workspace/packages/install\";s:8:\"register\";s:3:\"mgr\";s:5:\"topic\";s:45:\"/workspace/package/install/sendgrid-1.0.1-pl/\";s:14:\"package_action\";i:0;}','sendGrid','a:38:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"54e92f76dc532f725a0276a3\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"package\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"54e896c0dc532f725a025ecc\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:12:\"display_name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:17:\"sendgrid-1.0.1-pl\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:4:\"name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"sendGrid\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"1.0.1\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:13:\"version_major\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"1\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:13:\"version_minor\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"0\";s:8:\"children\";a:0:{}}i:7;a:4:{s:4:\"name\";s:13:\"version_patch\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"1\";s:8:\"children\";a:0:{}}i:8;a:4:{s:4:\"name\";s:7:\"release\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:9;a:4:{s:4:\"name\";s:8:\"vrelease\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:10;a:3:{s:4:\"name\";s:14:\"vrelease_index\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:11;a:4:{s:4:\"name\";s:6:\"author\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:10:\"gelstudios\";s:8:\"children\";a:0:{}}i:12;a:4:{s:4:\"name\";s:11:\"description\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:605:\"<p>This will send emails from formit via sendGrid. Get a free account to route up to 400 emails per day.&nbsp;</p><p>This is meant as a replacement for formItAutoResponder, however currently with less options.</p><p>Created due to certain mail services sending authorised/valid email to spam highly annoying!&nbsp;</p><p>Ensure that your visitors get your autoresponders/emails by sending through sendGrid.</p><p>Free to use by anyone - Donated to the MODX community by GEL Studios Ltd.&nbsp;</p><p>We love MODX, and we hope you do too!</p><p>Graeme Leighfield&nbsp;</p><p>http://www.gelstudios.co.uk/</p>\";s:8:\"children\";a:0:{}}i:13;a:4:{s:4:\"name\";s:12:\"instructions\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:806:\"<p><b>Usage</b></p><p>You need to have a basic free sendGrid account in order to use this extra.</p><p><b>The first step</b> is to enter your sendGrid username and password into the two system settings, that can be found under send grid.</p><p><b>sendGrid Properties</b></p><p>Call in your FormIt hooks settings. i.e &#91;&#91;!FormIt? &amp;hooks=`sendGrid`&#93;&#93;</p><p><ul><li>&amp;sgTpl = The name of the chunk you wish to use to render your content. Required</li><li>&amp;sgFrom = Email address from. Defaults to system setting emailsender</li><li>&amp;sgTo = Email address of receipient. Defaults to email field</li><li>&amp;sgToName = Name of the person reciving the email. Defaults to name field</li><li>&amp;sgFromName = &nbsp;Name of the sender of the email. Defaults to site_name</li></ul></p>\";s:8:\"children\";a:0:{}}i:14;a:4:{s:4:\"name\";s:9:\"changelog\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:336:\"<p>Change log for sendGrid</p><p>sendGrid 1.0.1&nbsp;---------------------------------</p><p><ul><li>Packaged with my component</li><li>Added sendgrid username and password in as system settings</li><li>Made snippet static</li></ul></p><p>sendGrid 1.0.0&nbsp;---------------------------------</p><p><ul><li>Initial Version</li></ul></p>\";s:8:\"children\";a:0:{}}i:15;a:4:{s:4:\"name\";s:9:\"createdon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2015-02-22T01:23:02+0000\";s:8:\"children\";a:0:{}}i:16;a:4:{s:4:\"name\";s:9:\"createdby\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:10:\"gelstudios\";s:8:\"children\";a:0:{}}i:17;a:4:{s:4:\"name\";s:8:\"editedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2015-04-14T15:46:52+0000\";s:8:\"children\";a:0:{}}i:18;a:4:{s:4:\"name\";s:10:\"releasedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2015-02-22T02:38:50+0000\";s:8:\"children\";a:0:{}}i:19;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"45\";s:8:\"children\";a:0:{}}i:20;a:4:{s:4:\"name\";s:8:\"approved\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:21;a:4:{s:4:\"name\";s:7:\"audited\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:22;a:4:{s:4:\"name\";s:8:\"featured\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:23;a:4:{s:4:\"name\";s:10:\"deprecated\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:24;a:4:{s:4:\"name\";s:7:\"license\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"GPLv2\";s:8:\"children\";a:0:{}}i:25;a:3:{s:4:\"name\";s:7:\"smf_url\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:26;a:4:{s:4:\"name\";s:10:\"repository\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d4c3fa6b2b0830da9000001\";s:8:\"children\";a:0:{}}i:27;a:4:{s:4:\"name\";s:8:\"supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"2.2\";s:8:\"children\";a:0:{}}i:28;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=54e92f76dc532f725a0276a4\";s:8:\"children\";a:0:{}}i:29;a:4:{s:4:\"name\";s:9:\"signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:17:\"sendgrid-1.0.1-pl\";s:8:\"children\";a:0:{}}i:30;a:4:{s:4:\"name\";s:11:\"supports_db\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"mysql,sqlsrv\";s:8:\"children\";a:0:{}}i:31;a:4:{s:4:\"name\";s:16:\"minimum_supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"2.2\";s:8:\"children\";a:0:{}}i:32;a:4:{s:4:\"name\";s:9:\"breaks_at\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"2.3\";s:8:\"children\";a:0:{}}i:33;a:4:{s:4:\"name\";s:10:\"screenshot\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:65:\"http://modx.s3.amazonaws.com/extras%2F54e896c0dc532f725a025ecc%2F\";s:8:\"children\";a:0:{}}i:34;a:3:{s:4:\"name\";s:4:\"file\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:7:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"54e92f76dc532f725a0276a4\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"54e92f76dc532f725a0276a3\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:8:\"filename\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:31:\"sendgrid-1.0.1-pl.transport.zip\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"42\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:6:\"lastip\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:13:\"77.221.130.49\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:9:\"transport\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=54e92f76dc532f725a0276a4\";s:8:\"children\";a:0:{}}}}i:35;a:4:{s:4:\"name\";s:17:\"package-signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:17:\"sendgrid-1.0.1-pl\";s:8:\"children\";a:0:{}}i:36;a:4:{s:4:\"name\";s:10:\"categories\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:11:\"email,forms\";s:8:\"children\";a:0:{}}i:37;a:3:{s:4:\"name\";s:4:\"tags\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}}',1,0,1,'pl',0),
	('quickemail-1.1.1-pl','2015-04-14 12:55:32','2015-04-14 13:57:06','2015-04-14 12:57:06',0,1,1,0,'quickemail-1.1.1-pl.transport.zip',NULL,'a:33:{s:7:\"license\";s:15504:\"GNU GENERAL PUBLIC LICENSE\r\n   Version 2, June 1991\r\n--------------------------\r\n\r\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\r\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\r\n\r\nEveryone is permitted to copy and distribute verbatim copies\r\nof this license document, but changing it is not allowed.\r\n\r\nPreamble\r\n--------\r\n\r\n  The licenses for most software are designed to take away your\r\nfreedom to share and change it.  By contrast, the GNU General Public\r\nLicense is intended to guarantee your freedom to share and change free\r\nsoftware--to make sure the software is free for all its users.  This\r\nGeneral Public License applies to most of the Free Software\r\nFoundation\'s software and to any other program whose authors commit to\r\nusing it.  (Some other Free Software Foundation software is covered by\r\nthe GNU Library General Public License instead.)  You can apply it to\r\nyour programs, too.\r\n\r\n  When we speak of free software, we are referring to freedom, not\r\nprice.  Our General Public Licenses are designed to make sure that you\r\nhave the freedom to distribute copies of free software (and charge for\r\nthis service if you wish), that you receive source code or can get it\r\nif you want it, that you can change the software or use pieces of it\r\nin new free programs; and that you know you can do these things.\r\n\r\n  To protect your rights, we need to make restrictions that forbid\r\nanyone to deny you these rights or to ask you to surrender the rights.\r\nThese restrictions translate to certain responsibilities for you if you\r\ndistribute copies of the software, or if you modify it.\r\n\r\n  For example, if you distribute copies of such a program, whether\r\ngratis or for a fee, you must give the recipients all the rights that\r\nyou have.  You must make sure that they, too, receive or can get the\r\nsource code.  And you must show them these terms so they know their\r\nrights.\r\n\r\n  We protect your rights with two steps: (1) copyright the software, and\r\n(2) offer you this license which gives you legal permission to copy,\r\ndistribute and/or modify the software.\r\n\r\n  Also, for each author\'s protection and ours, we want to make certain\r\nthat everyone understands that there is no warranty for this free\r\nsoftware.  If the software is modified by someone else and passed on, we\r\nwant its recipients to know that what they have is not the original, so\r\nthat any problems introduced by others will not reflect on the original\r\nauthors\' reputations.\r\n\r\n  Finally, any free program is threatened constantly by software\r\npatents.  We wish to avoid the danger that redistributors of a free\r\nprogram will individually obtain patent licenses, in effect making the\r\nprogram proprietary.  To prevent this, we have made it clear that any\r\npatent must be licensed for everyone\'s free use or not licensed at all.\r\n\r\n  The precise terms and conditions for copying, distribution and\r\nmodification follow.\r\n\r\n\r\nGNU GENERAL PUBLIC LICENSE\r\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\r\n---------------------------------------------------------------\r\n\r\n  0. This License applies to any program or other work which contains\r\na notice placed by the copyright holder saying it may be distributed\r\nunder the terms of this General Public License.  The \"Program\", below,\r\nrefers to any such program or work, and a \"work based on the Program\"\r\nmeans either the Program or any derivative work under copyright law:\r\nthat is to say, a work containing the Program or a portion of it,\r\neither verbatim or with modifications and/or translated into another\r\nlanguage.  (Hereinafter, translation is included without limitation in\r\nthe term \"modification\".)  Each licensee is addressed as \"you\".\r\n\r\nActivities other than copying, distribution and modification are not\r\ncovered by this License; they are outside its scope.  The act of\r\nrunning the Program is not restricted, and the output from the Program\r\nis covered only if its contents constitute a work based on the\r\nProgram (independent of having been made by running the Program).\r\nWhether that is true depends on what the Program does.\r\n\r\n  1. You may copy and distribute verbatim copies of the Program\'s\r\nsource code as you receive it, in any medium, provided that you\r\nconspicuously and appropriately publish on each copy an appropriate\r\ncopyright notice and disclaimer of warranty; keep intact all the\r\nnotices that refer to this License and to the absence of any warranty;\r\nand give any other recipients of the Program a copy of this License\r\nalong with the Program.\r\n\r\nYou may charge a fee for the physical act of transferring a copy, and\r\nyou may at your option offer warranty protection in exchange for a fee.\r\n\r\n  2. You may modify your copy or copies of the Program or any portion\r\nof it, thus forming a work based on the Program, and copy and\r\ndistribute such modifications or work under the terms of Section 1\r\nabove, provided that you also meet all of these conditions:\r\n\r\n    a) You must cause the modified files to carry prominent notices\r\n    stating that you changed the files and the date of any change.\r\n\r\n    b) You must cause any work that you distribute or publish, that in\r\n    whole or in part contains or is derived from the Program or any\r\n    part thereof, to be licensed as a whole at no charge to all third\r\n    parties under the terms of this License.\r\n\r\n    c) If the modified program normally reads commands interactively\r\n    when run, you must cause it, when started running for such\r\n    interactive use in the most ordinary way, to print or display an\r\n    announcement including an appropriate copyright notice and a\r\n    notice that there is no warranty (or else, saying that you provide\r\n    a warranty) and that users may redistribute the program under\r\n    these conditions, and telling the user how to view a copy of this\r\n    License.  (Exception: if the Program itself is interactive but\r\n    does not normally print such an announcement, your work based on\r\n    the Program is not required to print an announcement.)\r\n\r\nThese requirements apply to the modified work as a whole.  If\r\nidentifiable sections of that work are not derived from the Program,\r\nand can be reasonably considered independent and separate works in\r\nthemselves, then this License, and its terms, do not apply to those\r\nsections when you distribute them as separate works.  But when you\r\ndistribute the same sections as part of a whole which is a work based\r\non the Program, the distribution of the whole must be on the terms of\r\nthis License, whose permissions for other licensees extend to the\r\nentire whole, and thus to each and every part regardless of who wrote it.\r\n\r\nThus, it is not the intent of this section to claim rights or contest\r\nyour rights to work written entirely by you; rather, the intent is to\r\nexercise the right to control the distribution of derivative or\r\ncollective works based on the Program.\r\n\r\nIn addition, mere aggregation of another work not based on the Program\r\nwith the Program (or with a work based on the Program) on a volume of\r\na storage or distribution medium does not bring the other work under\r\nthe scope of this License.\r\n\r\n  3. You may copy and distribute the Program (or a work based on it,\r\nunder Section 2) in object code or executable form under the terms of\r\nSections 1 and 2 above provided that you also do one of the following:\r\n\r\n    a) Accompany it with the complete corresponding machine-readable\r\n    source code, which must be distributed under the terms of Sections\r\n    1 and 2 above on a medium customarily used for software interchange; or,\r\n\r\n    b) Accompany it with a written offer, valid for at least three\r\n    years, to give any third party, for a charge no more than your\r\n    cost of physically performing source distribution, a complete\r\n    machine-readable copy of the corresponding source code, to be\r\n    distributed under the terms of Sections 1 and 2 above on a medium\r\n    customarily used for software interchange; or,\r\n\r\n    c) Accompany it with the information you received as to the offer\r\n    to distribute corresponding source code.  (This alternative is\r\n    allowed only for noncommercial distribution and only if you\r\n    received the program in object code or executable form with such\r\n    an offer, in accord with Subsection b above.)\r\n\r\nThe source code for a work means the preferred form of the work for\r\nmaking modifications to it.  For an executable work, complete source\r\ncode means all the source code for all modules it contains, plus any\r\nassociated interface definition files, plus the scripts used to\r\ncontrol compilation and installation of the executable.  However, as a\r\nspecial exception, the source code distributed need not include\r\nanything that is normally distributed (in either source or binary\r\nform) with the major components (compiler, kernel, and so on) of the\r\noperating system on which the executable runs, unless that component\r\nitself accompanies the executable.\r\n\r\nIf distribution of executable or object code is made by offering\r\naccess to copy from a designated place, then offering equivalent\r\naccess to copy the source code from the same place counts as\r\ndistribution of the source code, even though third parties are not\r\ncompelled to copy the source along with the object code.\r\n\r\n  4. You may not copy, modify, sublicense, or distribute the Program\r\nexcept as expressly provided under this License.  Any attempt\r\notherwise to copy, modify, sublicense or distribute the Program is\r\nvoid, and will automatically terminate your rights under this License.\r\nHowever, parties who have received copies, or rights, from you under\r\nthis License will not have their licenses terminated so long as such\r\nparties remain in full compliance.\r\n\r\n  5. You are not required to accept this License, since you have not\r\nsigned it.  However, nothing else grants you permission to modify or\r\ndistribute the Program or its derivative works.  These actions are\r\nprohibited by law if you do not accept this License.  Therefore, by\r\nmodifying or distributing the Program (or any work based on the\r\nProgram), you indicate your acceptance of this License to do so, and\r\nall its terms and conditions for copying, distributing or modifying\r\nthe Program or works based on it.\r\n\r\n  6. Each time you redistribute the Program (or any work based on the\r\nProgram), the recipient automatically receives a license from the\r\noriginal licensor to copy, distribute or modify the Program subject to\r\nthese terms and conditions.  You may not impose any further\r\nrestrictions on the recipients\' exercise of the rights granted herein.\r\nYou are not responsible for enforcing compliance by third parties to\r\nthis License.\r\n\r\n  7. If, as a consequence of a court judgment or allegation of patent\r\ninfringement or for any other reason (not limited to patent issues),\r\nconditions are imposed on you (whether by court order, agreement or\r\notherwise) that contradict the conditions of this License, they do not\r\nexcuse you from the conditions of this License.  If you cannot\r\ndistribute so as to satisfy simultaneously your obligations under this\r\nLicense and any other pertinent obligations, then as a consequence you\r\nmay not distribute the Program at all.  For example, if a patent\r\nlicense would not permit royalty-free redistribution of the Program by\r\nall those who receive copies directly or indirectly through you, then\r\nthe only way you could satisfy both it and this License would be to\r\nrefrain entirely from distribution of the Program.\r\n\r\nIf any portion of this section is held invalid or unenforceable under\r\nany particular circumstance, the balance of the section is intended to\r\napply and the section as a whole is intended to apply in other\r\ncircumstances.\r\n\r\nIt is not the purpose of this section to induce you to infringe any\r\npatents or other property right claims or to contest validity of any\r\nsuch claims; this section has the sole purpose of protecting the\r\nintegrity of the free software distribution system, which is\r\nimplemented by public license practices.  Many people have made\r\ngenerous contributions to the wide range of software distributed\r\nthrough that system in reliance on consistent application of that\r\nsystem; it is up to the author/donor to decide if he or she is willing\r\nto distribute software through any other system and a licensee cannot\r\nimpose that choice.\r\n\r\nThis section is intended to make thoroughly clear what is believed to\r\nbe a consequence of the rest of this License.\r\n\r\n  8. If the distribution and/or use of the Program is restricted in\r\ncertain countries either by patents or by copyrighted interfaces, the\r\noriginal copyright holder who places the Program under this License\r\nmay add an explicit geographical distribution limitation excluding\r\nthose countries, so that distribution is permitted only in or among\r\ncountries not thus excluded.  In such case, this License incorporates\r\nthe limitation as if written in the body of this License.\r\n\r\n  9. The Free Software Foundation may publish revised and/or new versions\r\nof the General Public License from time to time.  Such new versions will\r\nbe similar in spirit to the present version, but may differ in detail to\r\naddress new problems or concerns.\r\n\r\nEach version is given a distinguishing version number.  If the Program\r\nspecifies a version number of this License which applies to it and \"any\r\nlater version\", you have the option of following the terms and conditions\r\neither of that version or of any later version published by the Free\r\nSoftware Foundation.  If the Program does not specify a version number of\r\nthis License, you may choose any version ever published by the Free Software\r\nFoundation.\r\n\r\n  10. If you wish to incorporate parts of the Program into other free\r\nprograms whose distribution conditions are different, write to the author\r\nto ask for permission.  For software which is copyrighted by the Free\r\nSoftware Foundation, write to the Free Software Foundation; we sometimes\r\nmake exceptions for this.  Our decision will be guided by the two goals\r\nof preserving the free status of all derivatives of our free software and\r\nof promoting the sharing and reuse of software generally.\r\n\r\nNO WARRANTY\r\n-----------\r\n\r\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\r\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\r\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\r\nPROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\r\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\r\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\r\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\r\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\r\nREPAIR OR CORRECTION.\r\n\r\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\r\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\r\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\r\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\r\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\r\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\r\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\r\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\r\nPOSSIBILITY OF SUCH DAMAGES.\r\n\r\n---------------------------\r\nEND OF TERMS AND CONDITIONS\";s:6:\"readme\";s:483:\"--------------------\r\nSnippet: QuickEmail\r\n--------------------\r\nAuthor: Bob Ray <http://bobsguides.com>\r\n\r\nA simple snippet for sending email and diagnosing email problems for MODX Revolution.\r\n\r\nDocumentation:\r\nhttp://bobsguides.com/quickemail-snippet-tutorial.html\r\n\r\nAlso: Edit the QuickEmail snippet and click on the Properties tab.\r\n      Click on the plus sign next to any property to see its description.\r\n\r\nBugs and Requests:\r\nhttps://github.com/BobRay/QuickEmail/issues\r\n\r\n\";s:9:\"changelog\";s:717:\"Changelog for QuickEmail.\r\n\r\nQuickEmail 1.1.1-pl\r\n===================\r\nFix bug with replyTo property\r\n\r\nQuickEmail 1.1.0-pl\r\n===================\r\nBail if user not logged in to manager\r\nFix bug with message Tpl name\r\nTweak output format\r\nStop E_NOTICE warnings in build\r\nFix bug with fromName\r\n\r\nQuickEmail 1.0.4\r\n================\r\n - Fixed bug with stristr() when setting defaults\r\n\r\nQuickEmail 1.0.3\r\n================\r\n - Fixed bug in transport package\r\n - Added tutorial to docs\r\n\r\nQuickEmail 1.0.2\r\n================\r\n - Updated PhpDoc comments\r\n - Made snippet uncached in docs\r\n\r\nQuickEmail 1.0.1\r\n================\r\n - Added build\r\n - Updated readme\r\n\r\n\r\nQuickEmail 1.0.0\r\n================\r\n- Initial commit\r\n\r\n\r\n\";s:9:\"signature\";s:19:\"quickemail-1.1.1-pl\";s:13:\"initialConfig\";s:15:\"[object Object]\";s:4:\"text\";s:8:\"Continue\";s:2:\"id\";s:19:\"package-install-btn\";s:6:\"hidden\";s:5:\"false\";s:7:\"handler\";s:557:\"function (va){var g=Ext.getCmp(\'modx-package-grid\');if(!g)return false;var r=g.menu.record.data?g.menu.record.data:g.menu.record;var topic=\'/workspace/package/install/\'+r.signature+\'/\';g.loadConsole(Ext.getBody(),topic);va=va||{};Ext.apply(va,{action:\'workspace/packages/install\',signature:r.signature,register:\'mgr\',topic:topic});MODx.Ajax.request({url:MODx.config.connector_url,params:va,listeners:{\'success\':{fn:function(){this.activate();Ext.getCmp(\'modx-package-grid\').refresh();},scope:this},\'failure\':{fn:function(){this.activate();},scope:this}}});}\";s:5:\"scope\";s:15:\"[object Object]\";s:8:\"minWidth\";s:2:\"75\";s:10:\"removeMode\";s:9:\"container\";s:10:\"hideParent\";s:4:\"true\";s:6:\"events\";s:15:\"[object Object]\";s:7:\"ownerCt\";s:15:\"[object Object]\";s:9:\"container\";s:15:\"[object Object]\";s:8:\"rendered\";s:4:\"true\";s:8:\"template\";s:15:\"[object Object]\";s:5:\"btnEl\";s:15:\"[object Object]\";s:4:\"mons\";s:15:\"[object Object]\";s:2:\"el\";s:15:\"[object Object]\";s:4:\"icon\";s:0:\"\";s:7:\"iconCls\";s:0:\"\";s:8:\"boxReady\";s:4:\"true\";s:8:\"lastSize\";s:15:\"[object Object]\";s:11:\"useSetClass\";s:4:\"true\";s:6:\"oldCls\";s:12:\"x-btn-noicon\";s:3:\"doc\";s:15:\"[object Object]\";s:19:\"monitoringMouseOver\";s:4:\"true\";s:6:\"action\";s:26:\"workspace/packages/install\";s:8:\"register\";s:3:\"mgr\";s:5:\"topic\";s:47:\"/workspace/package/install/quickemail-1.1.1-pl/\";s:14:\"package_action\";i:0;}','QuickEmail','a:38:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"548ca3eddc532f2c5904b159\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"package\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d556d13b2b083396d000fb8\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:12:\"display_name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:19:\"quickemail-1.1.1-pl\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:4:\"name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:10:\"QuickEmail\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"1.1.1\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:13:\"version_major\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"1\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:13:\"version_minor\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"1\";s:8:\"children\";a:0:{}}i:7;a:4:{s:4:\"name\";s:13:\"version_patch\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"1\";s:8:\"children\";a:0:{}}i:8;a:4:{s:4:\"name\";s:7:\"release\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:9;a:4:{s:4:\"name\";s:8:\"vrelease\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:10;a:3:{s:4:\"name\";s:14:\"vrelease_index\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:11;a:4:{s:4:\"name\";s:6:\"author\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6:\"bobray\";s:8:\"children\";a:0:{}}i:12;a:4:{s:4:\"name\";s:11:\"description\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:121:\"<p>QuickEmail can be used to send an email form within a snippet, but its main purpose is to diagnose email problems.</p>\";s:8:\"children\";a:0:{}}i:13;a:4:{s:4:\"name\";s:12:\"instructions\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:381:\"<p>After installing QUickEmail, create a resource called QuickEmailCheck and add the following snippet tag to the content:</p>\n<p></p>\n<p>When you preview the page, you should receive an email. If not, change the tag to this and preview the page again:</p>\n<p></p>\n<p>&nbsp;</p>\nSee the tutorial at <a href=\"http://bobsguides.com/quickemail-snippet-tutorial.html\">Bob\'s Guides</a>.\";s:8:\"children\";a:0:{}}i:14;a:4:{s:4:\"name\";s:9:\"changelog\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:344:\"<p>QuickEmail 1.1.0-pl</p>===============<ul><li>Fix bug with replyTo property</li></ul><p>QuickEmail 1.1.0-pl</p><ul></ul>===============<ul><li>Bail if user not logged in to manager (stops bots from triggering emails)</li><li>Fix bug with message Tpl name</li><li>Stop E_NOTICE warnings in build</li><li>Fix bug with fromName</li></ul><p></p>\";s:8:\"children\";a:0:{}}i:15;a:4:{s:4:\"name\";s:9:\"createdon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2014-12-13T20:39:09+0000\";s:8:\"children\";a:0:{}}i:16;a:4:{s:4:\"name\";s:9:\"createdby\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:6:\"bobray\";s:8:\"children\";a:0:{}}i:17;a:4:{s:4:\"name\";s:8:\"editedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2015-04-14T17:55:05+0000\";s:8:\"children\";a:0:{}}i:18;a:4:{s:4:\"name\";s:10:\"releasedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2014-12-13T20:39:09+0000\";s:8:\"children\";a:0:{}}i:19;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"4188\";s:8:\"children\";a:0:{}}i:20;a:4:{s:4:\"name\";s:8:\"approved\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:21;a:4:{s:4:\"name\";s:7:\"audited\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:22;a:4:{s:4:\"name\";s:8:\"featured\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:23;a:4:{s:4:\"name\";s:10:\"deprecated\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:24;a:4:{s:4:\"name\";s:7:\"license\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"GPLv2\";s:8:\"children\";a:0:{}}i:25;a:3:{s:4:\"name\";s:7:\"smf_url\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:26;a:4:{s:4:\"name\";s:10:\"repository\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d4c3fa6b2b0830da9000001\";s:8:\"children\";a:0:{}}i:27;a:4:{s:4:\"name\";s:8:\"supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:28;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=548ca3eedc532f2c5904b15b\";s:8:\"children\";a:0:{}}i:29;a:4:{s:4:\"name\";s:9:\"signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:19:\"quickemail-1.1.1-pl\";s:8:\"children\";a:0:{}}i:30;a:4:{s:4:\"name\";s:11:\"supports_db\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"mysql,sqlsrv\";s:8:\"children\";a:0:{}}i:31;a:4:{s:4:\"name\";s:16:\"minimum_supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:32;a:4:{s:4:\"name\";s:9:\"breaks_at\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:8:\"10000000\";s:8:\"children\";a:0:{}}i:33;a:4:{s:4:\"name\";s:10:\"screenshot\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:94:\"http://modx.s3.amazonaws.com/extras/4d556d13b2b083396d000fb8/quickemail-1.1.0-pl.transport.zip\";s:8:\"children\";a:0:{}}i:34;a:3:{s:4:\"name\";s:4:\"file\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:7:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"548ca3eedc532f2c5904b15b\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"548ca3eddc532f2c5904b159\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:8:\"filename\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:33:\"quickemail-1.1.1-pl.transport.zip\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"549\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:6:\"lastip\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:15:\"104.238.101.217\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:9:\"transport\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=548ca3eedc532f2c5904b15b\";s:8:\"children\";a:0:{}}}}i:35;a:4:{s:4:\"name\";s:17:\"package-signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:19:\"quickemail-1.1.1-pl\";s:8:\"children\";a:0:{}}i:36;a:4:{s:4:\"name\";s:10:\"categories\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:29:\"communication,email,utilities\";s:8:\"children\";a:0:{}}i:37;a:3:{s:4:\"name\";s:4:\"tags\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}}',1,1,1,'pl',0),
	('analyticsdashboard-1.0.2-pl','2015-04-15 07:06:08','2015-04-15 07:08:00','2015-04-15 07:08:00',0,1,1,0,'analyticsdashboard-1.0.2-pl.transport.zip',NULL,'a:33:{s:7:\"license\";s:15214:\"GNU GENERAL PUBLIC LICENSE\n   Version 2, June 1991\n--------------------------\n\nCopyright (C) 1989, 1991 Free Software Foundation, Inc.\n59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\nEveryone is permitted to copy and distribute verbatim copies\nof this license document, but changing it is not allowed.\n\nPreamble\n--------\n\n  The licenses for most software are designed to take away your\nfreedom to share and change it.  By contrast, the GNU General Public\nLicense is intended to guarantee your freedom to share and change free\nsoftware--to make sure the software is free for all its users.  This\nGeneral Public License applies to most of the Free Software\nFoundation\'s software and to any other program whose authors commit to\nusing it.  (Some other Free Software Foundation software is covered by\nthe GNU Library General Public License instead.)  You can apply it to\nyour programs, too.\n\n  When we speak of free software, we are referring to freedom, not\nprice.  Our General Public Licenses are designed to make sure that you\nhave the freedom to distribute copies of free software (and charge for\nthis service if you wish), that you receive source code or can get it\nif you want it, that you can change the software or use pieces of it\nin new free programs; and that you know you can do these things.\n\n  To protect your rights, we need to make restrictions that forbid\nanyone to deny you these rights or to ask you to surrender the rights.\nThese restrictions translate to certain responsibilities for you if you\ndistribute copies of the software, or if you modify it.\n\n  For example, if you distribute copies of such a program, whether\ngratis or for a fee, you must give the recipients all the rights that\nyou have.  You must make sure that they, too, receive or can get the\nsource code.  And you must show them these terms so they know their\nrights.\n\n  We protect your rights with two steps: (1) copyright the software, and\n(2) offer you this license which gives you legal permission to copy,\ndistribute and/or modify the software.\n\n  Also, for each author\'s protection and ours, we want to make certain\nthat everyone understands that there is no warranty for this free\nsoftware.  If the software is modified by someone else and passed on, we\nwant its recipients to know that what they have is not the original, so\nthat any problems introduced by others will not reflect on the original\nauthors\' reputations.\n\n  Finally, any free program is threatened constantly by software\npatents.  We wish to avoid the danger that redistributors of a free\nprogram will individually obtain patent licenses, in effect making the\nprogram proprietary.  To prevent this, we have made it clear that any\npatent must be licensed for everyone\'s free use or not licensed at all.\n\n  The precise terms and conditions for copying, distribution and\nmodification follow.\n\n\nGNU GENERAL PUBLIC LICENSE\nTERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n---------------------------------------------------------------\n\n  0. This License applies to any program or other work which contains\na notice placed by the copyright holder saying it may be distributed\nunder the terms of this General Public License.  The \"Program\", below,\nrefers to any such program or work, and a \"work based on the Program\"\nmeans either the Program or any derivative work under copyright law:\nthat is to say, a work containing the Program or a portion of it,\neither verbatim or with modifications and/or translated into another\nlanguage.  (Hereinafter, translation is included without limitation in\nthe term \"modification\".)  Each licensee is addressed as \"you\".\n\nActivities other than copying, distribution and modification are not\ncovered by this License; they are outside its scope.  The act of\nrunning the Program is not restricted, and the output from the Program\nis covered only if its contents constitute a work based on the\nProgram (independent of having been made by running the Program).\nWhether that is true depends on what the Program does.\n\n  1. You may copy and distribute verbatim copies of the Program\'s\nsource code as you receive it, in any medium, provided that you\nconspicuously and appropriately publish on each copy an appropriate\ncopyright notice and disclaimer of warranty; keep intact all the\nnotices that refer to this License and to the absence of any warranty;\nand give any other recipients of the Program a copy of this License\nalong with the Program.\n\nYou may charge a fee for the physical act of transferring a copy, and\nyou may at your option offer warranty protection in exchange for a fee.\n\n  2. You may modify your copy or copies of the Program or any portion\nof it, thus forming a work based on the Program, and copy and\ndistribute such modifications or work under the terms of Section 1\nabove, provided that you also meet all of these conditions:\n\n    a) You must cause the modified files to carry prominent notices\n    stating that you changed the files and the date of any change.\n\n    b) You must cause any work that you distribute or publish, that in\n    whole or in part contains or is derived from the Program or any\n    part thereof, to be licensed as a whole at no charge to all third\n    parties under the terms of this License.\n\n    c) If the modified program normally reads commands interactively\n    when run, you must cause it, when started running for such\n    interactive use in the most ordinary way, to print or display an\n    announcement including an appropriate copyright notice and a\n    notice that there is no warranty (or else, saying that you provide\n    a warranty) and that users may redistribute the program under\n    these conditions, and telling the user how to view a copy of this\n    License.  (Exception: if the Program itself is interactive but\n    does not normally print such an announcement, your work based on\n    the Program is not required to print an announcement.)\n\nThese requirements apply to the modified work as a whole.  If\nidentifiable sections of that work are not derived from the Program,\nand can be reasonably considered independent and separate works in\nthemselves, then this License, and its terms, do not apply to those\nsections when you distribute them as separate works.  But when you\ndistribute the same sections as part of a whole which is a work based\non the Program, the distribution of the whole must be on the terms of\nthis License, whose permissions for other licensees extend to the\nentire whole, and thus to each and every part regardless of who wrote it.\n\nThus, it is not the intent of this section to claim rights or contest\nyour rights to work written entirely by you; rather, the intent is to\nexercise the right to control the distribution of derivative or\ncollective works based on the Program.\n\nIn addition, mere aggregation of another work not based on the Program\nwith the Program (or with a work based on the Program) on a volume of\na storage or distribution medium does not bring the other work under\nthe scope of this License.\n\n  3. You may copy and distribute the Program (or a work based on it,\nunder Section 2) in object code or executable form under the terms of\nSections 1 and 2 above provided that you also do one of the following:\n\n    a) Accompany it with the complete corresponding machine-readable\n    source code, which must be distributed under the terms of Sections\n    1 and 2 above on a medium customarily used for software interchange; or,\n\n    b) Accompany it with a written offer, valid for at least three\n    years, to give any third party, for a charge no more than your\n    cost of physically performing source distribution, a complete\n    machine-readable copy of the corresponding source code, to be\n    distributed under the terms of Sections 1 and 2 above on a medium\n    customarily used for software interchange; or,\n\n    c) Accompany it with the information you received as to the offer\n    to distribute corresponding source code.  (This alternative is\n    allowed only for noncommercial distribution and only if you\n    received the program in object code or executable form with such\n    an offer, in accord with Subsection b above.)\n\nThe source code for a work means the preferred form of the work for\nmaking modifications to it.  For an executable work, complete source\ncode means all the source code for all modules it contains, plus any\nassociated interface definition files, plus the scripts used to\ncontrol compilation and installation of the executable.  However, as a\nspecial exception, the source code distributed need not include\nanything that is normally distributed (in either source or binary\nform) with the major components (compiler, kernel, and so on) of the\noperating system on which the executable runs, unless that component\nitself accompanies the executable.\n\nIf distribution of executable or object code is made by offering\naccess to copy from a designated place, then offering equivalent\naccess to copy the source code from the same place counts as\ndistribution of the source code, even though third parties are not\ncompelled to copy the source along with the object code.\n\n  4. You may not copy, modify, sublicense, or distribute the Program\nexcept as expressly provided under this License.  Any attempt\notherwise to copy, modify, sublicense or distribute the Program is\nvoid, and will automatically terminate your rights under this License.\nHowever, parties who have received copies, or rights, from you under\nthis License will not have their licenses terminated so long as such\nparties remain in full compliance.\n\n  5. You are not required to accept this License, since you have not\nsigned it.  However, nothing else grants you permission to modify or\ndistribute the Program or its derivative works.  These actions are\nprohibited by law if you do not accept this License.  Therefore, by\nmodifying or distributing the Program (or any work based on the\nProgram), you indicate your acceptance of this License to do so, and\nall its terms and conditions for copying, distributing or modifying\nthe Program or works based on it.\n\n  6. Each time you redistribute the Program (or any work based on the\nProgram), the recipient automatically receives a license from the\noriginal licensor to copy, distribute or modify the Program subject to\nthese terms and conditions.  You may not impose any further\nrestrictions on the recipients\' exercise of the rights granted herein.\nYou are not responsible for enforcing compliance by third parties to\nthis License.\n\n  7. If, as a consequence of a court judgment or allegation of patent\ninfringement or for any other reason (not limited to patent issues),\nconditions are imposed on you (whether by court order, agreement or\notherwise) that contradict the conditions of this License, they do not\nexcuse you from the conditions of this License.  If you cannot\ndistribute so as to satisfy simultaneously your obligations under this\nLicense and any other pertinent obligations, then as a consequence you\nmay not distribute the Program at all.  For example, if a patent\nlicense would not permit royalty-free redistribution of the Program by\nall those who receive copies directly or indirectly through you, then\nthe only way you could satisfy both it and this License would be to\nrefrain entirely from distribution of the Program.\n\nIf any portion of this section is held invalid or unenforceable under\nany particular circumstance, the balance of the section is intended to\napply and the section as a whole is intended to apply in other\ncircumstances.\n\nIt is not the purpose of this section to induce you to infringe any\npatents or other property right claims or to contest validity of any\nsuch claims; this section has the sole purpose of protecting the\nintegrity of the free software distribution system, which is\nimplemented by public license practices.  Many people have made\ngenerous contributions to the wide range of software distributed\nthrough that system in reliance on consistent application of that\nsystem; it is up to the author/donor to decide if he or she is willing\nto distribute software through any other system and a licensee cannot\nimpose that choice.\n\nThis section is intended to make thoroughly clear what is believed to\nbe a consequence of the rest of this License.\n\n  8. If the distribution and/or use of the Program is restricted in\ncertain countries either by patents or by copyrighted interfaces, the\noriginal copyright holder who places the Program under this License\nmay add an explicit geographical distribution limitation excluding\nthose countries, so that distribution is permitted only in or among\ncountries not thus excluded.  In such case, this License incorporates\nthe limitation as if written in the body of this License.\n\n  9. The Free Software Foundation may publish revised and/or new versions\nof the General Public License from time to time.  Such new versions will\nbe similar in spirit to the present version, but may differ in detail to\naddress new problems or concerns.\n\nEach version is given a distinguishing version number.  If the Program\nspecifies a version number of this License which applies to it and \"any\nlater version\", you have the option of following the terms and conditions\neither of that version or of any later version published by the Free\nSoftware Foundation.  If the Program does not specify a version number of\nthis License, you may choose any version ever published by the Free Software\nFoundation.\n\n  10. If you wish to incorporate parts of the Program into other free\nprograms whose distribution conditions are different, write to the author\nto ask for permission.  For software which is copyrighted by the Free\nSoftware Foundation, write to the Free Software Foundation; we sometimes\nmake exceptions for this.  Our decision will be guided by the two goals\nof preserving the free status of all derivatives of our free software and\nof promoting the sharing and reuse of software generally.\n\nNO WARRANTY\n-----------\n\n  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY\nFOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN\nOTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES\nPROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED\nOR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\nMERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS\nTO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE\nPROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\nREPAIR OR CORRECTION.\n\n  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\nWILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR\nREDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,\nINCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING\nOUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED\nTO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY\nYOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER\nPROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE\nPOSSIBILITY OF SUCH DAMAGES.\n\n---------------------------\nEND OF TERMS AND CONDITIONS\";s:6:\"readme\";s:586:\"--------------------\nExtra: Analytics Dashboard widget\n--------------------\nVersion: 1.0.2-pl\nSince: December 9th, 2011\nAuthor: Wieger Sloot, Sterc <wieger@sterc.nl>\n\nA custom MODx dashboard widget that will show data from Google Analytics.\n\nYou need a Google Analytics account to use this plugin.\n\n\n--------------------\nFeatures\n--------------------\n\n- Check your site’s statistics from within MODx\n- Information about your site’s visitors, traffic sources, top content, goals and keywords\n- Visitors and goals are displayed in graphs\n- Traffic Sources are displayed in a pie chart\";s:9:\"changelog\";s:579:\"Analytics Dashboard widget 1.0.2\n====================================\n- Added quick fix for the dashboard widget layout\n\nAnalytics Dashboard widget 1.0.1\n====================================\n- All data-charts are working again\n- Changed MODX.on(\'ready\' to Ext.ready(\n- Authentication changed to make domain-verification fool-proof\n- Updated the Google\'s API for listing all the accounts\n- Added loading-screen after Analytics profile is selected\n\nAnalytics Dashboard widget 1.0.0\n====================================\n- Vertical tabs\n- Added Internel search\n- Ability to hide tabs\";s:9:\"signature\";s:27:\"analyticsdashboard-1.0.2-pl\";s:13:\"initialConfig\";s:15:\"[object Object]\";s:4:\"text\";s:8:\"Continue\";s:2:\"id\";s:19:\"package-install-btn\";s:6:\"hidden\";s:5:\"false\";s:7:\"handler\";s:579:\"function (va){var g=Ext.getCmp(\'modx-package-grid\');if(!g)return false;var r=g.menu.record.data?g.menu.record.data:g.menu.record;var topic=\'/workspace/package/install/\'+r.signature+\'/\';this.loadConsole(Ext.getBody(),topic);va=va||{};Ext.apply(va,{action:\'workspace/packages/install\',signature:r.signature,register:\'mgr\',topic:topic});var c=this.console;MODx.Ajax.request({url:MODx.config.connector_url,params:va,listeners:{\'success\':{fn:function(){this.activate();Ext.getCmp(\'modx-package-grid\').refresh();},scope:this},\'failure\':{fn:function(){this.activate();},scope:this}}});}\";s:5:\"scope\";s:15:\"[object Object]\";s:8:\"minWidth\";s:2:\"75\";s:10:\"removeMode\";s:9:\"container\";s:10:\"hideParent\";s:4:\"true\";s:6:\"events\";s:15:\"[object Object]\";s:7:\"ownerCt\";s:15:\"[object Object]\";s:9:\"container\";s:15:\"[object Object]\";s:8:\"rendered\";s:4:\"true\";s:8:\"template\";s:15:\"[object Object]\";s:5:\"btnEl\";s:15:\"[object Object]\";s:4:\"mons\";s:15:\"[object Object]\";s:2:\"el\";s:15:\"[object Object]\";s:4:\"icon\";s:0:\"\";s:7:\"iconCls\";s:0:\"\";s:8:\"boxReady\";s:4:\"true\";s:8:\"lastSize\";s:15:\"[object Object]\";s:11:\"useSetClass\";s:4:\"true\";s:6:\"oldCls\";s:12:\"x-btn-noicon\";s:3:\"doc\";s:15:\"[object Object]\";s:19:\"monitoringMouseOver\";s:4:\"true\";s:6:\"action\";s:26:\"workspace/packages/install\";s:8:\"register\";s:3:\"mgr\";s:5:\"topic\";s:55:\"/workspace/package/install/analyticsdashboard-1.0.2-pl/\";s:14:\"package_action\";i:0;}','Google Analytics Dashboard Widget','a:38:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"54ef0843dc532f725a035817\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"package\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4ee1c0d0f2455410aa000057\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:12:\"display_name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:27:\"analyticsdashboard-1.0.2-pl\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:4:\"name\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:33:\"Google Analytics Dashboard Widget\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"1.0.2\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:13:\"version_major\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"1\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:13:\"version_minor\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"0\";s:8:\"children\";a:0:{}}i:7;a:4:{s:4:\"name\";s:13:\"version_patch\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:1:\"2\";s:8:\"children\";a:0:{}}i:8;a:4:{s:4:\"name\";s:7:\"release\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:9;a:4:{s:4:\"name\";s:8:\"vrelease\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:2:\"pl\";s:8:\"children\";a:0:{}}i:10;a:3:{s:4:\"name\";s:14:\"vrelease_index\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:11;a:4:{s:4:\"name\";s:6:\"author\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"sterc\";s:8:\"children\";a:0:{}}i:12;a:4:{s:4:\"name\";s:11:\"description\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:701:\"<p></p><p>--------------------</p><p>Extra: Analytics Dashboard widget</p><p>--------------------</p><p>Version: 1.0.1</p><p>Since: December 9th, 2011</p><p>Author: Wieger Sloot, Sterc </p><p></p><p>A custom MODx dashboard widget that will show data from Google Analytics.</p><p></p><p>You need a Google Analytics account to use this plugin.</p><p></p><p></p><p>--------------------</p><p>Features</p><p>--------------------</p><p></p><p>- Check your site’s statistics from within MODx</p><p>- Information about your site’s visitors, traffic sources, top content, goals and keywords</p><p>- Visitors and goals are displayed in graphs</p><p>- Traffic Sources are displayed in a pie chart</p><p></p>\";s:8:\"children\";a:0:{}}i:13;a:4:{s:4:\"name\";s:12:\"instructions\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:72:\"<p>Install via Package Management.</p><p>Add widget to the dashboard</p>\";s:8:\"children\";a:0:{}}i:14;a:4:{s:4:\"name\";s:9:\"changelog\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:696:\"<p></p><p>Analytics Dashboard widget 1.0.2</p><p>====================================</p><p>- Added quick fix for the dashboard widget layout</p><p></p><p>Analytics Dashboard widget 1.0.1</p><p>====================================</p><p>- All data-charts are working again</p><p>- Changed MODX.on(\'ready\' to Ext.ready(</p><p>- Authentication changed to make domain-verification fool-proof</p><p>- Updated the Google\'s API for listing all the accounts</p><p>- Added loading-screen after Analytics profile is selected</p><p></p><p>Analytics Dashboard widget 1.0.0</p><p>====================================</p><p>- Vertical tabs</p><p>- Added Internel search</p><p>- Ability to hide tabs</p><p></p>\";s:8:\"children\";a:0:{}}i:15;a:4:{s:4:\"name\";s:9:\"createdon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2015-02-26T11:49:23+0000\";s:8:\"children\";a:0:{}}i:16;a:4:{s:4:\"name\";s:9:\"createdby\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"sterc\";s:8:\"children\";a:0:{}}i:17;a:4:{s:4:\"name\";s:8:\"editedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2015-04-15T10:50:07+0000\";s:8:\"children\";a:0:{}}i:18;a:4:{s:4:\"name\";s:10:\"releasedon\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"2015-02-26T11:49:23+0000\";s:8:\"children\";a:0:{}}i:19;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"7306\";s:8:\"children\";a:0:{}}i:20;a:4:{s:4:\"name\";s:8:\"approved\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:21;a:4:{s:4:\"name\";s:7:\"audited\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:22;a:4:{s:4:\"name\";s:8:\"featured\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:23;a:4:{s:4:\"name\";s:10:\"deprecated\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"false\";s:8:\"children\";a:0:{}}i:24;a:4:{s:4:\"name\";s:7:\"license\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:5:\"GPLv2\";s:8:\"children\";a:0:{}}i:25;a:3:{s:4:\"name\";s:7:\"smf_url\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}i:26;a:4:{s:4:\"name\";s:10:\"repository\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"4d4c3fa6b2b0830da9000001\";s:8:\"children\";a:0:{}}i:27;a:4:{s:4:\"name\";s:8:\"supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"2.2\";s:8:\"children\";a:0:{}}i:28;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=54ef0845dc532f725a035819\";s:8:\"children\";a:0:{}}i:29;a:4:{s:4:\"name\";s:9:\"signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:27:\"analyticsdashboard-1.0.2-pl\";s:8:\"children\";a:0:{}}i:30;a:4:{s:4:\"name\";s:11:\"supports_db\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:12:\"mysql,sqlsrv\";s:8:\"children\";a:0:{}}i:31;a:4:{s:4:\"name\";s:16:\"minimum_supports\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"2.2\";s:8:\"children\";a:0:{}}i:32;a:4:{s:4:\"name\";s:9:\"breaks_at\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"2.3\";s:8:\"children\";a:0:{}}i:33;a:4:{s:4:\"name\";s:10:\"screenshot\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:76:\"http://modx.s3.amazonaws.com/extras/4ee1c0d0f2455410aa000057/Analytics 1.png\";s:8:\"children\";a:0:{}}i:34;a:3:{s:4:\"name\";s:4:\"file\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:7:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"54ef0845dc532f725a035819\";s:8:\"children\";a:0:{}}i:1;a:4:{s:4:\"name\";s:7:\"version\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:24:\"54ef0843dc532f725a035817\";s:8:\"children\";a:0:{}}i:2;a:4:{s:4:\"name\";s:8:\"filename\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:41:\"analyticsdashboard-1.0.2-pl.transport.zip\";s:8:\"children\";a:0:{}}i:3;a:4:{s:4:\"name\";s:9:\"downloads\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:3:\"434\";s:8:\"children\";a:0:{}}i:4;a:4:{s:4:\"name\";s:6:\"lastip\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:13:\"82.146.36.143\";s:8:\"children\";a:0:{}}i:5;a:4:{s:4:\"name\";s:9:\"transport\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:4:\"true\";s:8:\"children\";a:0:{}}i:6;a:4:{s:4:\"name\";s:8:\"location\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:60:\"http://modx.com/extras/download/?id=54ef0845dc532f725a035819\";s:8:\"children\";a:0:{}}}}i:35;a:4:{s:4:\"name\";s:17:\"package-signature\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:27:\"analyticsdashboard-1.0.2-pl\";s:8:\"children\";a:0:{}}i:36;a:4:{s:4:\"name\";s:10:\"categories\";s:10:\"attributes\";a:0:{}s:4:\"text\";s:89:\"administration,administration,commenting-feedback,communication,core-extensions,utilities\";s:8:\"children\";a:0:{}}i:37;a:3:{s:4:\"name\";s:4:\"tags\";s:10:\"attributes\";a:0:{}s:8:\"children\";a:0:{}}}',1,0,2,'pl',0);

/*!40000 ALTER TABLE `modx_transport_packages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_transport_providers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_transport_providers`;

CREATE TABLE `modx_transport_providers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` mediumtext,
  `service_url` tinytext,
  `username` varchar(255) NOT NULL DEFAULT '',
  `api_key` varchar(255) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `api_key` (`api_key`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_transport_providers` WRITE;
/*!40000 ALTER TABLE `modx_transport_providers` DISABLE KEYS */;

INSERT INTO `modx_transport_providers` (`id`, `name`, `description`, `service_url`, `username`, `api_key`, `created`, `updated`)
VALUES
	(1,'modx.com','The official MODX transport facility for 3rd party components.','http://rest.modx.com/extras/','','','2014-07-22 15:36:51',NULL);

/*!40000 ALTER TABLE `modx_transport_providers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_user_attributes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_user_attributes`;

CREATE TABLE `modx_user_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `internalKey` int(10) NOT NULL,
  `fullname` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `phone` varchar(100) NOT NULL DEFAULT '',
  `mobilephone` varchar(100) NOT NULL DEFAULT '',
  `blocked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `blockeduntil` int(11) NOT NULL DEFAULT '0',
  `blockedafter` int(11) NOT NULL DEFAULT '0',
  `logincount` int(11) NOT NULL DEFAULT '0',
  `lastlogin` int(11) NOT NULL DEFAULT '0',
  `thislogin` int(11) NOT NULL DEFAULT '0',
  `failedlogincount` int(10) NOT NULL DEFAULT '0',
  `sessionid` varchar(100) NOT NULL DEFAULT '',
  `dob` int(10) NOT NULL DEFAULT '0',
  `gender` int(1) NOT NULL DEFAULT '0',
  `address` text NOT NULL,
  `country` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(25) NOT NULL DEFAULT '',
  `zip` varchar(25) NOT NULL DEFAULT '',
  `fax` varchar(100) NOT NULL DEFAULT '',
  `photo` varchar(255) NOT NULL DEFAULT '',
  `comment` text NOT NULL,
  `website` varchar(255) NOT NULL DEFAULT '',
  `extended` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `internalKey` (`internalKey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_user_attributes` WRITE;
/*!40000 ALTER TABLE `modx_user_attributes` DISABLE KEYS */;

INSERT INTO `modx_user_attributes` (`id`, `internalKey`, `fullname`, `email`, `phone`, `mobilephone`, `blocked`, `blockeduntil`, `blockedafter`, `logincount`, `lastlogin`, `thislogin`, `failedlogincount`, `sessionid`, `dob`, `gender`, `address`, `country`, `city`, `state`, `zip`, `fax`, `photo`, `comment`, `website`, `extended`)
VALUES
	(1,1,'Default Admin User','justin.lobaito@weloideas.com','','',0,0,0,24,1431991921,1439482180,0,'fhhqor1jjoi5hnrr64uhovdsk0',0,0,'','','','','','','','','',NULL),
	(2,2,'Adam Feller','adamfeller11@gmail.com','','',0,0,0,9,1427768181,1428353005,0,'012uh1i1u2qk39knh2dus7kuv0',0,0,'','','','','','','','','','[]'),
	(3,3,'','info@heartlandheating.com','','',0,0,0,2,1429891311,1432151350,0,'enso0slvmg1ijoeiejjkr7gqc6',0,0,'','','','','','','','','','[]');

/*!40000 ALTER TABLE `modx_user_attributes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_user_group_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_user_group_roles`;

CREATE TABLE `modx_user_group_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` mediumtext,
  `authority` int(10) unsigned NOT NULL DEFAULT '9999',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `authority` (`authority`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_user_group_roles` WRITE;
/*!40000 ALTER TABLE `modx_user_group_roles` DISABLE KEYS */;

INSERT INTO `modx_user_group_roles` (`id`, `name`, `description`, `authority`)
VALUES
	(1,'Member',NULL,9999),
	(2,'Super User',NULL,0);

/*!40000 ALTER TABLE `modx_user_group_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_user_group_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_user_group_settings`;

CREATE TABLE `modx_user_group_settings` (
  `group` int(10) unsigned NOT NULL DEFAULT '0',
  `key` varchar(50) NOT NULL,
  `value` text,
  `xtype` varchar(75) NOT NULL DEFAULT 'textfield',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `area` varchar(255) NOT NULL DEFAULT '',
  `editedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`group`,`key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_user_messages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_user_messages`;

CREATE TABLE `modx_user_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(15) NOT NULL DEFAULT '',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `sender` int(10) NOT NULL DEFAULT '0',
  `recipient` int(10) NOT NULL DEFAULT '0',
  `private` tinyint(4) NOT NULL DEFAULT '0',
  `date_sent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `read` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_user_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_user_settings`;

CREATE TABLE `modx_user_settings` (
  `user` int(11) NOT NULL DEFAULT '0',
  `key` varchar(50) NOT NULL DEFAULT '',
  `value` text,
  `xtype` varchar(75) NOT NULL DEFAULT 'textfield',
  `namespace` varchar(40) NOT NULL DEFAULT 'core',
  `area` varchar(255) NOT NULL DEFAULT '',
  `editedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user`,`key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table modx_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_users`;

CREATE TABLE `modx_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `cachepwd` varchar(100) NOT NULL DEFAULT '',
  `class_key` varchar(100) NOT NULL DEFAULT 'modUser',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `remote_key` varchar(255) DEFAULT NULL,
  `remote_data` text,
  `hash_class` varchar(100) NOT NULL DEFAULT 'hashing.modPBKDF2',
  `salt` varchar(100) NOT NULL DEFAULT '',
  `primary_group` int(10) unsigned NOT NULL DEFAULT '0',
  `session_stale` text,
  `sudo` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `class_key` (`class_key`),
  KEY `remote_key` (`remote_key`),
  KEY `primary_group` (`primary_group`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_users` WRITE;
/*!40000 ALTER TABLE `modx_users` DISABLE KEYS */;

INSERT INTO `modx_users` (`id`, `username`, `password`, `cachepwd`, `class_key`, `active`, `remote_key`, `remote_data`, `hash_class`, `salt`, `primary_group`, `session_stale`, `sudo`)
VALUES
	(1,'admin','j5fA8wP9h0c4oTlJKix01HmsjMwQpIx+3kAXkao44JQ=','','modUser',1,NULL,NULL,'hashing.modPBKDF2','640372726c0c74d0c7a13247343cc247',1,'a:2:{i:0;s:3:\"mgr\";i:1;s:3:\"web\";}',1),
	(2,'adam','Zix5mUyJKCboetRdPJvOi57W6Hsm98TZe1iLMK7CGxs=','','modUser',1,NULL,NULL,'hashing.modPBKDF2','2b3e9ca3ec72985eba1f2e76c5e4888d',1,NULL,0),
	(3,'heartland','iXWQF34lOYJusBeF20FnL1vm2HdR37NxE7PCgedKErc=','','modUser',1,NULL,NULL,'hashing.modPBKDF2','d7e50b3f8b60f1086d3fcff272757a9e',1,NULL,0);

/*!40000 ALTER TABLE `modx_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table modx_workspaces
# ------------------------------------------------------------

DROP TABLE IF EXISTS `modx_workspaces`;

CREATE TABLE `modx_workspaces` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(255) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `attributes` mediumtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `path` (`path`),
  KEY `name` (`name`),
  KEY `active` (`active`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `modx_workspaces` WRITE;
/*!40000 ALTER TABLE `modx_workspaces` DISABLE KEYS */;

INSERT INTO `modx_workspaces` (`id`, `name`, `path`, `created`, `active`, `attributes`)
VALUES
	(1,'Default MODX workspace','{core_path}','2014-10-06 00:33:46',1,NULL);

/*!40000 ALTER TABLE `modx_workspaces` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
